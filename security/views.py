from django.shortcuts import render
# from .models import Group
# from django.contrib.auth.forms import GroupForm
from django.contrib.auth.models import Group 

from authentication.models import CustomUser
from authentication.forms import SystemUserCreationForm, EditSystemUserCreationForm
from django.shortcuts import render, redirect, reverse
from .forms import GroupForm

def roles(request):
    groups = Group.objects.all()

    context = {
        'groups': groups
    }


    return render(request, 'security/roles.html', context)

def add_roles(request):
    form = GroupForm()
    if request.method == 'POST':
        form = GroupForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.creator = request.user
            user.save()
            return redirect(reverse('roles'))

    context = {'form': form}
    return render(request, 'security/add_group.html', context)

def edit_roles(request, pk):
    group = Group.objects.get(id=pk)
    form = GroupForm(instance=group)
    if request.method == 'POST':
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            user = form.save(commit=False)
            user.creator = request.user
            user.save()
            return redirect(reverse('roles'))
    
    context = {'form': form}
    return render(request, 'security/add_group.html', context)



def users(request):
    users = CustomUser.objects.all().select_related('branch', 'group')
    context = {
        'users': users
    }
    return render(request, 'security/users.html', context)



def add_user(request):
    form = SystemUserCreationForm()
    if request.method == 'POST':
        form = SystemUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.creator = request.user
            
            user.set_password(user.password)
            user.save()


            return redirect(reverse('users'))
    else:
       form = SystemUserCreationForm()
    context = {'form': form}
    return render(request, 'security/add_user.html', context)

def edit_users(request, pk):
    user = CustomUser.objects.get(id=pk)
    form = EditSystemUserCreationForm(instance=user)
    if request.method == 'POST':
        form = EditSystemUserCreationForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save(commit=False)
            user.creator = request.user
            # user.groups.add
            user.save()
            return redirect(reverse('users'))
    
    context = {'form': form}
    return render(request, 'security/add_user.html', context)




def audit(request):
    users = CustomUser.objects.all()
    context = {
        'users': users
    }
    return render(request, 'security/audittrail.html')
