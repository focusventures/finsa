from django.apps import AppConfig


class SecurityConfig(AppConfig):
    name = 'security'
    icon_name = 'lock'
