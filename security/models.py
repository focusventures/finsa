from django.db import models

from django.contrib.auth.models import Group as G
from django.utils.translation import ugettext_lazy as _



class Modules(models.Model):
    name = models.CharField(_("Module Name"), max_length=50)

    def __str__(self):
        return "%s" % (self.name)


# class Group(G):
    # modules = models.ManyToManyField("security.Modules", verbose_name=_("Enabled Modules"))
