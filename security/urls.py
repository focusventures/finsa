from django.urls import path

from . import views



urlpatterns = [
     path('roles', views.roles, name="roles"),
     path('roles/add_role', views.add_roles, name="add_roles" ),
     path('roles/<int:pk>', views.edit_roles, name="edit_roles" ),
     path('users', views.users, name="users" ),
     path('users/add_user', views.add_user, name="add_users" ),
     path('users/<int:pk>', views.edit_users, name="edit_users" ),
     path('audit', views.audit, name="audit" ),

]
