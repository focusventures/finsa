from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from django.contrib.auth.models import Group

class GroupForm(ModelForm):
    # selected_models = forms.ManyToMany
    # selected_permissions = forms
    class Meta:
        model = Group
        fields = ['name', 'permissions']
    

    def __init__(self, *args, **kwargs):
        super(GroupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            
            Row(
                Column('permissions', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )

