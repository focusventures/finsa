from django.db import models
from django.db.models import Sum
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.auth.models import  AbstractBaseUser, PermissionsMixin
from stdimage import StdImageField
from django.contrib.auth.models import Group 
# from apps.activity.models import Betslip


from django.utils import timezone
from django.utils.http import urlquote


from django.contrib.auth.models import BaseUserManager


class Role(models.Model):
        '''
        The Role entries are managed by the system,
        automatically created via a Django data migration.
        '''
        TENANT = 1
        CLIENT = 2
        AGENT = 3
        ADMIN = 4
        ROLE_CHOICES = (
            (TENANT, 'Tenant'),
            (AGENT, 'Agent'),
            (CLIENT, 'Client'),
            (ADMIN, 'admin'),
        )
        id = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, primary_key=True)

        def __str__(self):
            return self.get_id_display()
          


class CustomUserManager(BaseUserManager):
    def _create_user(self, phone_number, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not phone_number:
            raise ValueError('The given phone number must be set')
        user = self.model(phone_number=phone_number,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone_number, password=None, **extra_fields):
        return self._create_user(phone_number, password, False, False,
                                 **extra_fields)

    def create_superuser(self, phone_number, password, **extra_fields):
        return self._create_user(phone_number, password, True, True,
                                 **extra_fields)

counties = (
    ("Mombasa", "Mombasa"),
	("Kwale","Kwale"),
	("Kilifi","Kilifi"),	
	("Tana River",	"Tana River"),
	("Lamu", "Lamu"),	
	("Taita–Taveta","Taita–Taveta"),
	("Garissa",	"Garissa"),
	("Wajir","Wajir"),	
	("Mandera", "Mandera"),
	("Marsabit","Marsabit"),	
	("Isiolo","Isiolo"),	
	("Meru","Meru"),	
	("Tharaka-Nithi",	"Tharaka-Nithi"),
	("Embu","Embu"),	
	("Kitui","Kitui"),	
	("Machakos","Machakos"),	
	("Makueni","Makueni"),
	("Nyandarua","Nyandarua"),	
	("Nyeri","Nyeri"),
	("Kirinyaga","Kirinyaga"),	
	("Murang'a", "Murang'a"),
	("Kiambu","Kiambu"),	
	("Turkana", "Turkana"),
	("West Pokot",	"West Pokot"),
	("Samburu", "Samburu"),
	("Trans-Nzoia","Trans-Nzoia"),
	("Uasin Gishu",	 "Uasin Gishu"),	
	("Elgeyo-Marakwet",	"Elgeyo-Marakwet"),
	("Nandi", "Nandi"),	
	("Baringo", "Baringo"),
	("Laikipia", "Laikipia"),
	("Nakuru", "Nakuru"),	
    ("Narok", "Narok"),
	("Kajiado", "Kajiado"),
	("Kericho", "Kericho"),
	("Bomet", "Bomet"),	 
	("Kakamega","Kakamega"),
	("Vihiga", "Vihiga"),	
	("Bungoma", "Bungoma"),
	("Busia", "Busia"),
	("Siaya", "Siaya"),	 
	("Kisumu",	 "Kisumu"),
	("Homa Bay", "Homa Bay"),	
	("Migori",	 "Migori"),
	("Kisii", "Kisii"),
	("Nyamira",	"Nyamira"),
    ("Nairobi", "Nairobi"),


)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    phone_regex = RegexValidator(regex=r'^\d{10}$',
                                 message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.")
    
    Male = 'M'
    Female = 'F'
    genders = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    alias = models.CharField(max_length=12, help_text='username is required', null=True)
    email = models.EmailField(blank=True)
    username = models.CharField(max_length=12, help_text='username is required', null=True)
    county = models.CharField(max_length=50, choices=counties, null=True, help_text='Your County')
    phone_number = models.CharField(validators=[phone_regex], blank=True, max_length=15, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True)
    middle_name = models.CharField(_('Middle name'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True)
    gender = models.CharField(choices=genders, max_length=12, default='F', null=True)
    # branch = models.ForeignKey("ostructure.Branch", verbose_name=_("User Branch"), blank=True, null=True, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, null=True, blank=True, verbose_name=_("User Role"), on_delete=models.CASCADE)
    # groups = models.ForeignKey("security.Group", null=True, blank=True, related_name="User Groups+", verbose_name=_("User Role"), on_delete=models.CASCADE)

    # roles = models.ManyToManyField(Role, _('Roles'))

    avatar = StdImageField(_("Profile Picture"), upload_to='media/',
                          variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True, default='user.jpg')
    
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    # account_type = models.CharField(_('Account Type'), max)
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    is_tenant = models.BooleanField(_('active'), default=False,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    is_linked = models.BooleanField(_('active'), default=False,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    is_has_roles = models.BooleanField(_('active'), default=False,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    has_dash_access = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be able to access Dashboard feature '
                    ''))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.email)

    def __str__(self):
        return "%s" % (self.username)

    @property
    def get_full_names(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.first_name, self.last_name, self.middle_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def get_phone_number(self):
        return self.user.phonenumber

    @property
    def has_access_access(self):
        if self.is_staff:
            return True
        if self.is_linked:
            return True
        return self.is_tenant







