from django.shortcuts import render
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib import messages


from portal_auth.models import CustomUser

from .forms import SignUpForm, ProfileForm,ChangePasswordForm, SystemUserCreationForm

def signup(request):
    if request.user.is_authenticated:
        return redirect('/')

    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(user.password)
            user.save()
        
            return redirect('/auth/login')
    else:
        form = SignUpForm()
    context = {'form': form}
    return render(request, 'portal_auth/signup.html', context)



def login(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    if request.method == "POST":
        phone = request.POST.get('phone_number')
        password = request.POST.get('password')
        

        auth = authenticate(phone_number=phone, password=password)


        if auth is not None:
            auth_login(request, auth)
            messages.success(request, 'Welcome Back')  # <-

            return redirect('/')
        
        messages.success(request, 'Phone Number and Password provided did not match. Try again') 

    return render(request, 'portal_auth/login.html', context)



def reset(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    return render(request, 'portal_auth/reset.html', context)



def confirm(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    return render(request, 'portal_auth/confirm.html', context)




from django.contrib.auth.decorators import login_required

@login_required()
def profile(request):
    context = {}
    print(">>>>>>>>>>>>>>>>>>>>>>>>>")
    print(request.user)
    user = CustomUser.objects.get(phone_number=request.user)
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    form = ProfileForm(instance=user)
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('/auth/profile')
    else:
        form = ProfileForm(instance=user)
        password_form = ChangePasswordForm()
    context = {'form': form, 
    "password_form": password_form,  
    "form": form,
        "documents": "",
        "messages": "",
        "notifications": "",
        "activities": ""
        }


    return render(request, 'portal_auth/profile.html', context)


def change_password(request):
    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        user = CustomUser.objects.get(phone_number__iexact=request.user)

        form = ChangePasswordForm(request.POST, request.FILES, instance=user)
        if form.is_valid():

            user.set_password(form.password)
            user.save()

    return redirect('/auth/profile')


def user_logout(request):
    logout(request)
    messages.success(request, 'Session Closed') 

    return redirect('/auth/login')