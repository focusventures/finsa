from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'portal_auth'
    icon_name = 'user'

    def ready(self):
        import portal_auth.signals

