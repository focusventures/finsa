from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import  CustomUser

from utils.mixins.export import ExportCsvMixin



class CustomUserAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'phone_number', 'username', 'email', 'first_name', 'last_name', 'date_joined', 'group', 'gender', 'is_staff', 'is_active'
    )
    search_fields = (
        'phone_number','username', 'email', 'first_name', 'last_name', 'date_joined'
    )
    icon_name = 'person'
    actions = ["export_as_csv"]


from django.contrib.admin import AdminSite
from django.contrib import admin


class EventAdminSite(AdminSite):
    site_header = "FinGate Portal Management Portal"
    site_title = "FinGate Portal Management Portal"
    index_title = "Welcome to FinGate Portal Management Portal"



event_admin_site = EventAdminSite(name='main_admin')
event_admin_site.register(CustomUser, CustomUserAdmin)
