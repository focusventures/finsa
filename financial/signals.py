# from django.db.models.signals import post_save, post_delete
# from django.dispatch import receiver

# from .models import Transactions
# from dashboard.models import SnapShot
# from dashboard.models import TodaySnapShot

# from audittrial.models import ActivityLog
# from chartsoa.models import AccountEvent, Account

# from .models import TransactionsArchive, GroupTransactions, GroupTransactionsArchive, Transactions, Expense, ExpenseArchive

# @receiver(post_save, sender=GroupTransactions)
# def update_snapshot_on_save(sender, instance, created, **kwargs):
#     if instance.slip != None and instance.modified_by == None:
#         archive = GroupTransactionsArchive()
#         archive.id = instance.id
#         archive.financial_month = instance.financial_month or None
#         archive.financial_year = instance.financial_year or None
#         archive.double_entry = instance.double_entry or None
#         archive.type = instance.type or None
#         archive.action = instance.action or None
#         archive.acc = instance.acc or None
#         archive.payment_method = instance.payment_method or None
#         archive.slip = instance.slip or None
#         archive.session = instance.session or None
#         archive.code = instance.code or None
#         archive.description = instance.description or None
#         archive.expense = instance.expense or None
#         archive.fees = instance.fees or None
#         archive.member = instance.member or None
#         archive.group = instance.group or None
#         archive.receiver = instance.receiver or None
#         archive.receiving_member = instance.receiving_member or None
#         archive.date = instance.date or None
#         archive.processed = instance.processed or None
#         archive.amount = instance.amount or None
#         archive.coa = instance.coa or None
#         archive.actual_amount = instance.actual_amount or 0.00
#         archive.creator = instance.creator or None
#         archive.date_modified = instance.date_modified or None
#         archive.date_created = instance.date_created or None
#         archive.modified_by = instance.modified_by or None
#         archive.save()
#     elif instance.slip != None and instance.modified_by != None:
#         archive = GroupTransactionsArchive.object.get(id=instance.id)
#         archive.financial_month = instance.financial_month or None
#         archive.financial_year = instance.financial_year or None
#         archive.double_entry = instance.double_entry or None
#         archive.type = instance.type or None
#         archive.action = instance.action or None
#         archive.acc = instance.acc or None
#         archive.payment_method = instance.payment_method or None
#         archive.slip = instance.slip or None
#         archive.session = instance.session or None
#         archive.code = instance.code or None
#         archive.description = instance.description or None
#         archive.expense = instance.expense or None
#         archive.fees = instance.fees or None
#         archive.member = instance.member or None
#         archive.group = instance.group or None
#         archive.receiver = instance.receiver or None
#         archive.receiving_member = instance.receiving_member or None
#         archive.date = instance.date or None
#         archive.processed = instance.processed or None
#         archive.amount = instance.amount or None
#         archive.coa = instance.coa or None
#         archive.actual_amount = instance.actual_amount or 0.00
#         archive.creator = instance.creator or None
#         archive.date_modified = instance.date_modified or None
#         archive.date_created = instance.date_created or None
#         archive.modified_by = instance.modified_by or None
#         archive.save()






# @receiver(post_save, sender=Transactions)
# def update_individual_transaction_on_save(sender, instance, created, **kwargs):
#     if instance.slip != None and instance.modified_by == None:
#         archive = TransactionsArchive()
#         archive.id = instance.id
#         archive.financial_month = instance.financial_month or None
#         archive.financial_year = instance.financial_year or None
#         archive.double_entry = instance.double_entry or None
#         archive.loc_account = instance.loc_account or None
#         archive.type = instance.type or None
#         archive.action = instance.action or None
#         archive.acc = instance.acc or None
#         archive.payment_method = instance.payment_method or None
#         archive.slip = instance.slip or None
#         archive.session = instance.session or None
#         archive.code = instance.code or None
#         archive.description = instance.description or None
#         archive.expense = instance.expense or None
#         archive.cheque = instance.cheque or None
#         archive.fees = instance.fees or None
#         archive.member = instance.member or None
#         # archive.group = instance.group or None
#         archive.receiver = instance.receiver or None
#         archive.receiving_member = instance.receiving_member or None
#         archive.date = instance.date or None
#         archive.processed = instance.processed or None
#         archive.amount = instance.amount or None
#         archive.coa = instance.coa or None
#         archive.cash_acc = instance.cash_acc or None
#         archive.bank_acc = instance.bank_acc or None
#         archive.actual_amount = instance.actual_amount or 0.00
#         archive.creator = instance.creator or None
#         archive.date_modified = instance.date_modified or None
#         archive.date_created = instance.date_created or None
#         archive.modified_by = instance.modified_by or None
#         archive.save()

#     elif instance.slip != None and instance.modified_by != None:
#         archive = TransactionsArchive.object.get(id=instance.id)
#         archive.financial_month = instance.financial_month or None
#         archive.financial_year = instance.financial_year or None
#         archive.double_entry = instance.double_entry or None
#         archive.loc_account = instance.loc_account or None
#         archive.type = instance.type or None
#         archive.action = instance.action or None
#         archive.acc = instance.acc or None
#         archive.payment_method = instance.payment_method or None
#         archive.slip = instance.slip or None
#         archive.session = instance.session or None
#         archive.code = instance.code or None
#         archive.description = instance.description or None
#         archive.expense = instance.expense or None
#         archive.cheque = instance.cheque or None
#         archive.fees = instance.fees or None
#         archive.member = instance.member or None
#         # archive.group = instance.group or None
#         archive.receiver = instance.receiver or None
#         archive.receiving_member = instance.receiving_member or None
#         archive.date = instance.date or None
#         archive.processed = instance.processed or None
#         archive.amount = instance.amount or None
#         archive.coa = instance.coa or None
#         archive.cash_acc = instance.cash_acc or None
#         archive.bank_acc = instance.bank_acc or None
#         archive.actual_amount = instance.actual_amount or 0.00
#         archive.creator = instance.creator or None
#         archive.date_modified = instance.date_modified or None
#         archive.date_created = instance.date_created or None
#         archive.modified_by = instance.modified_by or None
#         archive.save()




# @receiver(post_save, sender=Expense)
# def update_expense_on_save(sender, instance, created, **kwargs):
#     if instance.slip != None and instance.modified_by == None:
#         archive = ExpenseArchive()
#         archive.id = instance.id
#         archive.financial_month = instance.financial_month or None
#         archive.financial_year = instance.financial_year or None
#         archive.cheque_number = instance.cheque_number or None
#         archive.paid_to = instance.paid_to or None
#         archive.date = instance.date or None
#         archive.transactions = instance.transactions or None
#         archive.pv_number = instance.pv_number or None
#         archive.total = instance.total or None
#         archive.creator = instance.creator or None
#         archive.date_modified = instance.date_modified or None
#         archive.date_created = instance.date_created or None
#         archive.modified_by = instance.modified_by or None
#         archive.save()

#     elif instance.slip != None and instance.modified_by != None:
#         archive = ExpenseArchive.object.get(id=instance.id)
#         archive.financial_month = instance.financial_month or None
#         archive.financial_year = instance.financial_year or None
#         archive.cheque_number = instance.cheque_number or None
#         archive.paid_to = instance.paid_to or None
#         archive.date = instance.date or None
#         archive.transactions = instance.transactions or None
#         archive.pv_number = instance.pv_number or None
#         archive.total = instance.total or None
#         archive.creator = instance.creator or None
#         archive.date_modified = instance.date_modified or None
#         archive.date_created = instance.date_created or None
#         archive.modified_by = instance.modified_by or None
#         archive.save()


