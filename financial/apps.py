

from django.apps import AppConfig


class FinancialConfig(AppConfig):
    name = 'financial'
    icon_name = 'business'



    def ready(self):
        import financial.signals