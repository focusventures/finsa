from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from savings.models import Savingcontracts, Savingproducts, SavingDepositFee, SavingWithdrawFee
from django.contrib import messages
from members.models import Member, MemberGroup
from chartsoa.models import Account
from financial.forms import TransactionsForm
from django.db.models import Q

import datetime
import decimal 

from .models import Transactions, GroupTransactions
from django.forms import modelformset_factory

from savings.models import SavingAccount, CurrentAccount,SavingTransferFee
from parameters.models import Month, Periods
from fosa.models import Slips

from financial.models import GroupCustomAccount, MemberCustomAccount
import datetime

from utils.services.savings_procedure import SavingsProcedure

@login_required()
def payment(request, pk):
    parameters = request.parameters
    type_ = request.POST['type']
    amount = request.POST['amount']
    instruct = request.POST['submit']
    member = Member.objects.get(id=pk)

    tranaction = Transactions()
    tranaction.actual_amount = amount
    tranaction.type = type_
    tranaction.financial_month = Month.objects.get(id=parameters["settings"]["financial_month"])
    tranaction.financial_period = Periods.objects.get(id=parameters["settings"]["financial_period"])
    tranaction.creator = request.user
    tranaction.date = datetime.datetime.now()
    tranaction.date_created = datetime.datetime.now()
    tranaction.member = member

    slip = Slips()
    slip.number = slip.generate_slip_number(member.id, 'M')
    slip.creator = request.user
    slip.date_created = datetime.datetime.now()
    slip.member = member
    slip.save()

    savings = SavingsProcedure(member.id, amount,type_, slip, request)

    if type_ == "deposit_savings":
        savings.process_deposit(request)

    elif type_ == "withdraw_savings":
        savings.process_withdraw(request)

    elif type_ == "transfer_savings":
        savings.process_transfer(request)

    elif type_ == "pay_loan":
        pass

    elif type_ == "transfer_fund":
        pass

    elif type_ == "deposit_current":
        try:
            acc = CurrentAccount.objects.get(member__id=member.id)

        except SavingAccount.DoesNotExist:
            messages.success(request, 'Member\'s Current Account Does Not Exist')
            return redirect(reverse('members_details', args=(pk,)))
  
        tranaction.description = "Deposit Current Account"
        tranaction.acc = acc.acc_number
        tranaction.save()
        acc.acc_balance += decimal.Decimal(amount)
        acc.save()

    elif type_ == "withdraw_current":
        try:
            acc = CurrentAccount.objects.get(member__id=member.id)

        except SavingAccount.DoesNotExist:
            messages.success(request, 'Member\'s Current Account Does Not Exist')
            return redirect(reverse('members_details', args=(pk,)))

        tranaction.acc = acc.acc_number
        tranaction.amount = amount
        tranaction.save()
        acc.acc_balance -= decimal.Decimal(amount)
        acc.save()

    return redirect(reverse('members_details', args=(pk,)))








@login_required()
def group_payment(request, pk):
    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            GroupTransactions,
            fields=('amount','action',  ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            slip = GroupSlips()
            slip.slip_no = slip.generate_slip_number(member.id, 'M')
            slip.creator = request.user
            slip.date_created = datetime.datetime.now()
            slip.member = group
            slip.save()

            for form in formset:
                if form.cleaned_data.get('amount'):
                    try: 
                        custom_acc = GroupCustomAccount.objects.get(group=pk)
                    except GroupCustomAccount.DoesNotExist:
                        custom_acc = GroupCustomAccount()
                        custom_acc.creator = request.user
                        custom_acc.acc_type = form.cleaned_data.get('action')
                        custom_acc.group = MemberGroup.objects.get(id=pk)
                        custom_acc.save()

                    form.save()
                    custom_acc.acc_balance += custom_acc.acc_balance + int(form.cleaned_data.get('amount'))
                    custom_acc.save()

    return redirect(reverse('groups_details', args=(pk,)))

