# Generated by Django 3.0.4 on 2020-07-18 07:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0021_auto_20200718_1034'),
        ('financial', '0031_auto_20200718_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grouptransactions',
            name='type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.Events', verbose_name='Events'),
        ),
        migrations.AlterField(
            model_name='grouptransactionsarchive',
            name='type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.Events', verbose_name='Events'),
        ),
    ]
