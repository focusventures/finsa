# Generated by Django 3.0.4 on 2020-06-22 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0021_groupcustomaccount_membercustomaccount'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptransactionsarchive',
            name='action',
            field=models.CharField(choices=[('deposit_savings', 'Savings'), ('pay_loan', 'Loan Repayment'), ('bbf', 'BBF')], max_length=50, null=True, verbose_name='Event Type'),
        ),
        migrations.AlterField(
            model_name='groupcustomaccount',
            name='acc_type',
            field=models.CharField(choices=[('deposit_savings', 'Savings'), ('pay_loan', 'Loan Repayment'), ('bbf', 'BBF')], max_length=50, verbose_name='Account Type'),
        ),
        migrations.AlterField(
            model_name='membercustomaccount',
            name='acc_type',
            field=models.CharField(choices=[('deposit_savings', 'Savings'), ('pay_loan', 'Loan Repayment'), ('bbf', 'BBF')], max_length=50, verbose_name='Account Type'),
        ),
    ]
