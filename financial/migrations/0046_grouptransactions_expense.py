# Generated by Django 3.0.4 on 2020-08-14 11:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0045_groupcustomaccount_opening_balance'),
    ]

    operations = [
        migrations.AddField(
            model_name='grouptransactions',
            name='expense',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='Expense Affecting+', to='financial.Expense', verbose_name='Expense'),
        ),
    ]
