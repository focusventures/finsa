from django.contrib import admin

# Register your models here.
# from .models import Codes
from .models import Transactions, GroupAccount, TransactionsArchive, GroupTransactions, GroupTransactionsArchive, MemberCustomAccount, GroupCustomAccount, Expense

# from .models import SavingAccount, CurrentAccount


class CodesModelAdmin(admin.ModelAdmin):
    field = ['code', 'type']
    list_display = ['code', 'type']
    ordering =  ['code', 'type']
    icon_name = 'map'

    search_fields =  ['code', 'type']
    list_filter =  ['code', 'type']

class TransactionsModelAdmin(admin.ModelAdmin):
    field = ['type', 'acc',  'amount','description', 'member', 'receiving_member', 'date', 'processed', 'creator', 'date_created', 'date_modified']
    list_display = ['type', 'acc', 'amount', 'description', 'member', 'receiving_member', 'date', 'processed', 'creator', 'date_created', 'date_modified']
    ordering =  ['type', 'acc', 'description', 'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    icon_name = 'map'

    search_fields = ['type__title', 'acc', 'description','date', 'amount', ]
    list_filter = ['type', 'acc', 'description', 'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']



class TransactionsArchiveModelAdmin(admin.ModelAdmin):
    field = ['type', 'acc',  'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    list_display = ['type', 'acc',  'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    ordering =  ['type', 'acc',  'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    icon_name = 'map'

    search_fields = ['type', 'acc',  'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    list_filter = ['type', 'acc', 'member', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']



class GroupModelAdmin(admin.ModelAdmin):
    field = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator', 'date_modifed', 'modified_by']
    list_display = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']
    ordering = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']
    icon_name = 'map'

    search_fields =['acc_number', 'acc_balance', 'member',]
    list_filter = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']



class GroupTransactionsModelAdmin(admin.ModelAdmin):
    field = ['action','type', 'slip', 'acc', 'amount',  'processed', 'creator', 'date_created', 'date_modified']
    list_display = ['id','action','type', 'slip', 'acc', 'amount',  'processed', 'creator', 'date_created', 'date_modified']
    ordering =  ['action','type', 'slip', 'acc', 'amount',  'processed', 'creator', 'date_created', 'date_modified']
    icon_name = 'map'
    search_fields = ['action__title','type__title', 'slip__number', 'amount','id' ]
    list_filter =['action','type', 'slip', 'acc', 'amount',  'processed', 'creator', 'date_created', 'date_modified']




class GroupTransactionsArchiveModelAdmin(admin.ModelAdmin):
    field = ['action','type', 'slip', 'acc',  'group', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    list_display = ['action', 'type','slip','acc', 'group', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    ordering =  ['action','type', 'slip','acc', 'group', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    icon_name = 'map'
    search_fields = ['action', 'type', 'slip','acc', 'group', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']
    list_filter = ['action', 'type','slip', 'acc', 'group', 'receiving_member', 'date', 'processed', 'amount', 'creator', 'date_created', 'date_modified']



class GroupCustomAccountModelAdmin(admin.ModelAdmin):
    field = ['acc_type','acc_number', 'acc_balance', 'group', 'creator', 'date_created', 'date_modified']
    list_display =  ['acc_type','acc_number', 'acc_balance', 'group', 'creator', 'date_created', 'date_modified']
    ordering =   ['acc_type','acc_number', 'acc_balance', 'group', 'creator', 'date_created', 'date_modified']
    icon_name = 'map'
    search_fields = ['acc_type__title','acc_number', 'acc_balance', 'group__group_name',]
    list_filter =  ['acc_type','acc_number', 'acc_balance', 'group', 'creator', 'date_created', 'date_modified']



class MemberCustomAccountModelAdmin(admin.ModelAdmin):
    field = ['acc_type','acc_number', 'acc_balance', 'member', 'creator', 'date_created', 'date_modified']
    list_display =  ['acc_type','acc_number', 'acc_balance', 'member', 'creator', 'date_created', 'date_modified']
    ordering =   ['acc_type','acc_number', 'acc_balance', 'member', 'creator', 'date_created', 'date_modified']
    icon_name = 'map'
    search_fields = ['acc_type__title','member__account_number', 'acc_balance', 'member__first_name', 'member__middle_name', 'member__last_name', ]
    list_filter =  ['acc_type','acc_number', 'acc_balance', 'member', 'creator', 'date_created', 'date_modified']


admin.site.register(Transactions, TransactionsModelAdmin)
admin.site.register(GroupTransactions, GroupTransactionsModelAdmin)
admin.site.register(GroupTransactionsArchive, GroupTransactionsArchiveModelAdmin)
admin.site.register(TransactionsArchive, TransactionsArchiveModelAdmin)
admin.site.register(GroupAccount, GroupModelAdmin)
admin.site.register(MemberCustomAccount, MemberCustomAccountModelAdmin)
admin.site.register(GroupCustomAccount,GroupCustomAccountModelAdmin)
admin.site.register(Expense)