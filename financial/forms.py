
from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from .models import Transactions

from members.models import Member, MemberGroup
class SimpleForm(ModelForm):
    class Meta:
        model = Transactions
        fields = ['type', 'amount','member']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self,pk, *args, **kwargs):
        super(SimpleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
      
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "member":
            kwargs["queryset"] = Member.objects.filter(group__id=pk)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class TransactionsForm(ModelForm):
    class Meta:
        model = Transactions
        fields = ['type', 'description', 'amount','receiving_member']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(TransactionsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['receiving_member'].required = False
        self.fields['description'].required = False
        self.helper.layout = Layout(
            Row(
                Column('type', css_class='form-group col-md-6 mb-0'),
                Column('amount', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
         
          
          

            Row(
                Column('description', css_class='form-group col-md-12 mb-0'),
        
              
                css_class='form-row'
            ),
          Row(
                Column('receiving_member', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

       
          
            Row(
                Submit('submit', 'Process', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "receiving_member":
            kwargs["queryset"] = Member.objects.filter(name__in=['God', 'Demi God'])
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


from .models import CashFromBank
from chartsoa.models import PaymentMethod

class CashFromBankForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = CashFromBank
        fields = ['payment_method', 'date', 'amount','pv_number', 'cheque_number']
    

    def __init__(self, *args, **kwargs):
        super(CashFromBankForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['payment_method'].queryset = PaymentMethod.objects.filter(loc__is_bank=True)
        self.helper.layout = Layout(
            Row(
                Column('payment_method', css_class='form-group col-md-6 mb-0'),
                Column('date', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('cheque_number', css_class='form-group col-md-6 mb-0'),
                Column('pv_number', css_class='form-group col-md-6 mb-0'),

              
                css_class='form-row'
            ),
            Row(
                Column('amount', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Submit('submit', 'Process', css_class="btn btn-success btn-block btn-md m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )


class FosaTransactionsForm(ModelForm):
    class Meta:
        model = Transactions
        fields = ['type', 'description', 'amount','receiving_member']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(FosaTransactionsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['receiving_member'].required = False
        self.fields['description'].required = False
        self.helper.layout = Layout(
            Row(
                Column('type', css_class='form-group col-md-6 mb-0'),
                Column('amount', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('description', css_class='form-group col-md-12 mb-0'),
        
              
                css_class='form-row'
            ),
            Row(
                Column('receiving_member', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Submit('submit', 'Process', css_class="btn btn-success btn-block btn-md m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



class ModelTransactionForm(ModelForm):
    class Meta:
        model = Transactions
        fields = ['member', 'amount', 'type']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args,  **kwargs):
        super(ModelTransactionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['member'].queryset = Member.objects.filter(group=1)
      



class SimpleTransactionsForm(ModelForm):
    class Meta:
        model = Transactions
        fields = ['type', 'description', 'amount']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(SimpleTransactionsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['description'].required = False
        self.helper.layout = Layout(
            Row(
                Column('type', css_class='form-group col-md-3 mb-0'),
                Column('description', css_class='form-group col-md-12 mb-0'),
                Column('amount', css_class='form-group col-md-3 mb-0'),
                Submit('submit', 'Process', css_class="btn btn-success btn-block btn-md m-2"),
                css_class='form-row'
            )
         
            
        )



from financial.models import Cheque

class ChequeForm(ModelForm):
    class Meta:
        model = Cheque
        fields = ['cheque_number', 'a_type', 'amount',]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(ChequeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('cheque_number', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('a_type', css_class='form-group col-md-12 mb-0'),
        
              
                css_class='form-row'
            ),
            Row(
                Column('amount', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Submit('submit', 'Process', css_class="btn btn-success btn-block btn-md m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )


from .models import Expense

class ExpensesForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Expense
        fields = ['paid_to', 'cheque_number', 'date','pv_number']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(ExpensesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('paid_to', css_class='form-group col-md-6 mb-0'),
                Column('cheque_number', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('date', css_class='form-group col-md-6 mb-0'),
                Column('pv_number', css_class='form-group col-md-6 mb-0'),
        
        
              
                css_class='form-row'
            ),
       
          

         
            
        )



class ExpensesEditForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Expense
        fields = ['paid_to', 'cheque_number', 'date','pv_number']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(ExpensesEditForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('paid_to', css_class='form-group col-md-6 mb-0 disabled'),
                Column('cheque_number', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('date', css_class='form-group col-md-6 mb-0'),
                Column('pv_number', css_class='form-group col-md-6 mb-0'),
        
        
              
                css_class='form-row'
            ),
       
        
         
            
        )



from .models import Revenue




class RevenuesForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Revenue
        fields = ['received_from', 'cheque_number', 'date','pv_number']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(RevenuesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('received_from', css_class='form-group col-md-6 mb-0'),
                Column('cheque_number', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('date', css_class='form-group col-md-6 mb-0'),
                Column('pv_number', css_class='form-group col-md-6 mb-0'),
        
        
              
                css_class='form-row'
            ),
       
          

         
            
        )



class RevenuesEditForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Revenue
        fields = ['received_from', 'cheque_number', 'date','pv_number']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(RevenuesEditForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('paid_to', css_class='form-group col-md-6 mb-0 disabled'),
                Column('cheque_number', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('date', css_class='form-group col-md-6 mb-0'),
                Column('pv_number', css_class='form-group col-md-6 mb-0'),
        
        
              
                css_class='form-row'
            ),
       
        
         
            
        )