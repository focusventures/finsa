from django.db import models
from django.utils.translation import ugettext_lazy as _

event_type = (
    ("deposit_savings","Savings" ),
    # ("withdraw_savings","Debit Saving Account ( Withdraw )" ),
    # ("transfer_savings","Transfer From Saving Account (Transfer)" ),
    # ("deposit_current","Credit Current Account (  Deposit Current Account )" ),
    # ("withdraw_current","Debit Current Account (  Withdraw )" ),
    # ("transfer_fund_current","Transfer Funds From Current Account ( Transfer )" ),
    # ("pay_loan","Loan Repayment ( Credit )" ),
    ("share_capital","Share Capital" ),
    ("pay_loan","Loan Repayment" ),
    ("share_capital","Share Capital" ),
    ("loan_interest","Interest On Member Loans" ),
    ("entrance_fee","Entrance Fee"),
    ("loan_form", "Loan Form"),
    ("sumndry", "Sumndry Debtors"),
    ("bbf","BBF" ),
)



actions = (
    ("deposit_savings","Savings" ),
    ("pay_loan","Loan Repayment" ),


)



import datetime

d_entry = (
    ("DR", "Debit"),
    ("CR", "Credit"),
)

account = (
    ("cash", "Cash"),
    ("bank", "Bank"),
)

class Transactions(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    double_entry = models.CharField(_("Double Entry"), choices=d_entry, default="DR",  max_length=50)
    loc_account = models.CharField(_("Loc Account"), choices=account, max_length=50, default='cash')
    # type = models.CharField(_("Event Type"), choices=event_type, max_length=50, null=True)
    type = models.ForeignKey("parameters.Events", verbose_name=_("Events"), on_delete=models.DO_NOTHING, null=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)

    action = models.CharField(_("Event Type"), choices=actions, max_length=50, null=True)
    acc = models.CharField(_("Account Number"), max_length=50, null=True, blank=True)
    slip = models.ForeignKey("fosa.Slips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    session = models.ForeignKey("fosa.Sessions", verbose_name=_("Session"), on_delete=models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(_("Code"), max_length=50, null=True, blank=True)
    description = models.CharField(_("Particulars"), max_length=50, null=True, blank=True)

    fees = models.ForeignKey("financial.Transactions", verbose_name=_("Fees"), related_name="Fees+", null=True, blank=True, on_delete=models.DO_NOTHING)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"),null=True, blank=True, on_delete=models.CASCADE)
    receiver = models.CharField(_("Receiving Account"), max_length=50, blank=True, null=True)
    receiving_member =  models.ForeignKey("members.Member", verbose_name=_("Receiving Account"), related_name="Receiving Account+", on_delete=models.CASCADE,null=True, blank=True, default=None)
    date = models.DateTimeField(_("Date of Trasaction"), auto_now=False, auto_now_add=False, null=True)
    processed = models.ForeignKey("authentication.CustomUser", verbose_name=_("Processed By"), on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(_("Amount Transacted"), default=0, max_digits=20, decimal_places=2)
    dr_amount = models.DecimalField(_("Amount Debited Transacted"), default=0, max_digits=20, decimal_places=2)
    cr_amount = models.DecimalField(_("Amount Credited Transacted"), default=0, max_digits=20, decimal_places=2)
    coa = models.ForeignKey("chartsoa.Account", verbose_name=_("Chart Of Account"),null=True, blank=True, on_delete=models.DO_NOTHING)
    cheque = models.CharField(_("Cheque"), max_length=50, null=True, blank=True)

    expense = models.ForeignKey("financial.Expense", verbose_name=_("Expense"), related_name="Expense Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)
    revenue = models.ForeignKey("financial.Revenue", verbose_name=_("Revenue"), related_name="Revenu Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)

    cash_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Cash Account"), related_name="Cash Account+", on_delete=models.DO_NOTHING, null=True, blank=True)
    bank_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Bank Account"), related_name="Bank Account+", on_delete=models.DO_NOTHING, null=True, blank=True)

    actual_amount = models.DecimalField(_("Actual Amount Transacted"), max_digits=20, decimal_places=2, default=0.00)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Item Created By+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    date_created = models.DateField(_("Date Created"), auto_now=True)

    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {} - {} -{} - {} - {}".format(self.id, self.description, self.member, self.acc,  self.type, self.amount)

    class Meta:
        verbose_name = "Transaction"
        verbose_name_plural = 'Transactions'




class TransactionsArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    double_entry = models.CharField(_("Double Entry"), choices=d_entry, default="DR",  max_length=50)
    loc_account = models.CharField(_("Loc Account"), choices=account, max_length=50, default='cash')
    # type = models.CharField(_("Event Type"), choices=event_type, max_length=50, null=True)
    type = models.ForeignKey("parameters.Events", verbose_name=_("Events"), on_delete=models.DO_NOTHING, null=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)

    action = models.CharField(_("Event Type"), choices=actions, max_length=50, null=True)
    acc = models.CharField(_("Account Number"), max_length=50, null=True, blank=True)
    slip = models.ForeignKey("fosa.Slips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    session = models.ForeignKey("fosa.Sessions", verbose_name=_("Session"), on_delete=models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(_("Code"), max_length=50, null=True, blank=True)
    # description = models.TextField(_("Particulars"), null=True, blank=True)
    description = models.CharField(_("Particulars"), max_length=50, null=True, blank=True)

    fees = models.ForeignKey("financial.Transactions", verbose_name=_("Fees"), related_name="Fees+", null=True, blank=True, on_delete=models.DO_NOTHING)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"),null=True, blank=True, on_delete=models.CASCADE)
    receiver = models.CharField(_("Receiving Account"), max_length=50, blank=True, null=True)
    receiving_member =  models.ForeignKey("members.Member", verbose_name=_("Receiving Account"), related_name="Receiving Account+", on_delete=models.CASCADE,null=True, blank=True, default=None)
    date = models.DateTimeField(_("Date of Trasaction"), auto_now=False, auto_now_add=False, null=True)
    processed = models.ForeignKey("authentication.CustomUser", verbose_name=_("Processed By"), on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(_("Amount Transacted"), default=0, max_digits=20, decimal_places=2)
    dr_amount = models.DecimalField(_("Amount Debited Transacted"), default=0, max_digits=20, decimal_places=2)
    cr_amount = models.DecimalField(_("Amount Credited Transacted"), default=0, max_digits=20, decimal_places=2)
    coa = models.ForeignKey("chartsoa.Account", verbose_name=_("Chart Of Account"),null=True, blank=True, on_delete=models.DO_NOTHING)
    cheque = models.CharField(_("Cheque"), max_length=50, null=True, blank=True)

    expense = models.ForeignKey("financial.Expense", verbose_name=_("Expense"), related_name="Expense Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)
    revenue = models.ForeignKey("financial.Revenue", verbose_name=_("Revenue"), related_name="Revenu Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)

    cash_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Cash Account"), related_name="Cash Account+", on_delete=models.DO_NOTHING, null=True, blank=True)
    bank_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Bank Account"), related_name="Bank Account+", on_delete=models.DO_NOTHING, null=True, blank=True)

    actual_amount = models.DecimalField(_("Actual Amount Transacted"), max_digits=20, decimal_places=2, default=0.00)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Item Created By+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    date_created = models.DateField(_("Date Created"), auto_now=True)

    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {} - {} -{} - {} - {}".format(self.id, self.description, self.member, self.acc,  self.type, self.amount)

    class Meta:
        verbose_name = "Transactions Archive"
        verbose_name_plural = 'Transactions Archive'


actions = (
    ("deposit_savings","Savings" ),
    ("pay_loan","Loan Repayment" ),
    ("share_capital","Share Capital" ),
    ("loan_interest","Interest On Member Loans" ),
    ("entrance_fee","Entrance Fee"),
    ("loan_form", "Loan Form"),
    ("sumndry", "Sumndry Debtors"),
    ("bbf","BBF" ),

)

class GroupTransactions(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    double_entry = models.CharField(_("Double Entry"), choices=d_entry, default="DR",  max_length=50)
    # type = models.CharField(_("Event Type"), choices=event_type, max_length=50, null=True)
    type = models.ForeignKey("parameters.Events", verbose_name=_("Events"), related_name="Event Type+", on_delete=models.DO_NOTHING, null=True)
    action = models.ForeignKey("parameters.Events", verbose_name=_("Events"), related_name="action+", on_delete=models.DO_NOTHING, null=True)
    # action = models.CharField(_("Event Type"), choices=actions, max_length=50, null=True)
    acc = models.CharField(_("Account Number"), max_length=50, null=True, blank=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    slip = models.ForeignKey("fosa.GroupSlips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    session = models.ForeignKey("fosa.Sessions", verbose_name=_("Session"), on_delete=models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(_("Code"), max_length=50, null=True, blank=True)
    description = models.TextField(_("Particulars"), null=True, blank=True)
    expense = models.ForeignKey("financial.Expense", verbose_name=_("Expense"), related_name="Expense Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)
    revenue = models.ForeignKey("financial.Revenue", verbose_name=_("Revenue"), related_name="Revenu Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)

    fees = models.ForeignKey("financial.Transactions", verbose_name=_("Fees"), related_name="Fees+", null=True, blank=True, on_delete=models.DO_NOTHING)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"),null=True, blank=True, on_delete=models.CASCADE)
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("Group"), null=True, blank=True, on_delete=models.DO_NOTHING)
    receiver = models.CharField(_("Receiving Account"), max_length=50, blank=True, null=True)
    receiving_member =  models.ForeignKey("members.Member", verbose_name=_("Receiving Account"), related_name="Receiving Account+", on_delete=models.CASCADE,null=True, blank=True, default=None)
    date = models.DateTimeField(_("Date of Trasaction"), auto_now=False, auto_now_add=False, null=True)
    processed = models.ForeignKey("authentication.CustomUser", verbose_name=_("Processed By"), on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(_("Amount Transacted"), default=0, max_digits=20, decimal_places=2)
    dr_amount = models.DecimalField(_("Amount Debited Transacted"), default=0, max_digits=20, decimal_places=2)
    cr_amount = models.DecimalField(_("Amount Credited Transacted"), default=0, max_digits=20, decimal_places=2)
    coa = models.ForeignKey("chartsoa.Account", verbose_name=_("Chart Of Account"),null=True, blank=True, on_delete=models.DO_NOTHING)
    actual_amount = models.DecimalField(_("Actual Amount Transacted"), max_digits=20, decimal_places=2, default=0.00)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Item Created By+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    date_created = models.DateField(_("Date Created"), auto_now=True)

    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {} -{} - {} - {}".format(self.slip, self.group, self.acc,  self.type, self.amount)


    def description(self):
        return "{}".format(self.type)

    @property
    def descr(self):
        return "{}".format(self.action)

    class Meta:
        verbose_name = "Group Transaction"
        verbose_name_plural = 'Group Transactions'

class GroupTransactionsArchive(models.Model):
    id = models.IntegerField(_(""), unique=True, primary_key=True)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    double_entry = models.CharField(_("Double Entry"), choices=d_entry, default="DR",  max_length=50)
    type = models.ForeignKey("parameters.Events", verbose_name=_("Events"), related_name="Event Type+", on_delete=models.DO_NOTHING, null=True)
    action = models.ForeignKey("parameters.Events", verbose_name=_("Events"), related_name="action+", on_delete=models.DO_NOTHING, null=True)
    acc = models.CharField(_("Account Number"), max_length=50, null=True, blank=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    slip = models.ForeignKey("fosa.GroupSlips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    session = models.ForeignKey("fosa.Sessions", verbose_name=_("Session"), on_delete=models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(_("Code"), max_length=50, null=True, blank=True)
    description = models.TextField(_("Particulars"), null=True, blank=True)
    expense = models.ForeignKey("financial.Expense", verbose_name=_("Expense"), related_name="Expense Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)
    revenue = models.ForeignKey("financial.Revenue", verbose_name=_("Revenue"), related_name="Revenu Affecting+", on_delete=models.DO_NOTHING, null=True, blank=True)

    fees = models.ForeignKey("financial.Transactions", verbose_name=_("Fees"), related_name="Fees+", null=True, blank=True, on_delete=models.DO_NOTHING)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"),null=True, blank=True, on_delete=models.CASCADE)
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("Group"), null=True, blank=True, on_delete=models.DO_NOTHING)
    receiver = models.CharField(_("Receiving Account"), max_length=50, blank=True, null=True)
    receiving_member =  models.ForeignKey("members.Member", verbose_name=_("Receiving Account"), related_name="Receiving Account+", on_delete=models.CASCADE,null=True, blank=True, default=None)
    date = models.DateTimeField(_("Date of Trasaction"), auto_now=False, auto_now_add=False, null=True)
    processed = models.ForeignKey("authentication.CustomUser", verbose_name=_("Processed By"), on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(_("Amount Transacted"), default=0, max_digits=20, decimal_places=2)
    dr_amount = models.DecimalField(_("Amount Debited Transacted"), default=0, max_digits=20, decimal_places=2)
    cr_amount = models.DecimalField(_("Amount Credited Transacted"), default=0, max_digits=20, decimal_places=2)
    coa = models.ForeignKey("chartsoa.Account", verbose_name=_("Chart Of Account"),null=True, blank=True, on_delete=models.DO_NOTHING)
    actual_amount = models.DecimalField(_("Actual Amount Transacted"), max_digits=20, decimal_places=2, default=0.00)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Item Created By+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    date_created = models.DateField(_("Date Created"), auto_now=True)

    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {} -{} - {} - {}".format(self.slip, self.group, self.acc,  self.type, self.amount)


    def description(self):
        return "{}".format(self.type)

    @property
    def descr(self):
        return "{}".format(self.action)

    class Meta:
        verbose_name = "Group Transactions Archive"
        verbose_name_plural = 'Group Transactions Archive'

class GroupAccount(models.Model):
    acc_number = models.CharField(_("Account Number"), max_length=50)
    acc_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", default=None, verbose_name=_("Member"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Group Account'
        verbose_name_plural = 'Group Accounts'


# class Ledgers(models.Model):



class CashBook(models.Model):
    financial_period = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


class BalanceSheet(models.Model):
    financial_period = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {}".format(self.financial_period, self.financial_month)


    class Meta:
        verbose_name = 'Balance Sheet'
        verbose_name_plural = 'Balance Sheet'




type = (
    ("deposit_savings","Savings" ),
    ("pay_loan","Loan Repayment" ),
    ("bbf","BBF" ),

)


class MemberCustomAccount(models.Model):
    acc_type = models.ForeignKey("parameters.Events", verbose_name=_("Account Type"), on_delete=models.DO_NOTHING, null=True)
    acc_number = models.CharField(_("Account Number"), max_length=50)
    acc_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    opening_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", default=None, verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Member Custom Account'
        verbose_name_plural = 'Member Custom Accounts'




class GroupCustomAccount(models.Model):
    # acc_type = models.CharField(_("Account Type"), choices=type, max_length=50)
    acc_type = models.ForeignKey("parameters.Events", verbose_name=_("Account Type"), on_delete=models.DO_NOTHING, null=True)
    acc_number = models.CharField(_("Account Number"), max_length=50)
    acc_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    opening_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("Member Group"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Group Custom Account'
        verbose_name_plural = 'Group Custom Accounts'



t = (
    ('savings', 'Savings'),
    ('current', 'Current'),
)

class Cheque(models.Model):
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    cheque_number = models.CharField(_("Cheque Number"), max_length=50)
    a_type = models.CharField(_("Account Type"), choices=t, max_length=50)
    amount = models.DecimalField(_("Amount"), max_digits=15, decimal_places=2)
    # fee = models.DecimalField(_("Fees"), max_digits=15, decimal_places=2)
    
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Cheque'
        verbose_name_plural = 'Cheque'




class Expense(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    cheque_number = models.CharField(verbose_name=_("Cheque Number"), max_length=50)
    paid_to = models.CharField(_("Paid To"), max_length=150)
    date = models.DateField(_("Date Of Payment"), auto_now=False, auto_now_add=False)
    transactions = models.ManyToManyField("financial.Transactions", related_name="Transaction Affected+",verbose_name=_("Transactions"))
    pv_number = models.CharField(_("P.V NO"), max_length=50)
    total = models.DecimalField(_("Total"), max_digits=15, decimal_places=2, default=0.00)
    date = models.DateField(_("Date Of Transaction"), auto_now=False, auto_now_add=False, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {}".format(self.cheque_number, self.paid_to)

    @property
    def pv_number_(self):
        return self.pv_number

    class Meta:
        verbose_name = 'Payment'
        verbose_name_plural = 'Payments'



class ExpenseArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    id = models.IntegerField(_("Id"), unique=True, primary_key=True)
    cheque_number = models.CharField(verbose_name=_("Cheque Number"), max_length=50)
    paid_to = models.CharField(_("Paid To"), max_length=150)
    date = models.DateField(_("Date Of Payment"), auto_now=False, auto_now_add=False)
    transactions = models.ManyToManyField("financial.Transactions", related_name="Transaction Affected+",verbose_name=_("Transactions"))
    pv_number = models.CharField(_("P.V NO"), max_length=50)
    total = models.DecimalField(_("Total"), max_digits=15, decimal_places=2, default=0.00)
    date = models.DateField(_("Date Of Transaction"), auto_now=False, auto_now_add=False, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {}".format(self.cheque_number, self.paid_to)

    class Meta:
        verbose_name = 'Payment Archive'
        verbose_name_plural = 'Payments Archive'



class Revenue(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    cheque_number = models.CharField(verbose_name=_("Cheque Number"), max_length=50)
    received_from = models.CharField(_("Received From"), max_length=150)
    date = models.DateField(_("Date Of Payment"), auto_now=False, auto_now_add=False)
    transactions = models.ManyToManyField("financial.Transactions", related_name="Transaction Affected+",verbose_name=_("Transactions"))
    pv_number = models.CharField(_("P.V NO"), max_length=50)
    total = models.DecimalField(_("Total"), max_digits=15, decimal_places=2, default=0.00)
    date = models.DateField(_("Date Of Transaction"), auto_now=False, auto_now_add=False, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {}".format(self.cheque_number, self.paid_to)

    @property
    def pv_number_(self):
        return self.pv_number





class RevenueArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    id = models.IntegerField(_("Id"), unique=True, primary_key=True)
    cheque_number = models.CharField(verbose_name=_("Cheque Number"), max_length=50)
    received_from = models.CharField(_("Received From"), max_length=150)
    date = models.DateField(_("Date Of Payment"), auto_now=False, auto_now_add=False)
    transactions = models.ManyToManyField("financial.Transactions", related_name="Transaction Affected+",verbose_name=_("Transactions"))
    pv_number = models.CharField(_("P.V NO"), max_length=50)
    total = models.DecimalField(_("Total"), max_digits=15, decimal_places=2, default=0.00)
    date = models.DateField(_("Date Of Transaction"), auto_now=False, auto_now_add=False, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {}".format(self.cheque_number, self.paid_to)




class CashFromBank(models.Model):
    amount = models.DecimalField(_("Amount"), max_digits=20, decimal_places=2)
    cheque_number = models.CharField(verbose_name=_("Cheque Number"), max_length=50)

    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    pv_number = models.CharField(_("P.V NO"), max_length=50)
    date = models.DateField(_("Date Of Transaction"), auto_now=False, auto_now_add=False, null=True, blank=True)


