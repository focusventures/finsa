# from django.contrib import admin

# from .models import Employee, EmployeeDocument, SalaryGroups, Professions, Roles, BankInfo


# class RoleAdminModel(admin.ModelAdmin):
#     list_display = ['title',     'department', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     ordering =  ['title',     'department', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     icon_name = 'map'

#     search_fields =  ['title',     'department',  ]
#     list_filter =  ['title',     'department', 'date_modified', 'modified_by', 'creator', 'date_created' ]




# class ProfessionAdminModel(admin.ModelAdmin):
#     list_display = ['title',     'salary_group', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     ordering =  ['title',     'salary_group', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     icon_name = 'map'

#     search_fields =  ['title',     'salary_group',  ]
#     list_filter =  ['title',     'salary_group', 'date_modified', 'modified_by', 'creator', 'date_created' ]



# class SalaryGroupsAdminModel(admin.ModelAdmin):
#     list_display = ['title',     'salary', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     ordering =  ['title',     'salary', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     icon_name = 'map'

#     search_fields =  ['title',     'salary', ]
#     list_filter =  ['title',     'salary', 'date_modified', 'modified_by', 'creator', 'date_created' ]

# class EmployeeDocumentAdminModel(admin.ModelAdmin):
#     list_display = ['title',     'document', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     ordering =  ['title',     'document', 'date_modified', 'modified_by', 'creator', 'date_created' ]
#     icon_name = 'map'

#     search_fields =  ['title',     'document',  ]
#     list_filter =  ['title',     'document', 'date_modified', 'modified_by', 'creator', 'date_created' ]

# class EmployeeModelAdmin(admin.ModelAdmin):
#     list_display = [
#         'first_name', 'middle_name', 'last_name', 'business_name' , 'id_number', 'phone_number', 'profession', 'department', 'role',  'sex', 'birth_date', 'avatar','id_copy',
#     ]
#     order = [
#         'first_name', 'middle_name', 'last_name', 'business_name' , 'id_number', 'phone_number', 'profession', 'department', 'role',  'sex', 'birth_date', 'avatar','id_copy',
#     ]
#     icon_name = 'people'
#     search_fields =[
#         'first_name', 'middle_name', 'last_name', 'business_name' , 'id_number', 'phone_number', 'profession', 'department', 'role',  'sex', 'birth_date', 'avatar','id_copy',
#     ]
#     list_filter = [
#         'first_name', 'middle_name', 'last_name', 'business_name' , 'id_number', 'phone_number', 'profession', 'department', 'role',  'sex', 'birth_date', 'avatar','id_copy',
#     ]
  
#     list_per_page = 15

# admin.site.register(Employee, EmployeeModelAdmin)
# admin.site.register(EmployeeDocument, EmployeeDocumentAdminModel)
# admin.site.register(SalaryGroups, SalaryGroupsAdminModel)
# admin.site.register(Professions, ProfessionAdminModel)
# admin.site.register(Roles, RoleAdminModel)
# admin.site.register(BankInfo)
