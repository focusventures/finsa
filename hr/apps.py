from django.apps import AppConfig


class HrConfig(AppConfig):
    name = 'hr'
    icon_name = 'people'
