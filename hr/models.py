from django.db import models
from stdimage import StdImageField
from django.utils.translation import ugettext_lazy as _

from authentication.models import CustomUser
from django.db import connection


def get_tenant_specific_upload_folder(instance, filename):
    upload_folder = 'media/{0}/{}1'.format(
        connection.tenant,
        filename
    )
    return upload_folder



counties = (
    ("Mombasa", "Mombasa"),
	("Kwale","Kwale"),
	("Kilifi","Kilifi"),	
	("Tana River",	"Tana River"),
	("Lamu", "Lamu"),	
	("Taita–Taveta","Taita–Taveta"),
	("Garissa",	"Garissa"),
	("Wajir","Wajir"),	
	("Mandera", "Mandera"),
	("Marsabit","Marsabit"),	
	("Isiolo","Isiolo"),	
	("Meru","Meru"),	
	("Tharaka-Nithi",	"Tharaka-Nithi"),
	("Embu","Embu"),	
	("Kitui","Kitui"),	
	("Machakos","Machakos"),	
	("Makueni","Makueni"),
	("Nyandarua","Nyandarua"),	
	("Nyeri","Nyeri"),
	("Kirinyaga","Kirinyaga"),	
	("Murang'a", "Murang'a"),
	("Kiambu","Kiambu"),	
	("Turkana", "Turkana"),
	("West Pokot",	"West Pokot"),
	("Samburu", "Samburu"),
	("Trans-Nzoia","Trans-Nzoia"),
	("Uasin Gishu",	 "Uasin Gishu"),	
	("Elgeyo-Marakwet",	"Elgeyo-Marakwet"),
	("Nandi", "Nandi"),	
	("Baringo", "Baringo"),
	("Laikipia", "Laikipia"),
	("Nakuru", "Nakuru"),	
    ("Narok", "Narok"),
	("Kajiado", "Kajiado"),
	("Kericho", "Kericho"),
	("Bomet", "Bomet"),	 
	("Kakamega","Kakamega"),
	("Vihiga", "Vihiga"),	
	("Bungoma", "Bungoma"),
	("Busia", "Busia"),
	("Siaya", "Siaya"),	 
	("Kisumu",	 "Kisumu"),
	("Homa Bay", "Homa Bay"),	
	("Migori",	 "Migori"),
	("Kisii", "Kisii"),
	("Nyamira",	"Nyamira"),
    ("Nairobi", "Nairobi"),


)


gender = (
    ("MALE", "MALE"),
    ("FEMALE", "FEMALE"),
)

status = (
    ("MARRIED", "MARRIED"),
    ("SINGLE", "SINGLE"),
)

sex = (
    ("Male", "MALE"),
    ("Female", "FEMALE"),
)

def get_tenant_specific_upload_folder_avatar(instance, filename):
    upload_folder = '{0}/employees/avatars/{1}'.format(
        connection.tenant.schema_name,
        filename
    )
    return upload_folder



def get_tenant_specific_upload_folder_signature(instance, filename):
    upload_folder = '{0}/employees/signatures/{1}'.format(
        connection.tenant.schema_name,
        filename
    )
    return upload_folder


def get_tenant_specific_upload_folder_document(instance, filename):
    upload_folder = '{0}/employees/ducoments/{1}'.format(
        connection.tenant.schema_name,
        filename
    )
    return upload_folder


class Employee(models.Model):
    sex = models.CharField(max_length=10, choices=sex, default="Female")
    birth_date = models.DateTimeField(blank=True, null=True)
    image_path = models.CharField(max_length=500, blank=True, null=True)
    father_name = models.CharField(max_length=200, blank=True, null=True)
    birth_place = models.CharField(max_length=50, blank=True, null=True)
    nationality = models.CharField(max_length=50, blank=True, null=True)
    profession = models.ForeignKey("hr.Professions", verbose_name=_("Profession"), on_delete=models.CASCADE)
    first_name = models.CharField(_("First Name"), max_length=50, null=True, blank=True,)
    middle_name = models.CharField(_("Middle Name"), max_length=50, null=True, blank=True,)
    last_name = models.CharField(_("Last Name"), max_length=50, null=True, blank=True,)
    account_number = models.ForeignKey("hr.BankInfo", verbose_name=_("Banking Info"), on_delete=models.CASCADE)
    phone_number = models.CharField(_("Phone Number"), max_length=50, null=True, blank=True,)
    id_number = models.CharField(_("National Id Number"), max_length=50, null=True, blank=True)
    tell_phone = models.CharField(_("Tell Phone"), max_length=50, null=True, blank=True,)
    business_name = models.CharField(_("Company Name"), max_length=50, null=True, blank=True,)
    role = models.ForeignKey("hr.Roles", verbose_name=_(" Employee Role"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("MemberGroup"), null=True, blank=True, on_delete=models.CASCADE)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)
    roomNumber = models.CharField(_("Room Number"), max_length=50, null=True, blank=True)
    house = models.CharField(_("House"), max_length=50, null=True, blank=True)
    streetName = models.CharField(_("Street Name"), max_length=50, null=True, blank=True)
    town = models.CharField(_("Town"), max_length=50, null=True, blank=True)
    city = models.CharField(_("City"), max_length=50, null=True, blank=True)
    branch = models.ForeignKey("ostructure.Branch", verbose_name=_("Member Branch"),null=True, on_delete=models.CASCADE)
    county = models.CharField(_("County"), choices=counties, max_length=50, null=True, blank=True)
    physical_address = models.CharField(_("State"), max_length=100, null=True, blank=True)
    date_of_birth = models.DateField(_("Date Of Birth"), default=None, auto_now=False, auto_now_add=False)
    gender = models.CharField(_("Gender"), choices=gender, default="FEMALE", max_length=50)
    marrital_status = models.CharField(_("Marital Status"), default="SINGLE", choices=status, max_length=50)
    email = models.EmailField(_("Employee Email"), null=True, blank=True, max_length=254)
    department = models.ForeignKey("ostructure.Department", verbose_name=_("Department"), on_delete=models.CASCADE)


    avatar = StdImageField(_("Employee Passport"), upload_to=get_tenant_specific_upload_folder_avatar,
                          variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True, default='user.jpg')
    
    id_copy = StdImageField(_("Employee Id Copy"), upload_to=get_tenant_specific_upload_folder_signature,
                          variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True, default='user.jpg')
    documents = models.ManyToManyField("hr.EmployeeDocument", verbose_name=_("Employee Documents"))

    class Meta:
        verbose_name = "Emplyee"
        verbose_name_plural = 'Employees'


    @property
    def get_names(self):
        return "{} {}".format(self.first_name, self.last_name)


    
    def get_savings(self):

        return self.saving_acc.acc_balance

    @property
    def get_building(self):
        return "%s" %('Hello')

    @property
    def get_arrears(self):
        return "%s" %('Hello')

    @property
    def get_overpayments(self):
        return "%s" %('Hello')

    @property
    def get_c_arrears(self):
        return "%s" %('Hello')

    @property
    def get_c_overpayments(self):
        return "%s" %('Hello')

    @property
    def get_unit(self):
        print(self.current_unit)
        return "%s" %(self.current_unit)


    @property
    def get_payment_status(self):
        return "%s" %('Hello')


    def __str__(self):
        return self.phone_number
    

class EmployeeDocument(models.Model):
    title = models.CharField(_("Title Of The Document"), max_length=50)
    document = StdImageField(_("Documents"), upload_to=get_tenant_specific_upload_folder_document,
                          variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True, default='user.jpg')
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Employee Document"
        verbose_name_plural = 'Employee Documents'


    def __str__(self):
        return self.title
    

class Professions(models.Model):
    title = models.CharField(_("Title Of Profession"), max_length=50)
    salary_group = models.ForeignKey("hr.SalaryGroups", verbose_name=_("Salary Group"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Professions"
        verbose_name_plural = 'Professions'

    def __str__(self):
        return self.title


class BankInfo(models.Model):
    bank = models.CharField(_("Bank Name"), max_length=50)
    account_number = models.CharField(_("Bank Account"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Banking Info"
        verbose_name_plural = 'Banking Info'

    def __str__(self):
        return self.bank
    



class SalaryGroups(models.Model):
    title = models.CharField(_("Title Of Salary Group"), max_length=50)
    salary = models.DecimalField(_("Salary"), max_digits=10, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Salaries"
        verbose_name_plural = 'Salaries'

    def __str__(self):
        return self.title
    

class Roles(models.Model):
    title = models.CharField(_("Role"), max_length=50)
    department = models.ForeignKey("ostructure.Department", verbose_name=_("Department"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Roles"
        verbose_name_plural = 'Roles'

    def __str__(self):
        return self.title
    