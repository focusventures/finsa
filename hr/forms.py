from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from .models import Employee, Professions, SalaryGroups, Roles

class EmployeeForm(ModelForm):
    date_of_birth = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))
    phone_number = forms.RegexField(regex=r'^\+?1?\d{9,10}$',
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': 'phone number'}),
                                        help_text=(
                                        "Phone number must be entered in the format: ''. Up to 10 digits allowed. format is 0710121314"))

    class Meta:
        model = Employee
        fields = ['first_name', 'middle_name', 'last_name','avatar','id_copy','documents', 'gender', 'marrital_status', 'email', 'date_of_birth','account_number', 'department',
        'phone_number', 'id_number', 'tell_phone', 'profession', 'role',
        'physical_address',]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(EmployeeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['id_number'].required = True
        self.helper.layout = Layout(
            Row(
                Column('first_name', css_class='form-group col-md-4 mb-0'),
                Column('middle_name', css_class='form-group col-md-4 mb-0'),
                Column('last_name', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('phone_number', css_class='form-group col-md-6 mb-0'),
                Column('id_number', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

            Row(
                Column('account_number', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
               Row(
                Column('avatar', css_class='form-group col-md-4 mb-0'),
                Column('id_copy', css_class='form-group col-md-4 mb-0'),
                Column('documents', css_class='form-group col-md-4 mb-0'),

                css_class='form-row'
            ),
                 Row(
                Column('email', css_class='form-group col-md-6 mb-0'),
                Column('date_of_birth', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
              Row(
                Column('gender', css_class='form-group col-md-4 mb-0'),
                Column('marrital_status', css_class='form-group col-md-4 mb-0'),

                css_class='form-row'
            ),
           
            Row(
                Column('branch', css_class='form-group col-md-6 mb-0'),
        
              
                css_class='form-row'
            ),
            Row(
                Column('group', css_class='form-group col-md-6 mb-0'),
                Column('economic_activity', css_class='form-group col-md-6 mb-0'),

              
                css_class='form-row'
            ),
            Row(
                Column('tell_phone', css_class='form-group col-md-8 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Column('physical_address', css_class='form-group col-md-4 mb-0'),
   
                css_class='form-row'
            ),

             Row(
                Column('department', css_class='form-group col-md-4 mb-0'),
   
                css_class='form-row'
            ),
            Row(
                Column('profession', css_class='form-group col-md-4 mb-0'),
                Column('role', css_class='form-group col-md-4 mb-0'),
                
              
                css_class='form-row'
            ),
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )




class ProfessionForm(ModelForm):
    class Meta:
        model = Professions
        fields = ['title', 'salary_group' ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(ProfessionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

             Row(
                Column('salary_group', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



class SalaryGroupForm(ModelForm):
    class Meta:
        model = SalaryGroups
        fields = ['title', 'salary' ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(SalaryGroupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

             Row(
                Column('salary', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )





class RolesForm(ModelForm):
    class Meta:
        model = Roles
        fields = ['title', 'department' ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(RolesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

             Row(
                Column('department', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )