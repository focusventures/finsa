from django.urls import path

from . import views



urlpatterns = [
    path('hr', views.index, name="hr"),
    path('employees/list', views.list, name="employees_list"),
    path('employees/details/<int:pk>', views.details, name="employees_details"),
    path('employees/aoe', views.aoe, name="employees_aoe"),
    path('employees/edit/<int:pk>', views.edit, name="employees_edit"),

    path('roles/list', views.roles_list, name="roles_list"),
    path('roles/details/<int:pk>', views.roles_details, name="roles_details"),
    path('roles/aoe', views.roles_aoe, name="roles_aoe"),
    path('roles/edit/<int:pk>', views.roles_edit, name="roles_edit"),


    path('salaries_groups/list', views.salaries_list, name="salaries_list"),
    path('salaries_groups/details/<int:pk>', views.salaries_details, name="salaries_details"),
    path('salaries_groups/aoe', views.salaries_aoe, name="salaries_aoe"),
    path('salaries_groups/edit/<int:pk>', views.salaries_edit, name="salaries_edit"),



    path('professions/list', views.professions_list, name="professions_list"),
    path('professions/details/<int:pk>', views.professions_details, name="professions_details"),
    path('professions/aoe', views.professions_aoe, name="professions_aoe"),
    path('professions/edit/<int:pk>', views.professions_edit, name="professions_edit"),
]
