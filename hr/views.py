from django.shortcuts import render
from .models import Employee, SalaryGroups, Professions, Roles

from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


from .forms import EmployeeForm, ProfessionForm, RolesForm, SalaryGroupForm

from django.db.models import Q

def index(request):
    context = {
        "roles": Roles.objects.all().count,
        "employees": Employee.objects.all().count,
        "professions": Professions.objects.all().count,
        "salaries": SalaryGroups.objects.all().count
    }
    return render(request, 'hr/index.html', context)

    
@login_required()
def list(request):
    members = []
    if True:

        members_list = Employee.objects.all()


        
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)

        
    else:
        
        members_list = Employee.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)
    context = {
        'members': members
    }
    return render(request, 'hr/employees.html', context)

@login_required()
def aoe(request):
    form = EmployeeForm()
    if request.method == 'POST':
        form = EmployeeForm(request.POST, request.FILES)
        if form.is_valid():
            member = form.save(commit=False)
            member.creator = request.user
            member.save()
            return redirect(reverse('employees_list'))
    else:
       form = EmployeeForm()
    context = {'form': form}
    return render(request, 'hr/employee_aoe.html', context)


@login_required()
def edit(request, pk):
    form = EmployeeForm()
    if request.method == 'POST':
        form = EmployeeForm(request.POST, request.FILES, instance=Employee.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('employees_list'))
    else:
       form = EmployeeForm(instance=Employee.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'hr/employee_aoe.html', context)



@login_required()
def details(request, pk):
    tenant = Employee.objects.get(id=pk)
    context = {
        "tenant": tenant
    }
    
    return render(request, 'hr/employee_details.html', context)



@login_required()
def roles_list(request):
    members = []
    if True:

        members_list = Roles.objects.all()


        
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)

        
    else:
        
        members_list = Roles.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)
    context = {
        'members': members
    }
    return render(request, 'hr/roles.html', context)

@login_required()
def roles_aoe(request):
    form = RolesForm()
    if request.method == 'POST':
        form = RolesForm(request.POST, request.FILES)
        if form.is_valid():
            member = form.save(commit=False)
            member.creator = request.user
            member.save()
            return redirect(reverse('roles_list'))
    else:
       form = RolesForm()
    context = {'form': form}
    return render(request, 'hr/roles_aoe.html', context)


@login_required()
def roles_edit(request, pk):
    form = RolesForm()
    if request.method == 'POST':
        form = RolesForm(request.POST, request.FILES, instance=Roles.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('roles_list'))
    else:
       form = RolesForm(instance=Roles.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'hr/roles_aoe.html', context)



@login_required()
def roles_details(request, pk):
    context = {
    }
    
    return render(request, 'hr/roles_details.html')













    
@login_required()
def professions_list(request):
    members = []
    if True:

        members_list = Professions.objects.all()


        
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)

        
    else:
        
        members_list = Professions.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)
    context = {
        'members': members
    }
    return render(request, 'hr/professions.html', context)

@login_required()
def professions_aoe(request):
    form = ProfessionForm()
    if request.method == 'POST':
        form = ProfessionForm(request.POST, request.FILES)
        if form.is_valid():
            member = form.save(commit=False)
            member.creator = request.user
            member.save()
            return redirect(reverse('professions_list'))
    else:
       form = ProfessionForm()
    context = {'form': form}
    return render(request, 'hr/professions_aoe.html', context)


@login_required()
def professions_edit(request, pk):
    form = ProfessionForm()
    if request.method == 'POST':
        form = ProfessionForm(request.POST, request.FILES, instance=Professions.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('professions_list'))
    else:
       form = ProfessionForm(instance=Professions.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'hr/professions_aoe.html', context)



@login_required()
def professions_details(request, pk):
    context = {
    }
    
    return render(request, 'hr/professions_details.html')






















@login_required()
def salaries_list(request):
    members = []
    if True:

        members_list = SalaryGroups.objects.all()


        
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)

        
    else:
        
        members_list = SalaryGroups.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)
    context = {
        'members': members
    }
    return render(request, 'hr/salaries.html', context)

@login_required()
def salaries_aoe(request):
    form = SalaryGroupForm()
    if request.method == 'POST':
        form = SalaryGroupForm(request.POST, request.FILES)
        if form.is_valid():
            member = form.save(commit=False)
            member.creator = request.user
            member.save()
            return redirect(reverse('salaries_list'))
    else:
       form = SalaryGroupForm()
    context = {'form': form}
    return render(request, 'hr/salaries_aoe.html', context)


@login_required()
def salaries_edit(request, pk):
    form = SalaryGroupForm()
    if request.method == 'POST':
        form = SalaryGroupForm(request.POST, request.FILES, instance=SalaryGroups.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse(salaries_list))
    else:
       form = SalaryGroupForm(instance=SalaryGroups.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'hr/salaries_aoe.html', context)



@login_required()
def salaries_details(request, pk):
    context = {
    }
    
    return render(request, 'hr/salaries_details.html')

