from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import CustomUser
from dashboard.models import SnapShot


# @receiver(post_save, sender=CustomUser)
# def update_snapshot_on_save(sender, instance, created, **kwargs):
#     if created:
#         snapshot = SnapShot.objects.get_or_create(id=1)
#         snapshot.users += 1
#         snapshot.save()


# @receiver(post_save, sender=CustomUser)
# def update_snapshot_on_delete(sender, instance, **kwargs):
#     snapshot = SnapShot.objects.get_or_create(id=1)
#     snapshot.save()


