from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import  CustomUser
from django.contrib.auth.models import Group 


from utils.mixins.export import ExportCsvMixin



class CustomUserAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = (
        'phone_number', 'username', 'email','group', 'first_name', 'last_name', 'date_joined', 'group', 'gender', 'is_staff', 'is_active', 'creator', 'date_created', 'date_modified', 'modified_by'
    )
 
    search_fields = (
        'phone_number','username', 'email', 'first_name', 'last_name', 'date_joined'
    )

    list_filter = [
        'phone_number', 'username', 'email', 'first_name', 'last_name', 'date_joined', 'group', 'gender', 'is_staff', 'is_active'
    ]

    ordering = [
        'phone_number', 'username', 'email', 'first_name', 'last_name', 'date_joined', 'group', 'gender', 'is_staff', 'is_active'
    ]
    icon_name = 'person'
    actions = ["export_as_csv"]

admin.site.register(CustomUser, CustomUserAdmin)


