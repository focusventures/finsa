from django.urls import path

from . import views



urlpatterns = [
     path('login', views.login, name="signup"),
     path('login', views.login, name="login" ),
     path('logout', views.user_logout, name="logout" ),
     path('confirm', views.confirm, name="confirm" ),
     path('reset', views.reset, name="reset" ),
     path('profile', views.profile, name="profile" ),
     path('password_reset', views.reset, name="password_reset" ),
]
