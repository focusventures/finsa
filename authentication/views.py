from django.shortcuts import render
from django.contrib.auth import authenticate,logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.contrib import messages


from authentication.models import CustomUser

from .forms import SignUpForm, ProfileForm,ChangePasswordForm, SystemUserCreationForm

def signup(request):
    if request.user.is_authenticated:
        return redirect('/')

    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(user.password)
            user.save()
        
            return redirect('/auth/login')
    else:
        form = SignUpForm()
    context = {'form': form}
    return render(request, 'authentication/signup.html', context)



def login(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    if request.method == "POST":
        phone = request.POST.get('phone_number')
        password = request.POST.get('password')
        

        auth = authenticate(phone_number=phone, password=password)


        if auth is not None:
            auth_login(request, auth)
            messages.success(request, 'Welcome Back')  # <-
            user = CustomUser.objects.get(phone_number=phone)

            if user.is_active:
                return redirect('/')
            else:
                messages.success(request, 'You do not have permission to access this time. Contact System Administrator')
                logout(request)
                return render(request, 'authentication/login.html', context)





            
        
        messages.success(request, 'Phone Number and Password provided did not match. Try again')


    return render(request, 'authentication/login.html', context)

from authentication.models import CustomUser

def reset(request):
    if request.user.is_authenticated:
        return redirect('/')

    if request.method == "POST":
        email = request.POST.get('email')

        try:
            user = CustomUser.objects.get(email__iexact=email)
        except CustomUser.DoesNotExist:

            messages.success(request, 'User Not Found. Try Again') 
   
    return render(request, 'authentication/reset.html')



def confirm(request):
    if request.user.is_authenticated:
        return redirect('/')
    context = {}
    return render(request, 'authentication/confirm.html', context)




from django.contrib.auth.decorators import login_required

@login_required()
def profile(request):
    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    form = ProfileForm(instance=request.user)
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('/auth/profile')
    else:
        form = ProfileForm(instance=request.user)
        password_form = ChangePasswordForm()
    context = {'form': form, 
    "password_form": password_form,  
    "form": form,
        "documents": "",
        "messages": "",
        "notifications": "",
        "activities": ""
        }


    return render(request, 'authentication/profile.html', context)

@login_required()
def change_password(request):
    context = {}
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        user = CustomUser.objects.get(phone_number__iexact=request.user)

        form = ChangePasswordForm(request.POST, request.FILES, instance=user)
        if form.is_valid():

            user.set_password(form.password)
            user.save()

    return redirect('/auth/profile')

@login_required()
def user_logout(request):
    logout(request)
    messages.success(request, 'Session Closed') 

    return redirect('/auth/login')