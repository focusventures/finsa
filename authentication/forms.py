from django import forms 
from authentication.models import CustomUser
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from crispy_forms.layout import (Layout, Fieldset, Field, Row, Column,
                                 ButtonHolder, Submit, Div)
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
# from .models import CustomUser


class ProfileForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=False)

    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=False)
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=False)
    
    email = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=75,
        required=False)
    phone_number = forms.RegexField(regex=r'^\+?1?\d{9,15}$',
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': 'phone number'}),
                                        help_text=(
                                        "Phone number must be entered in the format: ''. Up to 15 digits allowed."))


    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'middle_name', 'gender', 'county',
                  'email', 'phone_number', 'avatar']

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
               Row(
                Column('username', css_class='form-group col-md-6 mb-0'),
             
                css_class='form-row'
            ),
            Row(
                Column('phone_number', css_class='form-group col-md-6 mb-0', dissabled=True),
                Column('email', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Column('first_name', css_class='form-group col-md-4 mb-0'),
                Column('middle_name', css_class='form-group col-md-4 mb-0'),
                 Column('last_name', css_class='form-group col-md-4 mb-0'),
        
              
                css_class='form-row'
            ),
                 Row(
                Column('gender', css_class='form-group col-md-8 mb-0'),

        
              
                css_class='form-row'
            ),
              Row(
                Column('county', css_class='form-group col-md-8 mb-0'),

        
              
                css_class='form-row'
            ),
            Row(
                Column('avatar', css_class='form-group col-md-12 mb-0'),
        
              
                css_class='form-row'
            ),
            ButtonHolder(
                Submit('submit', 'Update',
                       css_class='float-right btn-warning mr-3')
            )
        )


class ChangePasswordForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput())
    old_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Old password",
        required=True)

    new_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="New password",
        required=True)
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Confirm new password",
        required=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'old_password', 'new_password', 'confirm_password']

    def clean(self):
        super(ChangePasswordForm, self).clean()
        old_password = self.cleaned_data.get('old_password')
        new_password = self.cleaned_data.get('new_password')
        confirm_password = self.cleaned_data.get('confirm_password')
        id = self.cleaned_data.get('id')
        user = CustomUser.objects.get(pk=id)
        if not user.check_password(old_password):
            self._errors['old_password'] = self.error_class([
                'Old password don\'t match'])
        if new_password and new_password != confirm_password:
            self._errors['new_password'] = self.error_class([
                'Passwords don\'t match'])
        return self.cleaned_data


    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)


        self.helper.layout = Layout(
               Row(
                Column('old_password', css_class='form-group col-md-12 mb-0'),
             
                css_class='form-row'
            ),
            Row(
                Column('new_password', css_class='form-group col-md-12 mb-0', dissabled=True),
              
                css_class='form-row'
            ),
             Row(
                Column('id', css_class='form-group col-md-12 mb-0', dissabled=True),
              
                css_class='form-row'
            ),
          
           
              Row(
                Column('confirm_password', css_class='form-group col-md-12 mb-0'),

        
              
                css_class='form-row'
            ),
          
            ButtonHolder(
                Submit('submit', 'Update',
                       css_class='float-right btn-dark mr-3')
            )
        )



class SignUpForm(forms.ModelForm):
    # phone_number = forms.CharField(
    #     widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'phone number'}),
    #     max_length=10,
    #     required=False)
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}), label='Username')
    phone_number = forms.RegexField(regex=r'^\d{10}$',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'phone number'}),
                                    help_text=(
                                        "Phone number must be entered in the format: '0721XXXXXX'."))
     # noqa: E261
    password = forms.CharField(
                                   widget=forms.PasswordInput(
                                       attrs={'class': 'form-control', 'placeholder': 'password'}))
    confirm_password = forms.CharField(
                                           widget=forms.PasswordInput(
                                               attrs={'class': 'form-control', 'placeholder': 'confirm password'}),
                                           label="Confirm your password",
                                           required=True)

    class Meta:
            model = CustomUser
            exclude = ['last_login', 'date_joined']
            fields = ['username', 'phone_number',  'password', 'confirm_password'  ]

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
               Row(
                Column('username', css_class='form-group col-md-12 mb-0'),
             
                css_class='form-row'
            ),
            Row(
                Column('phone_number', css_class='form-group col-md-12 mb-0', dissabled=True),
              
                css_class='form-row'
            ),
          
                 Row(
                Column('password', css_class='form-group col-md-12 mb-0'),

        
              
                css_class='form-row'
            ),
              Row(
                Column('confirm_password', css_class='form-group col-md-12 mb-0'),

        
              
                css_class='form-row'
            ),
          
            ButtonHolder(
                Submit('submit', 'Sign Up',
                       css_class='float-right btn-dark mr-3')
            )
        )




    def clean(self):
            super(SignUpForm, self).clean()
            password = self.cleaned_data.get('password')
            confirm_password = self.cleaned_data.get('confirm_password')
            if password and password != confirm_password:
                self._errors['password'] = self.error_class(
                    ['Passwords don\'t match'])
            return self.cleaned_data




class CustomUserCreationForm(UserCreationForm):
        """
        A form that creates a user, with no privileges, from the given email and
        password.
        """

        def __init__(self, *args, **kargs):
            super(CustomUserCreationForm, self).__init__(*args, **kargs)
            del self.fields['phone_number']

        class Meta:
            model = CustomUser
            fields = ("phone_number",)

class CustomUserChangeForm(UserChangeForm):
        """A form for updating users. Includes all the fields on
        the user, but replaces the password field with admin's
        password hash display field.
        """

        def __init__(self, *args, **kargs):
            super(CustomUserChangeForm, self).__init__(*args, **kargs)
            del self.fields['phone_number']

        class Meta:
            model = CustomUser
            exclude = ('is_staff',)



class SystemUserCreationForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}), label='Username')
    phone_number = forms.RegexField(regex=r'^\d{10}$',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'phone number'}),
                                    help_text=(
                                        "Phone number must be entered in the format: '0721XXXXXX'."))
    password = forms.CharField(
                                   widget=forms.PasswordInput(
                                       attrs={'class': 'form-control', 'placeholder': 'password'}))
    confirm_password = forms.CharField(
                                           widget=forms.PasswordInput(
                                               attrs={'class': 'form-control', 'placeholder': 'confirm password'}),
                                           label="Confirm your password",
                                           required=True)

    class Meta:
            model = CustomUser
            exclude = ['last_login', 'date_joined']
            fields = ['username', 'phone_number',  'password', 'confirm_password', 'branch', 'group', 'gender'  ]

    def __init__(self, *args, **kwargs):
        super(SystemUserCreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
               Row(
                Column('username', css_class='form-group col-md-6 mb-0'),
                Column('gender', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('phone_number', css_class='form-group col-md-12 mb-0', dissabled=True),
              
                css_class='form-row'
            ),
              Row(
                Column('branch', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
              Row(
                Column('group', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
          
                 Row(
                Column('password', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
              Row(
                Column('confirm_password', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
          
            ButtonHolder(
                Submit('submit', 'Sign Up',
                       css_class='float-right btn-dark mr-3')
            )
        )




    def clean(self):
            super(SystemUserCreationForm, self).clean()
            password = self.cleaned_data.get('password')
            confirm_password = self.cleaned_data.get('confirm_password')
            if password and password != confirm_password:
                self._errors['password'] = self.error_class(
                    ['Passwords don\'t match'])
            return self.cleaned_data


class EditSystemUserCreationForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}), label='Username')
    phone_number = forms.RegexField(regex=r'^\d{10}$',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'phone number'}),
                                    help_text=(
                                        "Phone number must be entered in the format: '0721XXXXXX'."))

    class Meta:
            model = CustomUser
            exclude = ['last_login', 'date_joined']
            fields = ['username', 'phone_number',   'branch', 'group', 'gender'  ]

    def __init__(self, *args, **kwargs):
        super(EditSystemUserCreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
               Row(
                Column('username', css_class='form-group col-md-6 mb-0'),
                Column('gender', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('phone_number', css_class='form-group col-md-12 mb-0', dissabled=True),
              
                css_class='form-row'
            ),
              Row(
                Column('branch', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
              Row(
                Column('group', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
          
           
          
            ButtonHolder(
                Submit('submit', 'Edit',
                       css_class='float-right btn-dark mr-3')
            )
        )





