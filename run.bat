@echo off

echo "Changing directory..."
:: =========================================
::    Edit your django project directory
:: =========================================
cd D:\sacco\finsa_test

call ..\env\scripts\activate.bat

echo Now Server will be run at port 80 by default
:: =========================================
::		Run command (You can change it)
:: if you change this part, should be change
::			openBrowser.bat File
:: =========================================
python manage.py runserver finsa:80