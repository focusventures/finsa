from django.apps import AppConfig


class FosaApiConfig(AppConfig):
    name = 'fosa_api'
