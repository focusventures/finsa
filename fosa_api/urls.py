from django.urls import path, include
from authentication.models import CustomUser
from rest_framework import routers, serializers, viewsets
from .viewsets.users import UserViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)




urlpatterns = [
    path('', include(router.urls)),
]
