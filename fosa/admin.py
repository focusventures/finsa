from django.contrib import admin

from fosa.models import Slips, Sessions, GroupSlips


admin.site.register(Slips)
admin.site.register(GroupSlips)
admin.site.register(Sessions)
