from django.urls import path

from . import views
from . import indi_views


urlpatterns = [
    path('', views.fosa, name="fosa"),
    path('search', views.search, name="fosa_search"),
    path('home', views.index, name="fosa_index"),

    path('members/transact/<int:pk>', indi_views.member_payments, name="member_transaction"),
    path('slip/<int:pk>', views.member_end_transactions, name="member_end_transaction"),
    path('members/history/<int:pk>', views.member_transaction_history, name="member_transaction_history"),
    path('member/statement/<int:pk>', views.member_statement, name="member_statement"),
    path('member/profile/<int:pk>', views.member_profile, name="member_profile"),
    
    path('member/transact/slips/<int:pk>', indi_views.member_payments, name="fosa_member_payments"),
    path('member/slips/<int:pk>', views.view_member_slips, name="one_member_slips"),
    path('member/accounts/<int:pk>', views.accounts, name="accounts"),
    path('member/accounts/details/<int:pk>', views.accounts_details, name="accounts_details"),
    path('member/disburse_loans/<int:pk>', views.disburse_member_loans, name="disburse_member_loans"),


    path('members/loans/<int:pk>', views.loans, name="member_loans"),
    path('member/loans/payment/<int:pk>', views.member_loan, name="fosa_loan_processing"),
    path('member/loans/slip/<int:pk>', views.loan_slip, name="member_loan_slips"),
    path('member/loans/edit_loan/<int:pk>', views.edit_loan_form, name="edit_loan"),

    path('member/loans/refinancing/<int:pk>', views.refinancing, name="refinancing"),
    path('member/loans/wave_all_interest/<int:pk>', views.wave_all_interest, name="wave_all_interest"),
    path('member/loans/wave_all_penalty/<int:pk>', views.wave_all_penalty, name="wave_all_penalty"),
    path('member/loans/wave_interest/<int:pk>', views.wave_interest, name="wave_interest"),
    path('member/loans/wave_penalty/<int:pk>', views.wave_penalty, name="wave_penalty"),

    # path('member/loans/application/<int:pk>', views.loan_form, name="loan_form"),
    # path('member/loans/individuals/pledges/<int:pk>', views.loan_individual_pledges, name="loan_individual_pledges_pos"),
    # path('member/loans/application/guarantors/group/<int:pk>', views.loan_group_guarantors, name="loan_guarantors_pos"),
    # path('member/loans/application/guarantors/external/<int:pk>', views.loan_external_guarantors, name="loan_external_guarantors_pos"),
    # path('member/loans/application/guarantors/external/pledges/<int:pk>', views.loan_external_guarantors_pledges, name="loan_external_guarantors_pledges_pos"),
    # path('member/loans/application/details/<int:pk>', views.loan_details_pos, name="loan_details_pos"),
    # path('member/loans/application/guarantors/lock/<int:pk>', views.loan_guarantors_lock, name="loan_guarantors_lock_pos"),
    # path('member/loans/details/<int:pk>', views.loan_details, name="loan_details"),
    # path('member/loans/slip/<int:pk>', views.loan_slip, name="loan_details"),


    path('application/<int:pk>', views.loan_form, name="loan_form"),
    path('individuals/pledges/<int:pk>', views.loan_individual_pledges, name="loan_individual_pledges_pos"),
    path('guarantors/group/<int:pk>', views.loan_group_guarantors, name="loan_guarantors_pos"),
    path('guarantors/external/<int:pk>', views.loan_external_guarantors, name="loan_external_guarantors_pos"),
    path('guarantors/external/pledges/<int:pk>', views.loan_external_guarantors_pledges, name="loan_external_guarantors_pledges_pos"),
    path('loans/details/<int:pk>', views.loan_details_pos, name="loan_details_pos"),
    path('application/guarantors/lock/<int:pk>', views.loan_guarantors_lock, name="loan_guarantors_lock_pos"),

    path('members/transfer/<int:pk>', views.member_transfer, name="member_transfer"),
    path('member/transfer/processing/<int:pk>', views.member_transfer, name="fosa_transfer_processing"),
    path('member/transfer/slip/<int:pk>', views.transfer_slip, name="member_transfer_slips"),


    path('members/cheque/<int:pk>', views.member_cheque, name="member_cheque"),
    path('member/cheque/processing/<int:pk>', views.member_cheque, name="fosa_cheque_processing"),
    path('member/cheque/slip/<int:pk>', views.cheque_slip, name="member_cheque_slip"),



    path('groups/transact/<int:pk>', views.group_transactions, name="group_transaction"),
    path('groups/history/<int:pk>', views.group_transaction_history, name="group_transaction_history"),
    path('groups/statement/<int:pk>', views.group_statement, name="group_statement"),
    path('group_slip/<int:pk>', views.group_end_transactions, name="group_end_transaction"),
    path('group_slip/disburse_loans/<int:pk>', views.disburse_loans, name="disburse_loans"),
    path('groups/accounts/<int:pk>', views.grouP_accounts, name="groups_accounts"),
    path('groups/accounts/details/<int:pk>', views.grouP_accounts_details, name="groups_accounts_details"),

    
    path('group_transaction/<int:pk>', views.group_payment, name='group_payment'),
    path('group_slips/<int:pk>', views.view_group_slips, name='group_slips'),
    path('group_slips/edit/<int:pk>', views.edit_group_slip, name='group_slip_edit'),

    path('slip/disburse/<int:pk>', views.disburse_to_members, name='disburse_to_members'),
    path('slip/members/<int:pk>', views.members_slips, name='members_slips'),
    path('slips', views.all_slips, name='all_slips'),

    path('savings', views.index, name="savings_transactions"),


    path('loans', views.index, name="loans_transactions"),


    path('shares', views.index, name="shares_transactions"),

    path('payments/list', views.expenses_list, name="fosa_expenses_list"),
    path('payments/add', views.expenses, name="fosa_expenses"),
    path('payments/process', views.process_expenses, name="process_expenses"),
    path('payments/details/<int:pk>', views.expenses_detail, name="expense_detail"),
    path('payments/edit/<int:pk>', views.expenses_edit, name="expense_edit"),
    path('payments/search', views.search_expenses, name="search_expenses"),
    


    path('slips', views.slips, name="slips"),
    path('slips/search', views.search_slips, name="search_slips"),
    path('disbursement_report/<int:pk>', views.disburse_report, name="disburse_report"),

    path('status', views.status_request, name="status"),
    path('ac', views.acc_numbers, name="ac"),


    path('calculator', views.calculator, name="calculator"),
    path('event_reassign', views.event_reassign, name="event_reassign"),

    path('trans', views.trans, name="trans"),
    path('dr_cr', views.dr_cr, name="dr_cr"),



    path('revenues/list', views.revenue_list, name="fosa_revenues_list"),
    path('revenues/add', views.revenue, name="fosa_revenues"),
    path('revenues/process', views.process_revenue, name="process_revenues"),
    path('revenues/details/<int:pk>', views.revenue_detail, name="revenues_detail"),
    path('revenues/edit/<int:pk>', views.revenue_edit, name="revenue_edit"),
    path('revenues/search', views.search_revenue, name="search_revenues"),

    path('adjust', views.adjust_coa_events, name="adjust_account_balances"),

    path('month_year', views.month_year),

    path('recovery/loc/debit', views.recovery, name="recovery"),
    path('recovery/loc/credit', views.recovery_debit_loc, name="recovery_debit_loc"),

    path('redeem', views.redeem, name="redeem"),

    path('cash_from_bank', views.cash_from_bank, name="cash_from_bank_pos"),
    path('banking', views.bankingloc, name="banking_pos"),
    path('all_accounts', views.all_accounts, name="all_accounts"),
    path('posting', views.posting, name="posting"),


    path('dividend', views.dividend, name="dividend"),

    path('dividend_distribution', views.dividend_distribution, name="dividend_distribution"),
    path('petty_cash', views.petty_cash, name="petty_cash"),


    path('update_loans', views.update_loans),

    

]