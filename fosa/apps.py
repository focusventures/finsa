from django.apps import AppConfig


class FosaConfig(AppConfig):
    name = 'fosa'
