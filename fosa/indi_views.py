from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from django.contrib import messages
from members.models import Member
from financial.forms import TransactionsForm
import datetime
import decimal 

from financial.models import Transactions
from parameters.models import Month, Periods
from fosa.models import Slips
import datetime

from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from django.db.models import Q

from authentication.models import CustomUser
from members.models import MemberGroup, Member
from ostructure.models import Branch
from django.views.decorators.csrf import csrf_exempt

from financial.models import Transactions, GroupTransactions, GroupCustomAccount, GroupTransactions, MemberCustomAccount
from financial.forms import TransactionsForm, FosaTransactionsForm, ModelTransactionForm
from fosa.forms import GroupSubmitForm
from fosa.models import Slips, Sessions, GroupSlips

from financial.forms import ExpensesForm, ExpensesEditForm
from financial.models import Expense

from parameters.models import LOC, LOCEvent
from chartsoa.models import PaymentMethod, Account, AccountEvent

from django.forms import modelformset_factory

from dateutil.relativedelta import *

from decimal import Decimal

from parameters.models import Events


@login_required()
def member_payments(request, pk):
    member = Member.objects.get(id=pk)
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    if request.method == "POST":
        receipt = request.POST.get('receipt')
        cheque = request.POST.get('cheque')
        date =  request.POST.get('date')
        method = request.POST.get('method')

        TransactionsModelFormset = modelformset_factory(
            Transactions,
            fields=('amount','type', ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            slip = Slips()
            slip.creator = request.user
            slip.date_created = datetime.datetime.now()
            slip.member = member
            slip.save()
            slip.number = receipt
            slip.cheque_number = cheque
            slip.save()

            events_types = []
            for form in formset:
                if form.cleaned_data.get('amount'):
                    amount = form.cleaned_data.get('amount')
                    event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()

                    method = request.POST.get('method')
                    payment_method = PaymentMethod.objects.filter(name=method).select_related('account', 'loc').first()

                    if event.can_create_accounts:
                        try:
                            account = MemberCustomAccount.objects.get(Q(acc_type=event) & Q(member=member))

                        except MemberCustomAccount.DoesNotExist:
                            account = MemberCustomAccount()
                            account.acc_type = event
                            account.member = member
                            account.save()

                        account.creator = request.user
                        account.date_created = datetime.datetime.now()
                        account.save()

                        #update member account
                        account.acc_balance = account.acc_balance + Decimal(amount)
                        account.save()


                    #create Transaction dr
                    archive = Transactions(
                        financial_month = month,
                        financial_year = year,
                        type = event,
                        # action = event,
                        payment_method = payment_method,
                        slip = slip,
                        description = event.title,
                        cheque = cheque,
                        member = member,
                        date = date ,
                        processed = request.user,
                        dr_amount = amount,
                        amount = amount ,
                        actual_amount = amount,
                        creator = request.user,
                        date_created = datetime.datetime.now()
                    )
                    archive.save()

                    loc = LOC.objects.filter(id=payment_method.loc.id).select_related('type').first()
                    loc.remainder_amount = loc.remainder_amount + decimal.Decimal(archive.amount)
                    loc.save()

 
                    #Create Loc Event
                    loc_event = LOCEvent(
                        financial_month = month,
                        financial_year = year,
                        loc_account = payment_method.name,
                        description = event.title,
                        loc = loc,
                        payment_method = payment_method,
                        transaction = archive,
                        amount = archive.amount,
                        remainder = loc.remainder_amount,
                        type = "Dr",
                        slip = slip,
                        date = archive.date,
                        date_created = archive.date_created,
                        creator = request.user,
                        date_modified = archive.date_modified,
                        modified_by = request.user
                    )
                    loc_event.save()


                    #Update Loc Event COA OR Payment Method LOC
                    loc_event_coa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=loc.type)).first()
                    loc_event_coa.balance = loc_event_coa.balance + decimal.Decimal(archive.amount)
                    loc_event_coa.save()



                    coa_event = AccountEvent(
                        account = loc_event_coa,
                        transaction = archive,
                        amount = archive.amount,
                        coa_balance = loc_event_coa.balance,
                        month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                        financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                        date_created = datetime.datetime.now(),
                        creator = request.user
                    )
                    coa_event.save()




#UPDATE EVENT LOC
                    #create Transaction dr
                    a = Transactions(
                        financial_month = month,
                        financial_year = year,
                        type = event,
                        # action = event,
                        payment_method = payment_method,
                        slip = slip,
                        description = event.title,
                        cheque = cheque,
                        member = member,
                        date = date ,
                        processed = request.user,
                        cr_amount = amount,
                        amount = amount ,
                        actual_amount = amount,
                        creator = request.user,
                        date_created = datetime.datetime.now()
                    )
                    a.save()

                    #Update Loc Event COA OR Payment Method LOC
                    event_coa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                    event_coa.balance = event_coa.balance + decimal.Decimal(a.amount)
                    event_coa.save()



                    coa_event = AccountEvent(
                        account = event_coa,
                        transaction = a,
                        amount = archive.amount,
                        coa_balance = event_coa.balance,
                        month = month,
                        financial_year = year,
                        date_created = datetime.datetime.now(),
                        creator = request.user
                    )
                    coa_event.save()



                    slip.total = decimal.Decimal(slip.total) + decimal.Decimal(archive.amount)
                    slip.save()

                    if event.secondary_event is not None:
                        # if event is repayment
                        secondary_event = Events.objects.get(id=event.secondary_event.id)
                        chart_oa_secondary = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=secondary_event)).first()
                        chart_oa_secondary.balance -= decimal.Decimal(archive.amount)
                        chart_oa_secondary.save()

                        coa_event_sec = AccountEvent(
                            account = chart_oa_secondary,
                            transaction = archive,
                            amount = archive.amount,
                            coa_balance = chart_oa_secondary.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )
                        coa_event_sec.save()


                


            return redirect(reverse('member_end_transaction', args=(slip.id,)))

    TransactionsModelFormset = modelformset_factory(
        Transactions,
        fields=('amount','type', ),
        extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none())
    for form in formset:
        form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')


    context = {
        "member": member,
        "transaction_form": FosaTransactionsForm(),
        "formset": formset,
        "payments_methods": PaymentMethod.objects.all()
    }
    return render(request, 'fosa/member_transaction.html', context)

