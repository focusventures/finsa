from django.db import models
from django.utils.translation import ugettext_lazy as _



class Sessions(models.Model):
    user = models.ForeignKey("authentication.CustomUser", verbose_name=_("User"), on_delete=models.DO_NOTHING)
    active = models.BooleanField(_("Sifnifies Whther the Session is active"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)



class Slips(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    number = models.CharField(_("Slip Number"), max_length=50)
    total = models.DecimalField(_("Total Amount Transacted "), max_digits=10, decimal_places=2, default=0.00)
    sub_total = models.DecimalField(_("Sub Total Amount Transacted "), max_digits=5, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING, blank=True, null=True)
    group_slip = models.ForeignKey("fosa.GroupSlips", verbose_name=_("Group Slip"),null=True, blank=True, on_delete=models.DO_NOTHING)
    date = models.DateField(_("Date"), auto_now=False, auto_now_add=False, null=True, blank=True)
    tax = models.DecimalField(_("Total Tax"), max_digits=5, decimal_places=2, default=0.00)
    fees = models.DecimalField(_("Total Fees"), max_digits=5, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    cheque_number = models.CharField(_("Cheque NO"), max_length=50, null=True, blank=True)
    settling = models.BooleanField(_("Disbursed"),default=False)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


    class Meta:
        verbose_name = "Slip"
        verbose_name_plural = 'Slip'

    def __str__(self):
        return "%s" % (self.number)

    def generate_slip_number(self, id, mode):
        import datetime
        import random

        return "{}{}#{}".format(mode, id, datetime.datetime.now())


class GroupSlips(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    number = models.CharField(_("Slip Number"), max_length=50)
    total = models.DecimalField(_("Total Amount Transacted "), max_digits=10, decimal_places=2, default=0.00)
    sub_total = models.DecimalField(_("Sub Total Amount Transacted "), max_digits=10, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING, blank=True, null=True)
    tax = models.DecimalField(_("Total Tax"), max_digits=5, decimal_places=2, default=0.00)
    fees = models.DecimalField(_("Total Fees"), max_digits=5, decimal_places=2, default=0.00)
    date = models.DateField(_("Date"), auto_now=False, auto_now_add=False, null=True, blank=True)
    cheque_number = models.CharField(_("Cheque NO"), max_length=50, null=True, blank=True)
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("Member Group"), blank=True, null=True, on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    disbursed = models.BooleanField(_("Disbursed"),default=False)
    disbursed_partial = models.BooleanField(_("Partially Disbursed"),default=False, help_text="Events with event type of settling are not yet processed")
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


    class Meta:
        verbose_name = "Group Slip"
        verbose_name_plural = 'Group Slip'

    def __str__(self):
        return "%s" % (self.number)

    def generate_slip_number(self, id, mode):
        import datetime
        import random

        return "{}#{}-{}".format(self.id, id, mode)



class Recovery(models.Model):
    amount = models.DecimalField(_("Amount Recovered"), max_digits=10, decimal_places=2)
    event = models.ForeignKey("parameters.Events", verbose_name=_("Event"), on_delete=models.DO_NOTHING)
    method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING)
    date = models.DateField(_("Date Of Transaction"), auto_now=False, auto_now_add=False, null=True)


scheme_rates = (
    ("flat", "Flat(Fixed Pricinpal, Fixed Interest)"),
    ("declining", "Declining Principal"),
)


class Calculator(models.Model):
    payment_period = models.IntegerField(_("Payment period in Months"), default=0)
    loan_product = models.ForeignKey("loans.LoanProduct", verbose_name=_("Loan Product"), on_delete=models.DO_NOTHING)
    amount = models.DecimalField(_("Amount Requested"), max_digits=20, decimal_places=2)
    application_month = models.DateField(_("Disbursement Date"), auto_now=False, auto_now_add=False)
    # interest_scheme = models.CharField(_("Interest Scheme"), choices=scheme_rates, max_length=50)