
from django import forms
from django.forms import ModelForm, Form
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button, HTML



from members.models import Member

from financial.forms import SimpleTransactionsForm
from financial.models import Transactions

from loans.models import *


from parameters.models import Events
from .models import *
from members.forms import MemberForm
from crispy_forms.layout import (Layout, Fieldset, Field,Row, Column, Button,
                                 ButtonHolder, Submit, Div)




class GroupSubmitForm(ModelForm):
    # serial = forms.CharField(forms.TextInput(attrs={'autocomplete':'off',}))
    # serial.label = ''

    class Meta:
        model = Transactions
        fields = ['type', 'description', 'amount']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(GroupSubmitForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.fields['description'].requ
        # ired = False
        self.helper.layout = Layout(
            HTML('<button class="btn btn-primary">Add</button>'),
            Row(
                Column('type', css_class='form-group col-md-3 mb-0'),
                Column('description', css_class='form-group col-md-3 mb-0'),
                Column('amount', css_class='form-group col-md-3 mb-0'),
                # Column(PrependedText('serial', 'Serial #', '<button class="btn btn-primary">Submit</button>', css_class='input-xlarge'), css_class='span1'),
                css_class='form-row'
            ),
            Row(
                Submit('submit', 'Process', css_class="btn btn-success btn-block btn-md m-2"),
                css_class='form-row'
            )
         
            
        )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "receiving_member":
            kwargs["queryset"] = Member.objects.filter(name__in=['God', 'Demi God'])
        return super().formfield_for_foreignkey(db_field, request, **kwargs)





# class CalculatorForm(Form):
#     loan_type = forms.CharField(forms.TextInput())
#     amount = forms.CharField(forms.TextInput())

#     duration = forms.CharField(forms.TextInput())


#     def __init__(self, *args, **kwargs):
#         super(CalculatorForm, self).__init__(*args, **kwargs)
#         self.helper = FormHelper(self)

#         self.helper.layout = Layout(
         
#             Row(
#                 Column('loan_type', css_class='form-group col-md-3 mb-0'),
#                 Column('amount', css_class='form-group col-md-3 mb-0'),
#                 Column('duration', css_class='form-group col-md-3 mb-0'),
             
#                 css_class='form-row'
#             ),
#             Row(
#                 Submit('submit', 'Calculate', css_class="btn btn-success btn-block btn-md m-2"),
#                 css_class='form-row'
#             )
         
            
#         )









class LoanGuarantorsForm(ModelForm):
    class Meta:
        model = LoanGuarantors
        
        fields = ['sex', 'first_name', 'middle_name','last_name',
       'phone_number', 'id_number',
        'pledges', 
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(LoanGuarantorsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Extended Loan Guarantor Form',
                    Row(
                        Column('first_name', css_class='form-group col-md-2 mb-0'),
                        Column('middle_name', css_class='form-group col-md-2 mb-0'),
                        Column('last_name', css_class='form-group col-md-2 mb-0'),
                        Column('sex', css_class='form-group col-md-2 mb-0'),
                        Column('phone_number', css_class='form-group col-md-2 mb-0'),
                        Column('id_number', css_class='form-group col-md-2 mb-0'),
                        css_class='form-row '
                        ),
                  
                    ),
           
            ),
            ButtonHolder(
                Submit('submit', 'Submit Request',
                       css_class='float-right btn-warning mr-3')
            )

        )




class GuarantorsPledgeForm(ModelForm):
    class Meta:
        model = GuarantorsPledge
        
        fields = ['count', 'item_title', 'description',
        'serial_number','estimated_value',
       
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, request, pk, *args, **kwargs):
        super(GuarantorsPledgeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(

            Row(
                    Column('count', css_class='form-group col-md-3 mb-0'),
                    Column('item_title', css_class='form-group col-md-3 mb-0'),
                    Column('description', css_class='form-group col-md-3 mb-0'),
             

                    css_class='form-row'
                ),

           Row(
                    Column('year', css_class='form-group col-md-3 mb-0'),
                    Column('serial_number', css_class='form-group col-md-3 mb-0'),
                    Column('estimated_value', css_class='form-group col-md-3 mb-0'),

                    css_class='form-row'
                ),
           
            ButtonHolder(
                Submit('submit', 'Submit',
                       css_class='float-right btn-warning mr-3')
            )

        )


from .models import Recovery

class RecoveryForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Recovery
        
        fields = ['amount', 'event', 'method', 'date'
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(RecoveryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['event'].queryset = Events.objects.all().order_by('title')
        self.helper.layout = Layout(
            Row(
            Column('method', css_class='form-group col-md-6 mb-0'),
            Column('event', css_class='form-group col-md-6 mb-0'),
            css_class='form-row '
            ),

            Row(
                Column('date', css_class='form-group col-md-6 mb-0'),
                Column('amount', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

            ButtonHolder(
                Submit('submit', 'Submit',
                       css_class='float-right btn-warning mr-3')
            )

        )

from .models import Calculator 
class LoanCalculatorForm(ModelForm):
    application_month = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))
    class Meta:
        model = Calculator
        
        fields = ['payment_period', 'loan_product', 'amount','application_month'
        ]


    def __init__(self, *args, **kwargs):
        super(LoanCalculatorForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            # Row(
            # Column('loan_product', css_class='form-group col-md-12 mb-0'),
            # # Column('interest_scheme', css_class='form-group col-md-6 mb-0'),
            # css_class='form-row '
            # ),

            Row(
                Column('loan_product', css_class='form-group col-md-6 mb-0'),
                Column('payment_period', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),


            Row(
                Column('application_month', css_class='form-group col-md-6 mb-0'),
                Column('amount', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

            ButtonHolder(
                Submit('submit', 'Calculate',
                       css_class='float-right btn-block btn-warning ')
            )

        )

