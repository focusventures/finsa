from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from django.db.models import Q

from authentication.models import CustomUser
from members.models import MemberGroup, Member
from ostructure.models import Branch
from django.views.decorators.csrf import csrf_exempt

from financial.models import Transactions, GroupTransactions, GroupCustomAccount, GroupTransactions, MemberCustomAccount
from financial.forms import TransactionsForm, FosaTransactionsForm, ModelTransactionForm
from fosa.forms import GroupSubmitForm
from fosa.models import Slips, Sessions, GroupSlips

from utils.services.savings_procedure import SavingsProcedure
from financial.forms import ExpensesForm, ExpensesEditForm
from financial.models import Expense
from parameters.models import LOC, LOCEvent

from chartsoa.models import PaymentMethod

from django.forms import modelformset_factory


from dateutil.relativedelta import *

def generate_slip_number(slip, group, mode, number):
    return "{}{}#{}#{}".format(mode,group, slip, number)

@login_required
def fosa(request):
    return render(request, 'fosa/index.html')


@login_required
def index(request):
    return render(request, 'fosa/index.html')

@login_required
def expenses_detail(request, pk):
    expenses = Expense.objects.filter(id=pk)
    loc_events = LOCEvent.objects.filter(transaction__expense__id=expenses.first().id)
    locs = [event.transaction.id for event in loc_events]
    context = {
        'expense': expenses.first(),
        'transactions': Transactions.objects.filter(id__in=locs).select_related('type')
    }
    return render(request, 'fosa/expenses_detail.html', context)


@login_required
def expenses_edit(request, pk):
    expenses = Expense.objects.filter(id=pk)

    TransactionsModelFormset = modelformset_factory(
    Transactions,
    fields=('amount','description', ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.filter(expense=expenses.first()))
    

    context = {
        'expense': ExpensesEditForm(instance=expenses.first()),
        'e':  Expense.objects.filter(id=expenses.first().id).first(),
        'formset': formset
    }
    return render(request, 'fosa/expenses_edit.html', context)



@login_required
def expenses_list(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    month = Month.objects.get(id=month.id)
    year = Periods.objects.get(id=year.id)

    context = {
        'expenses': Expense.objects.all().order_by('pv_number'),
    }
    return render(request, 'fosa/expenses_list.html', context)


@login_required
def search_expenses(request):

    if request.method == 'POST':
        term = request.POST.get('search_term')
        context = {
            'expenses': Expense.objects.filter(Q(paid_to__icontains=term)),
        }
        return render(request, 'fosa/expenses_list.html', context)

@login_required
def expenses(request):
    p = Periods.objects.get(id=request.parameters["settings"]["financial_period"]) 
    print(p)

    if request.method == 'GET':
        TransactionsModelFormset = modelformset_factory(
        Transactions,
        fields=('amount','description', 'type'),
        extra=1
        )
        formset = TransactionsModelFormset(queryset=Transactions.objects.none())

        form = ExpensesForm()

        for form in formset:
            # form.fields['type'].queryset = Events.objects.filter(Q(type='expense') | Q(type="settling")).order_by('title')
            form.fields['type'].queryset = Events.objects.all().order_by('title')

        
        context = {
            'expense_form': form,
            "formset": formset,
            "payments_methods": PaymentMethod.objects.all()
        }
        return render(request, 'fosa/expenses.html', context)


import decimal

@login_required
def process_expenses(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    p = Periods.objects.get(id=request.parameters["settings"]["financial_period"]) 
    TransactionsModelFormset = modelformset_factory(
    Transactions,
    fields=('amount','description', 'type' ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none(),)
    form = ExpensesForm(prefix="sa")
    if request.method == 'POST':
        # event = Events.objects.get(code='expense')
        paid_to = request.POST.get('sa-paid_to')
        cheque_number = request.POST.get('sa-cheque_number')
        date = request.POST.get('sa-date')
        pv = request.POST.get('sa-pv_number')
        method = request.POST.get('method')

        expense = ExpensesForm(request.POST, prefix="sa")
        expense = Expense()
        # expense = expense.save(commit=False)
        expense.paid_to = paid_to
        expense.cheque_number = cheque_number
        expense.date = date
        expense.pv_number = pv
        expense.total = 0.00
        expense.date_created = datetime.datetime.now()
        expense.creator = request.user
        expense.save()

        payment_m = PaymentMethod.objects.filter(id=method).select_related('account', 'loc').first()

        if payment_m.is_cash:
            formset = TransactionsModelFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    if form.cleaned_data.get('amount'):
                        event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()
                        transaction = form.save(commit=False)
                        # transaction.type = event
                        transaction.date = expense.date
                        transaction.expense = expense
                        # transaction.acc = ''
                        transaction.financial_month = month
                        transaction.financial_period = p
                        transaction.creator = request.user
                        transaction.date_created = expense.date
                        transaction.actual_amount = transaction.amount
                        
                        # expense.total +=  decimal.Decimal(transaction.amount)
                        expense.total = decimal.Decimal(expense.total) + decimal.Decimal(transaction.amount)
                        expense.save()

                        if event.classication == 'asset' and event.is_expense:
                            transaction.dr_amount = transaction.amount
                            transaction.save()

                            event_coa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                            event_coa.balance -= decimal.Decimal(transaction.amount)
                            event_coa.save()

                        elif event.classication == 'liability' and event.is_income:
                            transaction.dr_amount = transaction.amount
                            transaction.save()

                            event_coa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                            event_coa.balance -= decimal.Decimal(transaction.amount)
                            event_coa.save()

                  

                        event_coa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                        event_coa.balance += decimal.Decimal(transaction.amount)
                        event_coa.save()

                        transaction.coa = event_coa

                        transaction.save()

                        loc = LOC.objects.get(id=payment_m.loc.id)
                        loc.remainder_amount -= Decimal(transaction.amount)
                        loc.save()



                        coa_event = AccountEvent(
                            account = event_coa,
                            transaction = transaction,
                            amount = transaction.amount,
                            coa_balance = event_coa.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )
                        coa_event.save()





                        g_t = Transactions()
                        g_t.financial_month = month
                        g_t.financial_period = year
                        g_t.financial_year = p
                        g_t.double_entry = 'Cr'
                        g_t.type = event
                        g_t.action = event
                        g_t.description = event.title
                        g_t.date = expense.date
                        g_t.expense = expense
                        g_t.processed = request.user 
                        g_t.amount = transaction.amount
                        g_t.cr_amount = transaction.amount
                        g_t.payment_method = payment_m
                        g_t.actual_amount = transaction.amount
                        g_t.expense = expense
                        g_t.creator = request.user
                        g_t.date_created =  expense.date
                        g_t.save()

                        loc_event = LOCEvent()
                        loc_event.loc = loc
                        loc_event.financial_month = month
                        loc_event.financial_year = p
                        loc_event.transaction = g_t
                        loc_event.description = g_t.description
                        loc_event.amount = transaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.type = 'Cr'
                        loc_event.creator = request.user
                        loc_event.is_payment = True
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()



                        chart_oa = Account.objects.filter(id=payment_m.account.id).first()
                        chart_oa.balance -= decimal.Decimal(transaction.amount)
                        chart_oa.save()


                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = g_t,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()


                        if event.secondary_event is None:
                            pass
                        else:
                            secondary_event = Events.objects.filter(id=event.secondary_event.id)


                            chart_oa_secondary = Account.objects.filter(Q(transaction_type=secondary_event) & Q(financial_year=year) & Q(financial_month=month)).first()
                            chart_oa_secondary.balance += decimal.Decimal(transaction.amount)
                            chart_oa_secondary.save()


                            coa_event_sec = AccountEvent(
                                account = chart_oa_secondary,
                                transaction = g_t,
                                amount = transaction.amount,
                                coa_balance = chart_oa_secondary.balance,
                                month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                                financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                                date_created = datetime.datetime.now(),
                                creator = request.user
                            )

                            coa_event_sec.save()



        if payment_m.is_bank:
            formset = TransactionsModelFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    if form.cleaned_data.get('amount'):
                        event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()
                        transaction = form.save(commit=False)
                
                        # transaction.type = event
                        transaction.date = expense.date
                        transaction.expense = expense
                    
                        # transaction.acc = ''
                        transaction.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                        transaction.financial_period = p
                        transaction.creator = request.user
                        transaction.date_created = expense.date
                        transaction.actual_amount = transaction.amount
                        transaction.dr_amount = transaction.amount
                        transaction.save()

                        if event.is_reducing:
                            chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                            chart_oa.balance -= decimal.Decimal(transaction.amount)
                            chart_oa.save()
                        else:

                            chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                            chart_oa.balance += decimal.Decimal(transaction.amount)
                            chart_oa.save()

                        transaction.coa = chart_oa
                        transaction.save()
                        
                        # expense.total +=  decimal.Decimal(transaction.amount)
                        expense.total = decimal.Decimal(expense.total) + decimal.Decimal(transaction.amount)
                        expense.save()

                        loc = LOC.objects.get(id=payment_m.loc.id)
                        loc.remainder_amount -= Decimal(transaction.amount)
                        loc.save()


                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = transaction,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month =month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()



                        g_t = Transactions()
                        g_t.financial_month = month
                        g_t.financial_period = year
                        g_t.financial_year = p
                        g_t.double_entry = 'Cr'
                        g_t.type = event
                        g_t.action = event
                        g_t.description = event.title
                        g_t.date = expense.date
                        g_t.expense = expense
                        g_t.processed = request.user 
                        g_t.amount = transaction.amount
                        g_t.cr_amount = transaction.amount
                        g_t.payment_method = payment_m
                        g_t.actual_amount = transaction.amount
                        g_t.expense = expense
                        g_t.creator = request.user
                        g_t.date_created =  expense.date
                        g_t.save()


                        loc_event = LOCEvent()
                        loc_event.loc = loc
                        loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                        loc_event.financial_year = p
                        loc_event.transaction = g_t
                        loc_event.description = g_t.description
                        loc_event.amount = transaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.type = 'Cr'
                        loc_event.creator = request.user
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()

                        # if event.type == 'transfer':
                        chart_oa = Account.objects.filter(id=payment_m.account.id).first()
                        chart_oa.balance -= decimal.Decimal(transaction.amount)
                        chart_oa.save()


                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = g_t,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()

                        if event.secondary_event is None:
                            pass
                        else:
                            secondary_event = Events.objects.get(id=event.secondary_event.id)


                            chart_oa_secondary = Account.objects.filter(Q(transaction_type=secondary_event) & Q(financial_year=year) & Q(financial_month=month)).first()
                            chart_oa_secondary.balance += decimal.Decimal(transaction.amount)
                            chart_oa_secondary.save()


                            coa_event_sec = AccountEvent(
                                account = chart_oa_secondary,
                                transaction = g_t,
                                amount = transaction.amount,
                                coa_balance = chart_oa_secondary.balance,
                                month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                                financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                                date_created = datetime.datetime.now(),
                                creator = request.user
                            )

                            coa_event_sec.save()
        return redirect(reverse('expense_detail', args=(expense.id,)))



@login_required()
def search(request):
    members = []
    groups = []
    users = []
    branches = []

    if request.method == "GET":

        term = request.GET.get('search_term', False)

        if term:
            members = Member.objects.filter(Q(id_number__icontains=term) | Q(first_name__icontains=term) | Q(middle_name__icontains=term) | Q(last_name__icontains=term) | Q(phone_number__icontains=term)).select_related('group')
            groups = MemberGroup.objects.filter(Q(group_name__icontains=term) | Q(county__icontains=term))
            users = CustomUser.objects.filter(Q(phone_number__icontains=term) | Q(username__icontains=term))
            branches = Branch.objects.filter(Q(name__icontains=term))

            context = {
                'term': term,
                'members': members,
                'groups': groups,
                'users': users,
                'branches': branches
            }

            return render(request, 'fosa/search.html', context)

        return render(request, 'fosa/search.html', context)


@csrf_exempt
@login_required()
def search_slips(request):
    slips = []
    if request.method == "GET":
        term = request.GET.get('search_term', False)
        if term: 
            slips = Slips.objects.filter(Q(number__icontains=term) | Q(id__icontains=term) | Q(member__id_number__icontains=term) | Q(member__first_name__icontains=term) | Q(member__middle_name__icontains=term) | Q(member__last_name__icontains=term) | Q(member__phone_number__icontains=term) & Q(total__gt=0)).select_related('member')
            group_slips = GroupSlips.objects.filter(Q(number__icontains=term) | Q(id__icontains=term) |  Q(group__group_name__icontains=term) & Q(total__gt=0)).select_related('group')
            context = {
                'slips': slips,
                'group_slips': group_slips

            }
            return render(request, 'fosa/slips_search.html', context)

        context = {
            'slips': [],
            'group_slips': []
        }
    return render(request, 'fosa/slips_search.html', context)



from reports.forms import ReportForm
@csrf_exempt
@login_required()
def all_slips(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    
    slips = []

    if request.method == "GET":
        slips = Slips.objects.all()
        group_slips = GroupSlips.objects.all()        

    context = {
        'form': ReportForm,
        'slips': slips,
        'group_slips': group_slips

    }

    return render(request, 'fosa/all_slips.html', context)

@login_required()
def view_member_slips(request, pk):
    slips = []

    if request.method == "GET":
        slips = Slips.objects.filter(member=pk)


    context = {
        'slips': slips,

    }

    return render(request, 'fosa/slips_search.html', context)



@login_required()
def view_group_slips(request, pk):
    slips = []

    if request.method == "GET":
        group_slips = GroupSlips.objects.filter(group=pk)
        

    context = {
        'group_slips': group_slips

    }

    return render(request, 'fosa/slips_search.html', context)







@login_required()
def slips(request):

    return render(request, 'fosa/slips.html')


@login_required
def member_transactions(request, pk):
    member = Member.objects.filter(id=pk).select_related('saving_acc', 'current_acc', 'branch', 'group').first()
    TransactionsModelFormset = modelformset_factory(
    Transactions,
    #  form=ModelTransactionForm(),
    fields=('amount','type', ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none())
    print("RTTEYYEUEEEEEEEEEEEEEEEEEEEIWWWWWWWWWWWWWWWWWW")
    print(PaymentMethod.objects.all())
    context = {
        "member": member,
        "transaction_form": FosaTransactionsForm(),
        "formset": formset,
        "payments_methods": PaymentMethod.objects.all()
    }
    return render(request, 'fosa/member_transaction.html', context)


from loans.models import LoanRepayment

@login_required
def member_end_transactions(request, pk):
    slip = Slips.objects.filter(id=pk).select_related('member').first()

    if slip.settling:

        member = Member.objects.filter(id=slip.member.id).select_related('saving_acc', 'current_acc', 'branch', 'group').first()

        loc_events = LOCEvent.objects.filter(transaction__slip__id=slip.id)
        locs = [event.transaction.id for event in loc_events]
        transactions = Transactions.objects.filter(Q(id__in=locs)  & Q(amount__gte = 0))

        repayments  = LoanRepayment.objects.filter(slip=slip).select_related('loan_status', 'slip','transaction', 'member')

        context = {
            "member": member,
            "transactions": transactions,
            "slip": slip,
            'repayments': repayments,
            "transaction_form": FosaTransactionsForm()
        }
        return render(request, 'fosa/member_end_transaction.html', context)

    else:
        member = Member.objects.filter(id=slip.member.id).select_related('saving_acc', 'current_acc', 'branch', 'group').first()
        loc_events = LOCEvent.objects.filter(transaction__slip__id=slip.id)
        locs = [event.transaction.id for event in loc_events]
        transactions = Transactions.objects.filter(Q(id__in=locs)  & Q(amount__gte = 0))
        # transactions = Transactions.objects.filter(Q(slip=slip)  & Q(amount__gte = 0))

        LoanRepaymentModelFormset = modelformset_factory(
        LoanRepayment,
        fields=('transaction','loan_status', ),
        extra=1
        )
        repaymentformset = LoanRepaymentModelFormset(queryset=LoanRepayment.objects.none())

        for form in repaymentformset:
            form.fields['transaction'].queryset = Transactions.objects.filter(slip=slip)
            form.fields['loan_status'].queryset = LoanStatus.objects.filter(member__id=slip.member.id)



    
        context = {
            "transactions": transactions,
            "slip": slip,
            "member": member,
            "transactions": transactions,
            "repayment_formset": repaymentformset
        }
        return render(request, 'fosa/settle_slip.html', context)



@login_required
def member_transaction_history(request, pk):
    member = Member.objects.get(id=pk)
    transactions = Transactions.objects.filter(Q(member=member) & Q(amount__gte = 0)).select_related('fees')

    context = {
        "member": member,
        "transactions": transactions
    }
    return render(request, 'fosa/history.html', context)


@login_required
def member_statement(request, pk):
    member = Member.objects.filter(id=pk).select_related('saving_acc', 'current_acc', 'branch', 'group').first()

    context = {
        "member": member,
    }
    return render(request, 'fosa/member_statement.html', context)


@login_required
def member_profile(request, pk):
    member = Member.objects.filter(id=pk).select_related('saving_acc', 'current_acc', 'branch', 'group').first()
    context = {
        "member": member,
    }
    return render(request, 'fosa/member_profile.html', context)


from utils.services.loans_prodedure import LoanProcedure

@login_required
def disburse_member_loans(request, pk):
    slip = Slips.objects.filter(id=pk).select_related('member').first()
    member = Member.objects.get(id=slip.member.id)



    if slip.settling:
        return redirect(reverse('member_end_transaction', args=(slip.id,)))

    if request.method == "POST":
        LoanRepaymentModelFormset = modelformset_factory(
        LoanRepayment,
        fields=('transaction','loan_status',  ),
        extra=1
        )
        formset = LoanRepaymentModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('transaction'):
                    loan = form.cleaned_data.get('loan_status')
                    transaction = form.cleaned_data.get('transaction')
                    loan_procedure = LoanProcedure(member, slip, loan.id,transaction.amount, transaction.id, request)
                    loan_procedure.process_loan_repayment()


        slip.settling = True
        slip.save()


    return redirect(reverse('member_end_transaction', args=(slip.id,)))


@login_required
def group_transaction_history(request, pk):
    group = MemberGroup.objects.get(id=pk)
    transactions = GroupTransactions.objects.filter(group=group).select_related('slip')

    context = {
        "group": group,
        "transactions": transactions
    }
    return render(request, 'fosa/group_history.html', context)


@login_required
def group_statement(request, pk):


    group = MemberGroup.objects.get(id=pk)
    account = GroupCustomAccount.objects.filter(group=group).select_related('group')

    context = {
        'group': group,
        'accounts':account,
    }

    return render(request, 'fosa/group_statement.html', context)


@login_required
def group_profile(request, pk):
    group = MemberGroup.objects.filter(id=pk).first()
    context = {
        "group": group,
    }
    return render(request, 'fosa/member_profile.html', context)


@login_required
def group_transactions(request, pk):
    group = MemberGroup.objects.filter(id=pk).first()
    TransactionsModelFormset = modelformset_factory(
    GroupTransactions,
    fields=('amount','type',  ),

    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none())
    for form in formset:
        form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')
    context = {
        'transaction_form': formset,
        "group": group,
        "payments_methods": PaymentMethod.objects.filter(is_cash=True)
    }
    return render(request, 'fosa/group_transactions.html', context)

from loans.models import LoanRepayment, LoanStatus

@login_required
def group_end_transactions(request, pk):
    slip = GroupSlips.objects.filter(id=pk).select_related('group').first()
    group = MemberGroup.objects.filter(id=slip.group.id).first()

    if slip.disbursed_partial:
        return redirect(reverse('disburse_loans', args=(slip.id,)))

    if slip.disbursed:
        return redirect(reverse('disburse_report', args=(slip.id,)))


    # loc_events = LOCEvent.objects.filter(transaction__slip__id=slip.id)
    # locs = [event.transaction.id for event in loc_events]
    transactions = GroupTransactions.objects.filter(slip=slip).select_related('type')
    TransactionsModelFormset = modelformset_factory(
    Transactions,
    fields=('amount','type','member',  ),

    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none())

    for form in formset:
        form.fields['member'].queryset = Member.objects.filter(group=group)
        form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')


    LoanRepaymentModelFormset = modelformset_factory(
    LoanRepayment,
    fields=('transaction','loan_status',  ),
    extra=1
    )
    repaymentformset = LoanRepaymentModelFormset(queryset=LoanRepayment.objects.none())

    for form in repaymentformset:
        form.fields['loan_status'].queryset = LoanStatus.objects.filter(member__group__id=group.id)


  
    context = {
        "member": group,
        "transactions": transactions,
        "slip": slip,
        'transaction_form': formset,
        "group": group,
        "repayment_formset": repaymentformset
    }
    return render(request, 'fosa/group_end_transaction.html', context)




@login_required
def disburse_loans(request, pk):
    slip = GroupSlips.objects.filter(id=pk).select_related('group').first()

    if request.method == "POST":
        LoanRepaymentModelFormset = modelformset_factory(
        LoanRepayment,
        fields=('transaction','loan_status',),
        extra=1
        )
        formset = LoanRepaymentModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('transaction'):
                    status = form.cleaned_data.get('loan_status')
                    transaction = form.cleaned_data.get('transaction')

                    status = LoanStatus.objects.filter(id=status.id).select_related('member').first()
                    member = Member.objects.get(id=status.member.id)
                    member_slip = Slips.objects.filter(Q(member=member) & Q(group_slip=slip)).first()
                    transaction = Transactions.objects.get(id=transaction.id)
                    loan_procedure = LoanProcedure(member, member_slip, status.id,transaction.amount, transaction.id, request)
                    loan_procedure.process_loan_repayment()

        slip.disbursed_partial = False
        member_slips = Slips.objects.filter(group_slip=slip)

        for m_slip in member_slips:
            m_slip.settling = False
            m_slip.save()
        slip.disbursed = True
        slip.save()
        return redirect(reverse('disburse_report', args=(slip.id,)))



    group = MemberGroup.objects.filter(id=slip.group.id)

    transactions = GroupTransactions.objects.filter(slip=slip.id).select_related('type')
    member_transactions = Transactions.objects.filter(slip__group_slip=slip).select_related('type')
    group = MemberGroup.objects.filter(id=slip.group.pk).first()

    LoanRepaymentModelFormset = modelformset_factory(
    LoanRepayment,
    fields=('transaction','loan_status', ),
    extra=1
    )
    repaymentformset = LoanRepaymentModelFormset(queryset=LoanRepayment.objects.none())

    for form in repaymentformset:
        form.fields['transaction'].queryset = Transactions.objects.filter(Q(slip__group_slip=slip) & Q(type__is_loan_event=True))
        form.fields['loan_status'].queryset = LoanStatus.objects.filter(member__group__id=group.id)

  
    context = {
        "transactions": transactions,
        'member_transactions': member_transactions,
        "slip": slip,
        "group": group,
        "repayment_formset": repaymentformset,
        "payments_methods": PaymentMethod.objects.all()
    }
    return render(request, 'fosa/group_end_transaction2.html', context)









from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from savings.models import Savingcontracts, Savingproducts, SavingDepositFee, SavingWithdrawFee
from django.contrib import messages
from members.models import Member
from chartsoa.models import Account
from financial.forms import TransactionsForm
import datetime
import decimal 

from utils.services.shares_procedure import SharesProcedure
from utils.services.custom_procedure import CustomProcedure

from financial.models import Transactions
from savings.models import SavingAccount, CurrentAccount,SavingTransferFee
from parameters.models import Month, Periods
from fosa.models import Slips
import datetime

from decimal import Decimal



@login_required()
def member_payments(request, pk):
    member = Member.objects.get(id=pk)
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    if request.method == "POST":
        receipt = request.POST.get('receipt')
        cheque = request.POST.get('cheque')
        date =  request.POST.get('date')
        method = request.POST.get('method')

        TransactionsModelFormset = modelformset_factory(
            Transactions,
            fields=('amount','type', ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            slip = Slips()
            slip.creator = request.user
            slip.date_created = datetime.datetime.now()
            slip.member = member
            slip.save()
            slip.number = receipt
            slip.cheque_number = cheque
            slip.save()

            events_types = []
            for form in formset:
                if form.cleaned_data.get('amount'):
                    amount = form.cleaned_data.get('amount')
                    event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()

                    method = request.POST.get('method')
                    payment_method = PaymentMethod.objects.filter(name=method).select_related('account', 'loc').first()

                    if event.can_create_accounts:

                        account, new = MemberCustomAccount.objects.get_or_create(Q(acc_type=event) & Q(member=member))
                        if new:
                            account.creator = request.user
                            account.date_created = datetime.datetime.now()
                            account.save()

                        #update member account
                        account.balance += Decimal(amount)
                        account.save()


                    #create Transaction dr

                    archive = Transactions()
                    archive.financial_month = month
                    archive.financial_year = year
                    # archive.loc_account = instance.loc_account or None
                    archive.type = event
                    archive.action = event
                    # archive.acc = instance.acc or None
                    archive.payment_method = payment_method
                    archive.slip = slip 
                    # archive.session = instance.session or None
                    # archive.code = instance.code or None
                    archive.description = event.title
                    # archive.expense = instance.expense or None
                    archive.cheque = cheque
                    # archive.fees = instance.fees or None
                    archive.member = member 
                    # archive.group = instance.group or None
                    # archive.receiver = instance.receiver or None
                    # archive.receiving_member = instance.receiving_member or None
                    archive.date = date 
                    archive.processed = request.user
                    archive.amount = amount 
                    # archive.coa = instance.coa or None
                    # archive.cash_acc = instance.cash_acc or None
                    # archive.bank_acc = instance.bank_acc or None
                    archive.actual_amount = amount
                    archive.creator = request.user
                
                    archive.date_created = datetime.datetime.now()
                    
                    archive.save()

                    loc = LOC.objects.filter(id=payment_method.loc.id).select_related('type').first()
                    loc.remainder_amount = loc.remainder_amount + int(form.cleaned_data.get('amount'))
                    loc.save()

 
                    #Create Loc Event
                    loc_event = LOCEvent()
                    loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                    loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                    loc_event.loc_account = payment_method.name
                    loc_event.description = event.title
                    loc_event.loc = loc
                    loc_event.payment_method = payment_method
                    loc_event.transaction = archive
                    loc_event.amount = archive.amount
                    loc_event.remainder = loc.remainder_amount
                    loc_event.type = "Dr"
                    loc_event.slip = slip
                    loc_event.date = archive.date
                    loc_event.date_created = archive.date_created
                    loc_event.creator = request.user
                    loc_event.date_modified = archive.date_modified
                    loc_event.modified_by = request.user
                    loc_event.save()


                    #Update Loc Event COA OR Payment Method LOC
                    loc_event_coa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=loc.type)).first()
                    loc_event_coa.balance += decimal.Decimal(archive.amount)
                    loc_event_coa.save()



                    coa_event = AccountEvent(
                        account = loc_event_coa,
                        transaction = archive,
                        amount = archive.amount,
                        coa_balance = loc_event_coa.balance,
                        month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                        financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                        date_created = datetime.datetime.now(),
                        creator = request.user
                    )
                    coa_event.save()

                    slip.total = decimal.Decimal(slip.total) + decimal.Decimal(archive.amount)
                    slip.save()

                    #create transaction cr

                    #get event coa

                    #update coa

                    #create event coa



                    if event.secondary_event is not None:
                        # if event is repayment
                        secondary_event = Events.objects.get(id=event.secondary_event.id)
                        chart_oa_secondary = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=secondary_event)).first()
                        chart_oa_secondary.balance -= decimal.Decimal(archive.amount)
                        chart_oa_secondary.save()

                        coa_event_sec = AccountEvent(
                            account = chart_oa_secondary,
                            transaction = archive,
                            amount = archive.amount,
                            coa_balance = chart_oa_secondary.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )
                        coa_event_sec.save()



                        








                    # if event.type == "savings":
                    #     share = CustomProcedure(member.id, amount, event, slip, request)
                    #     share.process_deposit(request)
                    #     events_types.append("savings")


                    # elif event.type == "loan":
                    #     share = CustomProcedure(member.id, amount, event, slip, request)
                    #     share.process_deposit(request)
                    #     events_types.append("loan")


                    # elif event.type == "settling":
                    #     share = CustomProcedure(member.id, amount, event, slip, request)
                    #     share.process_deposit(request)
                    #     events_types.append("settling")


            if "settling" in events_types:
                slip.settling = False
                slip.save()

            elif "loan" in events_types:
                slip.settling = False
                slip.save()
            else:
                slip.settling = True
                slip.save()

            return redirect(reverse('member_end_transaction', args=(slip.id,)))

    TransactionsModelFormset = modelformset_factory(
        Transactions,
        fields=('amount','type', ),
        extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none())
    for form in formset:
        form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')


    context = {
        "member": member,
        "transaction_form": FosaTransactionsForm(),
        "formset": formset,
        "payments_methods": PaymentMethod.objects.all()
    }
    return render(request, 'fosa/member_transaction.html', context)






# @login_required()
# def member_payments(request, pk):
#     member = Member.objects.get(id=pk)

#     if request.method == "POST":
#         receipt = request.POST.get('receipt')
#         cheque = request.POST.get('cheque')
#         TransactionsModelFormset = modelformset_factory(
#             Transactions,
#             fields=('amount','type', ),
#             extra=1
#         )
#         formset = TransactionsModelFormset(request.POST)
#         if formset.is_valid():
#             slip = Slips()
#             slip.creator = request.user
#             slip.date_created = datetime.datetime.now()
#             slip.member = member
#             slip.save()
#             slip.number = receipt
#             slip.cheque_number = cheque
#             slip.save()

#             events_types = []

#             for form in formset:
#                 if form.cleaned_data.get('amount'):
#                     type_ = form.cleaned_data.get('type')
#                     amount = form.cleaned_data.get('amount')
#                     event = Events.objects.get(id=form.cleaned_data.get('type').id)

#                     if event.type == "savings":
#                         share = CustomProcedure(member.id, amount, event, slip, request)
#                         share.process_deposit(request)
#                         events_types.append("savings")


#                     elif event.type == "loan":
#                         share = CustomProcedure(member.id, amount, event, slip, request)
#                         share.process_deposit(request)
#                         events_types.append("loan")


#                     elif event.type == "settling":
#                         share = CustomProcedure(member.id, amount, event, slip, request)
#                         share.process_deposit(request)
#                         events_types.append("settling")


#             if "settling" in events_types:
#                 slip.settling = False
#                 slip.save()

#             elif "loan" in events_types:
#                 slip.settling = False
#                 slip.save()
#             else:
#                 slip.settling = True
#                 slip.save()

#             return redirect(reverse('member_end_transaction', args=(slip.id,)))

#     TransactionsModelFormset = modelformset_factory(
#         Transactions,
#         fields=('amount','type', ),
#         extra=1
#     )
#     formset = TransactionsModelFormset(queryset=Transactions.objects.none())
#     for form in formset:
#         form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')


#     context = {
#         "member": member,
#         "transaction_form": FosaTransactionsForm(),
#         "formset": formset,
#         "payments_methods": PaymentMethod.objects.all()
#     }
#     return render(request, 'fosa/member_transaction.html', context)










from financial.forms import FosaTransactionsForm
@login_required()
def member_transfer(request, pk):
    parameters = request.parameters
    member = Member.objects.get(id=pk)
    receiving = Member.objects.get(id=pk)

    account = SavingAccount.objects.get(member=receiving)

    if request.method == "POST":
        type_ = "transfer_savings"
        amount = request.POST.get('amount')

        slip = Slips()
        slip.creator = request.user
        slip.date_created = datetime.datetime.now()
        slip.member = member
        slip.save()
        slip.number = generate_slip_number(slip.id, member.id, 'M')
        slip.save()

        if type_ == "transfer_savings":
            savings = SavingsProcedure(member.id, amount, type_, slip, request)
            savings.process_transfer(account)


   
    context = {
        "member": member,
        "form": FosaTransactionsForm(),
        "form": ''
    }
    return render(request, 'fosa/member_transfer.html', context)





@login_required()
def member_cheque(request, pk):
    parameters = request.parameters
    member = Member.objects.get(id=pk)

    if request.method == "POST":
        pass

                    

    context = {
        "member": member,
        "transaction_form": FosaTransactionsForm(),
    }
    return render(request, 'fosa/cheque.html', context)




@login_required()
def member_loan(request, pk):
    parameters = request.parameters
    member = Member.objects.get(id=pk)

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            Transactions,
            fields=('amount','type', ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)

        if formset.is_valid():

            slip = Slips()
            slip.number = generate_slip_number(00, member.id, 'M')
            slip.creator = request.user
            slip.date_created = datetime.datetime.now()
            slip.member = member
            slip.save()
            slip.number = generate_slip_number(slip.id, member.id, 'M')
            slip.save()

            for form in formset:
                messages.success(request,form.cleaned_data.get('type'))
                if form.cleaned_data.get('amount'):
                    type_ = form.cleaned_data.get('type')
                    amount = form.cleaned_data.get('amount')

                    if type_ == "pay_loan":
                        pass



                    return redirect(reverse('member_end_transaction', args=(slip.id,)))

    LoanRepaymentModelFormset = modelformset_factory(
    LoanRepayment,
    fields=('transaction','loan_status',  ),
    extra=1
    )
    formset = LoanRepaymentModelFormset(queryset=LoanRepayment.objects.none())

    context = {
        "member": member,
        "formset": formset
    }
    return render(request, 'fosa/member_loans.html', context)






@login_required()
def loan_slip(request, pk):
    slips = []

    if request.method == "GET":
        slips = Slips.objects.filter(member=pk)


    context = {
        'slips': slips,

    }

    return render(request, 'fosa/slips_search.html', context)



@login_required()
def transfer_slip(request, pk):
    slips = []

    if request.method == "GET":
        slips = Slips.objects.filter(member=pk)


    context = {
        'slips': slips,

    }

    return render(request, 'fosa/slips_search.html', context)



@login_required()
def cheque_slip(request, pk):
    slips = []

    if request.method == "GET":
        slips = Slips.objects.filter(member=pk)


    context = {
        'slips': slips,

    }

    return render(request, 'fosa/slips_search.html', context)









from decimal import Decimal
from parameters.models import Events
from chartsoa.models import AccountEvent



@login_required()
def group_payment(request, pk):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    group = MemberGroup.objects.get(id=pk)
    if request.method == "POST":
        receipt = request.POST.get('receipt')
        cheque = request.POST.get('cheque')
        date =  request.POST.get('date')
        method = request.POST.get('method')

        TransactionsModelFormset = modelformset_factory(
            GroupTransactions,
            fields=('amount','type',  ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            slip = GroupSlips()
            # slip.number = slip.generate_slip_number(group.id, 'G')
            slip.creator = request.user
            slip.date_created = datetime.datetime.now()
            slip.disbursed = False
            slip.group = group
            slip.save()
            # slip.number = generate_slip_number(slip.id,group.id, 'G')
            slip.date = date
            slip.number = receipt
            slip.cheque_number = cheque
            slip.financial_month = month
            slip.financial_period = year
            slip.save()

            for form in formset:
                if form.cleaned_data.get('amount'):

                    transaction = form.save(commit=False)
                    event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()
                    try: 
                        custom_acc = GroupCustomAccount.objects.get(Q(group=pk) & Q(acc_type=event))

                    except GroupCustomAccount.DoesNotExist:
                        custom_acc = GroupCustomAccount()
                        custom_acc.acc_type = event
                        custom_acc.acc_number = str(group.id) + '-' + event.code
                        custom_acc.creator = request.user
                        custom_acc.group = MemberGroup.objects.get(id=pk)
                        custom_acc.save()

                    if event.classication == 'asset' or event.classication == 'capital':
                        custom_acc.acc_balance -=  int(form.cleaned_data.get('amount'))
                        custom_acc.save()


                        transaction.financial_month = month
                        transaction.financial_year = year
                        transaction.save()

                        payment_method = PaymentMethod.objects.filter(name=method).select_related('account', 'loc').first()

                        transaction.payment_method = payment_method
                        transaction.save()



                        slip.total = decimal.Decimal(slip.total) + decimal.Decimal(transaction.amount)
                        slip.save()
                        transaction.date = slip.date
                        transaction.acc = custom_acc.acc_number
                        transaction.financial_month = month
                        transaction.financial_period = year
                        transaction.dr_amount = transaction.amount
                        transaction.group = group
                        transaction.double_entry = "DR"
                        transaction.payment_method = payment_method
                        transaction.slip = GroupSlips.objects.get(id=slip.id)
                        transaction.creator = request.user
                        transaction.date_created = datetime.datetime.now()
                        transaction.save()

                    elif event.classication == 'liability':

                        custom_acc.acc_balance +=  int(form.cleaned_data.get('amount'))
                        custom_acc.save()


                        transaction.financial_month = month
                        transaction.financial_year = year
                        transaction.save()

                        payment_method = PaymentMethod.objects.filter(name=method).select_related('account', 'loc').first()

                        transaction.payment_method = payment_method
                        transaction.save()



                        slip.total = decimal.Decimal(slip.total) + decimal.Decimal(transaction.amount)
                        slip.save()
                        transaction.date = slip.date
                        transaction.acc = custom_acc.acc_number
                        transaction.financial_month = month
                        transaction.financial_period = year
                        transaction.cr_amount = transaction.amount
                        transaction.group = group
                        transaction.double_entry = "DR"
                        transaction.payment_method = payment_method
                        transaction.slip = GroupSlips.objects.get(id=slip.id)
                        transaction.creator = request.user
                        transaction.date_created = datetime.datetime.now()
                        transaction.save()

        return redirect(reverse('group_end_transaction', args=(slip.id,)))

    TransactionsModelFormset = modelformset_factory(
    GroupTransactions,
    fields=('amount','type',  ),

    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none())
    context = {
        'transaction_form': formset,
        "group": group,
        "payments_methods": PaymentMethod.objects.filter(is_cash=True)
    }
    return render(request, 'fosa/group_transactions.html', context)




@login_required()
def disburse_to_members(request, pk):
    slip = GroupSlips.objects.filter(id=pk).select_related('group').first()
    group = MemberGroup.objects.get(id=slip.group.id)


    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
        Transactions,
        fields=('amount','type','member',  ),
        extra=1
        )
        formset = TransactionsModelFormset(request.POST)

        member_slips = {}

        member_ids = []

        events_total = {}

        transactions_events_total = {}

        # totals = decimal.Decimal(0.00)
        # for form in formset:
        #     if form.cleaned_data.get('amount'):
        #         totals += decimal.Decimal(form.cleaned_data.get('amount'))

            
        # if slip.total != totals:
        #     # messages.warning(request, 'The total of the transactions  %s does not equal the the slip total %s' % (totals, slip.total))
        #     message = 'The total of the transactions  %s does not equal the the slip total %s' % (totals, slip.total)

        #     transactions = GroupTransactions.objects.filter(slip=slip.id).select_related('type')
        #     TransactionsModelFormset = modelformset_factory(
        #     Transactions,
        #     fields=('amount','type','member',  ),
        #     extra=1
        #     )
        #     formset = TransactionsModelFormset(request.POST)
        #     for form in formset:
        #         form.fields['member'].queryset = Member.objects.filter(group=MemberGroup.objects.get(id=group.id))
        #         form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')
        #     context = {
        #         "message": message,
        #         "member": group,
        #         "transactions": transactions,
        #         "slip": slip,
        #         'transaction_form': formset,
        #         "group": group,
        #     }
        #     return render(request, 'fosa/group_end_transaction.html', context)


        transactions_ = GroupTransactions.objects.filter(slip=slip).select_related('type')

        for transaction in transactions_:
            if transaction.type.id in transactions_events_total:
                transactions_events_total[transaction.type.id] = transactions_events_total[transaction.type.id] + transaction.amount
            else:
                transactions_events_total.update({ transaction.type.id : transaction.amount })


        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('amount'):
                    event = Events.objects.get(id=form.cleaned_data.get('type').id)
                    if event.id in events_total:
                        events_total[event.id] = events_total[event.id] + form.cleaned_data.get('amount')
                    else:
                        events_total.update({ event.id : form.cleaned_data.get('amount') })



            messa = []
            for form in formset:
                if form.cleaned_data.get('amount'):
                    event = Events.objects.get(id=form.cleaned_data.get('type').id)
                    if transactions_events_total[event.id] == events_total[event.id]:
                        pass
                    else:
                        messa.append('The sum of %s does not equal to the total of the same event in the slip' % (event.title))

            if len(messa) > 0:
                for message in messa:
                    messages.add_message(request,1, message)


                transactions = GroupTransactions.objects.filter(slip=slip.id).select_related('type')
                group = MemberGroup.objects.filter(id=pk).first()
                TransactionsModelFormset = modelformset_factory(
                Transactions,
                fields=('amount','type','member',  ),
                extra=1
                )
                formset = TransactionsModelFormset(request.POST)
                for form in formset:
                    form.fields['member'].queryset = Member.objects.filter(group=MemberGroup.objects.get(id=group.id))
                    form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')

                context = {
                    "message": messa,
                    "member": group,
                    "transactions": transactions,
                    "slip": slip,
                    'transaction_form': formset,
                    "group": group,
                }
                return render(request, 'fosa/group_end_transaction.html', context)
                    
                        
            for form in formset:
                if form.cleaned_data.get('amount'):
                    event = Events.objects.get(id=form.cleaned_data.get('type').id)

                    if event.type == "savings":
                        pass

                    elif event.type == "loan":
                        pass

                    elif event.type == "settling":
                        slip.disbursed_partial = True

            slip.save()




            processed_total = Decimal(0.00)
            for form in formset:
                if form.cleaned_data.get('amount'):
                    # type_ = form.cleaned_data.get('type')
                    amount = form.cleaned_data.get('amount')
                    member = Member.objects.get(id=form.cleaned_data.get('member').id)

                    event = Events.objects.get(id=form.cleaned_data.get('type').id)

                    if member.id in member_ids:
                        member_slip = member_slips[member.id]

                    else:
                        member_slip = Slips()
                        member_slip.creator = request.user
                        member_slip.date_created = datetime.datetime.now()
                        member_slip.member = member
                        member_slip.group_slip = slip
                        member_slip.save()
                        member_slip.number = "%s - %s" % (member.id,  slip.number) 
                        
                        member_slip.date = slip.date
                        member_slip.save()
                        member_ids.append(member.id)
                        member_slips[member.id] = member_slip

                    print("LLLLLLLLLLLLLLLLLLLLLLLLLLLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
                    print("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
                    print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
                        

                    if event.type == "savings":
                        share = CustomProcedure(member.id, amount, event, member_slip, request)
                        share.process_deposit(request)
                        processed_total += amount


                    elif event.type == "settling":
                        slip.disbursed_partial = True
                        slip.save()

            if processed_total == slip.total:
                slip.disbursed = True
                slip.disbursed_partial = False
            elif processed_total != slip.total:
                slip.disbursed = False
                slip.disbursed_partial = True
            slip.save()
            
            if slip.disbursed_partial:
                return redirect(reverse('disburse_loans', args=(slip.id,)))

            elif slip.disbursed:
                return redirect(reverse('disburse_report', args=(slip.id,)))




        group = MemberGroup.objects.filter(id=slip.group.id)

        transactions = GroupTransactions.objects.filter(slip=slip.id).select_related('type')
        group = MemberGroup.objects.filter(id=pk).first()
        TransactionsModelFormset = modelformset_factory(
        Transactions,
        fields=('amount','type','member',  ),

        extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        for form in formset:
            form.fields['member'].queryset = Member.objects.filter(group=MemberGroup.objects.get(id=group.id))
            form.fields['type'].queryset = Events.objects.filter(include_in_pos=True).order_by('title')
        context = {
            "transactions": transactions,
            "slip": slip,
            'transaction_form': formset,
            "group": group,
        }
        return render(request, 'fosa/group_end_transaction.html', context)

    
   
    return redirect(reverse('group_end_transaction', args=(slip.id,)))





@login_required()
def members_slips(request, pk):
    group_slip = GroupSlips.objects.get(id=pk)
    members_slips = Slips.objects.filter(group_slip=group_slip)
    return redirect(reverse('members_slips', args=(group_slip.id,)))




@login_required()
def disburse_report(request, pk):
    group_slip = GroupSlips.objects.filter(id=pk).select_related('group').first()
    members_slips = Slips.objects.filter(group_slip=group_slip).select_related("member")


    context = {
        "group_slip": group_slip,
        "group": MemberGroup.objects.filter(id=group_slip.group.id).first(),
        "transactions": GroupTransactions.objects.filter(slip=group_slip),
        "slips": [

        ]
        
    }

    for slip in members_slips:
        slip_detail = {
            "slip": slip,
            "member": Member.objects.filter(id=slip.member.id).first(),
            "transactions": Transactions.objects.filter(Q(slip=slip) & Q(actual_amount__gt=0)),
            "repayments": LoanRepayment.objects.filter(slip=slip).select_related('loan_status')
        }
        context["slips"].append(slip_detail)
        
    return render(request, 'fosa/disburse_report.html', context)



@login_required()
def cheque(request, pk):
    context = {
        
    }
    return render(request, 'fosa/cheque.html', context)



from financial.models import MemberCustomAccount, GroupCustomAccount


@login_required
def grouP_accounts(request, pk):
    context = {
        'group':  MemberGroup.objects.get(id=pk),
        'accounts': GroupCustomAccount.objects.filter(group__id=pk).select_related('acc_type', 'group', 'creator')
    }
    return render(request, 'fosa/group_accounts.html', context)


@login_required
def grouP_accounts_details(request, pk):
    account = GroupCustomAccount.objects.filter(id=pk).select_related('group')
    group = MemberGroup.objects.get(id=account.first().group.id)

    context = {
        'group': group,
        'accounts':account,
        'transactions': GroupTransactions.objects.filter(acc=account.first().acc_number)
    }
    return render(request, 'fosa/group_accounts_history.html', context)



@login_required
def accounts(request, pk):
    member = Member.objects.get(id=pk)
    context = {
        'member': member,
        'accounts': MemberCustomAccount.objects.filter(Q(member=member) & Q(acc_type__can_create_accounts=True)).select_related('acc_type', 'member', 'creator')
    }
    return render(request, 'fosa/accounts.html', context)


@login_required
def accounts_details(request, pk):
    account = MemberCustomAccount.objects.filter(id=pk).select_related('member', 'acc_type')
    member = Member.objects.filter(id=account.first().member.id).first()
    context = {
        'member': member,
        'accounts':account,
        'transactions': Transactions.objects.filter(Q(member=member) & Q(type__title=account.first().acc_type.title))
    }
    return render(request, 'fosa/accounts_history.html', context)

from loans.models import LoanContract


@login_required
def loans(request, pk):
    member = Member.objects.filter(id=pk)
    loans = LoanContract.objects.filter(member=member.first())
    status = LoanStatus.objects.filter(member=member.first())
    context = {
        'member': member.first(),
        'loans': loans,
        'status': status
        
    }
    return render(request, 'fosa/loans.html', context)


@login_required
def loan_request(request, pk):
    account = MemberCustomAccount.objects.filter(id=pk).select_related('member')
    context = {
        
        'accounts':account,
        'transactions': Transactions.objects.filter(acc=account.first().acc_number)
    }
    return render(request, 'fosa/accounts_history.html', context)






@login_required
def status_request(request):
    loans = LoanContract.objects.all().select_related('member')
    for loan in loans:
        member = Member.objects.get(id=loan.member.id)

        status = LoanStatus()
        status.member = member

        status.total_balance = loan.balance
        status.total_interest = loan.interest
        status.total_loans = loan.requested_amount 
        status.total_repaid = loan.repaid
        status.save()
        status.loan_contracts.add(loan)
        status.save()

    return redirect(reverse('members_list'))



@login_required
def acc_numbers(request):
    accs = MemberCustomAccount.objects.all()
    for acc in accs:
        acc = MemberCustomAccount.objects.get(id=acc.id)
        acc.acc_number = acc.acc_number[0] + '-' + acc.acc_number[1:]
        acc.save()
    return redirect(reverse('members_list'))



@login_required
def edit_group_slip(request, pk):
    if request.method == 'GET':
        slip = GroupSlips.objects.filter(id=pk).select_related('group').first()
        group = MemberGroup.objects.filter(id=slip.group.id).first()
        TransactionsModelFormset = modelformset_factory(
        GroupTransactions,
        fields=('amount','type', 'id' ),

        extra=1
        )
        formset = TransactionsModelFormset(queryset=GroupTransactions.objects.filter(slip=slip))
        context = {
            'slip': slip,
            'transaction_form': formset,
            "group": group,
        }

        return render(request, 'fosa/group_slip_edit.html', context)

    if request.method == 'POST':
        TransactionsModelFormset = modelformset_factory(
            GroupTransactions,
            fields=('amount','type',  ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            slip = GroupSlips.objects.get(id=pk)

            for form in formset:
                if form.cleaned_data.get('amount') > 0:
                    transaction = GroupTransactions.objects.get(id=1)

                    event = Events.objects.get(id=form.cleaned_data.get('type').id)
                    try: 
                        custom_acc = GroupCustomAccount.objects.get(Q(group=pk) & Q(acc_type=event))
                        # custom_acc.acc_balance += form.cleaned_data.get('amount')
                        custom_acc.save()

                    except GroupCustomAccount.DoesNotExist:
                        custom_acc = GroupCustomAccount()
                        custom_acc.acc_type = event
                        custom_acc.acc_number = str(group.id) + '-' + event.code
                        custom_acc.creator = request.user
                        custom_acc.group = MemberGroup.objects.get(id=pk)
                        custom_acc.save()

                    custom_acc.acc_balance += decimal.Decimal(transaction.amount)
                    custom_acc.save()

                    transaction.type = event

                    # slip.total = slip.total - + decimal.Decimal(transaction.amount)
                    slip.total = decimal.Decimal(slip.total) + decimal.Decimal(transaction.amount)
                    slip.save()



        return render(request, 'fosa/group_slip_edit.html', context)


from .forms import LoanCalculatorForm
import datetime


@login_required
def calculator(request):
    if request.method == 'POST':
        product = request.POST.get('loan_product')
        amount = request.POST.get('amount')
        duration = request.POST.get('payment_period')
        date = request.POST.get('application_month')
        loans_details = {

        }
        
        principal_total = Decimal(0.0)
        interest_total = Decimal(0.0)
        total = Decimal(0.0)
        loan_product = LoanProduct.objects.filter(id=product).first()
        if loan_product.interest_rate_scheme == 'flat':
            principal_amount = amount
            p_payment_per_month = Decimal(principal_amount) / Decimal(duration)

            interest_rate_per_month = loan_product.monthly_rate_interest

            interest = Decimal(principal_amount) * Decimal(interest_rate_per_month) * Decimal(0.01)
            interest = interest.quantize(Decimal("0.01"))

            schedule = [{
                'id': i + 1,
                'date': datetime.datetime.strptime(date, '%Y-%m-%d') + relativedelta(months=i+1),
                'amount': p_payment_per_month.quantize(Decimal("0.01")),
                'interest': interest,
                'total': p_payment_per_month.quantize(Decimal("0.01")) + interest,
                'received': False
            }  for i in range(int(duration))]

            for entry in schedule:
                principal_total += entry["amount"]
                interest_total += entry["interest"]
                total += entry["total"]

            loans_details = {
                'principal': principal_amount,
                'loan_product': loan_product,
                'schedule': schedule,
                'date': datetime.datetime.strptime(date, '%Y-%m-%d') ,
                "principal_total": principal_total,
                "interest_total":interest_total,
                "total": total
            }

        elif loan_product.interest_rate_scheme == 'declining':
            principal_amount = Decimal(amount)
            p_payment_per_month = Decimal(principal_amount) / Decimal(duration)

            interest_rate_per_month = loan_product.monthly_rate_interest

            schedule = []

            new_principla = principal_amount
            
            for i in range(int(duration)):
                interest = Decimal(new_principla) * Decimal(interest_rate_per_month) * Decimal(0.01)
                interest = interest.quantize(Decimal("0.01"))
                a = {
                        'id': i + 1,
                        'date': datetime.datetime.strptime(date, '%Y-%m-%d') + relativedelta(months=i+1),
                        'amount': p_payment_per_month.quantize(Decimal("0.01")),
                        'interest': interest,
                        'total': p_payment_per_month.quantize(Decimal("0.01")) + interest,
                        'received': False
                    }  
                schedule.append(a)
                new_principla -= p_payment_per_month

            for entry in schedule:
                principal_total += entry["amount"]
                interest_total += entry["interest"]
                total += entry["total"]


            loans_details = {
                'principal': principal_amount,
                'loan_product': loan_product,
                'schedule': schedule,
                'date': datetime.datetime.strptime(date, '%Y-%m-%d') ,
                "principal_total": principal_total,
                "interest_total":interest_total,
                "total": total

            }

            

        context = {
            'loan_details':loans_details,
            'form': LoanCalculatorForm()

        }

        return render(request, 'fosa/calculator_details.html', context)

    context = {
        'form': LoanCalculatorForm()
    }
    return render(request, 'fosa/calculator.html', context)







@login_required
def event_reassign(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    if request.method == "POST":
        slips = request.POST.get('slips')

        slips = slips.split(',')
        event = Events.objects.get(title='Interest On Member Loan')



        for slip in slips:
            g_slip = GroupSlips.objects.filter(number=slip).select_related('group').first()

            transactions = GroupTransactions.objects.filter(slip=g_slip).select_related('type', 'group')

            group = MemberGroup.objects.get(id=g_slip.group.id)

            for transaction in transactions:
                transaction.type = event


                e = Events.objects.get(id=transaction.type.id)

                acc = GroupCustomAccount.objects.get(Q(group=group) & Q(acc_type=event))
                acc.acc_balance += transaction.amount
                acc.save()
                
                transaction.acc = acc.acc_number
                transaction.save()

                acc = GroupCustomAccount.objects.get(Q(group=group) & Q(acc_type=e))
                acc.acc_balance += transaction.amount
                acc.save()

                chart = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                chart.balance += decimal.Decimal(transaction.amount)
                chart.save()

                chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=e)).first()
                chart_oa.balance -= decimal.Decimal(transaction.amount)
                chart_oa.save()


        return render(request, 'fosa/slip_reassign.html')


    return render(request, 'fosa/slip_reassign.html')



def trans(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    trans = GroupTransactions.objects.all().select_related('type' ).order_by('date')
    loc = LOC.objects.get(id=3)

    for t in trans:
        e = Account.objects.filter(type__id=t.type.id).first()
        loc_event = LOCEvent()
        loc_event.financial_month = Month.objects.get(id=1) 
        loc_event.financial_year = Periods.objects.get(id=1)
        loc_event.loc_account = 'cash'
        loc_event.description = t.descr()
        loc_event.loc = loc
        loc_event.transaction = t
        loc_event.amount = t.amount
        loc_event.date = t.date
        loc_event.date_created = t.date_created
        loc_event.creator = CustomUser.objects.get(id=1)
        loc_event.date_modified = t.date_modified
        loc_event.modified_by = CustomUser.objects.get(id=1)
        loc_event.save()


        if t.type.type == 'expense':
            loc_event.type = 'Dr'
            loc_event.remainder = loc.remainder_amount - loc_event.amount
            loc.remainder_amount -= loc_event.amount
            loc_event.save()

        else:
            loc_event.type = 'Cr'
            loc_event.remainder = loc.remainder_amount + loc_event.amount
            loc.remainder_amount += loc_event.amount
            loc_event.save()



def dr_cr(request):
    events = LOCEvent.objects.filter(loc=LOC.objects.get(is_bank=True))
    for e in events:
        e.remainder += int(5529976)
        e.save()
    # acc = Account.objects.all()

    # for a in acc:
    #     a.financial_month = Month.objects.get(id=1)
    #     a.save()
            




from loans.forms import *


@login_required
def loan_form(request, pk):
    member = Member.objects.get(id=pk)
    form = LoanContractForm(request, pk)

    if request.method == 'POST':
        form = LoanContractForm(request, pk,request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            messages.success(request, 'Request Added Successfully')
            member.has_active_loan = True
            member.save()

            return redirect(reverse('loan_individual_pledges_pos', args=(bulding.id,)))
    context = {
        'member': member,
        'form': LoanContractForm(request, pk=pk)
    }
    return render(request, 'loans/loan_form.html', context)



@login_required
def edit_loan_form(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member').first()
    member = Member.objects.get(id=loan.member.id)

    if request.method == 'POST':
        form = LoanContractForm(request, pk,request.POST, request.FILES, instance=loan)
        if form.is_valid():
            loan_ = form.save(commit=False)
            loan_.member = member
            loan_.creator = request.user
            loan_.save()

            return redirect(reverse('loan_individual_pledges_pos', args=(loan_.id,)))
    context = {
        'form': LoanContractForm(request, pk=pk, instance=loan),
        'member': member
    }
    return render(request, 'fosa/edit_loan_form.html', context)


from loans.models import AccountsLock, InternalGuarantors, LoanGuarantors, GuarantorsPledge




@login_required
def loan_individual_pledges(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member').first()
    member = Member.objects.get(id=loan.member.id)

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            IndividualsPledge,
            fields=('item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():

            for form in formset:
                guarantor = form.save(commit=False)
                guarantor.contract = loan
                guarantor.creator = request.user
                guarantor.date_created = datetime.datetime.now()
                guarantor.save()

        return redirect(reverse('loan_guarantors_pos', args=(loan.id,)))

    TransactionsModelFormset = modelformset_factory(
    IndividualsPledge,
    fields=('item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=IndividualsPledge.objects.filter(contract=loan))

    context = { 'loan': loan, "formset": formset,  'member': member, 
    }
    return render(request, 'loans/loan_individuals_pledges.html', context)


@login_required
def loan_group_guarantors(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            InternalGuarantors,
            fields=('member','lock'),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('lock'):
                    guarantor = form.save(commit=False)
                    guarantor.contract = loan
                    # guarantor.member = member
                    guarantor.creator = request.user
                    guarantor.date_created = datetime.datetime.now()
                    guarantor.save()

        return redirect(reverse('loan_external_guarantors_pos', args=(loan.id,)))

    TransactionsModelFormset = modelformset_factory(
    InternalGuarantors,
    fields=('member','lock'),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=InternalGuarantors.objects.filter(contract=loan))

    for form in formset:
        form.fields['member'].queryset = Member.objects.filter(group=MemberGroup.objects.get(id=member.group.id))
    context = { 'loan': loan, "formset": formset,  'member': member,
    }
    return render(request, 'loans/loan_group_guarantors.html', context)




@login_required
def loan_external_guarantors(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member').first()
    member = Member.objects.get(id=loan.member.id)

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            LoanGuarantors,
            fields=('first_name', 'phone_number', 'id_number', 'sex'  ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('id_number'):
                    guarantor = form.save(commit=False)
                    guarantor.contract = loan
                    guarantor.creator = request.user
                    guarantor.date_created = datetime.datetime.now()
                    guarantor.save()

        return redirect(reverse('loan_external_guarantors_pledges_pos', args=(loan.id,)))

    TransactionsModelFormset = modelformset_factory(
    LoanGuarantors,
    fields=('first_name', 'phone_number', 'id_number', 'sex' ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=LoanGuarantors.objects.filter(contract=loan))
    context = { 'loan': loan, "formset": formset,  'member': member, 
    }
    return render(request, 'loans/loan_external_guarantors.html', context)





@login_required
def loan_external_guarantors_pledges(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member').first()
    member = Member.objects.get(id=loan.member.id)

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            GuarantorsPledge,
            fields=('guarantor', 'item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:

                # if form.cleaned_data.get('item_title'):
                guarantor = form.save(commit=False)
                guarantor.creator = request.user
                guarantor.date_created = datetime.datetime.now()
                guarantor.save()
                    # print("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP")

        return redirect(reverse('loan_details_pos', args=(loan.id,)))

    TransactionsModelFormset = modelformset_factory(
    GuarantorsPledge,
    fields=('guarantor', 'item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=GuarantorsPledge.objects.filter(guarantor__contract=loan))
    for form in formset:
        form.fields['guarantor'].queryset = LoanGuarantors.objects.filter(contract=loan)
    context = { 'loan': loan, "formset": formset,  'member': member, 
    }
    return render(request, 'loans/loan_external_guarantors_pledges.html', context)


















from loans.models import IndividualsPledge

@login_required
def loan_details_pos(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)

    amount = loan.requested_amount
    duration = loan.payment_period
    date = loan.date

    principal_total = Decimal(0.0)
    interest_total = Decimal(0.0)
    total = Decimal(0.0)
    loan_product = LoanProduct.objects.filter(id=loan.loan_product.id).first()

    loans_details = {

    }
    if loan_product.interest_rate_scheme == 'flat':
        principal_amount = amount
        p_payment_per_month = Decimal(principal_amount) / Decimal(duration)

        interest_rate_per_month = loan_product.monthly_rate_interest

        interest = Decimal(principal_amount) * Decimal(interest_rate_per_month) * Decimal(0.01)
        interest = interest.quantize(Decimal("0.01"))

        schedule = [{
            'id': i + 1,
            'date': date + relativedelta(months=i+1),
            'amount': p_payment_per_month.quantize(Decimal("0.01")),
            'interest': interest,
            'total': p_payment_per_month.quantize(Decimal("0.01")) + interest,
            'received': False
        }  for i in range(int(duration))]

        for entry in schedule:
            principal_total += entry["amount"]
            interest_total += entry["interest"]
            total += entry["total"]

        loans_details = {
            'principal': principal_amount,
            'loan_product': loan_product,
            'schedule': schedule,
            'date': date ,
            "principal_total": principal_total,
            "interest_total":interest_total,
            "total": total
        }

    elif loan_product.interest_rate_scheme == 'declining':
        principal_amount = Decimal(amount)
        p_payment_per_month = Decimal(principal_amount) / Decimal(duration)

        interest_rate_per_month = loan_product.monthly_rate_interest

        schedule = []

        new_principla = principal_amount
        
        for i in range(int(duration)):
            interest = Decimal(new_principla) * Decimal(interest_rate_per_month) * Decimal(0.01)
            interest = interest.quantize(Decimal("0.01"))
            a = {
                    'id': i + 1,
                    'date': date + relativedelta(months=i+1),
                    'amount': p_payment_per_month.quantize(Decimal("0.01")),
                    'interest': interest,
                    'total': p_payment_per_month.quantize(Decimal("0.01")) + interest,
                    'received': False
                }  
            schedule.append(a)
            new_principla -= p_payment_per_month

        for entry in schedule:
            principal_total += entry["amount"]
            interest_total += entry["interest"]
            total += entry["total"]


        loans_details = {
            'principal': principal_amount,
            'loan_product': loan_product,
            'schedule': schedule,
            'date': date ,
            "principal_total": principal_total,
            "interest_total":interest_total,
            "total": total

        }

            
    context = {
        'member': member,
        'loan': loan,
        'internal_gaurantors': internal_gaurantors,
        'external_pledges': external_pledges,
        'individual_pledges':individual_pledges,
        'loans_details': loans_details
    }
    return render(request, 'fosa/loan_details.html', context)





@login_required
def loan_guarantors_lock(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)
    context = {
        'member': member,
       
    }
    return render(request, 'loans/loan_details.html', context)



@login_required
def wave_interest(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)
    context = {
        'member': member,
        'loan': loan,
        'internal_gaurantors': internal_gaurantors,
        'external_pledges': external_pledges,
        'individual_pledges':individual_pledges,
        # 'loans_details': loans_details
       
    }
    return render(request, 'fosa/waveinterest.html', context)


@login_required
def wave_penalty(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)
    context = {
        'member': member,
        'loan': loan,
        'internal_gaurantors': internal_gaurantors,
        'external_pledges': external_pledges,
        'individual_pledges':individual_pledges,
       
    }
    return render(request, 'fosa/wavepenalty.html', context)


@login_required
def wave_all_interest(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)
    context = {
        'member': member,
        'loan': loan,
        'internal_gaurantors': internal_gaurantors,
        'external_pledges': external_pledges,
        'individual_pledges':individual_pledges,
        # 'loans_details': loans_details
      
       
    }
    return render(request, 'fosa/waveallinterest.html', context)


@login_required
def wave_all_penalty(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)
    context = {
        'member': member,
        'loan': loan,
        'internal_gaurantors': internal_gaurantors,
        'external_pledges': external_pledges,
        'individual_pledges':individual_pledges,
        # 'loans_details': loans_details
       
    }
    return render(request, 'fosa/waveallpenalty.html', context)




@login_required
def refinancing(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    member = Member.objects.filter(id=loan.member.id).select_related('group').first()
    internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
    external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
    individual_pledges = IndividualsPledge.objects.filter(contract=loan)
    context = {
        'member': member,
        'loan': loan,
        'internal_gaurantors': internal_gaurantors,
        'external_pledges': external_pledges,
        'individual_pledges':individual_pledges,
        # 'loans_details': loans_details
       
    }
    return render(request, 'fosa/refinancing.html', context)








# @login_required
# def loan_form(request, pk):
#     member = Member.objects.get(id=pk)
#     form = LoanContractForm(request, pk)

#     if request.method == 'POST':
#         form = LoanContractForm(request, pk,request.POST, request.FILES)
#         if form.is_valid():
#             bulding = form.save(commit=False)
#             bulding.creator = request.user
#             bulding.save()
#             messages.success(request, 'Request Added Successfully')
#             member.has_active_loan = True
#             member.save()

#             return redirect(reverse('loan_individual_pledges_pos', args=(bulding.id,)))
#     context = {
#         'member': member,
#         'form': LoanContractForm(request, pk=pk)
#     }
#     # return render(request, 'loans/loan_form.html', context)
#     return render(request, 'fosa/loan_form.html', context)






# from loans.models import AccountsLock, InternalGuarantors, LoanGuarantors, GuarantorsPledge




# @login_required
# def loan_individual_pledges(request, pk):
#     loan = LoanContract.objects.filter(id=pk).select_related('member').first()
#     member = Member.objects.get(id=loan.member.id)

#     if request.method == "POST":
#         TransactionsModelFormset = modelformset_factory(
#             IndividualsPledge,
#             fields=('item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
#             extra=1
#         )
#         formset = TransactionsModelFormset(request.POST)
#         if formset.is_valid():

#             for form in formset:
#                 guarantor = form.save(commit=False)
#                 guarantor.contract = loan
#                 guarantor.creator = request.user
#                 guarantor.date_created = datetime.datetime.now()
#                 guarantor.save()

#         return redirect(reverse('loan_guarantors_pos', args=(loan.id,)))

#     TransactionsModelFormset = modelformset_factory(
#     IndividualsPledge,
#     fields=('item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
#     extra=1
#     )
#     formset = TransactionsModelFormset(queryset=IndividualsPledge.objects.filter(contract=loan))

#     context = { 'loan': loan, "formset": formset,  'member': member, 
#     }
#     # return render(request, 'loans/loan_individuals_pledges.html', context)
#     return render(request, 'fosa/loan_individuals_pledges.html', context)


# @login_required
# def loan_group_guarantors(request, pk):
#     loan = LoanContract.objects.filter(id=pk).select_related('member').first()
#     member = Member.objects.filter(id=loan.member.id).select_related('group').first()

#     if request.method == "POST":
#         TransactionsModelFormset = modelformset_factory(
#             InternalGuarantors,
#             fields=('member','lock'),
#             extra=1
#         )
#         formset = TransactionsModelFormset(request.POST)
#         if formset.is_valid():
#             for form in formset:
#                 if form.cleaned_data.get('lock'):
#                     guarantor = form.save(commit=False)
#                     guarantor.contract = loan
#                     guarantor.member = member
#                     guarantor.creator = request.user
#                     guarantor.date_created = datetime.datetime.now()
#                     guarantor.save()

#         return redirect(reverse('loan_external_guarantors_pos', args=(loan.id,)))

#     TransactionsModelFormset = modelformset_factory(
#     InternalGuarantors,
#     fields=('member','lock'),
#     extra=1
#     )
#     formset = TransactionsModelFormset(queryset=InternalGuarantors.objects.filter(contract=loan))

#     for form in formset:
#         form.fields['member'].queryset = Member.objects.filter(group=MemberGroup.objects.get(id=member.group.id))
#     context = { 'loan': loan, "formset": formset,  'member': member, 'accounts': MemberCustomAccount.objects.filter(member=member).select_related('acc_type', 'member', 'creator')
#     }
#     return render(request, 'fosa/loan_group_guarantors.html', context)



# @login_required
# def loan_external_guarantors(request, pk):
#     loan = LoanContract.objects.filter(id=pk).select_related('member').first()
#     member = Member.objects.get(id=loan.member.id)

#     if request.method == "POST":
#         TransactionsModelFormset = modelformset_factory(
#             LoanGuarantors,
#             fields=('first_name', 'phone_number', 'id_number', 'sex'  ),
#             extra=1
#         )
#         formset = TransactionsModelFormset(request.POST)
#         if formset.is_valid():
#             for form in formset:
#                 if form.cleaned_data.get('id_number'):
#                     guarantor = form.save(commit=False)
#                     guarantor.contract = loan
#                     guarantor.creator = request.user
#                     guarantor.date_created = datetime.datetime.now()
#                     guarantor.save()

#         return redirect(reverse('loan_external_guarantors_pledges_pos', args=(loan.id,)))

#     TransactionsModelFormset = modelformset_factory(
#     LoanGuarantors,
#     fields=('first_name', 'phone_number', 'id_number', 'sex' ),
#     extra=1
#     )
#     formset = TransactionsModelFormset(queryset=LoanGuarantors.objects.filter(contract=loan))
#     context = { 'loan': loan, "formset": formset,  'member': member, 
#     }
#     # return render(request, 'loans/loan_external_guarantors.html', context)
#     return render(request, 'fosa/loan_external_guarantors.html', context)





# @login_required
# def loan_external_guarantors_pledges(request, pk):
#     loan = LoanContract.objects.filter(id=pk).select_related('member').first()
#     member = Member.objects.get(id=loan.member.id)

#     if request.method == "POST":
#         TransactionsModelFormset = modelformset_factory(
#             GuarantorsPledge,
#             fields=('guarantor', 'item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
#             extra=1
#         )
#         formset = TransactionsModelFormset(request.POST)
#         if formset.is_valid():
#             for form in formset:

#                 # if form.cleaned_data.get('item_title'):
#                 guarantor = form.save(commit=False)
#                 guarantor.creator = request.user
#                 guarantor.date_created = datetime.datetime.now()
#                 guarantor.save()
#                     # print("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP")

#         return redirect(reverse('loan_details_pos', args=(loan.id,)))

#     TransactionsModelFormset = modelformset_factory(
#     GuarantorsPledge,
#     fields=('guarantor', 'item_title', 'description', 'count' , 'serial_number', 'estimated_value' ),
#     extra=1
#     )
#     formset = TransactionsModelFormset(queryset=GuarantorsPledge.objects.filter(guarantor__contract=loan))
#     for form in formset:
#         form.fields['guarantor'].queryset = LoanGuarantors.objects.filter(contract=loan)
#     context = { 'loan': loan, "formset": formset,  'member': member, 
#     }
#     # return render(request, 'loans/loan_external_guarantors_pledges.html', context)
#     return render(request, 'fosa/loan_external_guarantors_pledges.html', context)




# from loans.models import IndividualsPledge

# @login_required
# def loan_details_pos(request, pk):
#     loan = LoanContract.objects.filter(id=pk).select_related('member').first()
#     member = Member.objects.filter(id=loan.member.id).select_related('group').first()
#     internal_gaurantors = InternalGuarantors.objects.filter(contract=loan)
#     external_pledges = GuarantorsPledge.objects.filter(guarantor__contract=loan).select_related('guarantor')
#     individual_pledges = IndividualsPledge.objects.filter(contract=loan)
#     context = {
#         'member': member,
#         'loan': loan,
#         'internal_gaurantors': internal_gaurantors,
#         'external_pledges': external_pledges,
#         'individual_pledges':individual_pledges
#     }
#     return render(request, 'fosa/loan_details.html', context)


# @login_required
# def loan_guarantors_lock(request, pk):
#     member = Member.objects.get(id=pk)
#     context = {
#         'member': member,
       
#     }
#     return render(request, 'loans/loan_details.html', context)



from financial.models import Revenue, RevenueArchive
from financial.forms import Revenue, RevenuesForm, RevenuesEditForm
#####################################################################
### REVENUES #########################

@login_required
def revenue_detail(request, pk):
    revenue = Revenue.objects.filter(id=pk)
    loc_events = LOCEvent.objects.filter(transaction__revenue__id=revenue.first().id)
    locs = [event.transaction.id for event in loc_events]
    context = {
        'expense': revenue.first(),
        'transactions': Transactions.objects.filter(id__in=locs).select_related('type')
    }
    return render(request, 'fosa/revenues_detail.html', context)


@login_required
def revenue_edit(request, pk):
    revenue = Revenue.objects.filter(id=pk)

    TransactionsModelFormset = modelformset_factory(
    Transactions,
    fields=('amount','description', ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.filter(revenue=revenue.first()))
    

    context = {
        'expense': ExpensesEditForm(instance=expenses.first()),
        'e':  Revenue.objects.filter(id=revenue.first().id).first(),
        'formset': formset
    }
    return render(request, 'fosa/revenues_edit.html', context)



@login_required
def revenue_list(request):
    context = {
        'expenses': Revenue.objects.all().order_by('pv_number'),
    }
    return render(request, 'fosa/revenues_list.html', context)


@login_required
def search_revenue(request):

    if request.method == 'POST':
        term = request.POST.get('search_term')
        context = {
            'expenses': Revenue.objects.filter(Q(received_from__icontains=term)),
        }
        return render(request, 'fosa/revenues_list.html', context)

@login_required
def revenue(request):
    p = Periods.objects.get(id=request.parameters["settings"]["financial_period"]) 
    print(p)

    if request.method == 'GET':
        TransactionsModelFormset = modelformset_factory(
        Transactions,
        fields=('amount','description', 'type'),
        extra=1
        )
        formset = TransactionsModelFormset(queryset=Transactions.objects.none())

        form = ExpensesForm()

        for form in formset:
            form.fields['type'].queryset = Events.objects.filter(type='revenue').order_by('title')

        
        context = {
            'expense_form': form,
            "formset": formset,
            "payments_methods": PaymentMethod.objects.all()
        }
        return render(request, 'fosa/revenues.html', context)


import decimal

@login_required
def process_revenue(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    p = Periods.objects.get(id=request.parameters["settings"]["financial_period"]) 
    TransactionsModelFormset = modelformset_factory(
    Transactions,
    fields=('amount','description', 'type' ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=Transactions.objects.none(),)

    form = RevenuesForm(prefix="sa")

    if request.method == 'POST':
        # event = Events.objects.get(code='expense')


        received_from = request.POST.get('sa-received_from')
        cheque_number = request.POST.get('sa-cheque_number')
        date = request.POST.get('sa-date')
        pv = request.POST.get('sa-pv_number')
        method = request.POST.get('method')

        expense = RevenuesForm(request.POST, prefix="sa")
        expense = Revenue()
        expense.received_from = received_from
        expense.cheque_number = cheque_number
        expense.date = date
        expense.pv_number = pv
        expense.total = 0.00
        expense.date_created = datetime.datetime.now()
        expense.creator = request.user
        expense.save()

        payment_m = PaymentMethod.objects.filter(id=method).select_related('account', 'loc').first()

        if payment_m.is_cash:
            formset = TransactionsModelFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    if form.cleaned_data.get('amount'):
                        event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()
                        transaction = form.save(commit=False)
                        # transaction.type = event
                        transaction.date = expense.date
                        transaction.revenue = expense
                        # transaction.acc = ''
                        transaction.financial_month = month
                        transaction.financial_period = p
                        transaction.creator = request.user
                        transaction.date_created = expense.date
                        transaction.actual_amount = transaction.amount
                        transaction.dr_amount = transaction.amount
                        
                        expense.total = decimal.Decimal(expense.total) + decimal.Decimal(transaction.amount)
                        expense.save()

                        chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                        chart_oa.balance += decimal.Decimal(transaction.amount)
                        chart_oa.save()

                        transaction.coa = chart_oa

                        transaction.save()

                        loc = LOC.objects.filter(id=payment_m.loc.id).select_related('type').first()
                        loc.remainder_amount += Decimal(transaction.amount)
                        loc.save()

                        g_transaction = Transactions()
                        g_transaction.financial_month = month
                        g_transaction.financial_period = year
                        g_transaction.financial_year = p
                        g_transaction.double_entry = 'Cr'
                        g_transaction.type = event
                        g_transaction.action = event
                        g_transaction.description = event.title
                        g_transaction.date = expense.date
                        g_transaction.revenue = expense
                        g_transaction.processed = request.user 
                        g_transaction.amount = transaction.amount
                        g_transaction.dr_amount = transaction.amount
                        g_transaction.payment_method = payment_m
                        g_transaction.actual_amount = transaction.amount
                        g_transaction.revenue = expense
                        g_transaction.creator = request.user
                        g_transaction.date_created =  expense.date
                        g_transaction.save()


                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = g_transaction,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()


                        loc_event = LOCEvent()
                        loc_event.loc = loc
                        loc_event.financial_month = month
                        loc_event.financial_year = p
                        loc_event.transaction = g_transaction
                        loc_event.description = g_transaction.description
                        loc_event.amount = transaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.type = 'Cr'
                        loc_event.creator = request.user
                        loc_event.is_payment = True
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()


                        g_tr = Transactions()
                        g_tr.financial_month = month
                        g_tr.financial_period = year
                        g_tr.financial_year = p
                        g_tr.double_entry = 'Cr'
                        g_tr.type = event
                        g_tr.action = event
                        g_tr.description = event.title
                        g_tr.date = expense.date
                        g_tr.revenue = expense
                        g_tr.processed = request.user 
                        g_tr.amount = transaction.amount
                        g_tr.dr_amount = transaction.amount
                        g_tr.payment_method = payment_m
                        g_tr.actual_amount = transaction.amount
                        g_tr.revenue = expense
                        g_tr.creator = request.user
                        g_tr.date_created =  expense.date
                        g_tr.save()

                        chart_oa = Account.objects.filter(id=payment_m.account.id).first()
                        chart_oa.balance += decimal.Decimal(transaction.amount)
                        chart_oa.save()




                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = g_tr,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()


                        if event.secondary_event is None:
                            pass
                        else:
                            secondary_event = Events.objects.get(id=event.secondary_event.id)

                            

                            if event.is_reducing:
                                chart_oa_secondary = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=secondary_event)).first()
                                chart_oa_secondary.balance -= decimal.Decimal(transaction.amount)
                                chart_oa_secondary.save()


                                coa_event_sec = AccountEvent(
                                    account = chart_oa_secondary,
                                    transaction = g_transaction,
                                    amount = transaction.amount,
                                    coa_balance = chart_oa_secondary.balance,
                                    month = month,
                                    financial_year = year,
                                    date_created = datetime.datetime.now(),
                                    creator = request.user
                                )

                                coa_event_sec.save()

                            else:
                                chart_oa_secondary = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=secondary_event)).first()
                                chart_oa_secondary.balance += decimal.Decimal(transaction.amount)
                                chart_oa_secondary.save()


                                coa_event_sec = AccountEvent(
                                    account = chart_oa_secondary,
                                    transaction = g_transaction,
                                    amount = transaction.amount,
                                    coa_balance = chart_oa_secondary.balance,
                                    month = month,
                                    financial_year = year,
                                    date_created = datetime.datetime.now(),
                                    creator = request.user
                                )

                                coa_event_sec.save()


        if payment_m.is_bank:
            formset = TransactionsModelFormset(request.POST)
            if formset.is_valid():
                for form in formset:
                    if form.cleaned_data.get('amount'):
                        event = Events.objects.filter(id=form.cleaned_data.get('type').id).select_related('secondary_event').first()
                        transaction = form.save(commit=False)
                
                        # transaction.type = event
                        transaction.date = expense.date
                        transaction.revenue = expense
                    
                        # transaction.acc = ''
                        transaction.financial_month = month
                        transaction.financial_period = p
                        transaction.creator = request.user
                        transaction.date_created = expense.date
                        transaction.actual_amount = transaction.amount
                        transaction.dr_amount = transaction.amount
                        transaction.save()
                        

                        chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()
                        chart_oa.balance += decimal.Decimal(transaction.amount)
                        chart_oa.save()

                        transaction.coa = chart_oa
                        transaction.save()
                        
                        # expense.total +=  decimal.Decimal(transaction.amount)
                        expense.total = decimal.Decimal(expense.total) + decimal.Decimal(transaction.amount)
                        expense.save()

                        loc = LOC.objects.filter(id=payment_m.loc.id).select_related('type').first()
                        loc.remainder_amount += Decimal(transaction.amount)
                        loc.save()

                        g_transaction = Transactions()
                        g_transaction.financial_month =month
                        g_transaction.financial_period = year
                        g_transaction.financial_year = p
                        g_transaction.double_entry = 'Cr'
                        g_transaction.type = event
                        g_transaction.action = event
                        # g_transaction.acc = 
                        # g_transaction.slip = 
                        # g_transaction.session =
                        # g_transaction.code = 
                        g_transaction.description = event.title
                        # g_transaction.fees = 
                        # g_transaction.member = 
                        # g_transaction.group = 
                        # g_transaction.receiver = 
                        # g_transaction.receiving_member =
                        g_transaction.date = expense.date
                        g_transaction.processed = request.user 
                        g_transaction.amount = transaction.amount
                        g_transaction.dr_amount = transaction.amount
                        g_transaction.payment_method = payment_m
                        g_transaction.revenue = expense
                        # g_transaction.coa = 
                        g_transaction.actual_amount = transaction.amount
                        g_transaction.creator = request.user
                        g_transaction.date_created =  expense.date
                        g_transaction.save()

                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = g_transaction,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()

                        loc_event = LOCEvent()
                        loc_event.loc = loc
                        loc_event.financial_month = month
                        loc_event.financial_year = p
                        loc_event.transaction = g_transaction
                        loc_event.description = g_transaction.description
                        loc_event.amount = transaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.type = 'Cr'
                        loc_event.creator = request.user
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()



                        g_tr = Transactions()
                        g_tr.financial_month = month
                        g_tr.financial_period = year
                        g_tr.financial_year = p
                        g_tr.double_entry = 'Cr'
                        g_tr.type = event
                        g_tr.action = event
                        g_tr.description = event.title
                        g_tr.date = expense.date
                        g_tr.revenue = expense
                        g_tr.processed = request.user 
                        g_tr.amount = transaction.amount
                        g_tr.dr_amount = transaction.amount
                        g_tr.payment_method = payment_m
                        g_tr.actual_amount = transaction.amount
                        g_tr.revenue = expense
                        g_tr.creator = request.user
                        g_tr.date_created =  expense.date
                        g_tr.save()

                        # if event.type == 'transfer':
                        chart_oa = Account.objects.filter(id=payment_m.account.id).first()
                        chart_oa.balance += decimal.Decimal(transaction.amount)
                        chart_oa.save()


                        coa_event = AccountEvent(
                            account = chart_oa,
                            transaction = g_tr,
                            amount = transaction.amount,
                            coa_balance = chart_oa.balance,
                            month = month,
                            financial_year = year,
                            date_created = datetime.datetime.now(),
                            creator = request.user
                        )

                        coa_event.save()

                        if event.secondary_event is None:
                            pass
                        else:
                            secondary_event = Events.objects.get(id=event.secondary_event.id)


                            if event.is_reducing:
                                chart_oa_secondary = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=secondary_event)).first()
                                chart_oa_secondary.balance -= decimal.Decimal(transaction.amount)
                                chart_oa_secondary.save()


                                coa_event_sec = AccountEvent(
                                    account = chart_oa_secondary,
                                    transaction = g_transaction,
                                    amount = transaction.amount,
                                    coa_balance = chart_oa_secondary.balance,
                                    month = month,
                                    financial_year = year,
                                    date_created = datetime.datetime.now(),
                                    creator = request.user
                                )

                                coa_event_sec.save()

                            else:
                                chart_oa_secondary = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=secondary_event)).first()
                                chart_oa_secondary.balance += decimal.Decimal(transaction.amount)
                                chart_oa_secondary.save()


                                coa_event_sec = AccountEvent(
                                    account = chart_oa_secondary,
                                    transaction = g_transaction,
                                    amount = transaction.amount,
                                    coa_balance = chart_oa_secondary.balance,
                                    month = month,
                                    financial_year = year,
                                    date_created = datetime.datetime.now(),
                                    creator = request.user
                                )

                                coa_event_sec.save()
        return redirect(reverse('revenues_detail', args=(expense.id,)))





@login_required
def adjust_coa_events(request):
    if request.method == "POST":
        action = request.POST.get('action')
        amount = request.POST.get('amount')
        de = request.POST.get('de')

        if de == "Debit":
            e = Events.objects.get(id=action.id)

            if e.is_reducing:
                account = Account.objects.get(transaction_type=e)
                account.balance -= Decimal(amount)
                account.save()

            else:
                account = Account.objects.get(transaction_type=e)
                account.balance += Decimal(amount)
                account.save()

        elif de == "Credit":
            e = Events.objects.get(id=action.id)

            if e.is_reducing:
                account = Account.objects.get(transaction_type=e)
                account.balance += Decimal(amount)
                account.save()

            else:
                account = Account.objects.get(transaction_type=e)
                account.balance -= Decimal(amount)
                account.save()

    context = {
        'events': Events.objects.all() 
    }

    return render(request, 'fosa/adjust.html', context)



@login_required
def recovery(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    if request.method == "POST":
        event = request.POST.get('event')
        method = request.POST.get('method')
        amount = request.POST.get('amount')
        date = request.POST.get('date')
        payment_m = PaymentMethod.objects.filter(id=method).select_related('account', 'loc').first()

        # get coa for event
        event = Events.objects.filter(id=event).first()
        chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()

        #update coa
        chart_oa.balance -= decimal.Decimal(amount)
        chart_oa.save()

        # create group transaction
        g_transaction = GroupTransactions()
        g_transaction.financial_month = month
        g_transaction.financial_year = year
        g_transaction.double_entry = 'Cr'
        g_transaction.type = event
        g_transaction.action = event
        g_transaction.description = event.title
        g_transaction.date = date
        g_transaction.processed = request.user 
        g_transaction.amount = amount
        g_transaction.cr_amount = amount
        g_transaction.payment_method = payment_m
        g_transaction.actual_amount = amount
        g_transaction.creator = request.user
        g_transaction.date_created =  datetime.datetime.now()
        g_transaction.save()


        # create coa event for event
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = g_transaction,
            amount = amount,
            coa_balance = chart_oa.balance,
            month = month,
            financial_year = year,
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()


        loc = LOC.objects.get(id=payment_m.loc.id)
        loc.remainder_amount += Decimal(g_transaction.amount)
        loc.save()

        # create loc event

        transaction = GroupTransactions()
        transaction.financial_month = month
        transaction.financial_year = year
        transaction.double_entry = 'Cr'
        transaction.type = event
        transaction.action = event
        transaction.description = event.title
        transaction.date = date
        transaction.processed = request.user 
        transaction.amount = amount
        transaction.dr_amount = amount
        transaction.payment_method = payment_m
        transaction.actual_amount = amount
        transaction.creator = request.user
        transaction.date_created =  datetime.datetime.now()
        transaction.save()


        loc_event = LOCEvent()
        loc_event.loc = loc
        loc_event.financial_month = month
        loc_event.financial_year = year
        loc_event.transaction = transaction
        loc_event.description = transaction.description
        loc_event.amount = transaction.amount
        loc_event.remainder = loc.remainder_amount
        loc_event.type = 'Cr'
        loc_event.creator = request.user
        loc_event.date_created = datetime.datetime.now()
        loc_event.save()

        chart = Account.objects.filter(id=payment_m.account.id).first()
        chart.balance += decimal.Decimal(transaction.amount)
        chart.save()

        coa_event_p = AccountEvent(
            account = chart,
            transaction = transaction,
            amount = transaction.amount,
            coa_balance = chart_oa.balance,
            month = month,
            financial_year = year,
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event_p.save()


    context = {
        'form': RecoveryForm,
    }


    return render(request, 'fosa/recovery.html', context)





@login_required
def recovery_debit_loc(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])

    if request.method == "POST":
        event = request.POST.get('event')
        method = request.POST.get('method')
        amount = request.POST.get('amount')
        date = request.POST.get('date')
        payment_m = PaymentMethod.objects.filter(id=method).select_related('account', 'loc').first()

        # get coa for event
        event = Events.objects.filter(id=event).first()
        chart_oa = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month) & Q(transaction_type=event)).first()

        #update coa
        chart_oa.balance += decimal.Decimal(amount)
        chart_oa.save()

        # create group transaction
        g_transaction = GroupTransactions()
        g_transaction.financial_month = month
        g_transaction.financial_year = year
        g_transaction.double_entry = 'Cr'
        g_transaction.type = event
        g_transaction.action = event
        g_transaction.description = event.title
        g_transaction.date = date
        g_transaction.processed = request.user 
        g_transaction.amount = amount
        g_transaction.dr_amount = amount
        g_transaction.payment_method = payment_m
        g_transaction.actual_amount = amount
        g_transaction.creator = request.user
        g_transaction.date_created =  datetime.datetime.now()
        g_transaction.save()


        # create coa event for event
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = g_transaction,
            amount = amount,
            coa_balance = chart_oa.balance,
            month = month,
            financial_year = year,
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()


        loc = LOC.objects.get(id=payment_m.loc.id)
        loc.remainder_amount -= Decimal(g_transaction.amount)
        loc.save()

        # create loc event

        transaction = GroupTransactions()
        transaction.financial_month = month
        transaction.financial_year = year
        transaction.double_entry = 'Cr'
        transaction.type = event
        transaction.action = event
        transaction.description = event.title
        transaction.date = date
        transaction.processed = request.user 
        transaction.amount = amount
        transaction.cr_amount = amount
        transaction.payment_method = payment_m
        transaction.actual_amount = amount
        transaction.creator = request.user
        transaction.date_created =  datetime.datetime.now()
        transaction.save()


        loc_event = LOCEvent()
        loc_event.loc = loc
        loc_event.financial_month = month
        loc_event.financial_year = year
        loc_event.transaction = transaction
        loc_event.description = transaction.description
        loc_event.amount = transaction.amount
        loc_event.remainder = loc.remainder_amount
        loc_event.type = 'Cr'
        loc_event.creator = request.user
        loc_event.date_created = datetime.datetime.now()
        loc_event.save()

        chart = Account.objects.filter(id=payment_m.account.id).first()
        chart.balance -= decimal.Decimal(transaction.amount)
        chart.save()

        coa_event_p = AccountEvent(
            account = chart,
            transaction = transaction,
            amount = transaction.amount,
            coa_balance = chart_oa.balance,
            month = month,
            financial_year = year,
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event_p.save()


    context = {
        'form': RecoveryForm,
    }


    return render(request, 'fosa/recovery_debit_loc.html', context)

@login_required
def redeem(request):


    return render(request, 'fosa/redeem.html')


@login_required
def month_year(request):
    accounts = Account.objects.all()

    for ac in accounts:
        ac.year_opening_amount = ac.opening_balance
        ac.save()

    locs = LOC.objects.all()
    for loc in locs:
        loc.year_opening_amount = loc.initial_amount
        loc.save()


    return render(request, 'fosa/adjust.html')





@login_required
def all_accounts(request):

    if request.method == "POST":

        search_term = request.POST.get('search_term')

        context = {
            "accounts": MemberCustomAccount.objects.filter(Q(acc_type__can_create_accounts=True) & Q(member__first_name__icontains=search_term)),
            "g_accounts": GroupCustomAccount.objects.filter(Q(acc_type__can_create_accounts=True) & Q(group__group_name__icontains=search_term)),
            'search_term': search_term
        }

        return render(request, 'fosa/accounts.html', context)


    context = {
        "accounts": [],
        "g_accounts": []
    }

    return render(request, 'fosa/accounts.html', context)





from parameters.models import LOCBanking
from parameters.forms import LOCBankingForm
from financial.models import Transactions, GroupTransactions
from parameters.models import Events, Month, Periods
import decimal

@login_required()
def bankingloc(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    posts_list = LOC.objects.filter(Q(is_cash=True) & Q(is_active=True)).select_related('creator', 'type')
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = LOCBankingForm(request.POST)
        if form.is_valid():
            if posts_list.first().is_cash:
                atype = form.save(commit = False)

                atype.loc = posts_list.first()
                
                atype.creator = request.user
                atype.save()

                event, new = Events.objects.get_or_create(code ='banking')
                if new:
                    event.title = "Banking"
                    event.save()

                tranaction_dep = GroupTransactions()
                tranaction_dep.amount = atype.amount
                tranaction_dep.description = event.title
                tranaction_dep.type = event
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date
                tranaction_dep.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction_dep.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction_dep.date_created = datetime.datetime.now()
                tranaction_dep.code = '978'
                tranaction_dep.double_entry = 'Dr'
                tranaction_dep.actual_amount = atype.amount
                tranaction_dep.dr_amount = atype.amount
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date

                chart_oa = Account.objects.get(Q(transaction_type=event) & Q(financial_year=year) & Q(financial_month=month))
                chart_oa.balance += decimal.Decimal(atype.amount)
                chart_oa.save()

                # coa_event = AccountEvent(
                #     account = chart_oa,
                #     transaction = tranaction_dep,
                #     amount = tranaction_dep.amount,
                #     coa_balance = chart_oa.balance,
                #     month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                #     financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                #     date_created = datetime.datetime.now(),
                #     creator = request.user
                # )
                # coa_event.save()


                loc = LOC.objects.get(Q(is_cash=True) & Q(is_active=True))
                loc.remainder_amount -= tranaction_dep.amount
                loc.save()


                tranaction_dep.loc = loc
                tranaction_dep.save()

                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = atype.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                loc_event = LOCEvent()
                loc_event.loc = loc
                loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                loc_event.transaction = tranaction_dep
                loc_event.description = tranaction_dep.description
                loc_event.amount = tranaction_dep.amount
                loc_event.date = atype.date
                loc_event.remainder = loc.remainder_amount
                loc_event.type = 'Cr'
                loc_event.creator = request.user
                loc_event.date_created = datetime.datetime.now()
                loc_event.save()




                tranaction = GroupTransactions()
                tranaction.amount = atype.amount
                tranaction.description = event.title
                tranaction.type = event
                tranaction.creator = request.user
                tranaction.date = datetime.datetime.now()
                tranaction.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction.date_created = datetime.datetime.now()
                tranaction.cr_amount = atype.amount
                tranaction.code = '978'
                tranaction.double_entry = 'Dr'
                tranaction.actual_amount = atype.amount
                tranaction.cr_amount = atype.amount
                tranaction.creator = request.user
                tranaction.date = atype.date
                tranaction.loc_account = 'bank'

                loc = LOC.objects.filter(Q(is_bank=True) & Q(is_active=True)).select_related('type').first()
                loc.remainder_amount += tranaction.amount
                loc.save()

 

                tranaction.loc = loc
                tranaction.save()


                ev = Events.objects.get(id=loc.type.id)
                chart_ = Account.objects.filter(Q(transaction_type=ev) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_.balance += decimal.Decimal(tranaction_dep.amount)
                chart_.save()
                coa_event = AccountEvent(
                    account = chart_,
                    transaction = tranaction_dep,
                    amount = tranaction_dep.amount,
                    coa_balance = chart_.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                event = LOCEvent()
                event.loc = loc
                event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                event.transaction = tranaction
                event.description = tranaction.description
                event.amount = tranaction.amount
                event.remainder = loc.remainder_amount
                event.loc_account = 'bank'
                event.type = 'Dr'
                event.date = atype.date
                event.creator = request.user
                event.date_created = datetime.datetime.now()
                event.save()


                # original = GroupTransactions.objects.filter(id=posts_list.first().transaction.id).select_related('payment_method').first()
                # method = PaymentMethod.objects.filter(id=original.payment_method.id).select_related('account')
                e = Events.objects.get(id=posts_list.first().type.id)
                chart_oa = Account.objects.filter(Q(transaction_type=e) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_oa.balance -= decimal.Decimal(tranaction_dep.amount)
                chart_oa.save()
                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = tranaction_dep.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()



            if posts_list.first().is_bank:
                atype = form.save(commit = False)

                atype.loc = posts_list.first()
                
                atype.creator = request.user
                atype.save()
                event, new = Events.objects.get_or_create(code ='cash_from_bank')
                if new:
                    event.title = "Cash From Bank"
                    event.save()

                tranaction_dep = GroupTransactions()
                tranaction_dep.amount = atype.amount
                tranaction_dep.description = event.title
                tranaction_dep.type = event
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date
                tranaction_dep.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction_dep.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction_dep.date_created = datetime.datetime.now()
                tranaction_dep.code = '978'
                tranaction_dep.double_entry = 'Cr'
                tranaction_dep.actual_amount = atype.amount
                tranaction_dep.dr_amount = atype.amount
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date

                chart_oa = Account.objects.get(Q(transaction_type=event) & Q(financial_year=year) & Q(financial_month=month))
                chart_oa.balance += decimal.Decimal(atype.amount)
                chart_oa.save()


                # coa_event = AccountEvent(
                #     account = chart_oa,
                #     transaction = tranaction_dep,
                #     amount = tranaction_dep.amount,
                #     coa_balance = chart_oa.balance,
                #     month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                #     financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                #     date_created = datetime.datetime.now(),
                #     creator = request.user
                # )
                # coa_event.save()

                loc = LOC.objects.get(Q(is_bank=True) & Q(is_active=True))
                loc.remainder_amount -= tranaction_dep.amount
                loc.save()

                tranaction_dep.loc = loc
                tranaction_dep.save()

                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = atype.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                loc_event = LOCEvent()
                loc_event.loc = loc
                loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                loc_event.transaction = tranaction_dep
                loc_event.description = tranaction_dep.description
                loc_event.amount = tranaction_dep.amount
                loc_event.date = atype.date
                loc_event.remainder = loc.remainder_amount
                loc_event.type = 'Cr'
                loc_event.creator = request.user
                loc_event.date_created = datetime.datetime.now()
                loc_event.save()

                tranaction = GroupTransactions()
                tranaction.amount = atype.amount
                tranaction.description = event.title
                tranaction.type = event
                tranaction.creator = request.user
                tranaction.date = atype.date
                tranaction.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction.code = '978'
                tranaction.double_entry = 'Dr'
                tranaction.date_created = datetime.datetime.now()
                tranaction.actual_amount = atype.amount
                tranaction.cr_amount = atype.amount
                tranaction.creator = request.user
                tranaction.date = atype.date
                tranaction.loc_account = 'bank'

                loc = LOC.objects.filter(Q(is_cash=True) & Q(is_active=True)).select_related('type').first()
                loc.remainder_amount += tranaction.amount
                loc.save()

                tranaction.loc = loc
                tranaction.save()



                ev = Events.objects.get(id=loc.type.id)
                chart_ = Account.objects.filter(Q(transaction_type=ev) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_.balance += decimal.Decimal(tranaction_dep.amount)
                chart_.save()
                coa_event = AccountEvent(
                    account = chart_,
                    transaction = tranaction,
                    amount = tranaction.amount,
                    coa_balance = chart_.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                event = LOCEvent()
                event.loc = loc
                event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                event.transaction = tranaction
                event.description = tranaction.description
                event.amount = tranaction.amount
                event.remainder = loc.remainder_amount
                event.loc_account = 'bank'
                event.type = 'Dr'
                event.date = atype.date
                event.creator = request.user
                event.date_created = datetime.datetime.now()
                event.save()



                e = Events.objects.get(id=posts_list.first().type.id)
                chart_oa = Account.objects.filter(Q(transaction_type=e) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_oa.balance -= decimal.Decimal(tranaction_dep.amount)
                chart_oa.save()
                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = tranaction_dep.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()


            return redirect(reverse('loc_list'))
    else:
        form = LOCBankingForm()
      
    context = {'form': form, "accounts": posts_list, "events": LOCBanking.objects.filter(loc=posts_list.first()), 'loc':posts_list.first()}
    return render(request, 'fosa/banking.html', context)


from financial.forms import CashFromBankForm
@login_required()
def cash_from_bank(request):
    # month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    # year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    # """
    # :param request:
    # :return: admission form to
    # logged in user.
    # """
    # if request.method == 'POST':
    #     form = CashFromBankForm(request.POST)
    #     if form.is_valid():
    #         cheque_number = request.POST.get('cheque_number')
    #         payment_method = request.POST.get('payment_method')
    #         date = request.POST.get('date')
    #         amount = request.POST.get('amount')

    #         method = PaymentMethod.objects.filter(id=payment_method.id).select_related('loc')

    #         posts_list = LOC.objects.filter(id=method.first().loc.id).select_related('creator', 'type')

    #         event, new = Events.objects.get_or_create(code ='cash_from_bank')
    #         if new:
    #             event.title = "Cash From Bank"
    #             event.save()


    #         loc = LOC.objects.get(Q(is_bank=True) & Q(is_active=True))
    #         loc.remainder_amount -= decimal.Decimal(amount)
    #         loc.save()

    #         tranaction_dep = GroupTransactions()
    #         tranaction_dep.amount = amount
    #         tranaction_dep.description = event.title
    #         tranaction_dep.type = event
    #         tranaction_dep.creator = request.user
    #         tranaction_dep.date = date
    #         tranaction_dep.financial_month = month
    #         tranaction_dep.financial_period = year
    #         tranaction_dep.date_created = datetime.datetime.now()
    #         tranaction_dep.code = '978'
    #         tranaction_dep.double_entry = 'Cr'
    #         tranaction_dep.actual_amount = amount
    #         tranaction_dep.cr_amount = amount
    #         tranaction_dep.creator = request.user
    #         tranaction_dep.date = date
    #         tranaction_dep.loc = loc
    #         tranaction_dep.save()


    #         chart_oa = Account.objects.get(Q(transaction_type=event) & Q(financial_year=year) & Q(financial_month=month))
    #         chart_oa.balance += decimal.Decimal(amount)
    #         chart_oa.save()

    #         coa_event = AccountEvent(
    #             account = chart_oa,
    #             transaction = tranaction_dep,
    #             amount = amount,
    #             coa_balance = chart_oa.balance,
    #             month = month,
    #             financial_year = year,
    #             date_created = datetime.datetime.now(),
    #             creator = request.user
    #         )
    #         coa_event.save()

    #         loc_event = LOCEvent()
    #         loc_event.loc = loc
    #         loc_event.financial_month = month
    #         loc_event.financial_year = year
    #         loc_event.transaction = tranaction_dep
    #         loc_event.description = tranaction_dep.description
    #         loc_event.amount = tranaction_dep.amount
    #         loc_event.date = date
    #         loc_event.remainder = loc.remainder_amount
    #         loc_event.type = 'Cr'
    #         loc_event.creator = request.user
    #         loc_event.date_created = datetime.datetime.now()
    #         loc_event.save()


    #         tranaction_dep = GroupTransactions()
    #         tranaction_dep.amount = amount
    #         tranaction_dep.description = event.title
    #         tranaction_dep.type = event
    #         tranaction_dep.creator = request.user
    #         tranaction_dep.date = date
    #         tranaction_dep.financial_month = month
    #         tranaction_dep.financial_period = year
    #         tranaction_dep.date_created = datetime.datetime.now()
    #         tranaction_dep.code = '978'
    #         tranaction_dep.double_entry = 'Cr'
    #         tranaction_dep.actual_amount = amount
    #         tranaction_dep.dr_amount = amount
    #         tranaction_dep.creator = request.user
    #         tranaction_dep.date = date
    #         tranaction_dep.loc = loc
    #         tranaction_dep.save()

    #         loc = LOC.objects.filter(Q(is_cash=True) & Q(is_active=True)).select_related('type').first()
    #         loc.remainder_amount += tranaction.amount
    #         loc.save()





    #         ev = Events.objects.get(id=loc.type.id)
    #         chart_ = Account.objects.filter(Q(transaction_type=ev) & Q(financial_year=year) & Q(financial_month=month)).first()
    #         chart_.balance += decimal.Decimal(tranaction_dep.amount)
    #         chart_.save()
    #         coa_event = AccountEvent(
    #             account = chart_,
    #             transaction = tranaction,
    #             amount = tranaction.amount,
    #             coa_balance = chart_.balance,
    #             month = month,
    #             financial_year = year,
    #             date_created = datetime.datetime.now(),
    #             creator = request.user
    #         )
    #         coa_event.save()

    #         cash_loc_event = LOCEvent()
    #         cash_loc_event.loc = loc
    #         cash_loc_event.financial_month = month
    #         cash_loc_event.financial_year = year
    #         cash_loc_event.transaction = tranaction
    #         cash_loc_event.description = tranaction.description
    #         cash_loc_event.amount = tranaction.amount
    #         cash_loc_event.remainder = loc.remainder_amount
    #         cash_loc_event.loc_account = 'bank'
    #         cash_loc_event.type = 'Dr'
    #         cash_loc_event.date = atype.date
    #         cash_loc_event.creator = request.user
    #         cash_loc_event.date_created = datetime.datetime.now()
    #         cash_loc_event.save()

    #         tranaction = GroupTransactions()
    #         tranaction.amount = amount
    #         tranaction.description = event.title
    #         tranaction.type = event
    #         tranaction.creator = request.user
    #         tranaction.date = atype.date
    #         tranaction.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    #         tranaction.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    #         tranaction.code = '978'
    #         tranaction.double_entry = 'Dr'
    #         tranaction.date_created = datetime.datetime.now()
    #         tranaction.actual_amount = atype.amount
    #         tranaction.dr_amount = atype.amount
    #         tranaction.creator = request.user
    #         tranaction.date = atype.date
    #         tranaction.loc_account = 'bank'

    #         tranaction.loc = loc
    #         tranaction.save()

    #         e = Events.objects.get(id=posts_list.first().type.id)
    #         chart_oa = Account.objects.filter(Q(transaction_type=e) & Q(financial_year=year) & Q(financial_month=month)).first()
    #         chart_oa.balance -= decimal.Decimal(tranaction_dep.amount)
    #         chart_oa.save()
    #         coa_event = AccountEvent(
    #             account = chart_oa,
    #             transaction = tranaction_dep,
    #             amount = tranaction_dep.amount,
    #             coa_balance = chart_oa.balance,
    #             month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
    #             financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
    #             date_created = datetime.datetime.now(),
    #             creator = request.user
    #         )
    #         coa_event.save()


    #         return redirect(reverse('loc_list'))
    # else:
    form = CashFromBankForm()
      
    context = {'form': form, "accounts": posts_list, "events": LOCBanking.objects.filter(loc=posts_list.first()), 'loc':posts_list.first()}
    return render(request, 'fosa/cash_from_bank.html', context)





from chartsoa.models import DirectLedgerPosting
@login_required
def posting(request):

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
        DirectLedgerPosting,
        fields=('reason', 'affecting', 'amount' , ),
        extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('amount'):
                    reason = form.cleaned_data.get('reason')
                    affect = form.cleaned_data.get('affect')

                    print("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL")
                    print("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL")
                    print(reason)

                    



    TransactionsModelFormset = modelformset_factory(
    DirectLedgerPosting,
    fields=('reason', 'affecting', 'amount' , ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=IndividualsPledge.objects.none())

    context = {
         "formset": formset,  
    }
    return render(request, 'fosa/posting.html', context)




@login_required
def dividend(request):

    if request.method == "POST":
        context = {
        
        }
        return redirect(reverse('dividend_distribution'))
    return render(request, 'fosa/dividend.html')

@login_required
def dividend_distribution(request):
    context = {
       
    }
    return render(request, 'fosa/dividend_distribution.html', context)



@login_required
def petty_cash(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    events = AccountEvent.objects.filter(Q(account__transaction_type__type__iexact='expense') & Q(month=month)).select_related('transaction').order_by('id')

    context = {
        'events':events
       
    }
    return render(request, 'fosa/petty_cash.html', context)


@login_required
def update_loans(request):
    loans = LoanContract.objects.filter(date=None)
    for l in loans:
        loan = LoanContract.objects.get(id=l.id)
        loan.payment_period = 36
        loan.date = '2020-01-01'
        loan.save()
