# Generated by Django 3.0.4 on 2020-09-14 13:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('chartsoa', '0018_auto_20200909_2314'),
        ('fosa', '0018_recovery_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recovery',
            name='method',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='chartsoa.PaymentMethod', verbose_name='Payment Method'),
        ),
    ]
