# Generated by Django 3.0.4 on 2020-06-22 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fosa', '0007_auto_20200622_2053'),
    ]

    operations = [
        migrations.AlterField(
            model_name='groupslips',
            name='sub_total',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='Sub Total Amount Transacted '),
        ),
        migrations.AlterField(
            model_name='groupslips',
            name='total',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=10, verbose_name='Total Amount Transacted '),
        ),
    ]
