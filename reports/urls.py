from django.urls import path
from . import views



urlpatterns = [
     path('active_loans', views.active_loans, name="active_loans"),
     path('active_savings', views.active_savings, name="active_savings" ),
     path('deliquent_loans', views.deliquent, name="deliquent"),
     path('disbursments', views.disbursements, name="disbursments" ),
     path('repayments', views.repayments, name="repayments" ),
     path('surplus_account', views.surplus_loss, name="surplus_account" ),
     path('trial_balance', views.trial, name="trial_balance" ),
     path('cashbook', views.cashbook, name="cashbook" ),
     path('ledgers', views.ledgers, name="ledgers" ),
     path('ledgers/<int:pk>', views.ledger_details, name="ledger_details" ),
     path('balancesheet', views.balancesheet, name="balancesheet" ),
     path('members', views.members, name="members_reports" ),
     path('groups', views.groups, name="groups_reports" ),
]
