from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from .models import Report

class ReportForm(ModelForm):


    class Meta:
        model = Report
        fields = ['financial_year','financial_month', ]
        exclude = ( )
    

    def __init__(self, *args, **kwargs):
        super(ReportForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)

        self.helper.layout = Layout(
            Row(
                Column('financial_year', css_class='form-group col-md-4 mb-0'),
                Column('financial_month', css_class='form-group col-md-4 mb-0'),

                css_class='form-row'
            ),
         
            
        )
