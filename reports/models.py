from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Report(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    # name = models.CharField(_("Line Of Credit"), max_length=50)
