from django.shortcuts import render

from django.db.models import Sum, Aggregate
from django.db.models.functions import Coalesce

from .forms import ReportForm

from parameters.models import LOCArchive, LOCEvent, Month, Periods
from financial.models import GroupTransactions
from loans.models import LoanStatus
from django.contrib import messages


from financial.models import GroupCustomAccount
from chartsoa.models import Account, AccountArchive

def active_loans(request):
    return render(request, 'reports/active_loans.html')

def active_savings(request):
    return render(request, 'reports/active_savings.html')

def repayments(request):
    return render(request, 'reports/repayments.html')

def disbursements(request):
    return render(request, 'reports/disbursments.html')

def deliquent(request):
    return render(request, 'reports/deliequent_loans.html')

def trialbalance(request):
    context = {
        'form': ReportForm,

    }
    return render(request, 'reports/trialbalance.html', context)



def members(request):
    return render(request, 'reports/members.html')

def groups(request):
    context = {
        'form': ReportForm,

    }
    return render(request, 'reports/groups.html', context)

from parameters.models import LOC, LOCArchive, LOCEventArchive





def cashbook(request):
    if request.method == 'POST':
        year = request.POST.get('financial_year')
        month = request.POST.get('financial_month')
        month = Month.objects.get(id=month)
        year = Periods.objects.get(id=year)
        transactions = LOCEvent.objects.filter(financial_month=month).select_related( 'slip', 'payment_method', 'transaction', 'loc').order_by('id')
        try:
            bank = LOC.objects.get(Q(is_bank=True) & Q(financial_month=month) & Q(financial_year=year))
        except LOC.DoesNotExist:
            messages.success(request, 'The Archives necessary to generate a cashook are not available. Possible reason are EOM operations are not done for the specific month')

        try:
            cash = LOC.objects.get(Q(is_cash=True)  & Q(financial_month=month) & Q(financial_year=year))
        except LOC.DoesNotExist:
            messages.success(request, 'The Archives necessary to generate a cashook are not available. Possible reason are EOM operations are not done for the specific month')


        dr_bank_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_bank=True) & Q(cancelled=False)).aggregate(Sum('transaction__dr_amount'))
        cr_bank_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_bank=True) & Q(cancelled=False)).aggregate(Sum('transaction__cr_amount'))
        dr_cash_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_cash=True) & Q(cancelled=True)).aggregate(Sum('transaction__dr_amount'))
        cr_cash_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_cash=True) & Q(cancelled=False)).aggregate(Sum('transaction__cr_amount'))

        context = {
            'form': ReportForm,
            'transactions': transactions,
            'bank': bank,
            'cash': cash,
            'year': year,
            'month': month,
            'dr_bank': dr_bank_total,
            'cr_bank': cr_bank_total,
            'dr_cash': cr_cash_total,
            'cr_cash': dr_cash_total

        }
        return render(request, 'reports/cashbook.html', context)
    context = {
        'form': ReportForm,

    }
    return render(request, 'reports/cashbook.html', context)

def balancesheet(request):
    if request.method == 'POST':
        year = request.POST.get('financial_year')
        month = request.POST.get('financial_month')
        month = Month.objects.get(id=month)
        year = Periods.objects.get(id=year)

        budget = Budget.objects.filter(id=1)[0]
        estimates = Estimates.objects.filter(budget=budget).select_related('event')

        current_assets_events = Events.objects.filter(sub_classification='current_asset')
        current_assets = []

        for event in current_assets_events:
            received = GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount'))
            budgeted = Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first()

            e = {
                'event': event,
                'received': received,
                'budgeted': budgeted,
                'variance': 0.00
            }

            current_assets.append(e)

        fixed_assets_events = Events.objects.filter(sub_classification='fixed_asset')
        fixed_assets = []

        for event in fixed_assets_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            fixed_assets.append(e)



        current_liablities_events = Events.objects.filter(sub_classification='current_liability')
        current_liablities = []

        for event in current_liablities_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            current_liablities.append(e)

        long_liablities_events = Events.objects.filter(sub_classification='fixed_liability')
        long_liablities = []

        for event in long_liablities_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            long_liablities.append(e)

        owners_equity_events = Events.objects.filter(sub_classification='capital')
        owners_equity = []

        for event in owners_equity_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            owners_equity.append(e)
        
        

  

        context = {
            'form': ReportForm,
            'current_assets': current_assets,
            'current_liablities': current_liablities,
            'fixed_assets': fixed_assets,
            'long_liablities': long_liablities,
            'owners_equity': owners_equity,


        }
        return render(request, 'reports/balancesheet.html', context)


    context = {
        'form': ReportForm,

    }
    return render(request, 'reports/balancesheet.html', context)

from decimal import Decimal
from chartsoa.models import Budget, Estimates
from django.db.models import Q
from parameters.models import Events






def ledgers(request):
    if request.method == 'POST':
        year = request.POST.get('financial_year')
        month = request.POST.get('financial_month')
        accounts = Account.objects.filter(Q(financial_month=month) & Q(financial_year=year))
 

        context = {
            'form': ReportForm,
            'accounts': accounts,
 

        }
        return render(request, 'reports/ledgers.html', context)


    context = {
        'form': ReportForm,

    }
    return render(request, 'reports/ledgers.html', context)

from financial.models import GroupTransactionsArchive
from chartsoa.models import AccountEvent

def ledger_details(request, pk):
    account_ = Account.objects.filter(id=pk)
    events = AccountEvent.objects.filter(Q(account=account_.first())).select_related('transaction').order_by('id')
    dr = AccountEvent.objects.filter(Q(account=account_.first())).select_related('transaction').aggregate(Sum('transaction__dr_amount'))
    cr = AccountEvent.objects.filter(Q(account=account_.first())).select_related('transaction').aggregate(Sum('transaction__cr_amount'))

    context={
        'accounts': account_,
        # 'form': form,
        'events': events,
        'pk':pk,
        'dr': dr,
        'cr': cr
    }
    return render(request, 'reports/ledger_detail.html', context)



from decimal import Decimal
from chartsoa.models import Budget, Estimates
from django.db.models import Q
from parameters.models import Events






def trial(request):
    if request.method == 'POST':
        year = request.POST.get('financial_year')
        month = request.POST.get('financial_month')
        month = Month.objects.get(id=month)
        year = Periods.objects.get(id=year)

        income_events = Events.objects.filter(Q(trial_balance_placement='cr') & Q(is_trial_balance_account=True))
        income = []

        income_total = Decimal(0.00)
        for event in income_events:
            account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month)).first()
            try:
                account_balance = account.balance
            except AttributeError:
                account_balance =  Decimal(0.00)

            income_total += account_balance
            e = {
                'event': event,
                'account': account,
                }
            income.append(e)

        expenses_events = Events.objects.filter(Q(trial_balance_placement='dr') & Q(is_trial_balance_account=True))
        expenses = []

        expenses_total = Decimal(0.00)
        for event in expenses_events:
            account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month)).first()
            try:
                account_balance = account.balance
            except AttributeError:
                account_balance =  Decimal(0.00)
            expenses_total += account_balance
            e = {
                'event': event,
                'account': account,
                }
            expenses.append(e)

        context = {
            'form': ReportForm,
            'month': month,
            'year': year,
            'income': income,
            'expenses': expenses,
            'income_total' : income_total,
            'expenses_total' : expenses_total,
        }
        return render(request, 'reports/trial.html', context)

    context = {
        'form': ReportForm,


    }
    return render(request, 'reports/trial.html', context)



def surplus_loss(request):


    if request.method == 'POST':
        print("===========================================================================================================================")
        print("===========================================================================================================================")
        year = request.POST.get('financial_year')
        month = request.POST.get('financial_month')
        month = Month.objects.get(id=month)
        year = Periods.objects.get(id=year)
        month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
        year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
        month = Month.objects.get(id=month.id)
        year = Periods.objects.get(id=year.id)

        budget = Budget.objects.get(financial_year=year)

        total = Decimal(0.00)
        income_events = Events.objects.filter(is_income=True)

        incomes_list = []
        income_budget_total = Decimal(0.00)
        income_variance_total = Decimal(0.00)
        income_total = Decimal(0.00)

        for event in income_events:
            account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month) & Q(financial_year=year)).first()
            estimate = Estimates.objects.filter(event=event).first()

            try:
                estimated_amount = estimate.estimate 
            except AttributeError:
                estimated_amount =  Decimal(0.00)

            try:
                account_opening_balance = account.opening_balance
            except AttributeError:
                account_opening_balance = Decimal(0.00)

            try:
                account_balance = account.balance
            except AttributeError:
                account_balance =  Decimal(0.00)

            print(account_balance - account_opening_balance)
            income_total += account_balance
            income_budget_total += estimated_amount
            income_variance_total += estimated_amount - (account_balance - account_opening_balance)

            e = {
                'event': event,
                'account': account,
                'estimate': estimated_amount,
                'balance': account_balance - account_opening_balance,
                'variance': estimated_amount - (account_balance - account_opening_balance)
                }
            incomes_list.append(e)


        expenses_events = Events.objects.filter(is_expense=True)

        expenses = []
        expenses_variance_total = Decimal(0.00)
        expenses_budget_total = Decimal(0.00)

        expenses_total = Decimal(0.00)
        for event in expenses_events:
            account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month) & Q(financial_year=year)).first()
            estimate = Estimates.objects.filter(event=event).first()

            try:
                estimated_amount = estimate.estimate 
            except AttributeError:
                estimated_amount =  Decimal(0.00)

            try:
                account_opening_balance = account.opening_balance
            except AttributeError:
                account_opening_balance =  Decimal(0.00)

            try:
                account_balance = account.balance
            except AttributeError:
                account_balance =  Decimal(0.00)

            expenses_total += (account_balance - account_opening_balance)
            expenses_variance_total += estimated_amount - (account_balance - account_opening_balance)
            expenses_budget_total += estimated_amount

            print(account_balance - account_opening_balance)

            e = {
                'event': event,
                'account': account,
                'estimate': estimated_amount,
                'balance': account_balance - account_opening_balance,
                'variance': estimated_amount - (account_balance - account_opening_balance)

                }
            expenses.append(e)

        context = {
            'form': ReportForm,
            'budget': budget,
            'month': month,
            'year': year,
            'income_list': incomes_list,
            'expenses': expenses,
            'income_total' : income_total,
            'expenses_total' : expenses_total,
            'total': total,
            'budget': budget,
            'month': month,
            'year': year,
            'income': incomes_list,
            'expenses': expenses,
            'income_variance_total' : income_variance_total,
            'income_budget_total' : income_budget_total,
            'income_total' : income_total,
            'expenses_variance_total' : expenses_variance_total,
            'expenses_budget_total' : expenses_budget_total,
            'expenses_total' : expenses_total,
        }
        return render(request, 'reports/surplus&loss.html', context)

    context = {
        'form': ReportForm,


    }
    return render(request, 'reports/surplus&loss.html', context)