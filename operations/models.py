from django.db import models
from django.utils.translation import ugettext_lazy as _


class EoD(models.Model):
    date = models.DateField(_("Date"), auto_now=False, auto_now_add=False)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING)
    description = models.TextField(_("Description"))
    date_run = models.DateTimeField(_("Date Run"), auto_now=True, auto_now_add=False)
    is_rerun = models.BooleanField(_("Has Been Rerun"))
    number_of_reruns = models.IntegerField(_("Number of Reruns"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Owner Creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Owner Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

class EoDReport(models.Model):
    day = models.DateTimeField(_("Date Of EOD"), auto_now=False, auto_now_add=False, null=True, blank=True)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Period"), on_delete=models.DO_NOTHING)

class EoM(models.Model):
    date = models.DateField(_("Date"), auto_now=True, auto_now_add=False)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING)
    month = models.ForeignKey("parameters.Month", verbose_name=_("Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    description = models.TextField(_("Description"), null=True, blank=True)
    date_run = models.DateTimeField(_("Date Run"), auto_now=True, auto_now_add=False)
    is_rerun = models.BooleanField(_("Has Been Rerun"), default=False)
    number_of_reruns = models.IntegerField(_("Number of Reruns"), default=0)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Owner Creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING,  null=True, blank=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Owner Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

class EoMReport(models.Model):
    month = models.CharField(_("Month"), max_length=50)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Period"), on_delete=models.DO_NOTHING)

class EoY(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING)
    description = models.TextField(_("Description"))
    date_run = models.DateTimeField(_("Date Run"), auto_now=True, auto_now_add=False)
    is_rerun = models.BooleanField(_("Has Been Rerun"))
    number_of_reruns = models.IntegerField(_("Number of Reruns"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Owner Creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Owner Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


class EoYReport(models.Model):
    month = models.CharField(_("Month"), max_length=50)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Period"), on_delete=models.DO_NOTHING)

