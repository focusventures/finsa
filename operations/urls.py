from django.urls import path
from . import views



urlpatterns = [
     path('eod', views.eod, name="eod"),
     path('eom', views.eom, name="eom" ),
     path('eom_current', views.eom_current, name="eom_current" ),
     path('eom_custom', views.eom_custom, name="eom_custom" ),
     path('eom_analysis/<int:pk>', views.eom_analysis, name="eom_analysis" ),
     path('eoy', views.eoy, name="eoy"),


]
