from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from operations.models import EoD, EoM, EoY 

class EoDForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))


    class Meta:
        model = EoD
        fields = ['date', 'financial_year', 'description',]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(EoDForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('date', css_class='form-group col-md-4 mb-0'),
             
                css_class='form-row'
            ),
            Row(
                Column('financial_year', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

            Row(
                Column('description', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
           
            Row(
                Submit('submit', 'Run', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )




class EoMForm(ModelForm):

    class Meta:
        model = EoM
        fields = [ 'financial_year', 'month', 'description',]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(EoMForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('month', css_class='form-group col-md-9 mb-0'),
             
                css_class='form-row'
            ),
            Row(
                Column('financial_year', css_class='form-group col-md-9 mb-0'),
              
                css_class='form-row'
            ),

            Row(
                Column('description', css_class='form-group col-md-9 mb-0'),
              
                css_class='form-row'
            ),
           
            Row(
                Submit('submit', 'Run', css_class="btn btn-info btn-block m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )

