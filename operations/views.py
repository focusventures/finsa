from django.shortcuts import render
from django.db.models import Q

from financial.models import Transactions
from parameters.models import SystemParameters, Month, Periods

from operations.models import EoD, EoM, EoY 
from operations.forms import EoDForm, EoMForm
from django.db import connection, transaction
from django.contrib import messages


from parameters.models import LOC, LOCEvent, LOCArchive
from django.db.models import Sum, Aggregate
from chartsoa.models import Account, AccountArchive, PaymentMethod

from financial.models import Expense, ExpenseArchive

from parameters.models import LOC, LOCArchive, LOCEvent, LOCEventArchive
from parameters.models import Events

def eod(request):
    form = EoDForm()

    context = {
        "form": form
    }

    return render(request, 'operations/eod.html', context)

def eom(request):
    system_settings = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()

    eoms = EoM.objects.all()
    context = {
        "eoms": eoms,
        "system": system_settings,
        "eomform": EoMForm(),
    } 

    return render(request, 'operations/eom.html', context)


import datetime
def eom_current(request):
    system_settings = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()

    # transactions = Transactions.objects.filter(Q(financial_year__id=system_settings.financial_period.id) & Q(financial_month__id=system_settings.financial_month.id))


    eom = EoM()
    eom.financial_year = system_settings.financial_period
    eom.month = system_settings.financial_month
    eom.save()

    previous_month = Month.objects.get(id=system_settings.financial_month.id)
    previous_year = Periods.objects.get(id=system_settings.financial_period.id)


    if system_settings.financial_month.name == "January":
        month, created = Month.objects.get_or_create(name="February")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "February":
        month, created = Month.objects.get_or_create(name="March")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "March":
        month, created = Month.objects.get_or_create(name="April")
        system_settings.financial_month = month
    
    elif system_settings.financial_month.name == "April":
        month, created = Month.objects.get_or_create(name="May")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "May":
        month, created = Month.objects.get_or_create(name="June")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "June":
        month, created = Month.objects.get_or_create(name="July")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "July":
        month, created = Month.objects.get_or_create(name="August")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "August":
        month, created = Month.objects.get_or_create(name="September")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "September":
        month, created = Month.objects.get_or_create(name="Octomber")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "Octomber":
        month, created = Month.objects.get_or_create(name="November")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "November":
        month, created = Month.objects.get_or_create(name="December")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "December":
        month, created = Month.objects.get_or_create(name="January")
        system_settings.financial_month = month
    

        year = Periods.objects.get_or_create(

        )

        system_settings.financial_year = year

    system_settings.save()

    system_settings = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()


    new_month = Month.objects.get(id=system_settings.financial_month.id)
    new_year = Periods.objects.get(id=system_settings.financial_period.id)


    locs = LOC.objects.filter(Q(financial_month=previous_month) & Q(financial_year=previous_year))
    for loc in locs:
        loc_arc = LOC()
        loc_arc.financial_month = new_month
        loc_arc.financial_year = new_year
        loc_arc.name = loc.name
        loc_arc.type = loc.type
        loc_arc.code = loc.code
        loc_arc.currency = loc.currency
        loc_arc.year_opening_amount = loc.year_opening_amount
        loc_arc.initial_amount = loc.remainder_amount
        loc_arc.remainder_amount = loc.remainder_amount
        loc_arc.anticipated_remainder_amount = loc.anticipated_remainder_amount
        loc_arc.date = loc.date
        loc_arc.is_bank = loc.is_bank
        loc_arc.is_cash = loc.is_cash
        loc_arc.is_active = True
        loc_arc.date_created = datetime.datetime.now()
        loc_arc.creator = request.user
        loc_arc.date_modified = datetime.datetime.now()
        loc_arc.modified_by = request.user
        loc_arc.save()

        loc.is_active = False
        loc.save()


    account = Account.objects.filter(Q(financial_month=previous_month) & Q(financial_year=previous_year))
    for ac in account:
        acc_arc = Account()
        acc_arc.financial_month = new_month
        acc_arc.financial_year = new_year
        acc_arc.parent = ac.parent
        acc_arc.name = ac.name
        acc_arc.account_number = ac.account_number
        acc_arc.type = ac.type
        acc_arc.transaction_type = ac.transaction_type
        acc_arc.description = ac.description
        acc_arc.year_opening_amount = ac.year_opening_amount
        acc_arc.balance = ac.balance
        acc_arc.opening_balance = ac.balance

        acc_arc.date_created = datetime.datetime.now()
        acc_arc.creator = request.user
        acc_arc.date_modified = datetime.datetime.now()
        acc_arc.modified_by = request.user
        acc_arc.save()

    methods = PaymentMethod.objects.filter(id__gt=0).select_related('loc')
    for method in methods:
        old_l = LOC.objects.filter(id=method.loc.id).select_related('type').first()
        event = Events.objects.get(id=old_l.type.id)
        loc = LOC.objects.filter(Q(financial_month=new_month) & Q(financial_year=new_year) & Q(type=event)).first()
        account = Account.objects.filter(Q(financial_month=new_month) & Q(financial_year=new_year) & Q(transaction_type=event)).first()
        method.account = account
        method.loc = loc
        method.save()




    return render(request, 'operations/eom_results.html')









def eom_custom(request):
    system_settings = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()

    if request.method == "POST":
        form = EoMForm(request.POST)
        form.save()

    eoms = EoM.objects.all()

    context = {
        "eoms": eoms,
        "system": system_settings,
        "eomform": EoMForm(),
    }



    return render(request, 'operations/eom_results.html', context)

from financial.models import Expense
from parameters.models import LOC, LOCArchive, LOCEventArchive


def eom_analysis(request, pk):
    eom = EoM.objects.filter(id=pk).select_related('month', 'financial_year').first()
    # transactions = Transactions.objects.filter(Q(financial_month__id=eom.month.id) & Q(financial_year__id=eom.financial_year.id))
    month = Month.objects.get(id=eom.month.id)
    year = Periods.objects.get(id=eom.financial_year.id)

    loc_archives = LOCArchive.objects.filter(Q(financial_month=month))
    # transactions = Transactions.objects.filter(Q(amount__gte = 0) &  Q(fees__isnull=False)).select_related('fees', 'coa', 'type')
    summary = Transactions.objects.filter(Q(financial_month__id=eom.month.id) & Q(financial_year__id=eom.financial_year.id))
    accounts = Account.objects.all().select_related('type').select_related('creator')
    account_ = LOC.objects.all()
    events =  LOCEvent.objects.filter(Q(financial_month=eom.month.id) & Q(financial_year=eom.financial_year.id)).select_related('transaction')
    expenses = Expense.objects.filter(Q(financial_month=eom.month.id) & Q(financial_year=eom.financial_year.id))


    transactions = LOCEvent.objects.filter(Q(financial_month=month)).select_related( 'slip', 'payment_method', 'transaction', 'loc')
    bank = None
    cash = None
    try:
        bank = LOCArchive.objects.get(Q(is_bank=True) & Q(financial_month=month))
    except LOCArchive.DoesNotExist:
        messages.success(request, 'The Archives necessary to generate a cashook are not available. Possible reason are EOM operations are not done for the specific month')

        

    try:
        cash = LOCArchive.objects.get(Q(is_cash=True)  & Q(financial_month=month))
    except LOCArchive.DoesNotExist:
        messages.success(request, 'The Archives necessary to generate a cashook are not available. Possible reason are EOM operations are not done for the specific month')


    dr_bank_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_bank=True)).aggregate(Sum('transaction__dr_amount'))
    cr_bank_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_bank=True)).aggregate(Sum('transaction__cr_amount'))
    dr_cash_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_cash=True)).aggregate(Sum('transaction__dr_amount'))
    cr_cash_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_cash=True)).aggregate(Sum('transaction__cr_amount'))
 

    ledgers = AccountArchive.objects.filter(Q(financial_month=month))
   
    context = {
        "eom": eom,
        "summary": summary,
        # "transactions": transactions,
        "accounts": accounts,
        'loc': account_,
        'expenses': expenses,
        'events': events,
        'transactions': transactions,
        'bank': bank,
        'cash': cash,
        'year': year,
        'month': month,
        'dr_bank': dr_bank_total,
        'cr_bank': cr_bank_total,
        'dr_cash': cr_cash_total,
        'cr_cash': dr_cash_total,
        'ledgers': ledgers,
        'loc_archives': loc_archives

    } 
    return render(request, 'operations/eom_results.html', context)


def eoy(request):
    return render(request, 'operations/eoy.html')



def generate_pdf(request, pk, type, redirect):
    return render(request, 'operations/eoy.html')
    


def eom_current_(request):
    system_settings = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()

    # transactions = Transactions.objects.filter(Q(financial_year__id=system_settings.financial_period.id) & Q(financial_month__id=system_settings.financial_month.id))


    eom = EoM()
    eom.financial_year = system_settings.financial_period
    eom.month = system_settings.financial_month

    eom.save()



    if system_settings.financial_month.name == "January":
        month, created = Month.objects.get_or_create(name="February")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "February":
        month, created = Month.objects.get_or_create(name="March")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "March":
        month, created = Month.objects.get_or_create(name="April")
        system_settings.financial_month = month
    
    elif system_settings.financial_month.name == "April":
        month, created = Month.objects.get_or_create(name="May")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "May":
        month, created = Month.objects.get_or_create(name="June")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "June":
        month, created = Month.objects.get_or_create(name="July")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "July":
        month, created = Month.objects.get_or_create(name="August")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "August":
        month, created = Month.objects.get_or_create(name="September")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "September":
        month, created = Month.objects.get_or_create(name="Octomber")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "Octomber":
        month, created = Month.objects.get_or_create(name="November")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "November":
        month, created = Month.objects.get_or_create(name="December")
        system_settings.financial_month = month

    elif system_settings.financial_month.name == "December":
        month, created = Month.objects.get_or_create(name="January")
        system_settings.financial_month = month
    

        year = Periods.objects.get_or_create(

        )

        system_settings.financial_year = year

    system_settings.save()

    system_settings = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()

    locs = LOC.objects.all()
    for loc in locs:
        loc_arc = LOCArchive()
        loc_arc.financial_month = loc.financial_month
        loc_arc.financial_year = loc.financial_year
        loc_arc.name = loc.name
        loc_arc.loc = loc
        loc_arc.code = loc.code
        loc_arc.currency = loc.currency
        loc_arc.initial_amount = loc.initial_amount
        loc_arc.remainder_amount = loc.remainder_amount
        loc_arc.anticipated_remainder_amount = loc.anticipated_remainder_amount
        loc_arc.date = loc.date
        loc_arc.is_bank = loc.is_bank
        loc_arc.is_cash = loc.is_cash
        loc_arc.date_created = loc.date_created
        loc_arc.creator = loc.creator
        loc_arc.date_modified = loc.date_modified
        loc_arc.modified_by = loc.modified_by
        loc_arc.save()

        ##loc events to archive

        loc.financial_year = Periods.objects.get(id=system_settings.financial_period.id)
        loc.financial_month = Month.objects.get(id=system_settings.financial_month.id)
        loc.initial_amount = loc.remainder_amount
        loc.save()

    account = Account.objects.all()
    for ac in account:
        acc_arc = AccountArchive()
        acc_arc.financial_month = ac.financial_month
        acc_arc.financial_year = ac.financial_year
        acc_arc.parent = ac.parent
        acc_arc.name = ac.name
        acc_arc.account_number = ac.account_number
        acc_arc.type = ac.type
        acc_arc.transaction_type = ac.transaction_type
        acc_arc.description = ac.description
        acc_arc.balance = ac.balance
        acc_arc.opening_balance = ac.opening_balance
        acc_arc.date_created = ac.date_created
        acc_arc.creator = ac.creator
        acc_arc.date_modified = ac.date_modified
        acc_arc.modified_by = ac.modified_by
        acc_arc.save()


        #get loc archive event
        ###Account Events to archive

        ac.opening_balance = ac.opening_balance
        ac.financial_year = Periods.objects.get(id=system_settings.financial_period.id)
        ac.financial_month = Month.objects.get(id=system_settings.financial_month.id)
        ac.opening_balance = ac.balance
        ac.balance = ac.balance
        ac.save()







    return render(request, 'operations/eom_results.html')




