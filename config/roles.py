from rolepermissions.roles import AbstractUserRole

class Admin(AbstractUserRole):
    available_permissions = {
        'create_medical_record': True,
    }

class Data(AbstractUserRole):
    available_permissions = {
        'edit_patient_file': True,
    }

class Accounts(AbstractUserRole):
    available_permissions = {
        'edit_patient_file': True,
    }

class Manager(AbstractUserRole):
    available_permissions = {
        'edit_patient_file': True,
    }


class AssistantManager(AbstractUserRole):
    available_permissions = {
        'edit_patient_file': True,
    }