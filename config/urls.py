"""crudlehub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

import notifications.urls

from main import views
from main.admin import event_admin_site
from home.views import index

admin.site.site_header = "FinSA Admin"
admin.site.site_title = "FinSA Admin Portal"
admin.site.index_title = "Welcome to FinSA Admin Portal"
urlpatterns = [
    
    path('', index, name="index" ),
    path('auth/', include("authentication.urls")),
    path('settings/', include("settings.urls")),
    path('dashboard/', include("dashboard.urls")),
    path('entities/members/', include("members.urls")),
    path('loans/', include("loans.urls")),
    path('savings/', include("savings.urls")),
    path('dashboard/locations/', include("locations.urls")),
    path('accounts/', include("chartsoa.urls")),
    path('products/', include("products.urls")),
    path('extras/', include("extras.urls")),
    path('security/', include("security.urls")),
    path('hr/', include("hr.urls")),
    path('shares/', include("shares.urls")),
    path('data/', include("data.urls")),
    path('sms/', include("sms.urls")),
    path('reports/', include("reports.urls")),
    path('operations/', include("operations.urls")),
    path('projects/', include("projects.urls")),
    path('financial/', include("financial.urls")),
    path('pos/', include("fosa.urls")),
    path('structure/', include("ostructure.urls")),
    path('admin/', admin.site.urls),
    path('super-admin/', event_admin_site.urls),
    path('notifications/', include(notifications.urls, namespace='notifications')),
    path('api-auth/', include('rest_framework.urls'))
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)







urlpatterns  += [
    path('customer-api/', include('api.urls')),
    path('fosa-api/', include('fosa_api.urls')),
]