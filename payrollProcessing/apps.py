from django.apps import AppConfig


class PayrollprocessingConfig(AppConfig):
    name = 'payrollProcessing'
