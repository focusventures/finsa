# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Accounts(models.Model):
    account_number = models.CharField(primary_key=True, max_length=32)
    label = models.CharField(max_length=256)
    is_debit = models.BooleanField(blank=True, null=True)
    is_balance_account = models.BooleanField(blank=True, null=True)
    can_be_negative = models.BooleanField(blank=True, null=True)
    id_category = models.ForeignKey('Categories', models.DO_NOTHING, db_column='id_category', blank=True, null=True)
    parent = models.ForeignKey('self', models.DO_NOTHING, db_column='parent', blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    close_date = models.DateTimeField(blank=True, null=True)
    type = models.SmallIntegerField(blank=True, null=True)
    is_direct = models.BooleanField(blank=True, null=True)
    lft = models.IntegerField(blank=True, null=True)
    rgt = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Accounts'


class Accrualinterestloanevents(models.Model):
    id = models.ForeignKey('Contractevents', models.DO_NOTHING, db_column='id')
    interest = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'AccrualInterestLoanEvents'


class Actionitems(models.Model):
    class_name = models.CharField(max_length=50)
    method_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'ActionItems'


class Advancedfields(models.Model):
    entity = models.ForeignKey('Advancedfieldsentities', models.DO_NOTHING)
    type = models.ForeignKey('Advancedfieldstypes', models.DO_NOTHING)
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=1000)
    is_mandatory = models.BooleanField()
    is_unique = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'AdvancedFields'


class Advancedfieldscollections(models.Model):
    field = models.ForeignKey(Advancedfields, models.DO_NOTHING)
    value = models.CharField(max_length=100)
    id = models.AutoField()

    class Meta:
        managed = False
        db_table = 'AdvancedFieldsCollections'


class Advancedfieldsentities(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'AdvancedFieldsEntities'


class Advancedfieldslinkentities(models.Model):
    link_type = models.CharField(max_length=1)
    link_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'AdvancedFieldsLinkEntities'


class Advancedfieldstypes(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'AdvancedFieldsTypes'


class Advancedfieldsvalues(models.Model):
    entity_field = models.ForeignKey(Advancedfieldslinkentities, models.DO_NOTHING)
    field = models.ForeignKey(Advancedfields, models.DO_NOTHING)
    value = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'AdvancedFieldsValues'


class Alertsettings(models.Model):
    parameter = models.CharField(max_length=20)
    value = models.CharField(max_length=5)

    class Meta:
        managed = False
        db_table = 'AlertSettings'


class Allowedroleactions(models.Model):
    action_item = models.OneToOneField('self', models.DO_NOTHING, primary_key=True)
    role = models.ForeignKey('Roles', models.DO_NOTHING)
    allowed = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'AllowedRoleActions'
        unique_together = (('action_item', 'action_item', 'role', 'role'),)


class Allowedrolemenus(models.Model):
    menu_item_id = models.IntegerField(primary_key=True)
    role = models.ForeignKey('Roles', models.DO_NOTHING)
    allowed = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'AllowedRoleMenus'
        unique_together = (('menu_item_id', 'role'),)


class Amountcycles(models.Model):
    cycle = models.OneToOneField('Cycles', models.DO_NOTHING, primary_key=True)
    number = models.IntegerField()
    amount_min = models.DecimalField(max_digits=19, decimal_places=4)
    amount_max = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'AmountCycles'
        unique_together = (('cycle', 'number'),)


class Banks(models.Model):
    address = models.CharField(max_length=200, blank=True, null=True)
    bic = models.CharField(db_column='BIC', max_length=50, blank=True, null=True)  # Field name made lowercase.
    iban1 = models.CharField(db_column='IBAN1', max_length=100, blank=True, null=True)  # Field name made lowercase.
    iban2 = models.CharField(db_column='IBAN2', max_length=100, blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(max_length=50, blank=True, null=True)
    customiban1 = models.BooleanField(db_column='customIBAN1', blank=True, null=True)  # Field name made lowercase.
    customiban2 = models.BooleanField(db_column='customIBAN2', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Banks'


class Booking(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    debitaccount = models.ForeignKey(Accounts, models.DO_NOTHING, db_column='DebitAccount', blank=True, null=True)  # Field name made lowercase.
    creditaccount = models.ForeignKey(Accounts, models.DO_NOTHING, db_column='CreditAccount', blank=True, null=True)  # Field name made lowercase.
    amount = models.DecimalField(db_column='Amount', max_digits=25, decimal_places=15, blank=True, null=True)  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
    loaneventid = models.IntegerField(db_column='LoanEventId')  # Field name made lowercase.
    savingeventid = models.IntegerField(db_column='SavingEventId')  # Field name made lowercase.
    loanid = models.IntegerField(db_column='LoanId', blank=True, null=True)  # Field name made lowercase.
    clientid = models.IntegerField(db_column='ClientId', blank=True, null=True)  # Field name made lowercase.
    userid = models.IntegerField(db_column='UserId', blank=True, null=True)  # Field name made lowercase.
    branchid = models.IntegerField(db_column='BranchId', blank=True, null=True)  # Field name made lowercase.
    advanceid = models.IntegerField(db_column='AdvanceId', blank=True, null=True)  # Field name made lowercase.
    staffid = models.IntegerField(db_column='StaffId', blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(db_column='Description', max_length=200, blank=True, null=True)  # Field name made lowercase.
    isexported = models.BooleanField(db_column='IsExported')  # Field name made lowercase.
    isdeleted = models.BooleanField(db_column='IsDeleted')  # Field name made lowercase.
    ismanualeditable = models.BooleanField(db_column='IsManualEditable', blank=True, null=True)  # Field name made lowercase.
    doc1 = models.CharField(max_length=255, blank=True, null=True)
    doc2 = models.CharField(max_length=255, blank=True, null=True)
    canceldate = models.DateField(db_column='CancelDate', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Booking'


class Bouncefeeaccrualevents(models.Model):
    id = models.OneToOneField('Contractevents', models.DO_NOTHING, db_column='id', primary_key=True)
    bounce_fee = models.DecimalField(max_digits=19, decimal_places=4)
    installment_number = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'BounceFeeAccrualEvents'


class Bouncewriteoffevents(models.Model):
    id = models.OneToOneField('Contractevents', models.DO_NOTHING, db_column='id', primary_key=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    installment_number = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'BounceWriteOffEvents'


class Branches(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    deleted = models.BooleanField()
    code = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Branches'


class Categories(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    parentid = models.IntegerField(db_column='ParentId', blank=True, null=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=128, blank=True, null=True)  # Field name made lowercase.
    lft = models.IntegerField(blank=True, null=True)
    rgt = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Categories'







class Collateralproducts(models.Model):
    name = models.CharField(unique=True, max_length=100)
    desc = models.CharField(max_length=1000)
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'CollateralProducts'


class Collateralproperties(models.Model):
    product = models.ForeignKey(Collateralproducts, models.DO_NOTHING)
    type = models.ForeignKey('Collateralpropertytypes', models.DO_NOTHING)
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=1000)

    class Meta:
        managed = False
        db_table = 'CollateralProperties'


class Collateralpropertycollections(models.Model):
    property = models.ForeignKey(Collateralproperties, models.DO_NOTHING)
    value = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'CollateralPropertyCollections'


class Collateralpropertytypes(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'CollateralPropertyTypes'


class Collateralpropertyvalues(models.Model):
    contract_collateral = models.ForeignKey('Collateralslinkcontracts', models.DO_NOTHING)
    property = models.ForeignKey(Collateralproperties, models.DO_NOTHING)
    value = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CollateralPropertyValues'


class Collateralslinkcontracts(models.Model):
    contract = models.ForeignKey('Contracts', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'CollateralsLinkContracts'


class Consolidateddata(models.Model):
    branch = models.CharField(max_length=20)
    date = models.DateTimeField()
    olb = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    par = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    number_of_clients = models.IntegerField(blank=True, null=True)
    number_of_contracts = models.IntegerField(blank=True, null=True)
    disbursements_amount = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    disbursements_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    repayments_principal = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    repayments_interest = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    repayments_commissions = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    repayments_penalties = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ConsolidatedData'
        unique_together = (('branch', 'date'),)






class Customfields(models.Model):
    caption = models.CharField(max_length=255)
    type = models.CharField(max_length=20)
    owner = models.CharField(max_length=20)
    tab = models.CharField(max_length=255)
    unique = models.BooleanField()
    mandatory = models.BooleanField()
    is_copied = models.BooleanField()
    order = models.IntegerField()
    extra = models.TextField(blank=True, null=True)
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'CustomFields'


class Customfieldsvalues(models.Model):
    field = models.ForeignKey(Customfields, models.DO_NOTHING, blank=True, null=True)
    owner_id = models.IntegerField()
    value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CustomFieldsValues'





class Followup(models.Model):
    project = models.ForeignKey('Projects', models.DO_NOTHING)
    year = models.IntegerField()
    ca = models.DecimalField(db_column='CA', max_digits=19, decimal_places=4)  # Field name made lowercase.
    jobs1 = models.IntegerField(db_column='Jobs1')  # Field name made lowercase.
    jobs2 = models.IntegerField(db_column='Jobs2')  # Field name made lowercase.
    personalsituation = models.CharField(db_column='PersonalSituation', max_length=50)  # Field name made lowercase.
    activity = models.CharField(max_length=50)
    comment = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'FollowUp'


class Fuseboxlogs(models.Model):
    fuse_name = models.CharField(max_length=50)
    started_at = models.DateTimeField()
    ended_at = models.DateTimeField()
    error_message = models.TextField(blank=True, null=True)
    stack_trace = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FuseboxLogs'


class Generalparameters(models.Model):
    key = models.CharField(primary_key=True, max_length=50)
    value = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'GeneralParameters'





class Housingsituation(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'HousingSituation'


class Info(models.Model):
    ceo = models.CharField(max_length=50, blank=True, null=True)
    accountant = models.CharField(max_length=50, blank=True, null=True)
    mfi = models.CharField(max_length=50, blank=True, null=True)
    branch = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(db_column='City', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cashier = models.CharField(max_length=50, blank=True, null=True)
    branchmanager = models.CharField(max_length=50, blank=True, null=True)
    branchadress = models.CharField(max_length=50, blank=True, null=True)
    bik = models.CharField(db_column='BIK', max_length=50, blank=True, null=True)  # Field name made lowercase.
    inn = models.CharField(db_column='INN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    an = models.CharField(db_column='AN', max_length=50, blank=True, null=True)  # Field name made lowercase.
    branchlicense = models.CharField(db_column='BranchLicense', max_length=100, blank=True, null=True)  # Field name made lowercase.
    la = models.CharField(db_column='LA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    superviser = models.CharField(db_column='Superviser', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Info'




class Linkguarantorcredit(models.Model):
    tiers = models.ForeignKey('Tiers', models.DO_NOTHING)
    contract = models.ForeignKey(Contracts, models.DO_NOTHING)
    guarantee_amount = models.DecimalField(max_digits=19, decimal_places=4)
    guarantee_desc = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'LinkGuarantorCredit'



class Mfi(models.Model):
    id = models.AutoField()
    name = models.CharField(max_length=55)
    login = models.CharField(max_length=55, blank=True, null=True)
    password = models.CharField(max_length=55, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MFI'


class Mapposition(models.Model):
    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)
    zoom = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'MapPosition'


class Menuitems(models.Model):
    id = models.AutoField()
    component_name = models.CharField(unique=True, max_length=100)
    type = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'MenuItems'


class Monitoring(models.Model):
    id = models.AutoField()
    object_id = models.IntegerField()
    date = models.DateTimeField(blank=True, null=True)
    purpose = models.CharField(max_length=255, blank=True, null=True)
    monitor = models.CharField(max_length=255, blank=True, null=True)
    comment = models.CharField(max_length=4000, blank=True, null=True)
    type = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'Monitoring'






class Publicholidays(models.Model):
    date = models.DateTimeField()
    name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'PublicHolidays'


class Questionnaire(models.Model):
    name = models.CharField(db_column='Name', max_length=256, blank=True, null=True)  # Field name made lowercase.
    country = models.CharField(db_column='Country', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=100, blank=True, null=True)  # Field name made lowercase.
    positionincompony = models.CharField(db_column='PositionInCompony', max_length=100, blank=True, null=True)  # Field name made lowercase.
    othermessages = models.CharField(db_column='OtherMessages', max_length=4000, blank=True, null=True)  # Field name made lowercase.
    grossportfolio = models.CharField(db_column='GrossPortfolio', max_length=50, blank=True, null=True)  # Field name made lowercase.
    numberofclients = models.CharField(db_column='NumberOfClients', max_length=50, blank=True, null=True)  # Field name made lowercase.
    personname = models.CharField(db_column='PersonName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    phone = models.CharField(db_column='Phone', max_length=200, blank=True, null=True)  # Field name made lowercase.
    skype = models.CharField(db_column='Skype', max_length=200, blank=True, null=True)  # Field name made lowercase.
    purposeofusage = models.CharField(db_column='PurposeOfUsage', max_length=200, blank=True, null=True)  # Field name made lowercase.
    is_sent = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'Questionnaire'



class RepOlbAndLlpData(models.Model):
    id = models.IntegerField()
    branch_name = models.CharField(max_length=50, blank=True, null=True)
    load_date = models.DateTimeField(blank=True, null=True)
    contract_code = models.CharField(max_length=255, blank=True, null=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    interest = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    late_days = models.IntegerField(blank=True, null=True)
    client_name = models.CharField(max_length=255, blank=True, null=True)
    loan_officer_name = models.CharField(max_length=255, blank=True, null=True)
    product_name = models.CharField(max_length=255, blank=True, null=True)
    district_name = models.CharField(max_length=255, blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    close_date = models.DateTimeField(blank=True, null=True)
    range_from = models.IntegerField(blank=True, null=True)
    range_to = models.IntegerField(blank=True, null=True)
    llp_rate = models.IntegerField(blank=True, null=True)
    llp = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    rescheduled = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Rep_OLB_and_LLP_Data'


class RepParAnalysisData(models.Model):
    id = models.IntegerField()
    branch_name = models.CharField(max_length=50, blank=True, null=True)
    load_date = models.DateTimeField(blank=True, null=True)
    break_down = models.CharField(max_length=150, blank=True, null=True)
    break_down_type = models.CharField(max_length=20, blank=True, null=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    par = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts = models.IntegerField(blank=True, null=True)
    clients = models.IntegerField(blank=True, null=True)
    all_contracts = models.IntegerField(blank=True, null=True)
    all_clients = models.IntegerField(blank=True, null=True)
    par_30 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_30 = models.IntegerField(blank=True, null=True)
    clients_30 = models.IntegerField(blank=True, null=True)
    par_1_30 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_1_30 = models.IntegerField(blank=True, null=True)
    clients_1_30 = models.IntegerField(blank=True, null=True)
    par_31_60 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_31_60 = models.IntegerField(blank=True, null=True)
    clients_31_60 = models.IntegerField(blank=True, null=True)
    par_61_90 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_61_90 = models.IntegerField(blank=True, null=True)
    clients_61_90 = models.IntegerField(blank=True, null=True)
    par_91_180 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_91_180 = models.IntegerField(blank=True, null=True)
    clients_91_180 = models.IntegerField(blank=True, null=True)
    par_181_365 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_181_365 = models.IntegerField(blank=True, null=True)
    clients_181_365 = models.IntegerField(blank=True, null=True)
    par_365 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    contracts_365 = models.IntegerField(blank=True, null=True)
    clients_365 = models.IntegerField(blank=True, null=True)
    break_down_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Rep_Par_Analysis_Data'


class Roles(models.Model):
    code = models.CharField(max_length=256)
    deleted = models.BooleanField()
    description = models.CharField(max_length=2048, blank=True, null=True)
    default_start_view = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'Roles'

class SetupActivitystate(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_ActivityState'


class SetupBanksituation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_BankSituation'


class SetupBusinessplan(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_BusinessPlan'


class SetupDwellingplace(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_DwellingPlace'


class SetupFamilysituation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_FamilySituation'


class SetupFiscalstatus(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_FiscalStatus'


class SetupHousinglocation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_HousingLocation'


class SetupHousingsituation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_HousingSituation'


class SetupInsertiontypes(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_InsertionTypes'


class SetupLegalstatus(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_LegalStatus'


class SetupPersonalsituation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_PersonalSituation'


class SetupProfessionalexperience(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_ProfessionalExperience'


class SetupProfessionalsituation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_ProfessionalSituation'


class SetupRegistre(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_Registre'


class SetupSagejournal(models.Model):
    product_code = models.CharField(primary_key=True, max_length=50)
    journal_code = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_SageJournal'
        unique_together = (('product_code', 'journal_code'),)


class SetupSocialsituation(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_SocialSituation'


class SetupSponsor1(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_Sponsor1'


class SetupSponsor2(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_Sponsor2'


class SetupStudylevel(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_StudyLevel'


class SetupSubventiontypes(models.Model):
    value = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'SetUp_SubventionTypes'


class Standardbookings(models.Model):
    id = models.AutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=128, blank=True, null=True)  # Field name made lowercase.
    debit_account_id = models.IntegerField()
    credit_account_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'StandardBookings'
        unique_together = (('name', 'debit_account_id', 'credit_account_id'),)


class Statuses(models.Model):
    id = models.AutoField()
    status_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Statuses'


class Technicalparameters(models.Model):
    name = models.CharField(primary_key=True, max_length=100)
    value = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'TechnicalParameters'




class Termdepositproducts(models.Model):
    id = models.OneToOneField(Savingproducts, models.DO_NOTHING, db_column='id', primary_key=True)
    installment_types = models.ForeignKey(Installmenttypes, models.DO_NOTHING)
    number_period = models.IntegerField(blank=True, null=True)
    number_period_min = models.IntegerField(blank=True, null=True)
    number_period_max = models.IntegerField(blank=True, null=True)
    interest_frequency = models.SmallIntegerField()
    withdrawal_fees_type = models.SmallIntegerField()
    withdrawal_fees_min = models.FloatField(blank=True, null=True)
    withdrawal_fees_max = models.FloatField(blank=True, null=True)
    withdrawal_fees = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TermDepositProducts'


class Test(models.Model):
    char_type = models.CharField(max_length=1, blank=True, null=True)
    varchar_type = models.CharField(max_length=50, blank=True, null=True)
    nvarchar_type = models.CharField(max_length=50, blank=True, null=True)
    integer_type = models.IntegerField(blank=True, null=True)
    double_type = models.FloatField(blank=True, null=True)
    money_type = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    boolean_type = models.BooleanField(blank=True, null=True)
    datetime_type = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Test'























class Userrole(models.Model):
    role = models.ForeignKey(Roles, models.DO_NOTHING)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    date_role_set = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'UserRole'


class Users(models.Model):
    deleted = models.BooleanField()
    user_name = models.CharField(max_length=50)
    password_hash = models.CharField(max_length=4000, blank=True, null=True)
    role_code = models.CharField(max_length=256)
    first_name = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=200, blank=True, null=True)
    mail = models.CharField(max_length=100)
    sex = models.CharField(max_length=1)
    phone = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Users'


class Usersbranches(models.Model):
    user = models.ForeignKey(Users, models.DO_NOTHING)
    branch_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'UsersBranches'


class Userssubordinates(models.Model):
    user = models.ForeignKey(Users, models.DO_NOTHING)
    subordinate_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'UsersSubordinates'



