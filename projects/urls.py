from django.urls import path

from . import views


urlpatterns = [
    path('projects', views.projects, name="projects"),

    path('requests', views.requests, name="projects_requests"),
    path('loan_request/<int:pk>', views.aoe_project_request, name="aoe_project_request"),
    path('loan_request/<int:pk>', views.edit_project_request, name="edit_project_request"),


    path('projects_request_approvals', views.approvals, name="projects_requests_approvals"),
    path('projects_request_confirms', views.confirms, name="projects_requests_confirms"),
    path('projects_request_details/<int:pk>', views.confirms_details, name="projects_confirms_details"),
    path('projects_request_diburse', views.disburse, name="projects_requests_funding"),
    path('projects_request_dibursement/<int:pk>', views.disbursement, name="projects_requests_disbursement"),

    path('project_details', views.project_details, name="project_load")

]
