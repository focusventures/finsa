from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required()
def projects(request):
  
    return render(request, 'loans/periodicity.html')



@login_required()
def requests(request):
  
    return render(request, 'loans/periodicity.html')



@login_required()
def aoe_project_request(request, pk):
  
    return render(request, 'loans/periodicity.html')


@login_required()
def edit_project_request(request):
  
    return render(request, 'loans/periodicity.html')

@login_required()
def approvals(request):
  
    return render(request, 'loans/periodicity.html')


@login_required()
def confirms(request):
  
    return render(request, 'loans/periodicity.html')



@login_required()
def confirms_details(request):
  
    return render(request, 'loans/periodicity.html')


@login_required()
def disburse(request):
  
    return render(request, 'loans/periodicity.html')


@login_required()
def disbursement(request):
  
    return render(request, 'loans/periodicity.html')



@login_required()
def project_details(request):
  
    return render(request, 'loans/periodicity.html')

