from django.db import models
from django.utils.translation import ugettext_lazy as _

class Projects(models.Model):
    # member = models.ForeignKey('members.Member', models.DO_NOTHING)
    status = models.SmallIntegerField()
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=255)
    aim = models.CharField(max_length=200)
    begin_date = models.DateTimeField()
    abilities = models.CharField(max_length=50, blank=True, null=True)
    experience = models.CharField(max_length=50, blank=True, null=True)
    market = models.CharField(max_length=50, blank=True, null=True)
    concurrence = models.CharField(max_length=50, blank=True, null=True)
    purpose = models.CharField(max_length=50, blank=True, null=True)
    # corporate_name = models.CharField(max_length=50, blank=True, null=True)
    # corporate_juridicstatus = models.CharField(db_column='corporate_juridicStatus', max_length=50, blank=True, null=True)  # Field name made lowercase.
    # corporate_fiscalstatus = models.CharField(db_column='corporate_FiscalStatus', max_length=50, blank=True, null=True)  # Field name made lowercase.
    # corporate_siret = models.CharField(max_length=50, blank=True, null=True)
    # corporate_ca = models.DecimalField(db_column='corporate_CA', max_digits=19, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    # corporate_nbofjobs = models.IntegerField(db_column='corporate_nbOfJobs', blank=True, null=True)  # Field name made lowercase.
    # corporate_financialplantype = models.CharField(db_column='corporate_financialPlanType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    # corporatefinancialplanamount = models.DecimalField(db_column='corporateFinancialPlanAmount', max_digits=19, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    # corporate_financialplantotalamount = models.DecimalField(db_column='corporate_financialPlanTotalAmount', max_digits=19, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    zipcode = models.CharField(db_column='zipCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    district_id = models.IntegerField(blank=True, null=True)
    home_phone = models.CharField(max_length=50, blank=True, null=True)
    personalphone = models.CharField(db_column='personalPhone', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hometype = models.CharField(max_length=50, blank=True, null=True)
    corporate_registre = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Projects'
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'

class Followup(models.Model):
    project = models.ForeignKey('projects.Projects', models.DO_NOTHING)
    year = models.IntegerField()
    ca = models.DecimalField(db_column='CA', max_digits=19, decimal_places=4)  # Field name made lowercase.
    jobs1 = models.IntegerField(db_column='Jobs1')  # Field name made lowercase.
    jobs2 = models.IntegerField(db_column='Jobs2')  # Field name made lowercase.
    personalsituation = models.CharField(db_column='PersonalSituation', max_length=50)  # Field name made lowercase.
    activity = models.CharField(max_length=50)
    comment = models.CharField(max_length=20)

    class Meta:
        managed = False
        verbose_name = 'Follow Up'
        verbose_name_plural = 'Follow Up'


class Contracts(models.Model):
    contract_code = models.CharField(max_length=255)
    branch_code = models.CharField(max_length=50)
    creation_date = models.DateTimeField()
    start_date = models.DateTimeField()
    close_date = models.DateTimeField()
    closed = models.BooleanField()
    project = models.ForeignKey('projects.Projects', models.DO_NOTHING)
    status = models.SmallIntegerField()
    credit_commitee_date = models.DateTimeField(blank=True, null=True)
    credit_commitee_comment = models.CharField(max_length=4000, blank=True, null=True)
    credit_commitee_code = models.CharField(max_length=100, blank=True, null=True)
    align_disbursed_date = models.DateTimeField(blank=True, null=True)
    loan_purpose = models.CharField(max_length=4000, blank=True, null=True)
    comments = models.CharField(max_length=4000, blank=True, null=True)
    nsg = models.ForeignKey('locations.Villages', models.DO_NOTHING, blank=True, null=True)
    activity = models.ForeignKey('loans.Loanpurpose', models.DO_NOTHING)
    preferred_first_installment_date = models.DateTimeField(blank=True, null=True)
    created_by = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING, db_column='created_by')

    class Meta:
        managed = False
        verbose_name = 'Projects Contracts'
        verbose_name_plural = 'Projects Contracts'

class Projectpurposes(models.Model):
    name = models.CharField(max_length=200)
    class Meta:
        verbose_name = 'Project Purposes'
        verbose_name_plural = 'Projects Purposes'

class ProjectAccount(models.Model):
    acc_number = models.CharField(_("Account Number"), max_length=50, null=True, blank=True)
    acc_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    # member = models.ForeignKey("members.Member", default=None, verbose_name=_("Member"), null=True, blank=True, on_delete=models.CASCADE)
    project = models.ForeignKey("projects.Projects", verbose_name=_("Project"), null=True, blank=True,  on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Project Account'
        verbose_name_plural = 'Project Accounts'

