from django.contrib import admin

from .models import ProjectAccount, Projectpurposes, Projects, Contracts, Followup



admin.site.register(ProjectAccount)
admin.site.register(Projectpurposes)
admin.site.register(Projects)
admin.site.register(Contracts)
admin.site.register(Followup)
