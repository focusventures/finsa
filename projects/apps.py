from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'projects'
    icon_name = "book"
