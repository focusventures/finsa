from django.urls import path

from . import views


urlpatterns = [
    path('mpesa', views.mpesa, name="mpesa"),
    path('subscription', views.subscriptions, name="subscriptions"),
    path('account_settings', views.account, name="account_settings"),
    path('app', views.app, name="app"),

  
 
]