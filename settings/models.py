from django.db import models

from django.utils.translation import ugettext_lazy as _


class Contacts(models.Model):
    name = models.CharField(_("Name Of Customer Care Contant"), max_length=50)
    phone_number = models.CharField(_("Phone Number"), max_length=50)
    office = models.CharField(_("Office"), max_length=50)
    location = models.CharField(_("Location"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


class Services(models.Model):
    title = models.CharField(_("Title Of The Service"), max_length=50)
    decription = models.TextField(_("Description Of The Service"))
    is_active = models.BooleanField(_("Service Is Active"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


class About(models.Model):
    banner = models.TextField(_("Banner"))
    what_we_do = models.TextField(_("What We Do"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


class Index(models.Model):
    message = models.TextField(_("Welcome Massage"), null=True, blank=True)
    tag = models.CharField(_("Introduction"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


