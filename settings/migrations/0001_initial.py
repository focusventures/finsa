# Generated by Django 3.0.4 on 2020-04-28 17:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Services',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='Title Of The Service')),
                ('decription', models.TextField(verbose_name='Description Of The Service')),
                ('is_active', models.BooleanField(verbose_name='Service Is Active')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
        ),
        migrations.CreateModel(
            name='Index',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=50, verbose_name='Introduction')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
        ),
        migrations.CreateModel(
            name='Contacts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Name Of Customer Care Contant')),
                ('phone_number', models.CharField(max_length=50, verbose_name='Phone Number')),
                ('office', models.CharField(max_length=50, verbose_name='Office')),
                ('location', models.CharField(max_length=50, verbose_name='Location')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
        ),
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('banner', models.TextField(verbose_name='Banner')),
                ('what_we_do', models.TextField(verbose_name='What We Do')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
        ),
    ]
