from django.shortcuts import render

def subscriptions(request):

    return render(request, 'settings/subscription.html')


def mpesa(request):
    
    return render(request, 'settings/mpesa.html')



def account(request):
    
    return render(request, 'settings/account.html')


def app(request):
    
    return render(request, 'settings/app.html')