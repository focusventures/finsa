from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper

from crispy_forms.layout import (Layout, Fieldset, Field,Row, Column, Button,
                                 ButtonHolder, Submit, Div)
from django.forms.models import inlineformset_factory
from members.models import Member

from savings.models import Savingbookcontracts, Savingbookproducts, Savingcontracts, Savingdepositcontracts, Savingevents, Savingproducts, Savingproductsclienttypes, SavingAccount, CurrentAccount




class SavingbookcontractsForm(ModelForm):

    class Meta:
        model = Savingbookcontracts
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SavingbookcontractsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('flat_withdraw_fees', css_class='form-group col-md-6 mb-0'),
                Column('rate_withdraw_fees', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),


            Row(
                Column('flat_transfer_fees', css_class='form-group col-md-6 mb-0'),
                Column('rate_transfer_fees', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),


             Row(
                Column('flat_deposit_fees', css_class='form-group col-md-6 mb-0'),
                Column('flat_close_fees', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),


            Row(
                Column('flat_management_fees', css_class='form-group col-md-6 mb-0'),
                Column('flat_overdraft_fees', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),

          

            Row(
                Column('in_overdraft', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

                Row(
                Column('rate_agio_fees', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),
            Row(
                Column('cheque_deposit_fees', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

                Row(
                Column('flat_reopen_fees', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),
             Row(
                Column('flat_ibt_fee', css_class='form-group col-md-6 mb-0'),
                Column('rate_ibt_fee', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),

              

            Row(
                Column('use_term_deposit', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

                Row(
                Column('term_deposit_period', css_class='form-group col-md-6 mb-0'),
                Column('term_deposit_period_min', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),
           

                Row(
                Column('term_deposit_period_min', css_class='form-group col-md-6 mb-0'),
                Column('term_deposit_period_max', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),
          
                Row(
                Column('transfer_account', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),
            Row(
                Column('rollover', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

                Row(
                Column('next_maturity', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )






class SavingbookproductsForm(ModelForm):

    class Meta:
        model = Savingbookproducts
        fields = '__all__'







class SavingcontractsForm(ModelForm):
    creation_date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))
    
    class Meta:
        model = Savingcontracts
        fields = ['product', 'creation_date', 'entry_fees', 'savings_officer', 'initial_amount']

    def __init__(self, *args, **kwargs):
        super(SavingcontractsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.fields['member'].queryset = Member.objects.get(id=pk)
        self.helper.layout = Layout(
            Row(
                Column('product', css_class='form-group col-md-6 mb-0'),
                Column('creation_date', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),


            Row(
                Column('code', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),


             Row(
                Column('interest_rate', css_class='form-group col-md-6 mb-0'),
                Column('initial_amount', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),

            Row(
                Column('entry_fees', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),


            Row(
                Column('savings_officer', css_class='form-group col-md-6 mb-0'),

                css_class='form-row '
                ),
            
        )






class SavingdepositcontractsForm(ModelForm):
    
    class Meta:
        model = Savingdepositcontracts
        fields = '__all__'






class SavingproductsForm(ModelForm):
    
    class Meta:
        model = Savingproducts
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(SavingproductsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)




class SavingAccountForm(ModelForm):
    
    class Meta:
        model = SavingAccount
        fields = [ ]

    def __init__(self, *args, **kwargs):
        super(SavingAccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.fields['acc_number'].required = False
        self.helper.layout = Layout(
            Row(
                Column('acc_number', css_class='form-group col-md-6 mb-0'),


                css_class='form-row'
                ),


        
            
        )



class CurrentAccountForm(ModelForm):
    
    class Meta:
        model = CurrentAccount
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CurrentAccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)