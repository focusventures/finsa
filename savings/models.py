from django.db import models
from django.utils.translation import ugettext_lazy as _
import django


class Savingbookcontracts(models.Model):
    contract = models.ForeignKey("savings.Savingcontracts", verbose_name=_("Saving Contracts"), null=True, blank=True, on_delete=models.DO_NOTHING)
    flat_withdraw_fees = models.DecimalField( _("Flat Withdraw Fees"), max_digits=19, decimal_places=4, blank=True, null=True)
    rate_withdraw_fees = models.FloatField( _("Rate Withdraw Fees"),blank=True, null=True)
    flat_transfer_fees = models.DecimalField(_("Flat Transfer Fees"),max_digits=19,  decimal_places=4, blank=True, null=True)
    rate_transfer_fees = models.FloatField( _("Rate Withdrawer Fees"), blank=True, null=True)
    flat_deposit_fees = models.DecimalField(  _("Flat Deposit Fees"),max_digits=19, decimal_places=4, blank=True, null=True)
    flat_close_fees = models.DecimalField(  _("Flat Close Fees"), max_digits=19, decimal_places=4,blank=True, null=True)
    flat_management_fees = models.DecimalField(  _("Flat Management Fees"),max_digits=19, decimal_places=4, blank=True, null=True)
    flat_overdraft_fees = models.DecimalField(  _("Flat Overdraft Fees"),max_digits=19, decimal_places=4, blank=True, null=True)
    in_overdraft = models.BooleanField (_("Is Overdraft"))
    rate_agio_fees = models.FloatField ( _("Rate Agio Fees"), blank=True,null=True)
    cheque_deposit_fees = models.DecimalField(_("Cheque Deposit Fees"),max_digits=19,  decimal_places=4, blank=True, null=True)
    flat_reopen_fees = models.DecimalField(_("Flat Reopen Fees"),max_digits=19, decimal_places=4, blank=True, null=True)
    flat_ibt_fee = models.DecimalField(_("Flat ibt Fees"), max_digits=19,decimal_places=4, blank=True, null=True)
    rate_ibt_fee = models.FloatField(_("Rate ibt Fees"),blank=True,  null=True)
    use_term_deposit = models.BooleanField( _("User Term deposit"),)
    term_deposit_period = models.IntegerField( _("Term Deposit Period"),)
    term_deposit_period_min = models.IntegerField( _("Minimum Term deposit Period"),blank=True, null=True)
    term_deposit_period_max = models.IntegerField( _("Minimum Term deposit Period"),blank=True,  null=True)
    transfer_account = models.CharField( _("Transfer Account"),max_length=50, blank=True, null=True)
    rollover = models.IntegerField( _("Rollover"), blank=True, null=True, )
    next_maturity = models.DateTimeField( _("Next Maturity"), blank=True, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
 
        verbose_name = 'Saving Book Contracts'
        verbose_name_plural = 'Saving Book Contracts'
     


class Savingbookproducts(models.Model):
    savings_product = models.ForeignKey("savings.Savingproducts", verbose_name=_("Saving Product"),null=True, on_delete=models.DO_NOTHING)
    interest_base = models.SmallIntegerField( _("Interest Base"))
    interest_frequency = models.SmallIntegerField( _("Interest Frequency"))
    calcul_amount_base = models.SmallIntegerField(  _("C"),blank=True, null=True)
    withdraw_fees_type = models.SmallIntegerField()
    flat_withdraw_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    flat_withdraw_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    flat_withdraw_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    rate_withdraw_fees_min = models.FloatField(blank=True, null=True)
    rate_withdraw_fees_max = models.FloatField(blank=True, null=True)
    rate_withdraw_fees = models.FloatField(blank=True, null=True)
    transfer_fees_type = models.SmallIntegerField()
    flat_transfer_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    flat_transfer_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    flat_transfer_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    rate_transfer_fees_min = models.FloatField(blank=True, null=True)
    rate_transfer_fees_max = models.FloatField(blank=True, null=True)
    rate_transfer_fees = models.FloatField(blank=True, null=True)
    deposit_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    close_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    close_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    close_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    management_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    management_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    management_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    management_fees_freq = models.IntegerField()
    overdraft_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    overdraft_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    overdraft_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    agio_fees = models.FloatField(blank=True, null=True)
    agio_fees_max = models.FloatField(blank=True, null=True)
    agio_fees_min = models.FloatField(blank=True, null=True)
    agio_fees_freq = models.IntegerField()
    cheque_deposit_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    cheque_deposit_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    cheque_deposit_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    cheque_deposit_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    cheque_deposit_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    reopen_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    reopen_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    reopen_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    is_ibt_fee_flat = models.BooleanField()
    ibt_fee_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    ibt_fee_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    ibt_fee = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    use_term_deposit = models.BooleanField()
    term_deposit_period_min = models.IntegerField(blank=True, null=True)
    term_deposit_period_max = models.IntegerField(blank=True, null=True)
    posting_frequency = models.IntegerField(blank=True, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Savings Book Contracts'
        verbose_name_plural = 'saving Book Contract'


class Savingcontracts(models.Model):
    product = models.ForeignKey("savings.Savingproducts", verbose_name=_("Saving Product"), on_delete=models.DO_NOTHING)
    member = models.ForeignKey('members.Member',  verbose_name=_("Member"), on_delete=models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(  _("Code"), max_length=50)
    creation_date = models.DateTimeField( _("Creation Date"))
    # interest_rate = models.FloatField( _("Interest Rate"))
    # status = models.SmallIntegerField( _("Contract Status"))
    closed_date = models.DateTimeField( _("Closed Date"), blank=True, null=True, )
    savings_officer = models.ForeignKey("authentication.CustomUser", verbose_name=_("Savings Officer"), null=True, blank=True, on_delete=models.DO_NOTHING)
    initial_amount = models.DecimalField( _("Initial Amount"), max_digits=19, decimal_places=4, default=0.00)
    entry_fees = models.DecimalField(  _("Entry Fees"),max_digits=19, decimal_places=4, default=0.00)
    nsg_id = models.IntegerField(blank=True, null=True)
    loan_id = models.IntegerField(blank=True, null=True)
    # approving = models.ForeignKey("authentication.CustomUser", verbose_name=_(""), on_delete=models.DO_NOTHING)
    start_date = models.DateTimeField( _("Start Date"),blank=True, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Contracts'
        verbose_name_plural = 'Saving Contracts'



    


class Savingdepositcontracts(models.Model):
    savings_contract = models.OneToOneField('savings.Savingcontracts', models.DO_NOTHING, null=True, blank=True)
    number_periods = models.IntegerField( _("Number Periods"))
    rollover = models.SmallIntegerField( _("Rollover"))
    transfer_account = models.CharField(  _("Transfer Account"),max_length=50, blank=True, null=True)
    withdrawal_fees = models.FloatField( _("Withdrawal Fees"))
    next_maturity = models.DateTimeField(  _("Next Maturity"),blank=True, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Deposit Contracts'
        verbose_name_plural = 'Saving Deposit Contracts'


class Savingevents(models.Model):
    member = models.ForeignKey('members.Member', models.DO_NOTHING)
    contract = models.ForeignKey('savings.Savingcontracts', models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(max_length=4)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    description = models.CharField(max_length=200, blank=True, null=True)
    deleted = models.BooleanField()
    creation_date = models.DateTimeField()
    cancelable = models.BooleanField()
    is_fired = models.BooleanField()
    related_contract_code = models.CharField(max_length=50, blank=True, null=True)
    fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    is_exported = models.BooleanField()
    savings_method = models.SmallIntegerField(blank=True, null=True)
    pending = models.BooleanField()
    pending_event_id = models.IntegerField(blank=True, null=True)
    # teller = models.ForeignKey('extras.Tellers', models.DO_NOTHING, blank=True, null=True)
    loan_event_id = models.IntegerField(blank=True, null=True)
    cancel_date = models.DateTimeField(blank=True, null=True)
    doc1 = models.TextField(blank=True, null=True)
    parent_event_id = models.IntegerField(blank=True, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Events'
        verbose_name_plural = 'Saving Events'

class SavingDepositFee(models.Model):
    member = models.ForeignKey('members.Member', models.DO_NOTHING)
    contract = models.ForeignKey('savings.Savingcontracts', models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(max_length=4)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    description = models.CharField(max_length=200, blank=True, null=True)
    deleted = models.BooleanField(default=False)
    creation_date = models.DateTimeField(null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Deposit Fee Events'
        verbose_name_plural = 'Saving Deposit Fee Event'


class SavingWithdrawFee(models.Model):
    member = models.ForeignKey('members.Member', models.DO_NOTHING)
    contract = models.ForeignKey('savings.Savingcontracts', models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(max_length=4)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    description = models.CharField(max_length=200, blank=True, null=True)
    deleted = models.BooleanField(default=False)
    creation_date = models.DateTimeField(null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving WithDraw Fee Events'
        verbose_name_plural = 'Saving WithDraw Fee Events'





class SavingTransferFee(models.Model):
    member = models.ForeignKey('members.Member', models.DO_NOTHING)
    contract = models.ForeignKey('savings.Savingcontracts', models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(max_length=4)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    description = models.CharField(max_length=200, blank=True, null=True)
    deleted = models.BooleanField(default=False)
    creation_date = models.DateTimeField(null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Transfer Events'



curency = (
    ("Ksh", "Ksh"),
    
)


clients = (
    ("Individual", "Individual"),
    ("Group", "Group"),
    ("Corporate", "Corporate"),
)
class Savingproducts(models.Model):
    deleted = models.BooleanField(default=False)
    name = models.CharField(unique=True, max_length=100)
    client_type = models.CharField(max_length=15, choices=clients, blank=True, null=True)
    initial_amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    initial_amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    balance_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    balance_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdraw_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdraw_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdraw_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_fee = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    interest_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Interest Code"), related_name="Interest Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    initial_amount_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Opening Amount Code"), related_name="Initial Amount Code+",  on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    withdraw_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Withdraw Code"), related_name="Withdraw Fees Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    transfer_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Transfer Code"), related_name="Transfer Fee Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    deposit_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Deposit Fees Code"), related_name="Deposit Fee Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    entry_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Entry Fees Code"), related_name="Entry Fees Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    deposit_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Deposit Code"), related_name="Savings Deposit Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    withdraw_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Withdraw Code"), related_name="Withdraw Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    transfer_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Transfer Code"), related_name="Transfer Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)

    interest_rate = models.FloatField(blank=True, null=True)
    interest_rate_min = models.FloatField(blank=True, null=True)
    interest_rate_max = models.FloatField(blank=True, null=True)
    currency = models.CharField(_("curency"),choices=curency,  default="Kshs", max_length=10)
    entry_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    entry_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    entry_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    # product_type = models.CharField(max_length=1)
    # code = models.CharField(max_length=50)
    transfer_fee = models.DecimalField(max_digits=19, decimal_places=2, default=0.00)
    transfer_min = models.DecimalField(max_digits=19, decimal_places=4)
    transfer_max = models.DecimalField(max_digits=19, decimal_places=4)


    # type = models.IntegerField()
    # renew_auto = models.BooleanField()

    savings_deposit_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Savings Deposit Account"), null=True, blank=True, related_name="Savings Deposit Account+", on_delete=models.DO_NOTHING)
    deposit_fines_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Deposit Fines Account"), null=True, blank=True, related_name="Deposit Fines Account+", on_delete=models.DO_NOTHING)
    interest_earned_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Earned Account"), null=True, blank=True,  related_name="Interest Earned Account+",on_delete=models.DO_NOTHING)
    deposit_fee_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Deposit Fee Account"), null=True, blank=True, related_name="Deposit Fee Account+",on_delete=models.DO_NOTHING)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Savings Products'
        verbose_name_plural = "Savings Products"
    
    def __str__(self):
        return self.name


class Savingproductsclienttypes(models.Model):
    saving_product_id = models.IntegerField()
    client_type_id = models.IntegerField()
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Products Client Types'
        verbose_name_plural = 'Saving Products Clients Types'



class SavingAccount(models.Model):
    acc_number = models.CharField(_("Account Number"), max_length=50)
    acc_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", default=None, verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    saving_contract = models.ForeignKey("savings.Savingcontracts", verbose_name=_("Saving Contract"), null=True, blank=True, on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Account'
        verbose_name_plural = 'Saving Accounts'

class CurrentAccount(models.Model):
    acc_number = models.CharField(_("Account Number"), max_length=50)
    acc_balance = models.DecimalField(max_digits=20, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", default=None, verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Current Account'
        verbose_name_plural = 'Current Accounts'



class SavingContractEntryFee(models.Model):
    member = models.ForeignKey('members.Member', models.DO_NOTHING)
    contract = models.ForeignKey('savings.Savingcontracts', models.DO_NOTHING, null=True, blank=True)
    code = models.CharField(max_length=4, null=True, blank=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4, default=0.00)
    description = models.CharField(max_length=200, blank=True, null=True)
    deleted = models.BooleanField(default=False)
    creation_date = models.DateTimeField(null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Saving Deposit Fee Events'
        verbose_name_plural = 'Saving Deposit Fee Events'