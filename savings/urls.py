from django.urls import path


from .views import savingbookcontracts
from .views import savingproducts
from .views import contractproducts
from .views import depositcontracts
from .views import savingcontracts
from .views import accounts

urlpatterns = [

    path('sbc', savingbookcontracts.list, name='sbc_list'),
    path('details/<int:pk>', savingbookcontracts.details, name="sbc_details"),
    path('aoe', savingbookcontracts.aoe, name="sbc_aoe"),
    path('edit/<int:pk>', savingbookcontracts.edit, name="sbc_edit"),



    path('products', savingproducts.list, name='sproducts_list'),
    path('products/details/<int:pk>', savingproducts.details, name="sproducts_details"),
    path('products/aoe', savingproducts.aoe, name="sproducts_aoe"),
    path('products/edit/<int:pk>', savingproducts.edit, name="sproducts_edit"),


    path('contract_products', contractproducts.list, name='scontract_products_list'),
    path('contract_products/details/<int:pk>', contractproducts.details, name="scontract_products_details"),
    path('contract_products/aoe', contractproducts.aoe, name="scontract_products_aoe"),
    path('contract_products/edit/<int:pk>', contractproducts.edit, name="scontract_products_edit"),

    path('saving_contracts', savingcontracts.list, name='scontracts_list'),
    path('saving_contracts/details/<int:pk>', savingcontracts.details, name="scontracts_details"),
    path('saving_contracts/aoe', savingcontracts.aoe, name="scontracts_aoe"),
    path('saving_contracts/edit/<int:pk>', savingcontracts.edit, name="scontracts_edit"),


    path('deposit_contracts', depositcontracts.list, name='sdeposit_contracts_list'),
    path('deposit_contracts/details/<int:pk>', depositcontracts.details, name="sdeposit_contracts_details"),
    path('deposit_contracts/aoe', depositcontracts.aoe, name="sdeposit_contracts_aoe"),
    path('deposit_contracts/edit/<int:pk>', depositcontracts.edit, name="sdeposit_contracts_edit"),



    path('saving_account_opening/<int:pk>', accounts.open_saving_account, name='open_saving_account'),
    path('saving_account_edit/<int:pk>', accounts.edit_saving_account, name='edit_saving_account'),
    path('current_account_edit/<int:pk>', accounts.edit_current_account, name='edit_current_account'),


    # path('saving_account_deposit/<int:pk>', accounts.open_saving_account , name='saving_deposit'),
    # path('saving_account_deactivation/<int:pk>', accounts.open_saving_account , name='saving_deactivate'),
    # path('current_account_deposit/<int:pk>', accounts.edit_saving_account, name='current_deposit'),
    # path('current_account_deactivate/<int:pk>', accounts.edit_current_account, name='current_deactivate'),


]