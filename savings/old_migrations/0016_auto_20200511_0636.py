# Generated by Django 3.0.4 on 2020-05-11 03:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('savings', '0015_auto_20200430_1443'),
    ]

    operations = [
        migrations.AddField(
            model_name='savingproducts',
            name='withdraw_fees',
            field=models.DecimalField(blank=True, decimal_places=4, max_digits=19, null=True),
        ),
        migrations.AlterField(
            model_name='currentaccount',
            name='acc_balance',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20),
        ),
        migrations.AlterField(
            model_name='savingdepositfee',
            name='creation_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='savingdepositfee',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
    ]
