from django.apps import AppConfig


class SavingsConfig(AppConfig):
    name = 'savings'
    icon_name = 'book'


    # def ready(self):
    #     import loans.signals