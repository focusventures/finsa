from django.shortcuts import render
from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic.list import ListView

from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views import generic

from bootstrap_modal_forms.mixins import PassRequestMixin
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.template.loader import render_to_string, get_template
from django.db import transaction
from django.urls import reverse_lazy
from savings.models import Savingdepositcontracts
from savings.forms import SavingdepositcontractsForm

from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q


@login_required()
def list(request):
    contracts = Savingdepositcontracts.objects.all()

    context = {
        'contracts': contracts
    }
    
    return render(request, 'savings/depositcontracts/list.html', context)

@login_required()
def aoe(request):
    form = SavingdepositcontractsForm()
    if request.method == 'POST':
        form = SavingdepositcontractsForm(request.POST, request.FILES)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('members_list'))
    else:
       form = SavingdepositcontractsForm()
    context = {'form': form}
    return render(request, 'savings/depositcontracts/aoe.html', context)


@login_required()
def edit(request, pk):
    form = SavingdepositcontractsForm()
    if request.method == 'POST':
        form = SavingdepositcontractsForm(request.POST, request.FILES, instance=Savingdepositcontracts.objects.get(id=pk))
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()

       
            return redirect(reverse('members_list'))
    else:
       form = SavingdepositcontractsForm(instance=Savingdepositcontracts.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'savings/depositcontracts/aoe.html', context)

@login_required()
def details(request, pk):
    try:
        sp = Savingdepositcontracts.objects.get(id=pk)

    except Savingdepositcontracts.DoesNotExist:
        sp = None
 

    context = {
        "tenant": sp,
   
    
    
    }
    
    return render(request, 'savings/depositcontracts/details.html', context)



