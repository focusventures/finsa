from django.shortcuts import render
from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic.list import ListView

from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views import generic

from bootstrap_modal_forms.mixins import PassRequestMixin
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.template.loader import render_to_string, get_template
from django.db import transaction
from django.urls import reverse_lazy
from savings.models import Savingproducts
from savings.forms import SavingproductsForm

from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q


@login_required()
def list(request):
    products = Savingproducts.objects.all()

    context = {
        'products': products
    }

    print(products)
    
    return render(request, 'savings/savingsproducts/list.html', context)

@login_required()
def aoe(request):
    form = SavingproductsForm()
    if request.method == 'POST':
        form = SavingproductsForm(request.POST, request.FILES)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('members_list'))
    else:
       form = SavingproductsForm()
    context = {'form': form}
    return render(request, 'savings/savingsproducts/aoe.html', context)


@login_required()
def edit(request, pk):
    form = SavingproductsForm()
    if request.method == 'POST':
        form = SavingproductsForm(request.POST, request.FILES, instance=Savingproducts.objects.get(id=pk))
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()

       
            return redirect(reverse('members_list'))
    else:
       form = SavingproductsForm(instance=Savingproducts.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'savings/savingsproducts/aoe.html', context)

@login_required()
def details(request, pk):
    member = Savingproducts.objects.get(id=pk)
 

    context = {
        "tenant": member,
   
    
    
    }
    
    return render(request, 'savings/savingsproducts/details.html', context)



