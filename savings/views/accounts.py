from django.shortcuts import render
from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic.list import ListView

from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views import generic

from django.contrib import messages

from bootstrap_modal_forms.mixins import PassRequestMixin
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.template.loader import render_to_string, get_template
from django.db import transaction
from django.urls import reverse_lazy
from savings.models import SavingAccount, CurrentAccount, Savingproducts
from savings.forms import SavingproductsForm, SavingAccountForm, CurrentAccountForm

from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from members.models import Member

@login_required()
def list(request):
    contracts = Savingproducts.objects.all()

    context = {
        'contracts': contracts
    }
    
    return render(request, 'savings/savingsproducts/list.html', context)

@login_required()
def aoe(request):
    form = SavingproductsForm()
    if request.method == 'POST':
        form = SavingproductsForm(request.POST, request.FILES)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('members_list'))
    else:
       form = SavingproductsForm()
    context = {'form': form}
    return render(request, 'savings/savingsproducts/aoe.html', context)


@login_required()
def edit(request, pk):
    form = SavingproductsForm()
    if request.method == 'POST':
        form = SavingproductsForm(request.POST, request.FILES, instance=Savingproducts.objects.get(id=pk))
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()

       
            return redirect(reverse('members_list'))
    else:
       form = SavingproductsForm(instance=Savingproducts.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'savings/savingsproducts/aoe.html', context)

@login_required()
def details(request, pk):
    member = Savingproducts.objects.get(id=pk)
 

    context = {
        "tenant": member,
   
    
    
    }
    
    return render(request, 'savings/savingsproducts/details.html', context)

from savings.forms import SavingcontractsForm
from savings.models import Savingbookcontracts, Savingbookproducts, Savingcontracts, SavingContractEntryFee
from django.forms import formset_factory
from financial.models import Transactions
import datetime

@login_required()
def open_saving_account(request, pk):
    member = Member.objects.get(id=pk)

    if member.id_number == None:
        messages.error(request, 'You cannot Open A savings Account without A National Id Number. Kindly Update the Member Info By Clicking the EDIT BUTTON') 

        return redirect(reverse('members_details', args=(pk,)))

    form = SavingAccountForm(prefix="sa")
    contract_form = SavingcontractsForm(prefix="scf")
    if request.method == 'POST':
        saving_acc = SavingAccountForm(request.POST, prefix="sa")
        saving_contract = SavingcontractsForm(request.POST, prefix="scf")
        if saving_acc.is_valid() and saving_contract.is_valid():
            account = form.save(commit=False)
            contract = saving_contract.save(commit=False)


            context = {'form': saving_acc, "contract_form": saving_contract,  "member":member}
            saving_product = Savingproducts.objects.get(id=request.POST['scf-product'])

            if contract.initial_amount < saving_product.initial_amount_min:
                messages.error(request, 'The Savings Product You have choosen ({}) requires An Initial Amount of Ksh {} whilst you have provided an initial amount of Ksh {}'.format(saving_product.name, saving_product.initial_amount_min,contract.initial_amount )) 

                return render(request, 'savings/accounts/saving_account.html', context)

            if contract.entry_fees < saving_product.entry_fees_min:
                messages.error(request, 'The Savings Product You have choosen ({}) requires An Entry Fees Amount of Ksh {} whilst you have provided an initial amount of Ksh {}'.format(saving_product.name, saving_product.entry_fees_min,contract.entry_fees )) 
                return render(request, 'savings/accounts/saving_account.html', context)



            
            account.member = member
            account.creator = request.user
            account.initial_amount = contract.initial_amount
            account.acc_balance = contract.initial_amount

            contract.creator = request.user
            contract.member = member
            contract.save()

            account.acc_number = '010' + str(member.id_number)
            account.saving_contract = contract
            account.save()
            

            saving_d = SavingContractEntryFee()
            saving_d.member = member
            saving_d.contract = contract
            saving_d.amount = contract.entry_fees
            saving_d.creator = request.user
            saving_d.creation_date = account.date_created
            saving_d.save()

            tranaction_dep = Transactions()
            tranaction_dep.amount = contract.entry_fees
            tranaction_dep.type = "saving_product_entry_fees"
            tranaction_dep.description = "Saving Account Entry Fees "
            tranaction_dep.acc = account.acc_number

            tranaction_dep.creator = request.user
            tranaction_dep.date = datetime.datetime.now()
            tranaction_dep.date_created = datetime.datetime.now()
            tranaction_dep.member = member

            tranaction_dep.save()

            tranaction_dep = Transactions()
            tranaction_dep.amount = contract.initial_amount
            tranaction_dep.type = "saving_account_initial"
            tranaction_dep.description = "Saving Account Initial Amount"

            tranaction_dep.acc = account.acc_number


            tranaction_dep.creator = request.user
            tranaction_dep.date = datetime.datetime.now()
            tranaction_dep.date_created = datetime.datetime.now()
            tranaction_dep.member = member

            tranaction_dep.save()



            member.has_saving_acc = True
            member.saving_acc = account
            member.save()

            return redirect(reverse('members_details', args=(pk,)))
    context = {'form': form, "contract_form": contract_form,  "member":member}
    return render(request, 'savings/accounts/saving_account.html', context)

@login_required()
def edit_saving_account(request, pk):
    member = Member.objects.get(id=pk)

    acc = SavingAccount.objects.filter(member__id=pk).first()
    cont = Savingcontracts.objects.filter(member__id=pk).first()

    form = SavingAccountForm(instance=acc, prefix="sa")
    contract_form = SavingcontractsForm(instance=cont, prefix="scf")
    if request.method == 'POST':
        saving_acc = SavingAccountForm(request.POST, prefix="sa")
        saving_contract = SavingcontractsForm(request.POST, prefix="scf")
        if saving_acc.is_valid() and saving_contract.is_valid():

            context = {'form': saving_acc, "contract_form": saving_contract,  "member":member}
            saving_product = Savingproducts.objects.get(id=request.POST['scf-product'])
            # if saving_contract.initial_amount < saving_product.initial_amount:
            #     return render(request, 'savings/accounts/saving_account.html', context)

            # if saving_contract.entry_fees < saving_product.entry_fees:
            #     return render(request, 'savings/accounts/saving_account.html', context)



            account = form.save(commit=False)
            account.member = member
            account.creator = request.user

            contract = saving_contract.save(commit=False)
            contract.creator = request.user
            contract.member = member
            contract.save()

            # account.acc_number = request.POST['scf-product']
            account.saving_contract = contract
            account.save()
            


            member.has_saving_acc = True
            member.saving_acc = account
            member.save()

            return redirect(reverse('members_details', args=(pk,)))
    context = {'form': form, "contract_form": contract_form,  "member":member}
    return render(request, 'savings/accounts/saving_account_edit.html', context)



@login_required()
def edit_current_account(request, pk):
    member = Member.objects.get(id=pk)
    acc = CurrentAccount.objects.get(member=member)
    form = CurrentAccountForm(instance=acc)
    if request.method == 'POST':
        form = CurrentAccountForm(request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('members_list'))
    context = {'form': form}
    return render(request, 'savings/accounts/current_account.html', context)







from utils.multiple_forms import MultipleFormsView


class SavingContractFormsView(MultipleFormsView):
    template_name = 'savings/accounts/saving_account.html'
    success_url = '/'

    # here we specify all forms that should be displayed
    forms_classes = [
        SavingAccountForm,
        SavingcontractsForm,

    ]

    def get_forms_classes(self):
        # we hide staff_only forms from not-staff users
        # example how to dynamically change forms
        forms_classes = super(SavingContractFormsView, self).get_forms_classes()
        user = self.request.user
        # if not user.is_authenticated or not user.is_staff:
        #     return list(filter(lambda form: not getattr(form, 'staff_only', False), forms_classes))
        return forms_classes

    def form_valid(self, form): 
        return super(SavingContractFormsView).form_valid(form)