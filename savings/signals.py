from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import SavingContractEntryFee, SavingDepositFee, Savingevents
from dashboard.models import SnapShot
from dashboard.models import TodaySnapShot
from financial.models import Transactions
from audittrial.models import ActivityLog



import datetime

# @receiver(post_save, sender=SavingContractEntryFee)
# def update_on_save(sender, instance, created, **kwargs):

#     if created:
#         transaction = Transactions()
#         transaction.amount = instance.amount
#         transaction.type = "Contract Entry Fee" 
#         transaction.acc = 
#         transaction.code = instance.code
#         transaction.member = instance.member
#         transaction.receiver = instance.receiver
#         transaction.receiving_member = instance.receiving_member
#         transaction.date = instance.date
#         transaction.processed = instance.processed_by
#         transaction.creator = instance.creator
#         transaction.date_created = datetime.datetime.now()
#         transaction.save()




@receiver(post_delete, sender=SavingContractEntryFee)
def update_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.members -= 1
    snapshot.active_members -= 1
    snapshot.save()



