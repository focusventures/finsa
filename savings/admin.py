from django.contrib import admin
from .models import *


class SavingbookcontractsModelAdmin(admin.ModelAdmin):
    list_display = ['name',     'client_type', 'creator', 'date_created','date_modified', 'modified_by' ]
    ordering = ['date_created',  ]
    icon_name = 'map'

    search_fields = [
      'name', 

    ]
    list_filter = [
    'name',
    ]


class SavingContractModeladmin(admin.ModelAdmin):
    list_display = ['product',     'member', 'code', 'savings_officer', 'initial_amount', 'entry_fees',  'date_created','date_modified', 'modified_by' ]
    ordering = ['product',     'member', 'code', 'savings_officer', 'initial_amount', 'entry_fees',  'date_created','date_modified', 'modified_by' ]
    icon_name = 'map'

    search_fields = [  'code', 'initial_amount', 'entry_fees' ]
    list_filter = ['product',     'member', 'code', 'savings_officer', 'initial_amount', 'entry_fees',  'date_created','date_modified', 'modified_by' ]


class SavingProductAdminModel(admin.ModelAdmin):
    list_display = ['name',     'client_type', 'creator', 'date_created','date_modified', 'modified_by' ]
    ordering = ['date_created',  ]
    icon_name = 'map'

    search_fields = [
      'name', 

    ]
    list_filter = [
    'name',
    ]

    fieldsets = (
        (
            ' Savings Product Info', {
                'fields': (('name','client_type',), )
            }
        ),

        (
            'Initial Amount Info', {
                'fields': (('initial_amount_min', 'initial_amount_max', 'initial_amount_code',),)
            }
        ),

         (
            'Balance Info', {
                'fields': (('balance_min','balance_max',), )
            }
        ),
        (
            'Deposit Info', {
                'fields': (('deposit_min','deposit_max','deposit_fee', 'deposit_fees_code', 'deposit_code',), )
            }
        ),
        (
            'Interest Rate Info', {
                'fields': (('interest_rate','interest_rate_min','interest_rate_max', 'interest_code'))
            }
        ),
        (
            'Entry Fees Info', {
                'fields': (('entry_fees','entry_fees_min','entry_fees_max','entry_fees_code'),)
            }
        ),

            (
            'Withdraw Info', {
                'fields': (('withdraw_min','withdraw_max','withdraw_fees_code',),)
            }
        ),

          (
            'Withdraw Fees Info', {
                'fields': (('withdraw_fees',),)
            }
        ),

          (
            'Transfer Fees Info', {
                'fields': (('transfer_min','transfer_max', 'transfer_fee','transfer_fees_code',),)
            }
        ),
        (
            'Chart of Accounts Info', {
                'fields': (('savings_deposit_acc','deposit_fines_acc',),
                ('interest_earned_acc','deposit_fee_acc',  ))
            }
        ),
        (
            'System Specific Info', {
                'fields': (( 'creator', 'modified_by',),)
            }
        ),

           (
            'Deleted Info', {
                'fields': (( 'deleted',),)
            }
        ),
    )


class SavingDepositContractModeladmin(admin.ModelAdmin):
    list_display = ['savings_contract',     'number_periods', 'rollover', 'transfer_account', 'withdrawal_fees', 'next_maturity',  'date_created','date_modified', 'modified_by' ]
    ordering = ['savings_contract',     'number_periods', 'rollover', 'transfer_account', 'withdrawal_fees', 'next_maturity',  'date_created','date_modified', 'modified_by' ]
    icon_name = 'map'

    search_fields = ['savings_contract',     'number_periods', 'rollover', 'transfer_account', 'withdrawal_fees', 'next_maturity',   ]
    list_filter = ['savings_contract',     'number_periods', 'rollover', 'transfer_account', 'withdrawal_fees', 'next_maturity',  'date_created','date_modified', 'modified_by' ]



# class SavingDepositContractModeladmin(admin.ModelAdmin):
#     list_display = ['savings_product',     'interest_frequency',  'date_created','date_modified', 'modified_by' ]
#     ordering = ['savings_product',     'interest_frequency',  'date_created','date_modified', 'modified_by' ]
#     icon_name = 'map'

#     search_fields = ['savings_product',     'interest_frequency',  'date_created','date_modified', 'modified_by' ]
#     list_filter =['savings_product',     'interest_frequency',  'date_created','date_modified', 'modified_by' ]

# admin.site.register(Savingbookcontracts)
# admin.site.register(Savingbookproducts, )
# admin.site.register(Savingcontracts, SavingContractModeladmin)
# admin.site.register(Savingdepositcontracts, SavingDepositContractModeladmin)
# admin.site.register(Savingevents)
# admin.site.register(Savingproducts, SavingProductAdminModel)
# admin.site.register(Savingproductsclienttypes)


class SavingAccountModelAdmin(admin.ModelAdmin):
    field = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator', 'date_modifed', 'modified_by']
    list_display = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']
    ordering = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']
    icon_name = 'map'

    search_fields =['acc_number', 'acc_balance', ]
    list_filter = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']





class CurrentAccountModelAdmin(admin.ModelAdmin):
    field = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator', 'date_modifed', 'modified_by']
    list_display = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']
    ordering = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']
    icon_name = 'map'

    search_fields =['acc_number', 'acc_balance',]
    list_filter = ['acc_number', 'acc_balance', 'member', 'date_created', 'creator',  'modified_by']



# admin.site.register(SavingAccount, SavingAccountModelAdmin)
# admin.site.register(CurrentAccount, CurrentAccountModelAdmin)
