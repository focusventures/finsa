from members.models import Member, MemberGroup
from loans.models import LoanAccrual, LoanContract, LoanDisbursement, LoanEntryFee, LoanProduct, LoanPurpose
from chartsoa.models import Account
from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from financial.models import Transactions


from savings.models import CurrentAccount

from parameters.models import Periods, Month, LOC, LOCEvent, Events
from loans.models import LoanRepayment, LoanStatus
from fosa.models import Slips

from django.db.models import Q

from decimal import Decimal
import datetime
import decimal

class LoanProcedure(object):
    def __init__(self,member, slip, loan_id, amount,transaction, request):
        print("(((((((((((((((((((((((((((((((((((((9")
        print(type(slip))
        self.member_id = member
        self.member = member
        self.amount = amount
        self.remainder = Decimal(0.00)
        self.account = None
        self.product = None
        self.contract = None
        self.entry_fee = None
        self.transaction = transaction
        self.type = None
        self.slip = slip
        self.loan = loan_id
        self.deposit_transaction = None
        self.parameters = request.parameters
        self.request = request


    def check_if_user_has_account(self):
        return self.member.has_saving_acc

    def retireve_member(self):
        try:
            self.member = Member.objects.get(id=self.member_id)
        except Member.DoesNotExist:
            self.member = None

    def retireve_account(self):
        try:
            self.account = CurrentAccount.objects.filter(member__id=self.member.id).first()
        except CurrentAccount.DoesNotExist:
            self.saving_account = None

    def retireve_contract(self):
        try:
            self.contract = LoanContract.objects.filter(id=self.saving_account.saving_contract.id).select_related("loan_product").first()
        except LoanContract.DoesNotExist:
            self.contract = None
        
    def retireve_product(self):
        try:
            self.product = LoanProduct.objects.filter(id=self.contract.loan_product.id).select_related(
                'interest_due_acc', 'interest_income_acc', 'accrued_penalty_acc', 'penalty_income_acc', 'interest_due_not_received', 'interest_accrued_acc', 'interest_accrued_but_not_due_acc', 'interest_code',
                'loan_processing_code', 'penalty_code', 'interest_accrued_code', 'late_fees_code', 'payment_code', 'disburse_code',
                 ).first()
        except LoanProduct.DoesNotExist:
            self.product = None
        
    def process_loan(self, request):
        self.retireve_member()
        if self.member != None:
            self.retireve_account()
            if self.account != None:
                self.retireve_contract()
                if self.contract != None:
                    self.retireve_product()
                    if self.product == None:
                        messages.success(request, 'The Loan Product Attached to this member contract does not exist')
                        return redirect(reverse('members_details', args=(self.member.id,)))

                    


                else:
                    messages.success(request, 'The Loan Contract Attached to this member contract does not exist')
                    return redirect(reverse('members_details', args=(self.member.id,)))

            else:
                messages.success(request, 'The Current Account Attached to this member does not exist')
                return redirect(reverse('members_details', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('members_details', args=(self.member.id,)))



    def process_loan_repayment(self):
        transaction_dep = Transactions.objects.filter(id=self.transaction).select_related('type').first()
        event = Events.objects.get(id=transaction_dep.type.id)
        print("{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{")
        print(self.slip)

        repayment = LoanRepayment()
        repayment.member = self.member
        repayment.amount = self.amount

        repayment.creator = self.request.user
        repayment.date_created = datetime.datetime.now()

        status = LoanStatus.objects.get(id=self.loan)
        status.total_balance = status.total_balance - self.amount
        status.total_repaid = status.total_repaid + self.amount
        status.save()

        repayment.loan_status = status

        repayment.loan_balance = status.total_balance

        repayment.transaction = transaction_dep
        repayment.slip = self.slip
        repayment.amount 
        repayment.save()

        # chart_oa = Account.objects.filter(transaction_type__id=event.id).first()
        # chart_oa.balance += decimal.Decimal(self.amount)
        # chart_oa.save()

        # loc = LOC.objects.filter(id=1).first()
        # loc.remainder_amount += transaction_dep.amount
        # loc.save()

        # loc_event = LOCEvent()
        # loc_event.loc = loc
        # loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
        # loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
        # loc_event.transaction = transaction_dep
        # loc_event.description = transaction_dep.description
        # loc_event.amount = transaction_dep.amount
        # loc_event.remainder = loc.remainder_amount
        # loc_event.type = 'Dr'
        # loc_event.creator = self.request.user
        # loc_event.date_created = datetime.datetime.now()
        # loc_event.save()
        return True



    def backtrack_loan(self, request):
        self.retireve_member()
        if self.member != None:
            self.retireve_account()
            if self.account != None:
                self.retireve_contract()
                if self.contract != None:
                    self.retireve_product()
                    if self.product == None:
                        messages.success(request, 'The Loan Product Attached to this member contract does not exist')
                        return redirect(reverse('members_details', args=(self.member.id,)))

                else:
                    messages.success(request, 'The Loan Contract Attached to this member contract does not exist')
                    return redirect(reverse('members_details', args=(self.member.id,)))

            else:
                messages.success(request, 'The Current Account Attached to this member does not exist')
                return redirect(reverse('members_details', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('members_details', args=(self.member.id,)))


        








        






        





        


