from chartsoa.models import Account
from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from financial.models import Transactions

from fosa.models import Slips
from shares.models import ShareCapital, ShareCapitalEvents
from parameters.models import Periods, Month, SystemParameters, LOC, LOCEvent

from members.models import Member

from decimal import Decimal
import datetime
import decimal

class SharesProcedure(object):
    def __init__(self, member, amount, type, slip, request):
        self.request = request
        self.member_id = member
        self.member = None
        self.amount = amount
        self.remainder = Decimal(0.00)
        self.account = None
        self.deposit_transaction = None
        self.parameters = request.parameters
        self.type = type
        self.slip = slip
        self.parameters = SystemParameters.objects.get(id=1)

    def check_if_user_has_account(self):
        try:
            self.account = ShareCapital.objects.get(member__id=self.member_id)

        except ShareCapital.DoesNotExist:
            account = ShareCapital()
            account.member = self.member
            account.creator = self.request.user
            account.account = '030' + self.member.account_number
            account.date_created = datetime.datetime.now()
            account.save()
            self.account = account


    def retireve_member(self):
        try:
            self.member = Member.objects.get(id=self.member_id)
        except Member.DoesNotExist:
            self.member = None


    def process_deposit(self, request):
        self.retireve_member()
        if self.member != None:
            self.check_if_user_has_account()
            if self.account != None:
                if self.account.share_bal >= self.parameters.share_capital:
                    messages.success(request, 'This Member has finished settling the share capital required')
                    return redirect(reverse('member_transaction', args=(self.member.id,)))

                self.account.share_bal += self.amount
                self.account.save()

                share_event = ShareCapitalEvents()
                share_event.member = self.member
                share_event.share_acc = self.account
                share_event.date_created = datetime.datetime.now()
                share_event.share_bal = self.account.share_bal
                share_event.creator = self.request.user
                share_event.activity = 'Share Capital Deposit'
                share_event.save()


                tranaction_dep = Transactions()
                tranaction_dep.amount = self.amount
                tranaction_dep.description = "Share Capital Deposit"
                tranaction_dep.type = "share_capital"
                tranaction_dep.creator = request.user
                tranaction_dep.date = datetime.datetime.now()
                tranaction_dep.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                tranaction_dep.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                tranaction_dep.date_created = datetime.datetime.now()
                tranaction_dep.member = self.member
                tranaction_dep.code = '1222'
                tranaction_dep.slip = self.slip
                tranaction_dep.type = 'Dr'
                tranaction_dep.save()


                loc = LOC.objects.filter(id=1).first()
                loc.remainder_amount += tranaction_dep.amount
                loc.save()

                loc_event = LOCEvent()
                loc_event.loc = loc
                loc_event.transaction = tranaction_dep
                loc_event.description = tranaction_dep.description
                loc_event.amount = tranaction_dep.amount
                loc_event.remainder = loc.remainder_amount
                loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                loc_event.type = 'Dr'
                loc_event.creator = self.request.user
                loc_event.date_created = datetime.datetime.now()
                loc_event.save()



                
                
            else:
                messages.success(request, 'The Member Does not have a Share Account And An attempt to create one failed')
                return redirect(reverse('member_transaction', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('member_transaction', args=(self.member.id,)))


    def process_redeem(self, request):
        self.retireve_member()
        if self.member != None:
            self.check_if_user_has_account()
            if self.account != None:
                if self.account.share_bal == self.parameters.share_capital:
                    messages.success(request, 'This Member has finished settling the share capital required')
                    return redirect(reverse('member_transaction', args=(self.member.id,)))

                self.account.share_bal += self.amount
                self.account.save()

                share_event = ShareCapitalEvents()
                share_event.member = self.member
                share_event.share_acc = self.account
                share_event.date_created = datetime.datetime.now()
                share_event.creator = self.request.user
                share_event.activity = 'Share Deposit'
                share_event.save()
                
            else:
                messages.success(request, 'The Member Does not have a Share Account And An attempt to create one failed')
                return redirect(reverse('member_transaction', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('member_transaction', args=(self.member.id,)))


