from chartsoa.models import Account
from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from financial.models import Transactions, GroupTransactions

from django.db.models import Q


from fosa.models import Slips
from shares.models import ShareCapital, ShareCapitalEvents
from financial.models import MemberCustomAccount

from django.forms.models import model_to_dict
from parameters.models import Periods, Month, SystemParameters, LOC, LOCEvent

from members.models import Member
from products.models import Products

from decimal import Decimal
import datetime
import decimal
from parameters.models import SystemParameters

class CustomProcedure(object):
    def __init__(self, member, amount, type, slip, request):
        self.request = request
        self.member_id = member
        self.member = None
        self.amount = amount
        self.remainder = Decimal(0.00)
        self.account = None
        self.deposit_transaction = None
        self.parameters = request.parameters
        self.product = None
        self.type = type
        self.slip = slip
        self.parameters = request.parameters

    def check_if_user_has_account(self):
        try:
            self.account = MemberCustomAccount.objects.get(Q(member__id=self.member_id) & Q(acc_type = self.type))

        except MemberCustomAccount.DoesNotExist:
            account = MemberCustomAccount()
            account.acc_type = self.type
            account.member = self.member
            account.creator = self.request.user

            # if self.parameters["settings"]["account_code"] == "account_number":

            if len(str(self.type.id)) == 1:
                account.acc_number = '00'  + str(self.type.id) + '-' + self.member.account_number

            elif len(str(self.type.id)) == 2:
                account.acc_number = '0'  + str(self.type.id) + '-' + self.member.account_number

            elif len(str(self.type.id)) >= 3:
                account.acc_number =  str(self.type.id) + '-' + self.member.account_number


            # elif self.parameters["settings"]["account_code"] == "id_number":

                # if len(str(self.type.id)) == 1:
                #     account.account = '00'  + str(self.type.id) + '-' + self.member.id_number

                # elif len(str(self.type.id)) == 2:
                #     account.account = '0'  + str(self.type.id) + '-' + self.member.id_number

                # elif len(str(self.type.id)) >= 3:
                #     account.account =  str(self.type.id) + '-' + self.member.id_number



            account.date_created = datetime.datetime.now()
            account.save()
            self.account = account
            # system = SystemParameters.objects.get(id=1)
            # # check if system allows automatic creation of accounts
            # if system.allow_automatic_creation_of_accounts:

            #     #check if event allows creation of accounts
            #     if self.type.can_create_accounts:
            #         account = MemberCustomAccount()
            #         account.acc_type = self.type
            #         account.member = self.member
            #         account.creator = self.request.user

            #         # if self.parameters["settings"]["account_code"] == "account_number":

            #         if len(str(self.type.id)) == 1:
            #             account.acc_number = '00'  + str(self.type.id) + '-' + self.member.account_number

            #         elif len(str(self.type.id)) == 2:
            #             account.acc_number = '0'  + str(self.type.id) + '-' + self.member.account_number

            #         elif len(str(self.type.id)) >= 3:
            #             account.acc_number =  str(self.type.id) + '-' + self.member.account_number


            #         # elif self.parameters["settings"]["account_code"] == "id_number":

            #             # if len(str(self.type.id)) == 1:
            #             #     account.account = '00'  + str(self.type.id) + '-' + self.member.id_number

            #             # elif len(str(self.type.id)) == 2:
            #             #     account.account = '0'  + str(self.type.id) + '-' + self.member.id_number

            #             # elif len(str(self.type.id)) >= 3:
            #             #     account.account =  str(self.type.id) + '-' + self.member.id_number



            #         account.date_created = datetime.datetime.now()
            #         account.save()
            #         self.account = account

            #     else:
            #         messages.success(self.request, 'System Event Does Not Allow Creation Of Accounts.')


            # else:
            #     messages.success(self.request, 'System Does Not Allow Automatic Creation Of Accounts. Go To Admin ==> Parameters ==> Sytem Settings and check --> Allow Automatic Creation Of Accounts and then Click Save')
                


    def retireve_member(self):
        try:
            self.member = Member.objects.get(id=self.member_id)
        except Member.DoesNotExist:
            self.member = None


    def get_product(self):
        try:
            self.product = Products.objects.get(id=1)

        except Products.DoesNotExist:
            self.product = None



    def process_deposit(self, request):
        self.retireve_member()
        if self.member != None:
            self.check_if_user_has_account()
            if self.account != None:

                # p = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                p = Periods.objects.get(id=1)

                if self.product != None:
                    tranaction = Transactions()
                    tranaction.actual_amount = self.amount
                    tranaction.type = self.type
                    tranaction.description = "Savings Deposit"
                    tranaction.slip = self.slip

                    if self.product.deposit_fee >= 0:
                        self.remainder = decimal.Decimal(self.amount) -  decimal.Decimal(self.product.deposit_fee)

                        tranaction_dep = Transactions()
                        tranaction_dep.amount = self.product.deposit_fee
                        tranaction_dep.description = str(self.type.title) + 'Fee'
                        tranaction_dep.creator = request.user
                        tranaction_dep.date = self.slip.date
                        tranaction_dep.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        tranaction_dep.financial_period = p
                        tranaction_dep.date_created = datetime.datetime.now()
                        tranaction_dep.member = self.member
                        tranaction_dep.code = self.product.deposit_fees_code
                        tranaction_dep.slip = self.slip
                        tranaction_dep.type = 'Dr'

                        tranaction_dep.save()

                        # loc = LOC.objects.filter(id=1).first()
                        # loc.remainder_amount += tranaction_dep.amount
                        # loc.save()

                        # loc_event = LOCEvent()
                        # loc_event.loc = loc
                        # loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        # loc_event.financial_year = p
                        # loc_event.transaction = tranaction_dep
                        # loc_event.description = tranaction_dep.description
                        # loc_event.amount = tranaction_dep.amount
                        # loc_event.remainder = loc.remainder_amount
                        # loc_event.type = 'Dr'
                        # loc_event.creator = self.request.user
                        # loc_event.date_created = datetime.datetime.now()
                        # loc_event.save()


                        self.deposit_transaction = tranaction_dep

                        deposit_fee_acc = Account.objects.get(id=self.product.deposit_fee_acc.id)
                        deposit_fee_acc.balance += tranaction_dep.amount
                        deposit_fee_acc.save()

                        tranaction.fees = tranaction_dep

                    ## Deposit Remainder
                    tranaction.type = self.type
                    tranaction.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                    tranaction.financial_period = p
                    tranaction.creator = request.user
                    tranaction.date = datetime.datetime.now()
                    tranaction.date_created = datetime.datetime.now()
                    tranaction.member = self.member
                    tranaction.amount = self.remainder
                    tranaction.type = 'Dr'
                    tranaction.code = self.product.deposit_code
                        
                    # savings_chart_oa = Account.objects.get(id=self.product.savings_deposit_acc.id)
                    # savings_chart_oa.balance += decimal.Decimal(self.amount)
                    # savings_chart_oa.save()

                    # tranaction.coa = savings_chart_oa
                    tranaction.creator = request.user
                    tranaction.date = datetime.datetime.now()
                    tranaction.acc = self.account.acc_number
                    tranaction.save()

                    # loc = LOC.objects.filter(id=1).first()
                    # loc.remainder_amount += tranaction.amount
                    # loc.save()

                    # loc_event = LOCEvent()
                    # loc_event.loc = loc
                    # loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                    # loc_event.financial_year = p
                    # loc_event.transaction = tranaction
                    # loc_event.description = tranaction.description
                    # loc_event.amount = tranaction.amount
                    # loc_event.remainder = loc.remainder_amount
                    # loc_event.type = 'Dr'
                    # loc_event.creator = self.request.user
                    # loc_event.date_created = datetime.datetime.now()
                    # loc_event.save()

                    slip = Slips.objects.get(id=self.slip.id)

                    slip.tax = 0.00
                    slip.save()

                    self.account.acc_balance += decimal.Decimal(self.amount)
                    self.account.save()

                    return True


                else:
                    self.account.acc_balance = decimal.Decimal(self.account.acc_balance) +  decimal.Decimal(self.amount)
                    self.account.save()

                    tranaction_dep = Transactions()
                    tranaction_dep.amount = self.amount
                    tranaction_dep.description = self.type.title
                    tranaction_dep.type = self.type
                    tranaction_dep.creator = request.user
                    tranaction_dep.date = datetime.datetime.now()
                    tranaction_dep.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                    tranaction_dep.financial_period = p
                    tranaction_dep.date_created = datetime.datetime.now()
                    tranaction_dep.member = self.member
                    tranaction_dep.code = '978'
                    tranaction_dep.slip = self.slip
                    tranaction_dep.double_entry = 'Dr'
                    tranaction_dep.acc = self.account.acc_number
                    tranaction_dep.date_created = datetime.datetime.now()
                    tranaction_dep.actual_amount = self.amount
                    tranaction_dep.save()

                    # chart_oa = Account.objects.filter(transaction_type=self.type).first()
                    # chart_oa.balance += decimal.Decimal(self.amount)
                    # chart_oa.save()

                    # tranaction_dep.coa =  chart_oa
                    tranaction_dep.creator = request.user
                    tranaction_dep.date = datetime.datetime.now()
                    tranaction_dep.acc = self.account.acc_number
                    tranaction_dep.save()


                    # tranaction = GroupTransactions()
                    # tranaction.amount = self.amount
                    # tranaction.description = self.type.title
                    # tranaction.type = self.type
                    # tranaction.creator = request.user
                    # tranaction.date = datetime.datetime.now()
                    # tranaction.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                    # tranaction.financial_period = p
                    # tranaction.date_created = datetime.datetime.now()
                  
                    # tranaction.code = '978'
                    # # tranaction.slip = self.slip
                    # tranaction.double_entry = 'Dr'
                    # tranaction.acc = self.account.acc_number
                    # tranaction.
                    # tranaction.date_created = datetime.datetime.now()
                    # tranaction.actual_amount = self.amount
                    
                    # tranaction.save()

                    # # chart_oa = Account.objects.filter(transaction_type=self.type).first()
                    # # chart_oa.balance += decimal.Decimal(self.amount)
                    # # chart_oa.save()

                    # # tranaction.coa =  chart_oa
                    # tranaction.creator = request.user
                    # tranaction.date = datetime.datetime.now()
                    # tranaction.acc = self.account.acc_number
                    # tranaction.save()


                    # loc = LOC.objects.filter(is_cash=True).first()
                    # loc.remainder_amount += tranaction.amount
                    # loc.save()

                    # loc_event = LOCEvent()
                    # loc_event.loc = loc
                    # loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                    # loc_event.financial_year = p
                    # loc_event.transaction = tranaction
                    # loc_event.description = tranaction.description
                    # loc_event.amount = tranaction.amount
                    # loc_event.remainder = loc.remainder_amount
                    # loc_event.type = 'Dr'
                    # loc_event.creator = self.request.user
                    # loc_event.date_created = datetime.datetime.now()
                    # loc_event.save()


                    self.slip.total = decimal.Decimal(self.slip.total) + decimal.Decimal(self.amount)
                    self.slip.save()

                    return True



                
                
            else:
                messages.success(request, 'The Member Does not have a %s Account And An attempt to create one failed' % (self.type))
                return redirect(reverse('member_transaction', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('member_transaction', args=(self.member.id,)))


