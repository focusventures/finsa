from members.models import Member, MemberGroup
from savings.models import SavingAccount, SavingContractEntryFee, Savingevents, Savingproducts, Savingcontracts, SavingWithdrawFee, SavingTransferFee, SavingDepositFee
from chartsoa.models import Account
from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from financial.models import Transactions
from parameters.models import LOC, LOCEvent


from fosa.models import Slips

from parameters.models import Periods, Month

from decimal import Decimal
import datetime
import decimal

class SavingsProcedure(object):
    def __init__(self, member, amount, type, slip, request):
        self.request = request
        self.member_id = member
        self.member = None
        self.amount = amount
        self.remainder = Decimal(0.00)
        self.saving_account = None
        self.saving_product = None
        self.saving_contract = None
        self.deposit_fee = None
        self.deposit_transaction = None
        self.parameters = request.parameters
        self.type = type
        self.slip = slip


    def generate_slip(self):
        slip = Slips()
        slip.number = slip.generate_slip_number(self.member.id, 'M')
        slip.creator = self.request.user
        slip.date_created = datetime.datetime.now()
        slip.member = self.member
        slip.save()
        self.slip = slip
        return

    def check_if_user_has_account(self):
        return self.member.has_saving_acc

    def retireve_member(self):
        try:
            self.member = Member.objects.get(id=self.member_id)
        except Member.DoesNotExist:
            self.member = None

    def retireve_account(self):
        try:
            self.saving_account = SavingAccount.objects.filter(member__id=self.member.id).select_related("saving_contract").first()
        except SavingAccount.DoesNotExist:
            self.saving_account = None

    def retireve_contract(self):
        try:
            self.saving_contract = Savingcontracts.objects.filter(id=self.saving_account.saving_contract.id).select_related("product").first()
        except Savingcontracts.DoesNotExist:
            self.saving_contract = None
        
    def retireve_product(self):
        try:
            self.saving_product = Savingproducts.objects.filter(id=self.saving_contract.product.id).select_related(
                'deposit_fees_code', 'deposit_code', 'withdraw_code', 'transfer_code', 'savings_deposit_acc', 'deposit_fines_acc', 'interest_earned_acc', 'deposit_fee_acc' ).first()
        except Savingproducts.DoesNotExist:
            self.saving_product = None
        
    def process_deposit(self, request):
        self.retireve_member()
        if self.member != None:
            self.retireve_account()
            if self.saving_account != None:
                self.retireve_contract()
                if self.saving_contract != None:
                    self.retireve_product()
                    if self.saving_product == None:
                        messages.success(request, 'The saving Product Attached to this member contract does not exist')
                        return redirect(reverse('member_transaction', args=(self.member.id,)))

                    else:
                        tranaction = Transactions()
                        tranaction.actual_amount = self.amount
                        tranaction.type = self.type
                        tranaction.description = "Savings Deposit"
                        tranaction.slip = self.slip

                        if self.saving_product.deposit_fee >= 0:
                            self.remainder = decimal.Decimal(self.amount) -  decimal.Decimal(self.saving_product.deposit_fee)

                            deposit_fee = SavingDepositFee.objects.create(
                                member = self.member,
                                amount = self.saving_product.deposit_fee,
                                creator = request.user,
                                date_created = datetime.datetime.now(),
                                code = self.saving_product.deposit_fees_code.code,
                                contract = self.saving_contract,
                            )  

                            deposit_fee.save()

                            self.deposit_fee = deposit_fee

                            tranaction_dep = Transactions()
                            tranaction_dep.amount = self.saving_product.deposit_fee
                            tranaction_dep.description = "Saving Deposit Fee"
                            tranaction_dep.type = "saving_deposit_fee"
                            tranaction_dep.creator = request.user
                            tranaction_dep.date = datetime.datetime.now()
                            tranaction_dep.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                            tranaction_dep.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                            tranaction_dep.date_created = datetime.datetime.now()
                            tranaction_dep.member = self.member
                            tranaction_dep.code = self.saving_product.deposit_fees_code
                            tranaction_dep.slip = self.slip
                            tranaction_dep.type = 'Dr'

                            tranaction_dep.save()

                            loc = LOC.objects.filter(id=1).first()
                            loc.remainder_amount += tranaction_dep.amount
                            loc.save()

                            loc_event = LOCEvent()
                            loc_event.loc = loc
                            loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                            loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                            loc_event.transaction = tranaction_dep
                            loc_event.description = tranaction_dep.description
                            loc_event.amount = tranaction_dep.amount
                            loc_event.remainder = loc.remainder_amount
                            loc_event.type = 'Dr'
                            loc_event.creator = self.request.user
                            loc_event.date_created = datetime.datetime.now()
                            loc_event.save()


                            self.deposit_transaction = tranaction_dep

                            deposit_fee_acc = Account.objects.get(id=self.saving_product.deposit_fee_acc.id)
                            deposit_fee_acc.balance += deposit_fee.amount
                            deposit_fee_acc.save()

                            tranaction.fees = tranaction_dep

                        ## Deposit Remainder
                        tranaction.type = self.type
                        tranaction.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        tranaction.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                        tranaction.creator = request.user
                        tranaction.date = datetime.datetime.now()
                        tranaction.date_created = datetime.datetime.now()
                        tranaction.member = self.member
                        tranaction.amount = self.remainder
                        tranaction.type = 'Dr'
                        tranaction.code = self.saving_product.deposit_code
                            
                        savings_chart_oa = Account.objects.get(id=self.saving_product.savings_deposit_acc.id)
                        savings_chart_oa.balance += decimal.Decimal(self.amount)
                        savings_chart_oa.save()

                        tranaction.coa = savings_chart_oa
                        tranaction.creator = request.user
                        tranaction.date = datetime.datetime.now()
                        tranaction.acc = self.saving_account.acc_number
                        tranaction.save()

                        loc = LOC.objects.filter(id=1).first()
                        loc.remainder_amount += tranaction.amount
                        loc.save()

                        loc_event = LOCEvent()
                        loc_event.loc = loc
                        loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                        loc_event.transaction = tranaction
                        loc_event.description = tranaction.description
                        loc_event.amount = tranaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.type = 'Dr'
                        loc_event.creator = self.request.user
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()

                        slip = Slips.objects.get(id=self.slip.id)

                        # total = slip.total + decimal.Decimal(self.amount)
                        # fees = slip.fees + decimal.Decimal(self.saving_product.deposit_fee)
                        # slip.total = Decimal(int(slip.total) + int(self.amount))
                        # slip.fees = Decimal(int(slip.total) + int(tranaction_dep.amount))
                        slip.tax = 0.00
                        slip.save()


                        self.saving_account.acc_balance += decimal.Decimal(self.amount)
                        self.saving_account.save()

                else:
                    messages.success(request, 'The saving Contract Attached to this member contract does not exist')
                    return redirect(reverse('member_transaction', args=(self.member.id,)))

            else:
                messages.success(request, 'The Member Does not have a Saving Account Attached')
                return redirect(reverse('member_transaction', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('member_transaction', args=(self.member.id,)))






    def process_withdraw(self, request):
        self.retireve_member()
        if self.member != None:
            self.retireve_account()
            if self.saving_account != None:
                self.retireve_contract()
                if self.saving_contract != None:
                    self.retireve_product()
                    if self.saving_product == None:
                        messages.success(request, 'The saving Product Attached to this member contract does not exist')
                        return redirect(reverse('members_details', args=(self.member.id,)))

                    else:

                        tranaction = Transactions()
                      
                        tranaction.description = "Savings Withdraw"
                        tranaction.slip = self.slip

                        if self.saving_product.withdraw_fees >= 0:
                            self.remainder = decimal.Decimal(self.amount) +  decimal.Decimal(self.saving_product.withdraw_fees)

                            withdraw_fee = SavingWithdrawFee.objects.create(
                                member = self.member,
                                amount = self.saving_product.withdraw_fees,
                                creator = request.user,
                                date_created = datetime.datetime.now(),
                                code = self.saving_product.withdraw_fees_code,
                                contract = Savingcontracts.objects.get(id=self.saving_contract.id),
                            )
                            withdraw_fee.save()

                        
                            tranaction_dep = Transactions()
                            tranaction_dep.amount = self.saving_product.withdraw_fees
                            tranaction_dep.type = "saving_withdraw_fee"
                            tranaction_dep.description = "Saving Withdraw Fee"

                            tranaction_dep.creator = request.user
                            tranaction_dep.date = datetime.datetime.now()
                            tranaction_dep.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                            tranaction_dep.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                            tranaction_dep.date_created = datetime.datetime.now()
                            tranaction_dep.member = self.member
                            tranaction_dep.slip = self.slip
                            tranaction_dep.code = self.saving_product.withdraw_fees_code


                            deposit_fee_acc = Account.objects.get(id=self.saving_product.deposit_fee_acc.id)
                            deposit_fee_acc.balance += withdraw_fee.amount
                            deposit_fee_acc.save()

                            tranaction_dep.coa = deposit_fee_acc
                            tranaction_dep.save()


                            loc = LOC.objects.filter(id=1).first()
                            loc.remainder_amount += tranaction_dep.amount
                            loc.save()

                            loc_event = LOCEvent()
                            loc_event.description = tranaction_dep.description
                            loc_event.loc = loc
                            loc_event.amount = tranaction_dep.amount
                            loc_event.remainder = loc.remainder_amount
                            loc_event.transaction = tranaction_dep
                            loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                            loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                            loc_event.type = 'Dr'
                            loc_event.creator = self.request.user
                            loc_event.date_created = datetime.datetime.now()
                            loc_event.save()


                        tranaction.type = self.type

                        tranaction.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        tranaction.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                        tranaction.creator = self.request.user
                        tranaction.date = datetime.datetime.now()
                        tranaction.date_created = datetime.datetime.now()
                        tranaction.member = self.member
                        tranaction.amount = self.amount
                        tranaction.code = self.saving_product.transfer_fees_code

                        tranaction.actual_amount = self.remainder
                        tranaction.save()

                        loc = LOC.objects.filter(id=1).first()
                        loc.remainder_amount -= tranaction.amount
                        loc.save()

                        loc_event = LOCEvent()
                        loc_event.description = tranaction.description
                        loc_event.loc = loc
                        loc_event.amount = tranaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.transaction = tranaction
                        loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                        loc_event.type = 'Cr'
                        loc_event.creator = self.request.user
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()

                        slip = Slips.objects.get(id=self.slip.id)
                        slip.total += decimal.Decimal(self.amount)
                        slip.fees += decimal.Decimal(self.saving_product.deposit_fee)
                        slip.tax += decimal.Decimal(0.00)
                        slip.save()


                        self.saving_account.acc_balance -= (decimal.Decimal(self.amount) + self.saving_product.withdraw_fees)
                        self.saving_account.save()


                else:
                    messages.success(request, 'The saving Contract Attached to this member contract does not exist')
                    return redirect(reverse('members_details', args=(self.member.id,)))

            else:
                messages.success(request, 'The saving Account Attached to this member does not exist')
                return redirect(reverse('members_details', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('members_details', args=(self.member.id,)))




    def process_transfer(self, request):
        self.retireve_member()
        if self.member != None:
            self.retireve_account()
            if self.saving_account != None:
                self.retireve_contract()
                if self.saving_contract != None:
                    self.retireve_product()
                    if self.saving_product == None:
                        messages.success(request, 'The saving Product Attached to this member contract does not exist')
                        return redirect(reverse('members_details', args=(self.member.id,)))

                    else:

                        tranaction = Transactions()
                        tranaction.actual_amount = self.amount
                        tranaction.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        tranaction.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                        tranaction.creator = request.user
                        tranaction.date = datetime.datetime.now()
                        tranaction.date_created = datetime.datetime.now()
                        tranaction.member = self.member

                        if self.saving_product.withdraw_fees >= 0:
                            self.remainder = decimal.Decimal(self.amount) +  decimal.Decimal(self.saving_product.transfer_fee)

                            deposit_fee = SavingTransferFee.objects.create(
                                member = self.member,
                                amount = self.saving_contract.withdraw_fees,
                                creator = request.user,
                                date_created = datetime.datetime.now(),
                                code = self.saving_product.withdraw_fees_code,
                                contract = self.saving_contract,
                            )
                            deposit_fee.save()

                            tranaction_dep = Transactions()
                            tranaction_dep.amount = self.saving_product.transfer_fee
                            tranaction_dep.type = self.type
                            tranaction_dep.description = "Saving Transfer Fee"
                            tranaction_dep.code = self.saving_product.transfer_fees_code

                            tranaction_dep.amount = self.saving_product.withdraw_fees
                            tranaction_dep.type = "saving_transfer_fee"
                            tranaction_dep.description = "Saving Transfer Fee"

                            tranaction_dep.creator = request.user
                            tranaction_dep.date = datetime.datetime.now()
                            tranaction_dep.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                            tranaction_dep.financial_period = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                            tranaction_dep.date_created = datetime.datetime.now()
                            tranaction_dep.member = self.member
                            tranaction_dep.slip = self.slip
                            tranaction_dep.code = self.saving_product.withdraw_fees_code
                            tranaction_dep.save()

                            loc = LOC.objects.filter(id=1).first()
                            loc.remainder_amount += tranaction_dep.amount
                            loc.save()

                            loc_event = LOCEvent()
                            loc_event.description = tranaction_dep.description
                            loc_event.loc = loc
                            loc_event.amount = tranaction_dep.amount
                            loc_event.remainder = loc.remainder_amount
                            loc_event.transaction = tranaction_dep
                            loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                            loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                            loc_event.type = 'Dr'
                            loc_event.creator = self.request.user
                            loc_event.date_created = datetime.datetime.now()
                            loc_event.save()


                        deposit_fee_acc = Account.objects.get(id=self.saving_product.deposit_fee_acc.id)
                        deposit_fee_acc.balance += deposit_fee.amount
                        deposit_fee_acc.save()

                        savings_chart_oa = Account.objects.get(id=self.saving_product.savings_deposit_acc.id)
                        savings_chart_oa.balance -= decimal.Decimal(self.amount)
                        savings_chart_oa.save()

                        tranaction.coa = savings_chart_oa
                        tranaction.amount = self.remainder
                        tranaction.code = self.saving_product.transfer_code
                        tranaction.save()

                        loc = LOC.objects.filter(id=1).first()
                        loc.remainder_amount += tranaction.amount
                        loc.save()

                        loc_event = LOCEvent()
                        loc_event.description = tranaction.description
                        loc_event.loc = loc
                        loc_event.amount = tranaction.amount
                        loc_event.remainder = loc.remainder_amount
                        loc_event.transaction = tranaction
                        loc_event.financial_month = Month.objects.get(id=self.parameters["settings"]["financial_month"])
                        loc_event.financial_year = Periods.objects.get(id=self.parameters["settings"]["financial_period"])
                        loc_event.type = 'Dr'
                        loc_event.creator = self.request.user
                        loc_event.date_created = datetime.datetime.now()
                        loc_event.save()


                        slip = Slips.objects.get(id=self.slip.id)
                        slip.total += decimal.Decimal(self.amount)
                        slip.fees += decimal.Decimal(self.saving_product.deposit_fee)
                        slip.tax += decimal.Decimal(0.00)
                        slip.save()



                        self.saving_account.acc_balance -= decimal.Decimal(self.amount)
                        self.saving_account.save()
                      
                else:
                    messages.success(request, 'The saving Contract Attached to this member contract does not exist')
                    return redirect(reverse('members_details', args=(self.member.id,)))

            else:
                messages.success(request, 'The saving Account Attached to this member does not exist')
                return redirect(reverse('members_details', args=(self.member.id,)))

        else:
            messages.success(request, 'The Member Does Not Exist')
            return redirect(reverse('members_details', args=(self.member.id,)))





        






        





        


