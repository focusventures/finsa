from django.conf import settings
from django.db import connection
from django.http import Http404
from django.urls import set_urlconf
from django.contrib.auth.models  import AnonymousUser 
from django.utils.deprecation import MiddlewareMixin

from django.forms.models import model_to_dict

import json

from parameters.models import SystemParameters, Theme, Periods
from loans.models import LoanContract
from locations.models import Location
from authentication.models import CustomUser




class ParametersMiddleware(MiddlewareMixin):
    """
    This middleware should be placed at the very top of the middleware stack.
    Selects the proper database schema using the request host. Can fail in
    various ways which is better than corrupting or revealing data.
    """
    def process_request(self, request):
        if isinstance(request.user, AnonymousUser):
            pass
        else:
            user = CustomUser.objects.get(id=request.user.id)
            if user.is_superuser:
                location = "Super"
            else:
                location = model_to_dict(user.branch)

            parameters = {

            }

            try:
                set_ = SystemParameters.objects.filter(id=1).select_related('financial_period', 'financial_month').first()
                settings = model_to_dict(set_)
                parameters['month'] = set_.financial_month.name
                parameters['year'] = set_.financial_period.name

            except SystemParameters.DoesNotExist:
                settings = None

            parameters["settings"] = settings
            parameters["branch"] = location
            print("OOOOOOOOOOOOOOOPPPPPPPPPPPPPPPPPP>>>>>>>>>>>>>>>>>>")
            request.parameters = parameters
            print(request.parameters)
        

