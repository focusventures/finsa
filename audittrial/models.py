from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.admin.models import LogEntry

# Create your models here.
event_type = (
    ("Member Create","Member Create" ),
)

class ActivityLog(models.Model):
    type = models.CharField(_("Event Type"), choices=event_type, max_length=50, null=True)
    description = models.CharField(_("Event Description"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    member = models.ForeignKey("members.Member",  related_name="Member+", verbose_name=_("Member"), null=True, blank=True, on_delete=models.DO_NOTHING)
    model = models.CharField(_("Model Name"), max_length=50, null=True)
    item_id = models.IntegerField(_("Item Id"), null=True)
    cash_flow = models.BooleanField(_("About Money"), default=True)
    date = models.DateField(_("Date of Transaction"), auto_now=False, auto_now_add=False, null=True)
    processed_by = models.ForeignKey("authentication.CustomUser", verbose_name=_("Processed By"), on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(_("Amount Transacted"), default=0, max_digits=20, decimal_places=2)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} - {}".format(self.name, self.account_number, self.type)


    class Meta:
        verbose_name_plural = "Activity Log"
