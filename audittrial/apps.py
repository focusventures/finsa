from django.apps import AppConfig


class AudittrialConfig(AppConfig):
    name = 'audittrial'
    icon_name = 'perm_device_information'
