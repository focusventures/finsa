from django.contrib import admin

from .models import ActivityLog

from django.contrib.admin.models import LogEntry



class LogEntryModelAdmin(admin.ModelAdmin):
    # field = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    list_display = ['user_id', 'content_type_id', 'object_id',  ]
    # ordering = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    # icon_name = 'money'

    # search_fields =['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    # list_filter = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']

admin.site.register(LogEntry, LogEntryModelAdmin)


class ActivityModelAdmin(admin.ModelAdmin):
    # field = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    list_display = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    ordering = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    icon_name = 'money'

    search_fields =['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']
    list_filter = ['type', 'description', 'date_created', 'member', 'model','item_id', 'cash_flow', 'date', 'processed_by', 'amount',  'creator', 'date_created', 'date_modified', 'modified_by']


admin.site.register(ActivityLog, ActivityModelAdmin)
