from django.db import models
from django.utils.translation import ugettext_lazy as _


counties = (
    ("Mombasa", "Mombasa"),
	("Kwale","Kwale"),
	("Kilifi","Kilifi"),	
	("Tana River",	"Tana River"),
	("Lamu", "Lamu"),	
	("Taita–Taveta","Taita–Taveta"),
	("Garissa",	"Garissa"),
	("Wajir","Wajir"),	
	("Mandera", "Mandera"),
	("Marsabit","Marsabit"),	
	("Isiolo","Isiolo"),	
	("Meru","Meru"),	
	("Tharaka-Nithi",	"Tharaka-Nithi"),
	("Embu","Embu"),	
	("Kitui","Kitui"),	
	("Machakos","Machakos"),	
	("Makueni","Makueni"),
	("Nyandarua","Nyandarua"),	
	("Nyeri","Nyeri"),
	("Kirinyaga","Kirinyaga"),	
	("Murang'a", "Murang'a"),
	("Kiambu","Kiambu"),	
	("Turkana", "Turkana"),
	("West Pokot",	"West Pokot"),
	("Samburu", "Samburu"),
	("Trans-Nzoia","Trans-Nzoia"),
	("Uasin Gishu",	 "Uasin Gishu"),	
	("Elgeyo-Marakwet",	"Elgeyo-Marakwet"),
	("Nandi", "Nandi"),	
	("Baringo", "Baringo"),
	("Laikipia", "Laikipia"),
	("Nakuru", "Nakuru"),	
    ("Narok", "Narok"),
	("Kajiado", "Kajiado"),
	("Kericho", "Kericho"),
	("Bomet", "Bomet"),	 
	("Kakamega","Kakamega"),
	("Vihiga", "Vihiga"),	
	("Bungoma", "Bungoma"),
	("Busia", "Busia"),
	("Siaya", "Siaya"),	 
	("Kisumu",	 "Kisumu"),
	("Homa Bay", "Homa Bay"),	
	("Migori",	 "Migori"),
	("Kisii", "Kisii"),
	("Nyamira",	"Nyamira"),
    ("Nairobi", "Nairobi"),


)
class Branch(models.Model):
    name = models.CharField(_("Branch Name"),  max_length=50)
    manager = models.ForeignKey("authentication.CustomUser",  related_name="Manager+", verbose_name=_("Managed by"), on_delete=models.CASCADE)
    agents = models.ManyToManyField("authentication.CustomUser", related_name="Agents+", verbose_name=_("Agents"), blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    town = models.CharField(_("Town"), max_length=50)
    city = models.CharField(_("City"), max_length=50)
    house = models.CharField(_("House"), max_length=50)
    room_number = models.CharField(_("Room Number"), max_length=50)
    phone_number = models.CharField(_("Phone Number"), max_length=50)
    county = models.CharField(_("County"), choices=counties,  max_length=50)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Building Creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False,  blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  blank=True, null=True,  related_name="Building Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Branch"
        verbose_name_plural = "Branches"

class Department(models.Model):
    name = models.CharField(_("Department Name"),  max_length=50)
    manager = models.ForeignKey("authentication.CustomUser",  related_name="Manager+", verbose_name=_("Managed by"), on_delete=models.CASCADE)
    branch = models.ForeignKey("ostructure.Branch", related_name="Branch+", verbose_name=_("Branch"), default=None, on_delete=models.DO_NOTHING, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Building Creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False,  blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  blank=True, null=True,  related_name="Building Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Department"
        verbose_name_plural = "Departments"


class Clientbranchhistory(models.Model):
    date_changed = models.DateTimeField(blank=True, null=True)
    branch_from = models.ForeignKey('ostructure.Branch', models.DO_NOTHING)
    branch_to_id = models.IntegerField()
    client = models.ForeignKey('members.Tiers', models.DO_NOTHING)
    user = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'ClientBranchHistory'
        verbose_name = "Client Branch History"
        verbose_name_plural = "Client Branch History"


class Clientlocation(models.Model):
    client_id = models.IntegerField()
    lat = models.FloatField(blank=True, null=True)
    lng = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ClientLocation'
        verbose_name = "Client Location"
        verbose_name_plural = "Client Location"

class Clienttypes(models.Model):
    type_name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'ClientTypes'
        verbose_name = "Client Type"
        verbose_name_plural = "Client Types"
