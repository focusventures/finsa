from django.urls import path

from . import views



urlpatterns = [
    path('branches', views.branches, name="branches"),
    path('branch/details/<int:pk>', views.branch_details, name="branch_details"),
    path('branch/aoe', views.branch_aoe, name="branch_aoe"),
    path('branch/edit/<int:pk>', views.branch_edit, name="branch_edit"),
    path('branch/delete/<int:pk>', views.branch_delete, name="branch_delete"),


    path('departments', views.departments, name="departments"),
    path('department/details/<int:pk>', views.department_details, name="department_details"),
    path('department/aoe', views.department_aoe, name="department_aoe"),
    path('department/edit/<int:pk>', views.department_edit, name="department_edit"),
    path('department/delete/<int:pk>', views.department_delete, name="department_delete"),

]
