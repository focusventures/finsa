from django.shortcuts import render, redirect, reverse
from .forms import BranchForm, DepartmentForm
from .models import Branch, Department

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def branches(request):
    posts_list = Branch.objects.all().select_related('manager')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    context = {
        "clients": posts
    }

    return render(request, 'ostructure/list.html', context)



def branch_aoe(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """

    if request.method == 'POST':
        form = BranchForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('branches'))
    else:
        form = BranchForm()
    context = {'form': form}
    return render(request, 'ostructure/aoe.html', context)


def branch_edit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    branch = Branch.objects.get(id=pk)
    if request.method == 'POST':
        form = BranchForm(request.POST,instance=branch )
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creater = request.user
            atype.save()
            return redirect(reverse('branches'))
    else:
        form = BranchForm(instance=branch)
      
    context = {'form': form}
    return render(request, 'ostructure/aoe.html', context)

def branch_delete(request, pk):
    object = Branch.objects.get(id=pk).delete()
    return redirect('/')


def branch_details(request, pk):
    
    posts = Branch.objects.filter(id=pk)
    context = {
        # "shop": shop,
        "account": posts
    }  
    return render(request, 'ostructure/details.html', context)


















def departments(request):
    posts_list = Department.objects.all().select_related('manager')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    context = {
        "clients": posts
    }

    return render(request, 'ostructure/departments.html', context)



def department_aoe(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """

    if request.method == 'POST':
        form = DepartmentForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('departments'))
    else:
        form = DepartmentForm()
    context = {'form': form}
    return render(request, 'ostructure/department_aoe.html', context)


def department_edit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    Branch = Department.objects.get(id=pk)
    if request.method == 'POST':
        form = DepartmentForm(request.POST,instance=Branch )
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creater = request.user
            atype.save()
            return redirect(reverse('branches'))
    else:
        form = DepartmentForm(instance=Branch)
      
    context = {'form': form}
    return render(request, 'ostructure/department_aoe.html', context)

def department_delete(request, pk):
    object = Department.objects.get(id=pk).delete()
    return redirect('/')


def department_details(request, pk):
    
    posts = Branch.objects.filter(id=pk)
    context = {
        # "shop": shop,
        "account": posts
    }  
    return render(request, 'ostructure/department_details.html', context)


