from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.layout import (Layout, Fieldset, Field, Row, Column, Button,
                                 ButtonHolder, Submit, Div)
from .models import Branch, Department

class BranchForm(ModelForm):
    class Meta:
        model = Branch
        fields = ['name', 'manager', 'agents', 'town', 'city', 'county', 'phone_number', 'house', 'room_number',
       
        
        ]
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(BranchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                  Column('phone_number', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('manager', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
          
            Row(
                Column('agents', css_class='form-group col-md-6 mb-0'),
             
              
                css_class='form-row'
            ),
            Row(
                 Column('county', css_class='form-group col-md-4 mb-0'),
                Column('city', css_class='form-group col-md-4 mb-0'),
                Column('town', css_class='form-group col-md-4 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                 Column('house', css_class='form-group col-md-6 mb-0'),
                Column('room_number', css_class='form-group col-md-6 mb-0'),

              
                css_class='form-row'
            ),
            

       
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )





class DepartmentForm(ModelForm):
    class Meta:
        model = Department
        fields = ['name', 'manager', 'branch', 
       
        
        ]
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(DepartmentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('manager', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
          
            Row(
                Column('branch', css_class='form-group col-md-6 mb-0'),
             
              
                css_class='form-row'
            ),
       

       
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )