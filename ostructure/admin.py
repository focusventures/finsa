from django.contrib import admin

from .models import Branch, Department, Clientbranchhistory


admin.site.register(Branch)
admin.site.register(Department)
admin.site.register(Clientbranchhistory)
