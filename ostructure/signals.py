from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Branch, Department
from dashboard.models import SnapShot


@receiver(post_save, sender=Branch)
def update_snapshot_on_save(sender, instance, created, **kwargs):

    if created:
        snapshot = SnapShot.objects.all()[0]
        snapshot.branches += 1
        snapshot.save()




@receiver(post_save, sender=Branch)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.branches -= 1
    snapshot.save()


@receiver(post_save, sender=Department)
def update_snapshot_on_save(sender, instance, created, **kwargs):

    if created:
        snapshot = SnapShot.objects.all()[0]
        snapshot.departments += 1
        snapshot.save()




@receiver(post_save, sender=Department)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.departments -= 1
    snapshot.save()