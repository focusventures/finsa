from django.apps import AppConfig


class OstructureConfig(AppConfig):
    name = 'ostructure'
    icon_name = 'reorder'
    


    def ready(self):
        import ostructure.signals
