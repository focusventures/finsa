from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper

from .models import *
from members.forms import MemberForm
from crispy_forms.layout import (Layout, Fieldset, Field,Row, Column, Button, HTML,
                                 ButtonHolder, Submit, Div)

# class LoanScaleForm(ModelForm):
    
#     class Meta:
#         model = LoanScale
#         fields = '__all__'

# class AccrualInterestLoanForm(ModelForm):
    
#     class Meta:
#         model = AccrualInterestLoan
#         fields = '__all__'

# class LoanShareAmountForm(ModelForm):
    
#     class Meta:
#         model = LoanShareAmount
#         fields = '__all__'

class LoanDisbursementForm(ModelForm):
    
    class Meta:
        model = LoanDisbursement
        fields = '__all__'

class LoanInterestAccrualForm(ModelForm):
    
    class Meta:
        model = LoanInterestAccrual
        fields = '__all__'

class LoanEntryFeeForm(ModelForm):

    class Meta:
        model = LoanEntryFee
        fields = ['fee']


class LoanPurposeForm(ModelForm):

    class Meta:
        model = LoanPurpose
        fields = '__all__'


# class PeriodicityForm(ModelForm):

#     class Meta:
#         model = Periodicity
#         fields = '__all__'



class PaymentPlanForm(ModelForm):

    class Meta:
        model = LoanPaymentPlan
        fields = ['name']

    def __init__(self, *args, **kwargs):
        super(PaymentPlanForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-4 mb-0'),
                css_class='form-row '
                ),
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



class PurposeForm(ModelForm):

    class Meta:
        model = LoanPurpose
        fields = ['name',]

    def __init__(self, *args, **kwargs):
        super(PurposeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),

                Row(
                Column('parent', css_class='form-group col-md-6 mb-0'),
                css_class='form-row '
                ),
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



class LoanProductForm(ModelForm):
    payment_plan = PaymentPlanForm()
    class Meta:
        model = LoanProduct
        fields = "__all__"
        # fields = ['product_title','interest_rate_scheme', 'client_type', 'use_loc',
        #  'loan_processing_fee', 'periodicity','payment_plan', 'installment_type','number_of_installments',
        #   'annual_interest_rate','grace_period', 'interest_over_grace_period', 'loc', 'principal_acc',
        #   'interest_due_acc', 'interest_income_acc', 'accrued_penalty_acc', 'penalty_income_acc', 'interest_due_not_received', 
        #   'interest_accrued_acc', 'use_guarantors_collaterals', 'gc_setttings', 'late_fees_loan_amount', 
        #   'late_fees_overdue_principal', 'late_fees_overdue_interest', 'late_fees_olb',
        #  'interest_rate_scheme',]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(LoanProductForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.helper.layout = Layout(
        #     TabHolder(
        #         Tab('Product Info',
        #             Row(
        #                 Column('product_title', css_class='form-group col-md-4 mb-0'),
        #                 css_class='form-row '
        #                 ),
        #             Row(
        #                 Column('installment_type', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             Row(
        #                 Column('annual_interest_rate', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),

        #             Row(
        #                 Column('periodicity', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('payment_plan', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             ),
        #         Tab('Fees Info',
                    
        #             Row(
        #                 Column('loan_processing_fee', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),

                    
                  
        #             ),
        #         Tab('Loan Commissions Info',
        #             Row(
        #                 Column('periodicity', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('payment_plan', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
                   
        #             ),
        #         Tab('Line Of Credit Info',
        #             Row(
        #                 Column('loc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
                    
        #             ),

        #          Tab('Accounting Info',
        #             Row(
        #                 Column('principal_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('interest_due_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),

        #             Row(
        #                 Column('interest_income_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('accrued_penalty_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             Row(
        #                 Column('penalty_income_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('interest_due_not_received', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             Row(
        #                 Column('interest_accrued_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
                   


                   
        #             ),
        #     ),
        #     ButtonHolder(
        #         Submit('submit', 'Addd Product',
        #                css_class='float-right btn-warning mr-3')
        #     )

        # )
from members.models import Member , MemberGroup

from .models import LoanGuarantors, GuarantorsPledge




class LoanGuarantorsForm(ModelForm):
    class Meta:
        model = LoanGuarantors
        
        fields = ['sex', 'first_name', 'middle_name','last_name',
       'phone_number', 'id_number',
        'pledges', 
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(LoanGuarantorsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Extended Loan Guarantor Form',
                    Row(
                        Column('first_name', css_class='form-group col-md-2 mb-0'),
                        Column('middle_name', css_class='form-group col-md-2 mb-0'),
                        Column('last_name', css_class='form-group col-md-2 mb-0'),
                        Column('sex', css_class='form-group col-md-2 mb-0'),
                        Column('phone_number', css_class='form-group col-md-2 mb-0'),
                        Column('id_number', css_class='form-group col-md-2 mb-0'),
                        css_class='form-row '
                        ),
                  
                    ),
           
            ),
            ButtonHolder(
                Submit('submit', 'Submit Request',
                       css_class='float-right btn-warning mr-3')
            )

        )





class GuarantorsPledgeForm(ModelForm):
    class Meta:
        model = GuarantorsPledge
        
        fields = ['count', 'item_title', 'description',
        'serial_number','estimated_value',
       
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, request, pk, *args, **kwargs):
        super(GuarantorsPledgeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(

            Row(
                    Column('count', css_class='form-group col-md-3 mb-0'),
                    Column('item_title', css_class='form-group col-md-3 mb-0'),
                    Column('description', css_class='form-group col-md-3 mb-0'),
             

                    css_class='form-row'
                ),

           Row(
                    Column('year', css_class='form-group col-md-3 mb-0'),
                    Column('serial_number', css_class='form-group col-md-3 mb-0'),
                    Column('estimated_value', css_class='form-group col-md-3 mb-0'),

                    css_class='form-row'
                ),
           
            ButtonHolder(
                Submit('submit', 'Submit',
                       css_class='float-right btn-warning mr-3')
            )

        )




from members.models import Member , MemberGroup
class LoanContractForm(ModelForm):
    class Meta:
        model = LoanContract
        # 'loan_purpose', 'loan_purpose_description','loan_payment_plan_description']
        
        fields = ['member', 'requested_amount', 'requested_amount_words','loan_product', 'cheque_number',
        'loan_purpose','loan_payment_plan_description', 'amount_to_be_paid','payment_period',
        'amount_to_be_paid_words', 'hard_copy_scans', 'approving_officer', 
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, request, pk, *args, **kwargs):
        super(LoanContractForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        loan = LoanContract.objects.filter(id=pk).select_related('member').first()
        self.fields['member'].queryset = Member.objects.filter(id=loan.member.id)
        self.fields['approving_officer'].required = True
        self.fields['guarantors'].required = False
        self.fields['guarantors'].queryset = Member.objects.filter(group__id=Member.objects.get(id=pk).group.id)
        self.fields['hard_copy_scans'].required = False
        self.fields['amount_to_be_paid'].requeired = False
        self.fields['amount_to_be_paid_words'].requeired = False
        # member_group = MemberGroup.objects.get(id=Member.objects.get(id=pk).id)

        # self.fields['guarantors'].queryset = Member.objects.filter(group__id=member_group.id)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Loan Request Form',
                    Row(
                        Column('member', css_class='form-group col-md-4 mb-0'),
                        Column('loan_product', css_class='form-group col-md-4 mb-0'),
                        Column('cheque_number', css_class='form-group col-md-4 mb-0'),

                        css_class='form-row '
                        ),

                        
                    Row(
                        Column('requested_amount', css_class='form-group col-md-6 mb-0'),
                        Column('requested_amount_words', css_class='form-group col-md-6 mb-0'),

              
                        css_class='form-row'
                    ),
                  

              
            
            
                  
                    ),
                Tab('Loan Purpose Info',
                    
                    Row(
                        Column('loan_purpose', css_class='form-group col-md-6 mb-0'),


                      Column('loan_payment_plan_description', css_class='form-group col-md-6 mb-0'),
              
                        css_class='form-row'
                    ),

                     
                  
                    ),
                # Tab('Loan Guarantors Info',
                #     Row(
                #         Column('guarantors', css_class='form-group col-md-6 mb-0'),
              
                #         css_class='form-row'
                #     ),
            
            
                
                   
                #     ),

                Tab('Loan Approval Info',
                    Row(
                        Column('approving_officer', css_class='form-group col-md-6 mb-0'),
              
                        css_class='form-row'
                    ),
            
            
                
                   
                    ),
                Tab('Hard Copy Scans',
                    Row(
                        Column('hard_copy_scans', css_class='form-group col-md-6 mb-0'),
              
                        css_class='form-row'
                    ),
            
            
                    
                    ),

             
            ),
            ButtonHolder(
                Submit('submit', 'Submit Request',
                       css_class='float-right btn-warning mr-3')
            )

        )


from .models import LoanGuarantors, GuarantorsPledge


class LoanGuarantorsForm(ModelForm):
    class Meta:
        model = LoanGuarantors
        
        fields = ['sex', 'first_name', 'middle_name','last_name',
       'phone_number', 'id_number',
        'pledges', 
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(LoanGuarantorsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Extended Loan Guarantor Form',
                    Row(
                        Column('first_name', css_class='form-group col-md-2 mb-0'),
                        Column('middle_name', css_class='form-group col-md-2 mb-0'),
                        Column('last_name', css_class='form-group col-md-2 mb-0'),
                        Column('sex', css_class='form-group col-md-2 mb-0'),
                        Column('phone_number', css_class='form-group col-md-2 mb-0'),
                        Column('id_number', css_class='form-group col-md-2 mb-0'),
                        css_class='form-row '
                        ),
                  
                    ),
           
            ),
            ButtonHolder(
                Submit('submit', 'Submit Request',
                       css_class='float-right btn-warning mr-3')
            )

        )




class GuarantorsPledgeForm(ModelForm):
    class Meta:
        model = GuarantorsPledge
        
        fields = ['count', 'item_title', 'description',
        'serial_number','estimated_value',
       
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, request, pk, *args, **kwargs):
        super(GuarantorsPledgeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(

            Row(
                    Column('count', css_class='form-group col-md-3 mb-0'),
                    Column('item_title', css_class='form-group col-md-3 mb-0'),
                    Column('description', css_class='form-group col-md-3 mb-0'),
             

                    css_class='form-row'
                ),

           Row(
                    Column('year', css_class='form-group col-md-3 mb-0'),
                    Column('serial_number', css_class='form-group col-md-3 mb-0'),
                    Column('estimated_value', css_class='form-group col-md-3 mb-0'),

                    css_class='form-row'
                ),
           
            ButtonHolder(
                Submit('submit', 'Submit',
                       css_class='float-right btn-warning mr-3')
            )

        )

from members.models import Member , MemberGroup
class LoanContractForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = LoanContract
        # 'loan_purpose', 'loan_purpose_description','loan_payment_plan_description']
        
        fields = ['member', 'requested_amount', 'requested_amount_words','loan_product', 'cheque_number',
        'loan_purpose','loan_payment_plan_description', 'amount_to_be_paid','payment_period',
        'amount_to_be_paid_words',  'approving_officer', 'date'
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, request, pk, *args, **kwargs):
        super(LoanContractForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        loan = LoanContract.objects.filter(id=pk).select_related('member').first()
        self.fields['member'].queryset = Member.objects.filter(id=loan.member.id)
        self.fields['approving_officer'].required = True
        # self.fields['guarantors'].required = False
        # self.fields['guarantors'].queryset = Member.objects.filter(group__id=Member.objects.get(id=pk).group.id)
        # self.fields['hard_copy_scans'].required = False
        self.fields['amount_to_be_paid'].requeired = False
        self.fields['amount_to_be_paid_words'].requeired = False
        # member_group = MemberGroup.objects.get(id=Member.objects.get(id=pk).id)

        # self.fields['guarantors'].queryset = Member.objects.filter(group__id=member_group.id)
        self.helper.layout = Layout(
  
            Div(
                Div('Loan Request Form',
                    Row(
                        Column('member', css_class='form-group col-md-4 mb-0'),
                        Column('loan_product', css_class='form-group col-md-4 mb-0'),
                        Column('cheque_number', css_class='form-group col-md-4 mb-0'),
                        css_class='form-row '
                        ),

                    
            Row(
                HTML('<div class="dropdown-divider"></div>')
            ),
                        
                    Row(
                        Column('requested_amount', css_class='form-group col-md-6 mb-0'),
                        Column('requested_amount_words', css_class='form-group col-md-6 mb-0'),

              
                        css_class='form-row'
                    ),
                  

              
            
            
                  
                    ),
                Div('Loan Purpose Info',
                    
                    Row(
                        Column('loan_purpose', css_class='form-group col-md-6 mb-0'),


                      Column('loan_payment_plan_description', css_class='form-group col-md-6 mb-0'),
              
                        css_class='form-row'
                    ),

                     
                  
                    ),

                    Div('Payment Period',
                    
                    Row(
                        Column('payment_period', css_class='form-group col-md-6 mb-0'),
                        Column('date', css_class='form-group col-md-6 mb-0'),


              
                        css_class='form-row'
                    ),

                     
                  
                    ),



                # Div('Loan Guarantors Info',
                #     Row(
                #         Column('guarantors', css_class='form-group col-md-6 mb-0'),
              
                #         css_class='form-row'
                #     ),
            
            
                
                   
                #     ),

                Div('Loan Approval Info',
                    Row(
                        Column('approving_officer', css_class='form-group col-md-6 mb-0'),
              
                        css_class='form-row'
                    ),
            
            
                
                   
                    ),
                # Div('Hard Copy Scans',
                #     Row(
                #         Column('hard_copy_scans', css_class='form-group col-md-6 mb-0'),
              
                #         css_class='form-row'
                #     ),
            
            
                    
                #     ),

             
            ),
            ButtonHolder(
                Submit('submit', 'Submit Request',
                       css_class='float-right btn-warning mr-3')
            )

        )



class LoanGuarantorsForm(ModelForm):
    class Meta:
        model = LoanGuarantors
        
        fields = ['sex', 'first_name', 'middle_name','last_name',
       'phone_number', 'id_number',
        'pledges', 
        
        
        ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(LoanGuarantorsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Extended Loan Guarantor Form',
                    Row(
                        Column('first_name', css_class='form-group col-md-2 mb-0'),
                        Column('middle_name', css_class='form-group col-md-2 mb-0'),
                        Column('last_name', css_class='form-group col-md-2 mb-0'),
                        Column('sex', css_class='form-group col-md-2 mb-0'),
                        Column('phone_number', css_class='form-group col-md-2 mb-0'),
                        Column('id_number', css_class='form-group col-md-2 mb-0'),
                        css_class='form-row '
                        ),
                  
                    ),
           
            ),
            ButtonHolder(
                Submit('submit', 'Submit Request',
                       css_class='float-right btn-warning mr-3')
            )

        )

