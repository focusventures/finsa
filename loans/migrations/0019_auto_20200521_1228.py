# Generated by Django 3.0.4 on 2020-05-21 09:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0018_auto_20200520_1211'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='figures_settings',
            options={'verbose_name': 'Figures Settings', 'verbose_name_plural': 'Figures Settings'},
        ),
        migrations.AlterModelOptions(
            name='gc_settings',
            options={'verbose_name': 'GC Settings', 'verbose_name_plural': 'GC Settings'},
        ),
        migrations.AlterModelOptions(
            name='interestwriteoffevents',
            options={'verbose_name': 'Interest Write Off Events', 'verbose_name_plural': 'Interest Write Off Events'},
        ),
        migrations.AlterModelOptions(
            name='loanaccrual',
            options={'verbose_name': 'Loan Interest Accrual', 'verbose_name_plural': 'Loan Interest Accrual'},
        ),
        migrations.AlterModelOptions(
            name='loancontract',
            options={'verbose_name': 'Loan Contracts', 'verbose_name_plural': 'Loan Contracts'},
        ),
        migrations.AlterModelOptions(
            name='loandisbursement',
            options={'verbose_name': 'Loan Disbursement Event', 'verbose_name_plural': 'Loan Disbursement Events'},
        ),
        migrations.AlterModelOptions(
            name='loanentryfee',
            options={'verbose_name': 'Loan Entry Fee', 'verbose_name_plural': 'Loan Entry Fees'},
        ),
        migrations.AlterModelOptions(
            name='loaninterestaccrual',
            options={'verbose_name': 'Loan Interest Accrual', 'verbose_name_plural': 'Loan Interest Accrual'},
        ),
        migrations.AlterModelOptions(
            name='loanpaymentplan',
            options={'verbose_name': 'Loan Payment Plan', 'verbose_name_plural': 'Loan Payment Plan'},
        ),
        migrations.AlterModelOptions(
            name='loanpurpose',
            options={'verbose_name': 'Loan Purpose', 'verbose_name_plural': 'Loan Purpose'},
        ),
        migrations.AlterModelOptions(
            name='overdueevents',
            options={'verbose_name': 'Overdue Events', 'verbose_name_plural': 'Overdue Events'},
        ),
        migrations.AlterModelOptions(
            name='repaymentevents',
            options={'verbose_name': 'Repayment Events', 'verbose_name_plural': 'Repayment Events'},
        ),
        migrations.AlterModelOptions(
            name='reschedulingofaloanevents',
            options={'verbose_name': 'Rescheduling Of A Loan Events', 'verbose_name_plural': 'Rescheduling Of A Loan Events'},
        ),
        migrations.AlterModelOptions(
            name='scannedimage',
            options={'verbose_name': 'Sacnned Images', 'verbose_name_plural': 'Scanned Immages'},
        ),
    ]
