# Generated by Django 3.0.4 on 2020-07-22 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0029_loancontract_repaid'),
    ]

    operations = [
        migrations.AddField(
            model_name='loancontract',
            name='balance',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Loan Balance'),
        ),
    ]
