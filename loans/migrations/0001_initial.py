# Generated by Django 3.0.4 on 2020-04-22 19:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('parameters', '0003_auto_20200422_0747'),
        ('chartsoa', '0003_exoticinstallments_fundinglineevents_fundinglines_linkbranchespaymentmethods_paymentmethods'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        # ('members', '0008_member_saving_acc'),
    ]

    operations = [
        migrations.CreateModel(
            name='Figures_Settings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('min', models.FloatField(default=0.0, verbose_name='Minimum Vaue')),
                ('max', models.FloatField(default=0.0, verbose_name='Maximum Vaue')),
                ('actual', models.FloatField(default=0.0, verbose_name='Actual Vaue')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(null=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Figures Settings',
            },
        ),
        migrations.CreateModel(
            name='GC_Settings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('min_percent', models.IntegerField(verbose_name='Minimum Percentage of Guarantors and Collaterals')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(null=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'GC Settings',
            },
        ),
        migrations.CreateModel(
            name='LoanContract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('requested_amount', models.FloatField(verbose_name='Amount Requested By the Member')),
                ('requested_amount_words', models.CharField(max_length=50, verbose_name='Amount Requested in words ')),
                ('loan_payment_plan_description', models.TextField(verbose_name='Describe The Payment Plan Of The Loan')),
                ('amount_to_be_paid', models.FloatField(verbose_name='Monthly Installments Amount Expected')),
                ('amount_to_be_paid_words', models.CharField(max_length=50, verbose_name='Amount Requested in words ')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                # ('guarantors', models.ManyToManyField(related_name='_loancontract_guarantors_+', to='members.Member', verbose_name='Guarantors')),
            ],
            options={
                'verbose_name': 'Loan Contract',
            },
        ),
        migrations.CreateModel(
            name='LoanDisbursement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.FloatField(verbose_name='Amount Disbursed')),
                ('fees', models.FloatField(verbose_name='Fees')),
                ('interest', models.FloatField(verbose_name='Interest')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('loan_contract', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='loans.LoanContract', verbose_name='Loan Disbursement')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
                ('payment_method', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='chartsoa.PaymentMethod', verbose_name='Disbursement Method')),
            ],
            options={
                'verbose_name': 'Loan Disbursement',
            },
        ),
        migrations.CreateModel(
            name='LoanEntryFee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fee', models.FloatField(verbose_name='Fee')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('disbursement_event', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='loans.LoanDisbursement', verbose_name='Loan Disbursement Event')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Loan Entry Fee',
            },
        ),
        migrations.CreateModel(
            name='LoanPaymentPlan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, null=True, verbose_name='Name')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Loan Payment Plan',
            },
        ),
        migrations.CreateModel(
            name='Overdueevents',
            fields=[
                ('loan_contract', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='loans.LoanContract')),
                ('olb', models.DecimalField(decimal_places=4, max_digits=19)),
                ('overdue_days', models.IntegerField()),
                ('overdue_principal', models.DecimalField(blank=True, decimal_places=4, max_digits=19, null=True)),
            ],
            options={
                'verbose_name': 'Overdue Events',
            },
        ),
        migrations.CreateModel(
            name='Reschedulingofaloanevents',
            fields=[
                ('loan_contract', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='loans.LoanContract')),
                ('amount', models.DecimalField(decimal_places=4, max_digits=19)),
                ('interest', models.DecimalField(decimal_places=4, max_digits=19)),
                ('nb_of_maturity', models.IntegerField()),
                ('grace_period', models.IntegerField()),
                ('charge_interest_during_grace_period', models.BooleanField()),
                ('previous_interest_rate', models.DecimalField(decimal_places=4, max_digits=19)),
                ('preferred_first_installment_date', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Rescheduling Of A Loan Events',
            },
        ),
        migrations.CreateModel(
            name='ScannedImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('document', models.ImageField(upload_to='scans/', verbose_name='Scanned Copy')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Saccned Images',
            },
        ),
        migrations.CreateModel(
            name='Repaymentevents',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('past_due_days', models.IntegerField()),
                ('principal', models.DecimalField(decimal_places=4, max_digits=19)),
                ('interests', models.DecimalField(decimal_places=4, max_digits=19)),
                ('installment_number', models.IntegerField()),
                ('commissions', models.DecimalField(decimal_places=4, max_digits=19)),
                ('penalties', models.DecimalField(decimal_places=4, max_digits=19)),
                ('payment_method_id', models.IntegerField(blank=True, null=True)),
                ('calculated_penalties', models.DecimalField(decimal_places=4, max_digits=19)),
                ('written_off_penalties', models.DecimalField(decimal_places=4, max_digits=19)),
                ('unpaid_penalties', models.DecimalField(decimal_places=4, max_digits=19)),
                ('bounce_fee', models.DecimalField(decimal_places=4, max_digits=19)),
                ('loan_contract', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='loans.LoanContract')),
            ],
            options={
                'verbose_name': 'Repayment Events',
            },
        ),
        migrations.CreateModel(
            name='LoanPurpose',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Loan Purpose')),
                ('particulars', models.TextField(verbose_name='Loan Purpose Description')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                # ('economic_activity', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='members.EconomicActivity', verbose_name='Loan Purpose')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Loan Purpose',
            },
        ),
        migrations.CreateModel(
            name='LoanProduct',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_title', models.CharField(max_length=50, verbose_name='Loan Product Title')),
                ('interest_rate_scheme', models.CharField(choices=[('All', 'All'), ('Flat', 'Flat(Fixed Pricinpal, Fixed Interest)'), ('Declining', 'Fixed Principal'), ('Deline', 'Declining Principal')], default='All', max_length=50, verbose_name='Interest Rate Scheme')),
                ('client_type', models.CharField(choices=[('Individual', 'Individaul')], default='Individual', max_length=50, verbose_name='Client Type')),
                ('use_loc', models.BooleanField(default=False, verbose_name='Use Line Of Credit')),
                ('installment_type', models.CharField(choices=[('All', 'All'), ('Monthly', 'Monthly'), ('Yearly', 'Yearly'), ('quoterly', 'quoterly')], default='All', max_length=50, verbose_name='Installment Type')),
                ('interest_over_grace_period', models.BooleanField(default=False, verbose_name='Change Interest Over Grace Period')),
                ('use_guarantors_collaterals', models.BooleanField(default=False, verbose_name='Use Guarantors And Collaterals')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('accrued_penalty_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Accrued Penalty+', to='chartsoa.Account', verbose_name='Accrued Penalty')),
                ('annual_interest_rate', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Annual Interest Rate+', to='loans.Figures_Settings', verbose_name='Annual Interest Rate')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('gc_setttings', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='loans.GC_Settings', verbose_name='Guarantors & Collaterals Settings')),
                ('grace_period', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Grace Period+', to='loans.Figures_Settings', verbose_name='Grace Period')),
                ('interest_accrued_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Accrued Account+', to='chartsoa.Account', verbose_name='Interest Accrued Account')),
                ('interest_accrued_but_not_due_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Accrued But Not Due Account+', to='chartsoa.Account', verbose_name='Interest Accrued But Not Due Account')),
                ('interest_due_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Due Account+', to='chartsoa.Account', verbose_name='Interest Due Account')),
                ('interest_due_not_received', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Due But Not Received+', to='chartsoa.Account', verbose_name='Interest Due But Not Received')),
                ('interest_income_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Income Account+', to='chartsoa.Account', verbose_name='Interest Income Account')),
                ('late_fees_loan_amount', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Late Fees On Loan Amount+', to='loans.Figures_Settings', verbose_name='Late Fees On Loan Amount in % Per day')),
                ('late_fees_olb', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Late Fees On OLB+', to='loans.Figures_Settings', verbose_name='Late Fees On OLB in % Per day')),
                ('late_fees_overdue_interest', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Late Fees On Overdue Interes+', to='loans.Figures_Settings', verbose_name='Late Fees On Overdue Interest in % Per day')),
                ('late_fees_overdue_principal', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Late Fees On Overdue Principal+', to='loans.Figures_Settings', verbose_name='Late Fees On Overdue Principal in % Per day')),
                ('loan_processing_fee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='loans.LoanEntryFee', verbose_name='Loan Processing Fee')),
                ('loc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Line Of Credit+', to='parameters.LOC', verbose_name='Line of Credit')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
                ('number_of_installments', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Number Of Intallments+', to='loans.Figures_Settings', verbose_name='Number Of Intallments')),
                ('payment_plan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='loans.LoanPaymentPlan', verbose_name='Payment Plan')),
                ('penalty_income_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Penalty Income+', to='chartsoa.Account', verbose_name='Penalty Income')),
                ('periodicity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='parameters.Periodicity', verbose_name='Payment Period')),
                ('principal_acc', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Principal Account+', to='chartsoa.Account', verbose_name='Principal Account')),
            ],
            options={
                'verbose_name': 'Loan Product',
            },
        ),
        migrations.CreateModel(
            name='LoanInterestAccrual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('interest_prepayment', models.FloatField(verbose_name='Loan Interest Prepayment')),
                ('accrued_interest', models.FloatField(verbose_name='Accrued Interest')),
                ('rescheduled', models.BooleanField(verbose_name='Is Rescheduled')),
                ('installment_number', models.IntegerField(verbose_name='Installment Number')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Loan Interest Accrual',
            },
        ),
        migrations.AddField(
            model_name='loancontract',
            name='hard_copy_scans',
            field=models.ManyToManyField(to='loans.ScannedImage', verbose_name='Scans'),
        ),
        migrations.AddField(
            model_name='loancontract',
            name='loan_product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Loan Product+', to='loans.LoanProduct', verbose_name='Loan Product'),
        ),
        migrations.AddField(
            model_name='loancontract',
            name='loan_purpose',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='loans.LoanPurpose', verbose_name='Purpose Of The Loan'),
        ),
        # migrations.AddField(
        #     model_name='loancontract',
        #     name='member',
        #     field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Member+', to='members.Member', verbose_name='Member'),
        # ),
        migrations.AddField(
            model_name='loancontract',
            name='modified_by',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by'),
        ),
        migrations.CreateModel(
            name='LoanAccrual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rescheduled', models.BooleanField(verbose_name='Is Rescheduled')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('loan_contract', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='loans.LoanContract', verbose_name='Loan Contract')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
            options={
                'verbose_name': 'Loan Interest Accrual',
            },
        ),
        migrations.CreateModel(
            name='Interestwriteoffevents',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=4, max_digits=19)),
                ('loan_contract', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='loans.LoanContract')),
            ],
            options={
                'verbose_name': 'Interest Write Off Events',
            },
        ),
    ]
