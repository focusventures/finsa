# Generated by Django 3.0.4 on 2020-07-29 08:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loans', '0034_loanstatus_total_repaid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loanstatus',
            name='total_balance',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Total Balance'),
        ),
        migrations.AlterField(
            model_name='loanstatus',
            name='total_interest',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Total Interest On Loans'),
        ),
        migrations.AlterField(
            model_name='loanstatus',
            name='total_loans',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Total Loans Amount'),
        ),
    ]
