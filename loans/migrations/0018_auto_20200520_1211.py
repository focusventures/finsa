# Generated by Django 3.0.4 on 2020-05-20 09:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0007_auto_20200519_1432'),
        ('loans', '0017_loanproduct_payment_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loanproduct',
            name='interest_accrued_code',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Accrued Code+', to='parameters.Codes', verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='loanproduct',
            name='interest_code',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Interest Code+', to='parameters.Codes', verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='loanproduct',
            name='late_fees_code',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Late Fees Code+', to='parameters.Codes', verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='loanproduct',
            name='loan_processing_code',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Loan Processing Code+', to='parameters.Codes', verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='loanproduct',
            name='payment_code',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Payment Code+', to='parameters.Codes', verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='loanproduct',
            name='penalty_code',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Penalty Code+', to='parameters.Codes', verbose_name='Code'),
        ),
    ]
