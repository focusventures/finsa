# Generated by Django 3.0.4 on 2020-04-22 16:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('savings', '0001_initial'),
        ('loans', '0006_auto_20200422_1901'),
    ]

    operations = [
        migrations.AddField(
            model_name='loanslinksavingsbook',
            name='savings',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='savings.Savingcontracts'),
        ),
    ]
