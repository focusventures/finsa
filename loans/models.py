from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator


class LoanEntryFee(models.Model):
    fee = models.FloatField(_("Fee"))
    disbursement_event = models.ForeignKey("loans.LoanDisbursement", null=True, verbose_name=_("Loan Disbursement Event"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Entry Fee"
        verbose_name_plural = 'Loan Entry Fees'

    def __str__(self):
        return "%s" %(self.fee)

class LoanInterestAccrual(models.Model):
    interest_prepayment = models.FloatField(_("Loan Interest Prepayment"))
    accrued_interest = models.FloatField(_("Accrued Interest"))
    rescheduled = models.BooleanField(_("Is Rescheduled"))
    installment_number = models.IntegerField(_("Installment Number"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Interest Accrual"
        verbose_name_plural = 'Loan Interest Accrual'


class LoanAccrual(models.Model):
    # interest_prepayment = models.FloatField(_("Loan Interest Prepayment"))
    # accrued_interest = models.FloatField(_("Accrued Interest"))
    rescheduled = models.BooleanField(_("Is Rescheduled"))
    # installment_number = models.IntegerField(_("Installment Number"))
    loan_contract = models.ForeignKey("loans.LoanContract", verbose_name=_("Loan Contract"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Interest Accrual"
        verbose_name_plural = 'Loan Interest Accrual'

class LoanPurpose(models.Model):
    name = models.CharField(_("Loan Purpose"), max_length=255)
    particulars = models.TextField(_("Loan Purpose Description"))
    economic_activity = models.ForeignKey("members.EconomicActivity", verbose_name=_("Loan Purpose"), null=True, on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


    class Meta:
        verbose_name = "Loan Purpose"
        verbose_name_plural = 'Loan Purpose'



    def __str__(self):
        return self.name
    


class LoanContract(models.Model):
    member = models.ForeignKey("members.Member",  null=True, verbose_name=_("Member"), related_name="Member+",on_delete=models.DO_NOTHING)
    requested_amount = models.DecimalField(_("Amount Requested By the Member"), max_digits=10, decimal_places=2, default=0.00)
    loan_product = models.ForeignKey("loans.LoanProduct", verbose_name=_("Loan Product"), related_name="Loan Product+",on_delete=models.DO_NOTHING)
    # requested_amount_words = models.CharField(_("Amount Requested in words "), max_length=50)
    requested_amount_words = models.TextField(_("Amount Requested in words "), null=True, blank=True)
    approved_amount = models.DecimalField(_("Amount Approved"), max_digits=10, decimal_places=2, default=0.00)
    loan_purpose = models.ForeignKey("loans.LoanPurpose", verbose_name=_("Purpose Of The Loan"), on_delete=models.DO_NOTHING)
    loan_payment_plan_description = models.TextField(_("Describe Loan Purpose"), null=True, blank=True)
    amount_to_be_paid = models.FloatField(_("Monthly Installments Amount Expected"), null=True, blank=True,  default=0.00)
    amount_to_be_paid_words = models.CharField(_("Amount Requested in words "), max_length=50, null=True, blank=True)
    # guarantors = models.ManyToManyField("loans.LoanGuarantors", verbose_name=_("Guarantors"), related_name="Loan Guarantors+")
    # guarantors_pledge = models.ManyToManyField("loans.GuarantorsPledge", verbose_name=_("Guarantors Pledges"), related_name="Loan Pledges+")
    hard_copy_scans = models.ManyToManyField("loans.ScannedImage", verbose_name=_("Scans"), null=True, blank=True)
    date = models.DateField(_("Date Disbursed"), auto_now=False, default=None, null=True, blank=True)
    is_approved = models.BooleanField(_("Loan is approved"), default=False)
    approving_officer = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="approving officer+", verbose_name=_("Approving Officer"), on_delete=models.DO_NOTHING)
    date_approved = models.DateField(_("Date created"), auto_now=False, default=None, null=True, blank=True)

    cheque_number = models.CharField(_("Cheque NO"), max_length=50, null=True, blank=True)

    is_confirmed = models.BooleanField(_("Loan is approved"), default=False)
    confirming_officer = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="confirming officer+", verbose_name=_("Confirmed by"), on_delete=models.DO_NOTHING)
    date_confirmed = models.DateField(_("Date created"), auto_now=False, default=None, null=True, blank=True)

    is_disbursed = models.BooleanField(_("Loan is Disbursed"), default=False)
    disbursing_officer = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="disbursing officer+", verbose_name=_("Disbursed by"), on_delete=models.DO_NOTHING)
    date_disbursed = models.DateField(_("Date Disbursed"), auto_now=False, default=None, null=True, blank=True)

    is_repaid = models.BooleanField(_("Loan is Repaid"), default=False)
    date_repaid = models.DateField(_("Date Repaid"), auto_now=False, default=None, null=True, blank=True)

    is_overdue = models.BooleanField(_("Loan Is OverDue"), default=False)
    date_overdue = models.DateField(_("Date Turned Overdue"), auto_now=False, auto_now_add=False, null=True, blank=True)
    payment_period = models.IntegerField(_("Payment Period in Months"), default=12)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


    @property
    def get_(self):
        return '%s - %s - %s' % (self.requested_amount, self.loan_product, self.date)

    class Meta:
        verbose_name = "Loan Contracts"
        verbose_name_plural = 'Loan Contracts'

class ScannedImage(models.Model):
    document = models.ImageField(_("Scanned Copy"), upload_to="scans/", height_field=None, width_field=None, max_length=None)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Sacnned Images"
        verbose_name_plural = 'Scanned Immages'

class LoanPaymentPlan(models.Model):
    name = models.CharField(_("Name"), null=True, max_length=50)
    # loan_percentage = models.IntegerField(_("Loan Percentage"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Payment Plan"
        verbose_name_plural = 'Loan Payment Plan'

    def __str__(self):
        return self.name



scheme_rates = (
    ("flat", "Flat(Fixed Pricinpal, Fixed Interest)"),
    ("declining", "Declining Principal"),
)

install_type = (
    ("All", "All"),
    ("Monthly", "Monthly"),
    ("Yearly", "Yearly"),
    ("quoterly", "quoterly")
)





client_type = (
    ("Individual", "Individual"),
    ("Group", "Group"),
    ("Corporate", "Corporate"),
)


class LoanProduct(models.Model):
    product_title = models.CharField(_("Loan Product Title"), max_length=50)
    interest_rate_scheme = models.CharField(_("Interest Rate Scheme"), choices=scheme_rates, default="All", max_length=50)
    client_type = models.CharField(_("Client Type"), choices=client_type, default="Individual", max_length=50)
    # loan_processing_fee = models.ForeignKey("loans.LoanEntryFee", verbose_name=_("Loan Processing Fee"), on_delete=models.DO_NOTHING)


    loan_fee_processing = models.DecimalField(_("Loan Processing Fee in % e.g 3"), max_digits=8, decimal_places=2, default=0.00)
    periodicity = models.ForeignKey("parameters.Periodicity", verbose_name=_("Payment Period"), on_delete=models.DO_NOTHING)
    payment_plan = models.ForeignKey("loans.LoanPaymentPlan", verbose_name=_("Payment Plan"), on_delete=models.DO_NOTHING)
    use_loc = models.BooleanField(_("Use Line Of Credit"), default=False)

    installment_type = models.CharField(_("Installment Type"), choices=install_type, default="All", max_length=50)
    # annual_interest_rate = models.ForeignKey("loans.Figures_Settings", verbose_name=_("Annual Interest Rate"), null=True, related_name="Annual Interest Rate+",on_delete=models.DO_NOTHING)

    annual_rate_interest = models.DecimalField(_("Annual Interest Rate in % p.a"), max_digits=5, decimal_places=2, default=0.00)
    monthly_rate_interest = models.DecimalField(_("Monthly Interest Rate in % P.m"), max_digits=5, decimal_places=2, default=0.00)

    accrued_penalty = models.DecimalField(_("Accrued Penalty Fee % Per Month"), max_digits=5, decimal_places=2, default=0.00)

    interest_accrued_penalty = models.DecimalField(_("Interest On Accrued Penalty Fee % Per Month"), max_digits=5, decimal_places=2, default=0.00)

    late_fees = models.DecimalField(_("Accrued Penalty Fee % Per Month"), max_digits=5, decimal_places=2, default=0.00)
    
    number_of_installments = models.ForeignKey("loans.Figures_settings", verbose_name=_("Number Of Intallments"), null=True, related_name="Number Of Intallments+", on_delete=models.DO_NOTHING)
    grace_period = models.ForeignKey("loans.Figures_settings", verbose_name=_("Grace Period"), null=True, related_name="Grace Period+", on_delete=models.DO_NOTHING)
    interest_over_grace_period = models.BooleanField(_("Change Interest Over Grace Period"), default=False)

    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line of Credit"), null=True, related_name="Line Of Credit+", on_delete=models.DO_NOTHING)
    principal_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Principal Account"), null=True, related_name="Principal Account+", on_delete=models.DO_NOTHING)
    
    interest_due_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Due Account"), null=True,  related_name="Interest Due Account+", on_delete=models.DO_NOTHING)
    interest_income_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Income Account"), null=True,  related_name="Interest Income Account+",on_delete=models.DO_NOTHING)
    accrued_penalty_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Accrued Penalty"),null=True,   related_name="Accrued Penalty+",on_delete=models.DO_NOTHING)
    penalty_income_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Penalty Income"), null=True,  related_name="Penalty Income+",on_delete=models.DO_NOTHING)
    interest_due_not_received = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Due But Not Received"), null=True, related_name="Interest Due But Not Received+", on_delete=models.DO_NOTHING)
    interest_accrued_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Accrued Account"), null=True,  related_name="Interest Accrued Account+",on_delete=models.DO_NOTHING)
    interest_accrued_but_not_due_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Accrued But Not Due Account"), null=True,  related_name="Interest Accrued But Not Due Account+",on_delete=models.DO_NOTHING)

    use_guarantors_collaterals = models.BooleanField(_("Use Guarantors And Collaterals"), default=False)
    # gc_settings = models.ForeignKey("loans.GC_Settings", verbose_name=_("Guarantors & Collaterals Settings"), null=True, on_delete=models.DO_NOTHING)
    gc_settings = models.DecimalField(_("Guarantors Minimum Percentage Lock"), max_digits=5, decimal_places=2, default=0.00)
    
    # late_fees_loan_amount = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On Loan Amount in % Per day"), null=True, related_name="Late Fees On Loan Amount+", on_delete=models.DO_NOTHING)
    # late_fees_overdue_principal = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On Overdue Principal in % Per day"), null=True, related_name="Late Fees On Overdue Principal+", on_delete=models.DO_NOTHING)
    # late_fees_overdue_interest = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On Overdue Interest in % Per day"), null=True, related_name="Late Fees On Overdue Interes+", on_delete=models.DO_NOTHING)
    # late_fees_olb = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On OLB in % Per day"), null=True, related_name="Late Fees On OLB+", on_delete=models.DO_NOTHING)

    late_fees_loan_amount = models.DecimalField(_("Late Fees On Loan Amount in % Per day"), max_digits=5, decimal_places=2, default=0.00)
    late_fees_overdue_principal = models.DecimalField(_("Late Fees On Overdue Principal in % Per day"), max_digits=5, decimal_places=2, default=0.00)
    late_fees_overdue_interest = models.DecimalField(_("Late Fees On Overdue Interest in % Per day"), max_digits=5, decimal_places=2, default=0.00)
    late_fees_olb = models.DecimalField(_("Late Fees On OLB in % Per day"), max_digits=5, decimal_places=2, default=0.00)


    interest_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Interest Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    loan_processing_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Loan Processing Code+",  on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    penalty_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Penalty Code+", on_delete=models.DO_NOTHING, null=True, blank=True,default=None)
    interest_accrued_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Interest Accrued Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    late_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Late Fees Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    payment_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Payment Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    disburse_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Code"), related_name="Penalty Code+", on_delete=models.DO_NOTHING, null=True, blank=True,default=None)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Product"
        # verboose_name_plural = "Loan Products"



    def __str__(self):
        return self.product_title
    


class LoanDisbursement(models.Model):
    loan_contract = models.ForeignKey("loans.LoanContract", verbose_name=_("Loan Disbursement"), on_delete=models.DO_NOTHING)
    amount = models.FloatField(_("Amount Disbursed"))
    fees = models.FloatField(_("Fees"))
    interest = models.FloatField(_("Interest"))
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Disbursement Method"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Disbursement Event"
        verbose_name_plural = 'Loan Disbursement Events'




class Repaymentevents(models.Model):
    loan_contract = models.ForeignKey('loans.LoanContract', models.DO_NOTHING)
    past_due_days = models.IntegerField()
    principal = models.DecimalField(max_digits=19, decimal_places=4)
    interests = models.DecimalField(max_digits=19, decimal_places=4)
    installment_number = models.IntegerField()
    commissions = models.DecimalField(max_digits=19, decimal_places=4)
    penalties = models.DecimalField(max_digits=19, decimal_places=4)
    payment_method_id = models.IntegerField(blank=True, null=True)
    calculated_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    written_off_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    unpaid_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    bounce_fee = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        verbose_name = 'Repayment Events'
        verbose_name_plural = 'Repayment Events'


class Reschedulingofaloanevents(models.Model):
    loan_contract = models.OneToOneField('loans.LoanContract', models.DO_NOTHING, primary_key=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    interest = models.DecimalField(max_digits=19, decimal_places=4)
    nb_of_maturity = models.IntegerField()
    grace_period = models.IntegerField()
    charge_interest_during_grace_period = models.BooleanField()
    previous_interest_rate = models.DecimalField(max_digits=19, decimal_places=4)
    preferred_first_installment_date = models.DateTimeField()

    class Meta:
        verbose_name = 'Rescheduling Of A Loan Events'
        verbose_name_plural = 'Rescheduling Of A Loan Events'


class Overdueevents(models.Model):
    loan_contract = models.OneToOneField('loans.LoanContract', models.DO_NOTHING, primary_key=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4)
    overdue_days = models.IntegerField()
    overdue_principal = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        verbose_name = 'Overdue Events'
        verbose_name_plural = 'Overdue Events'


class Interestwriteoffevents(models.Model):
    loan_contract = models.ForeignKey('loans.LoanContract', models.DO_NOTHING)
    amount = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        verbose_name = 'Interest Write Off Events'
        verbose_name_plural = 'Interest Write Off Events'

class Figures_Settings(models.Model):
    min = models.FloatField(_("Minimum Vaue"), default=0.0)
    max = models.FloatField(_("Maximum Vaue"), default=0.0)
    actual = models.FloatField(_("Actual Vaue"), default=0.0)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Figures Settings"
        verbose_name_plural = 'Figures Settings'

class GC_Settings(models.Model):
    min_percent = models.IntegerField(_("Minimum Percentage of Guarantors and Collaterals"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "GC Settings"
        verbose_name_plural = 'GC Settings'

    def __str__(self):
        return "%s" % (self.min_percent)

sex = (
    ("Male", "MALE"),
    ("Female", "FEMALE"),
)





class LoanRepayment(models.Model):
    slip = models.ForeignKey("fosa.Slips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    member = models.ForeignKey("members.Member",  null=True, verbose_name=_("Member"), related_name="Member+",on_delete=models.DO_NOTHING)
    amount = models.DecimalField(_("Amount"), max_digits=150, decimal_places=2, default=0.00)
    # loan_contract = models.ForeignKey("loans.LoanContract", verbose_name=_("Loan Contract"), related_name="Loan Product+",on_delete=models.DO_NOTHING)
    loan_status = models.ForeignKey("loans.LoanStatus", verbose_name=_("Loan Status"), on_delete=models.DO_NOTHING, null=True, blank=True)
    previous_balance = models.DecimalField(_("Previous Balance"), max_digits=20, decimal_places=2, default=0.00)
    loan_balance = models.DecimalField(_("Remainder"), max_digits=15, decimal_places=2, default=0.00)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_("Transactions"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Repayment"
        verbose_name_plural = 'Loan Repayment'

    def __str__(self):
        return "%s - %s - %s -%s" % (self.slip, self.member, self.amount, self.loan_status)




class LoanStatus(models.Model):
    member = models.ForeignKey("members.Member",  null=True, verbose_name=_("Member"), related_name="Member+",on_delete=models.DO_NOTHING)
    loan_contracts = models.ManyToManyField("loans.LoanContract", verbose_name=_("Loan Contracts"))
    total_loans = models.DecimalField(_("Total Loans Amount"), max_digits=20, decimal_places=2, default=0.00)
    total_interest = models.DecimalField(_("Total Interest On Loans"), max_digits=20, decimal_places=2, default=0.00)
    total_balance = models.DecimalField(_("Total Balance"), max_digits=20, decimal_places=2, default=0.00)
    total_repaid = models.DecimalField(_("Amount Repaid"), max_digits=20, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Loan Status"
        verbose_name_plural = 'Loan Status'

    def __str__(self):
        return "%s - %s - %s -%s" % (self.member, self.total_loans, self.total_interest, self.total_balance)




class AccountsLock(models.Model):
    account = models.ForeignKey("financial.MemberCustomAccount", verbose_name=_("Member Accounts"), on_delete=models.DO_NOTHING)
    guarantees = models.ManyToManyField("loans.InternalGuarantors", verbose_name=_("Guarantees"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)



class InternalGuarantors(models.Model):
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    contract = models.ForeignKey("loans.LoanContract", verbose_name=_("Loan Contract"), on_delete=models.DO_NOTHING)
    lock = models.DecimalField(_("Amount Locked"), max_digits=20, decimal_places=2, default=2000.00)
    active = models.BooleanField(_("Guarantee is Active"), default=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)



class LoanGuarantors(models.Model):
    phone_regex = RegexValidator(regex=r'^\d{10}$',
                                 message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.")
    sex = models.CharField(max_length=10, choices=sex, default="Female")
    first_name = models.CharField(_("First Name"), max_length=50, null=True, blank=True,)
    contract = models.ForeignKey("loans.LoanContract", verbose_name=_("Loan Contract"), null=True, blank=True, on_delete=models.DO_NOTHING)
    middle_name = models.CharField(_("Middle Name"), max_length=50, null=True, blank=True,)
    last_name = models.CharField(_("Last Name"), max_length=50, null=True, blank=True,)
    phone_number = models.CharField(_("Phone Number"), max_length=50, null=True, blank=True,)
    id_number = models.CharField(_("National Id Number"), max_length=50, null=True, blank=True)
    pledges = models.ManyToManyField("loans.GuarantorsPledge", verbose_name=_("Pledges"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return '%s - %s - %s' % (self.id_number, self.first_name, self.phone_number)
    
class GuarantorsPledge(models.Model):
    guarantor = models.ForeignKey("loans.LoanGuarantors", verbose_name=_("Loan Guarantors"), null=True, blank=True, on_delete=models.DO_NOTHING)
    count = models.IntegerField(_("Count"), default=1)
    item_title = models.CharField(_("Item Title"), max_length=50, null=True, blank=True)
    description = models.CharField(_("Item Description"),max_length=500, null=True, blank=True)
    # year = models.DateField(_("Date"), auto_now=False, auto_now_add=False)
    serial_number = models.CharField(_("Serial Number"), max_length=50, null=True, blank=True)
    estimated_value = models.DecimalField(_("Estimated Value"), max_digits=15, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)




class IndividualsPledge(models.Model):
    contract = models.ForeignKey("loans.LoanContract", verbose_name=_("Loan Contract"), on_delete=models.DO_NOTHING)
    count = models.IntegerField(_("Count"), default=1)
    item_title = models.CharField(_("Item Title"), max_length=50, null=True, blank=True)
    description = models.CharField(_("Item Description"),max_length=500, null=True, blank=True)
    # year = models.DateField(_("Date"), auto_now=False, auto_now_add=False)
    serial_number = models.CharField(_("Serial Number"), max_length=50, null=True, blank=True)
    estimated_value = models.DecimalField(_("Estimated Value"), max_digits=15, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)