from django.contrib import admin

# from .models import  AccrualInterestLoan, LoanDisbursement, LoanEntryFee, LoanInterestAccrual, LoanPurpose, LoanScale, LoanShareAmount, LoanLinkSavingBook, Periodicity, LoanPaymentPlan, ScannedImage, GC_Settings, Figures_Settings, LoanProduct, LoanContract, Loandisbursmentevents, Loanentryfeeevents, Loaninterestaccruingevents
# from .models import LoanContract, Loandisbursmentevents, Loanentryfeeevents, Loaninterestaccruingevents, Loanpenaltyaccrualevents
from .models import *

# class AccrualInterestLoanModel(admin.ModelAdmin):
#     list_display = ['amount', 'date_created', 'creator', 'date_modified', 'modified_by']
#     ordering = ['amount']
    
# admin.site.register(AccrualInterestLoan,AccrualInterestLoanModel)


class LoanDisbursementModelAdmin(admin.ModelAdmin):
    list_display = ['amount', 'fees', 'interest', 'payment_method', 'date_created', 'creator', 'date_modified', 'modified_by']
    ordering = ['amount']

class LoanProductAdminModel(admin.ModelAdmin):
    list_display = ['product_title',     'creator', 'date_modified', 'modified_by' ]
    ordering = ['product_title', 'interest_rate_scheme', 'client_type','loan_fee_processing', 'periodicity', 'payment_plan', 'use_loc', 'installment_type','annual_rate_interest', 'number_of_installments', 'grace_period', 'interest_over_grace_period', 'loc', 'principal_acc', 'interest_due_acc', 'interest_income_acc', 'accrued_penalty_acc', 'penalty_income_acc', 'interest_due_not_received', 'interest_accrued_acc','use_guarantors_collaterals', 'gc_settings',       'creator', 'date_modified', 'modified_by' ]
    icon_name = 'map'

    search_fields = [
      'product_title',
    ]
    list_filter = [
    'product_title', 'interest_rate_scheme', 'client_type','loan_fee_processing', 'periodicity', 'payment_plan', 'use_loc', 'installment_type','annual_rate_interest', 'number_of_installments', 'grace_period', 'interest_over_grace_period', 'loc', 'principal_acc', 'interest_due_acc', 'interest_income_acc', 'accrued_penalty_acc', 'penalty_income_acc', 'interest_due_not_received', 'interest_accrued_acc','use_guarantors_collaterals', 'gc_settings',       'creator', 'date_modified', 'modified_by'

    ]

    fieldsets = (
        (
            ' Loan Product Title', {
                'fields': (('product_title',), )
            }
        ),

         (
            ' Loan Product More Info', {
                'fields': (('client_type', 'loan_fee_processing', 'loan_processing_code',), )
            }
        ),

        (
            'Interest Scheme Info', {
                'fields': (('interest_rate_scheme', 'interest_code',),)
            }
        ),

        (
            'Annual Interest Rate Info', {
                'fields': (('annual_rate_interest',),)
            }
        ),

         (
            'Monthly Interest Rate Info', {
                'fields': (('monthly_rate_interest',),)
            }
        ),

        (
            'Accrued Penalties', {
                'fields': (('late_fees','interest_accrued_penalty','accrued_penalty', 'interest_accrued_code'),)
            }
        ),

         (
            'Interest Fees Info', {
                'fields': (('late_fees_loan_amount','late_fees_overdue_principal', 'late_fees_overdue_interest', 'late_fees_olb'), )
            }
        ),
        (
            'Payment Info', {
                'fields': (('periodicity','payment_plan', 'payment_code' ), )
            }
        ),
          (
            'Disbursement Info', {
                'fields': (('disburse_code', ), )
            }
        ),
        (
            'LOC Info', {
                'fields': (('use_loc', 'loc',))
            }
        ),
        (
            'Guarantors Info', {
                'fields': (('use_guarantors_collaterals','gc_settings',),)
            }
        ),
        (
            'Chart of Accounts Info', {
                'fields': (('principal_acc','interest_due_acc','interest_due_not_received',),
                ('accrued_penalty_acc','penalty_income_acc','interest_accrued_but_not_due_acc',  ), 
                ('interest_accrued_acc', ))
            }
        ),
        (
            'System Specific Info', {
                'fields': (( 'creator', 'modified_by',),)
            }
        ),
    )


class LoanContractAdminModel(admin.ModelAdmin):
    list_display = ['member', 'loan_product', 'requested_amount',   'creator', 'date_modified', 'modified_by' ]
    ordering =  ['member', 'loan_product',    'creator', 'date_modified', 'modified_by' ]
    icon_name = 'map'

    search_fields = [
      'requested_amount', 'member__first_name', 'member__middle_name', 'member__last_name', 'member__account_number',
    ]
    list_filter =  ['member', 'loan_product',    'creator', 'date_modified', 'modified_by' ]


class LoanEntryFeeModelAdmin(admin.ModelAdmin):
    fields = ['fee']


class GC_SettingsModelAdmin(admin.ModelAdmin):
    fields = ['min_percent']
admin.site.register(LoanDisbursement, LoanDisbursementModelAdmin)
admin.site.register(LoanEntryFee, LoanEntryFeeModelAdmin)
admin.site.register(LoanInterestAccrual)
admin.site.register(LoanPurpose)
# admin.site.register(LoanShareAmount)
# admin.site.register(LoanLinkSavingBook)
# admin.site.register(Periodicity)InternalGuarantors
admin.site.register(LoanPaymentPlan)
admin.site.register(InternalGuarantors)
# admin.site.register(GC_Settings, GC_SettingsModelAdmin)
# admin.site.register(Figures_Settings)
admin.site.register(LoanContract, LoanContractAdminModel)
admin.site.register(LoanGuarantors)
admin.site.register(GuarantorsPledge)
# admin.site.register(Loandisbursmentevents)
# admin.site.register(Loanentryfeeevents)
# admin.site.register(Loaninterestaccruingevents)
# admin.site.register(Loanpenaltyaccrualevents)
# admin.site.register(Loanproductsentryfees)
# admin.site.register(Loanshareamounts)
# admin.site.register(Loantransitionevents)
# admin.site.register(Loanslinksavingsbook)
# admin.site.register(Contractassignhistory)
# admin.site.register(Contractevents)
# admin.site.register(Contracts)
# admin.site.register(Writeoffevents)
# admin.site.register(Writeoffoptions)
# admin.site.register(Traceuserlogs)
# admin.site.register(Trancheevents)
# admin.site.register(RepRepaymentsData)
# admin.site.register(RepRescheduledLoansData)
admin.site.register(Repaymentevents)
admin.site.register(Reschedulingofaloanevents)
# admin.site.register(RepDisbursementsData)
# admin.site.register(Provisionevents)
# admin.site.register(Provisioningrules)
# admin.site.register(Penaltywriteoffevents)
# admin.site.register(Nonaccrualinterestevents)
# admin.site.register(Nonaccrualpenaltyevents)
admin.site.register(Overdueevents)
admin.site.register(Interestwriteoffevents)
# admin.site.register(Installments)
# admin.site.register(Installmenthistory)
# admin.site.register(Installmenttypes)
# admin.site.register(Packages)
# admin.site.register(Packagesclienttypes)
# admin.site.register(Projectpurposes)
# admin.site.register(Credit)
# admin.site.register(Creditentryfees)
# admin.site.register(Creditinsuranceevents)
admin.site.register(LoanProduct, LoanProductAdminModel)

admin.site.register(LoanRepayment)
admin.site.register(LoanStatus)