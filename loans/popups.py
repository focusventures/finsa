from .models import AccrualInterestLoan, LoanContract, LoanDisbursement, LoanEntryFee, LoanLinkSavingBook, LoanPaymentPlan, LoanProduct, LoanPurpose, LoanScale, LoanShareAmount, ScannedImage
# from loans.forms import PaymentPlanForm

# class AccrualInterestLoanPopupCRUDViewSet(PopupCRUDViewSet):
#      model = AccrualInterestLoan
#      form_class = AccrualInterestLoanForm
#      template_name_create = 'loans/accrual_create.html'
#      template_name_update = 'loans/accrual_update.html'


