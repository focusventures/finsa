from django.urls import path

from . import views

urlpatterns = [
    path('loans', views.loans, name="loans"),
    path('loans/statuses', views.loans_statuses, name="loans_statuses"),

    path('analysis', views.analysis, name="loans_analysis"),
    path('types', views.types, name="loans_types"),

    path('collateral', views.collateral, name="loans_collateral"),

    path('loanproducts', views.loanproducts, name="loans_products"),
    path('loanproducts/aoe', views.aoe_loan_products, name="aoe_loans_products"),
    path('loanproducts/edit/<int:pk>', views.edit_loan_products, name="edit_loans_products"),

    path('loanplans', views.loanplans, name="loans_plans"),
    path('loanplans/aoe', views.aoe_loan_plans, name="aoe_loans_plans"),
    path('loanplanss/edit/<int:pk>', views.edit_loan_plans, name="edit_loans_plans"),

    path('loanpurpose', views.loanplans, name="loans_purpose"),
    path('loanpurpose/aoe', views.aoe_loan_plans, name="aoe_loans_purpose"),
    path('loanpurpose/edit/<int:pk>', views.edit_loan_plans, name="edit_loans_purpose"),

    path('periodicty', views.periodicity, name="loans_periodicty"),
    path('purpose', views.purpose, name="loans_purpose"),
    path('reports', views.reports, name="loans_reports"),
    
    path('requests', views.requests, name="loans_requests"),
    path('loan_request/<int:pk>', views.aoe_loan_request, name="aoe_loan_request"),

    path('loan_guarantors/<int:pk>', views.loan_guarantors, name="loan_guarantors"),
    path('loan_pledges/<int:pk>', views.loan_pledges, name="loan_pledges"),

    path('loan_request/<int:pk>', views.edit_loan_request, name="edit_loan_request"),

    path('approve/<int:pk>', views.approve, name="approve"),
    path('confirm/<int:pk>', views.confirm, name="confirm"),
    path('disburse/<int:pk>', views.disbursing, name="disburse"),

    path('loan_backtracking', views.loans_backtracking, name="loans_backtrack"),


    path('loan_request_approvals', views.approvals, name="loans_requests_approvals"),
    path('loan_request_confirms', views.confirms, name="loans_requests_confirms"),
    path('loan_request_details/<int:pk>', views.confirms_details, name="confirms_details"),
    path('loan_request_diburse', views.disburse, name="loans_requests_disbursements"),
    path('loan_request_dibursement/<int:pk>', views.disbursement, name="loans_requests_disbursement"),
    path('loan_request_dibursement/<int:pk>', views.disbursement, name="loan_detailed_view"),
    # path('payment_plan/create', views.PaymentPlanCreatePopup, name="PaymentPlanCreate"),
    # path('payment_plan/(?P<pk>\d+)/edit', views.PaymentPlanEditPopup, name="PaymentPlanEdit"),
    # path('payment_plan/ajax/get_plan_id', views.get_plan_id, name="get_plan_by_id"),

    path('loan_details', views.loan_details, name="loan_load"),
    path('search_loan', views.search_loan, name="search_loans")

]
