from django.apps import AppConfig


class LoansConfig(AppConfig):
    name = 'loans'
    icon_name = 'book'

    def ready(self):
        import loans.signals

