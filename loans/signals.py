from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import LoanAccrual, LoanContract, LoanDisbursement, LoanEntryFee, LoanInterestAccrual, LoanPaymentPlan, LoanProduct, LoanPurpose
from dashboard.models import SnapShot


@receiver(post_save, sender=LoanContract)
def update_snapshot_on_save(sender, instance, created, **kwargs):

    if created:
        pass
    snapshot = SnapShot.objects.all()[0]
    snapshot.loan_requests += 1
    snapshot.d_loan_requests += 1
    snapshot.save()


@receiver(post_delete, sender=LoanContract)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.loan_requests -= 1
    snapshot.d_loan_requests -= 1
    snapshot.save()


@receiver(post_save, sender=LoanDisbursement)
def update_snapshot_on_save(sender, instance, created, **kwargs):
    if created:
        pass
    snapshot = SnapShot.objects.all()[0]
    snapshot.loan_disbursed += 1
    snapshot.d_loan_disbursed += 1
    snapshot.save()


@receiver(post_delete, sender=LoanDisbursement)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.loan_disbursed -= 1
    snapshot.d_loan_disbursed  -= 1
    snapshot.save()
