from django.shortcuts import render, redirect, reverse
from .forms import LoanProductForm, PaymentPlanForm, PurposeForm
from django.shortcuts import render, redirect, reverse,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from django.db.models import Q

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from django.contrib import messages
import json


from django.views.decorators.csrf import csrf_exempt
from loans.models import LoanContract, LoanProduct
from django.core.serializers.json import DjangoJSONEncoder

from members.models import Member, MemberGroup
from authentication.models import CustomUser

from savings.models import Savingproducts

from parameters.models import LOC

from django.forms import modelformset_factory
from django.db.models import Q

import json
from .models import *
from parameters.models import Periodicity
from .forms import *

from members.models import Member
from savings.models import CurrentAccount
from financial.models import Transactions


@login_required()
def loans_backtracking(request):
    if request.method == 'POST':
        search_term = request.POST["search_term"]
        members = Member.objects.filter(Q(first_name__icontains=search_term) | Q (middle_name__icontains=search_term) | Q(last_namae__icontains=search_term) | Q(id_number__icontains=search_term) | Q (phone_number__icontains=search_term))
        context = {
            "members":members
        }
        return render(request, 'loans/results.html', context) 
    return render(request, 'loans/backtracking.html')


@login_required()
def loans(request):
    loans = LoanContract.objects.filter(Q(is_approved=True) & Q(is_confirmed=True)& Q(is_disbursed=True))
    context = {
        'loans': loans
        
    }
    return render(request, 'loans/loans.html', context)



@login_required()
def loans_statuses(request):
    loans = LoanStatus.objects.all()
    context = {
        'statuses': loans
        
    }
    return render(request, 'loans/loans_statuses.html', context)


@login_required()
def reports(request):
    return render(request, 'loans/reports.html')

@login_required()
def types(request):
    return render(request, 'loans/types.html')



@csrf_exempt
@login_required()
def loan_details(request):
    if request.method == "POST":
        data = json.loads(request.body)
        loan = LoanContract.objects.filter(id=data['loan_id']).select_related('member', 'loan_product', 'creator', 'modified_by', 'guarantors', 'loan_purpose').values()
        loan = list(loan)
        loan = loan[0]
        member = Member.objects.filter(id=loan['member_id']).values()
        member = list(member)[0]
        product = LoanProduct.objects.filter(id=loan['loan_product_id']).values()
        product = list(product)[0]
        contracts = LoanContract.objects.filter(Q(member__id=data['loan_id']) & Q(is_disbursed=False)).select_related('member', 'loan_product', 'creator', 'modified_by', 'guarantors', 'loan_purpose').values()
        contracts = list(contracts)

        group = MemberGroup.objects.filter(id=member["group_id"]).values()
        group = list(group)[0]

        group_members = Member.objects.filter(group__id=group["id"])

      
        group["total_members"] = len(group_members)

        savings = 0
        loans = 0

        # for member in group_members:
        #     try:
        #         savings += member.get_savings()
        #     except AttributeError:
        #         savings += 0

        # print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")

        # all_loans = LoanContract.objects.filter(Q(member__group__id=group["id"]) & Q(is_disbursed = True) & Q(is_repaid = False))
        
        # for l in  all_loans:
        #     try:
        #         loans += l.approved_amount
        #     except AttributeError:
        #         loans += 0
        

        group["total_savings"] = savings
        group["total_loans"] = loans
        group["active_loans"] = loans


        # savings_plan = 

        loan['member'] = member
        loan['loan_product'] = product

        loan['approve_link'] = 'http://127.0.0.1:8000' + reverse('approve', args=(loan["id"],))
        loan['confirm_link'] = 'http://127.0.0.1:8000'  + reverse('confirm', args=(loan["id"],))
        loan['disburse_link'] = 'http://127.0.0.1:8000'  + reverse('disburse', args=(loan["id"],))

        data = {"loan": loan, "contracts": contracts, 'member': member, "product": product, "group": group, "savings_product": ''}
        return JsonResponse(data, safe=False)


@login_required()
def approve(request, pk):
    loan = LoanContract.objects.get(id=pk)

    if loan.approving_officer == request.user:
        loan.is_approved = True
        loan.save()
    else:
        messages.error(request, 'This loan can be approved by %s' % (loan.approving_officer))


    return redirect(reverse('loans_requests_approvals'))


@login_required()
def confirm(request, pk):
    loan = LoanContract.objects.get(id=pk)
    loan.is_confirmed = True
    loan.confirming_officer = request.user
    loan.save()
    return redirect(reverse('loans_requests_confirms'))

import datetime

@login_required()
def disbursing(request, pk):
    loan = LoanContract.objects.filter(id=pk).select_related('member', 'loan_product').first()
    product = LoanProduct.objects.filter(id=loan.loan_product.id).select_related('loc').first()
    member = Member.objects.get(id=loan.member.id)
    current_acc = CurrentAccount.objects.get(member__id=member.id)
    loc = LOC.objects.get(id=product.loc.id)

    if product.use_loc:

        if loc.remainder_amount  < loan.requested_amount:
            messages.success(request, 'The Line Of Credit Account doesnt have sufficient float to disburse this request')
            return redirect(reverse('loans_requests_disbursements'))

        tranaction = Transactions()
        tranaction.amount = product.loan_fee_processing
        tranaction.description = "Loan Processing Fee"
        tranaction.type = "loan_processing_fee"
        tranaction.creator = request.user
        tranaction.date = datetime.datetime.now()
        tranaction.date_created = datetime.datetime.now()
        tranaction.member = member
        tranaction.code = product.loan_processing_code.code
        tranaction.save()

        tranaction = Transactions()
        tranaction.amount = loan.approved_amount
        tranaction.description = "Loan Disbursement"
        tranaction.type = "loan_disbursement"
        tranaction.creator = request.user
        tranaction.date = datetime.datetime.now()
        tranaction.date_created = datetime.datetime.now()
        tranaction.member = member
        tranaction.code = product.disburse_code.code
        tranaction.save()


        current_acc.acc_balance += (current_acc.acc_balance + loan.requested_amount) -  product.loan_fee_processing
        current_acc.save()

        loc.remainder_amount -= (current_acc.acc_balance + loan.requested_amount) +  product.loan_fee_processing
        loc.save()

    loan.is_disbursed = True
    loan.disbursing_officer = request.user
    loan.save()
    messages.success(request, 'Loan Disburment Successfull')
    return redirect(reverse('loans_requests_disbursements'))

@login_required()
def periodicity(request):
    periods = Periodicity.objects.all()
    context = {
        'periods': periods
    }
    return render(request, 'loans/periodicity.html', context)



@login_required()
def approvals(request):
    approvals = LoanContract.objects.filter(Q(is_approved=False) & Q(approving_officer=request.user))
    recent = LoanContract.objects.filter(Q(is_approved=True) & Q(approving_officer=request.user))
    context = {
        'requests': approvals,
        'recent': recent
    }
    return render(request, 'loans/approvals.html', context)

@login_required()
def loan_approve_query(request):
    if request.method == "POST":
        pass


    context = {
        
    }



    return render(request, 'loans/approvals.html', context)












@login_required()
def search_loan(request, pk):


    return JsonResponse(data)

@login_required()
def confirms(request):
    confirms = LoanContract.objects.filter(Q(is_approved=True) & Q(is_confirmed=False))
    recent = LoanContract.objects.filter(Q(is_approved=True) & Q(is_confirmed=True))
    context = {
        'requests': confirms,
        'recent': recent
    }
    return render(request, 'loans/confirms.html', context)

@login_required()
def confirms_details(request, pk):
    confirms = LoanContract.objects.filter(Q(is_approved=False) & Q(approving_officer=request.user))
    context = {
        'requests': confirms
    }
    return render(request, 'loans/confirms_details.html', context)

@login_required()
def disburse(request):
    disburse = LoanContract.objects.filter(Q(is_approved=True) & Q(is_confirmed=True)& Q(is_disbursed=False))
    recent = LoanContract.objects.filter(Q(is_approved=True) & Q(is_confirmed=True)& Q(is_disbursed=True))
    context = {
        'requests': disburse,
        'recent': recent
    }
    return render(request, 'loans/disburse.html', context)

@login_required()
def disbursement(request, pk):
    disburse = LoanContract.objects.filter(Q(is_approved=False) & Q(approving_officer=request.user))
    context = {
        'requests': disburse
    }
    return render(request, 'loans/disburse.html', context)

@login_required()
def collateral(request):
    return render(request, 'loans/collateral.html')


@login_required()
def analysis(request):
    return render(request, 'loans/analysis.html')

@login_required()
def loanproducts(request):

    products = LoanProduct.objects.all()

    context = {
        "products": products
    }
    return render(request, 'loans/loanproducts.html', context)

@login_required()
def aoe_loan_products(request, pk):
    member = Member.objects.get(id=pk)
    form = LoanProductForm()
    if request.method == 'POST':
        form = LoanProductForm(request.POST, request.FILES)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('members_details', args=(pk,)))

    context = {'form': form, 'member':member}
    return render(request, 'loans/aoe_loan_products.html', context)


# def open_saving_account(request, pk):
#     member = Member.objects.get(id=pk)

#     if member.id_number == None:
#         messages.error(request, 'You cannot Open A savings Account without A National Id Number. Kindly Update the Member Info By Clicking the EDIT BUTTON') 

#         return redirect(reverse('members_details', args=(pk,)))

#     form = SavingAccountForm(prefix="sa")
#     contract_form = SavingcontractsForm(prefix="scf")
#     if request.method == 'POST':
#         saving_acc = SavingAccountForm(request.POST, prefix="sa")
#         saving_contract = SavingcontractsForm(request.POST, prefix="scf")
#         if saving_acc.is_valid() and saving_contract.is_valid():


#             return redirect(reverse('members_details', args=(pk,)))
#     context = {'form': form, "contract_form": contract_form,  "member":member}
#     return render(request, 'savings/accounts/saving_account.html', context)


@login_required()
def edit_loan_products(request, pk):
    form = LoanProductForm(instance=LoanProduct.objects.get(id=pk))
    if request.method == 'POST':
        form = LoanProductForm(request.POST, request.FILES, instance=LoanProduct.objects.get(id=pk))
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('loans_products'))
 
    context = {'form': form}
    return render(request, 'loans/aoe_loan_products.html', context)



from .models import LoanPaymentPlan
@login_required()
def PaymentPlanCreatePopup(request):
    form = PaymentPlanForm(request.POST or None)
    if form.is_valid():
        instance = form.save()
        return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_LoanPaymentPlan");</script>' % (instance.pk, instance))
    return render(request, "loans/plan_create.html", {"form" : form})


@login_required()
def PaymentPlanEditPopup(request, pk = None):
    instance = get_object_or_404(LoanPaymentPlan, pk = pk)
    form = PaymentPlanForm(request.POST or None, instance = instance)
    if form.is_valid():
        instance = form.save()
        return HttpResponse('<script>opener.closePopup(window, "%s", "%s", "#id_LoanPaymentPlan");</script>' % (instance.pk, instance))
    return render(request, "author_form.html", {"form" : form})




#????????
@csrf_exempt
def get_plan_id(request):
	if request.is_ajax():
		author_name = request.GET['author_name']
		author_id = LoanPaymentPlan.objects.get(name = author_name).id
		data = {'author_id':author_id,}
		return HttpResponse(json.dumps(data), content_type='application/json')
	return HttpResponse("/")

















def loanplans(request):
    plans = LoanPaymentPlan.objects.all()

    context = {
        'plans': plans
    }
    return render(request, 'loans/plan.html', context)

@login_required()
def aoe_loan_plans(request):
    form = PaymentPlanForm()
    if request.method == 'POST':
        form = PaymentPlanForm(request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('loans_plans'))

    context = {'form': form}
    return render(request, 'loans/aoe_loan_plans.html', context)


@login_required()
def edit_loan_plans(request, pk):
    form = PaymentPlanForm(instance=LoanPaymentPlan.objects.get(id=pk))
    if request.method == 'POST':
        form = PaymentPlanForm(request.POST, request.FILES, instance=LoanPaymentPlan.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('loans_plans'))
 
    context = {'form': form}
    return render(request, 'loans/aoe_loan_plans.html', context)



















def purpose(request):
    purpose = LoanPurpose.objects.all()
    context = {
        'purpose': purpose
    }
    return render(request, 'loans/purpose.html', context)

@login_required()
def aoe_loan_purpose(request):
    form = PaymentPlanForm()
    if request.method == 'POST':
        form = PaymentPlanForm(request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('loans_plans'))

    context = {'form': form}
    return render(request, 'loans/aoe_loan_plans.html', context)


@login_required()
def edit_loan_purpose(request, pk):
    form = PaymentPlanForm(instance=LoanPaymentPlan.objects.get(id=pk))
    if request.method == 'POST':
        form = PaymentPlanForm(request.POST, request.FILES, instance=LoanPaymentPlan.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('loans_plans'))
 
    context = {'form': form}
    return render(request, 'loans/aoe_loan_plans.html', context)




@login_required()
def requests(request):
    requests = LoanContract.objects.filter(Q(is_approved=False) & Q(is_confirmed=False)& Q(is_disbursed=False))

    context = {
        "requests": requests
    }
    return render(request, 'loans/requests.html', context)


@login_required()
def aoe_loan_request(request, pk):
    member = Member.objects.get(id=pk)
    form = LoanContractForm(request, pk)
    # messages.success(request, 'Request')

    if request.method == 'POST':
        # messages.success(request, 'Ooops')

        form = LoanContractForm(request, pk,request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            messages.success(request, 'Request Added Successfully')

            member.has_active_loan = True
            member.save()

            return redirect(reverse('loan_guarantors', args=(bulding.id,)))
    context = {'form': form, 'member': member}
    # messages.success(request, 'Process Loan Request')

    return render(request, 'loans/loan_request.html', context)


@login_required()
def loan_guarantors(request, pk):
    loan = LoanContract.objects.get(id=pk)
    form = LoanContractForm(request, pk)

    form_ = LoanGuarantorsForm()
    # messages.success(request, 'Request')

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            LoanGuarantors,
            fields=('first_name','middle_name','last_name', 'phone_number', 'id_number', 'sex' , 'id_number' ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
    
            for form in formset:
                if form.cleaned_data.get('id_number'):
                    guarantor = form.save(commit=False)
                    guarantor.creator = request.user
                    guarantor.date_created = datetime.datetime.now()
                    guarantor.save()

                    loan.guarantors.add(guarantor)
                    loan.save()

    TransactionsModelFormset = modelformset_factory(
    LoanGuarantors,
    fields=('first_name','middle_name','last_name', 'phone_number', 'id_number', 'sex' , 'id_number' ),

    extra=1
    )
    formset = TransactionsModelFormset(queryset=LoanGuarantors.objects.none())
    context = {'form': form, 'loan': loan, "formset": formset}

    return render(request, 'loans/loan_guarantors.html', context)





@login_required()
def loan_pledges(request, pk):
    loan = LoanContract.objects.get(id=pk)
    form = LoanContractForm(request, pk)

    form_ = LoanGuarantorsForm()

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            GuarantorsPledge,
            fields=('count','item_title','description', 'year', 'serial_number', 'estimated_value' ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
    
            for form in formset:
                if form.cleaned_data.get('estimated_value'):
                    pledge = form.save(commit=False)
                    pledge.creator = request.user
                    pledge.date_created = datetime.datetime.now()
                    pledge.save()

                    loan.guarantors_pledge.add(pledge)
                    loan.save()


    TransactionsModelFormset = modelformset_factory(
    GuarantorsPledge,
    fields=('count','item_title','description', 'year', 'serial_number', 'estimated_value' ),
    extra=1
    )
    formset = TransactionsModelFormset(queryset=LoanGuarantors.objects.none())
    context = {'form': form, 'loan': loan, "formset": formset}

    return render(request, 'loans/loan_pledges.html', context)


@login_required()
def edit_loan_request(request, pk):
    form = LoanContractForm(request, pk, instance=LoanContract.objects.get(id=pk))
    if request.method == 'POST':
        form = LoanContractForm(request.POST, request.FILES, instance=LoanContract.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('loans_products'))
 
    context = {'form': form}
    return render(request, 'loans/loan_request_edit.html', context)


