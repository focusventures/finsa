from django.db import models
from django.utils.translation import ugettext_lazy as _

class AccrualInterestLoan(models.Model):
    amount = models.FloatField(_("Amount Disbursed"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Accrual Interest Loan"

class LoanDisbursement(models.Model):
    amount = models.FloatField(_("Amount Disbursed"))
    fees = models.FloatField(_("Fees"))
    interest = models.FloatField(_("Interest"))
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Disbursement Method"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Loan Disbursement"

class LoanEntryFee(models.Model):
    fee = models.FloatField(_("Fee"))
    disbursement_event = models.ForeignKey("loans.LoanDisbursement", null=True, verbose_name=_("Loan Disbursement Event"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Loan Entry Fee"

    def __str__(self):
        return "%s" %(self.fee)

class LoanInterestAccrual(models.Model):
    interest_prepayment = models.FloatField(_("Loan Interest Prepayment"))
    accrued_interest = models.FloatField(_("Accrued Interest"))
    rescheduled = models.BooleanField(_("Is Rescheduled"))
    installment_number = models.IntegerField(_("Installment Number"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Loan Interest Accrual"


class LoanPurpose(models.Model):
    parent_id = models.IntegerField(blank=True, null=True)
    deleted = models.BooleanField(default=False)
    name = models.CharField(_("Loan Purpose"), max_length=255)
    economic_activity = models.ForeignKey("members.EconomicActivity", verbose_name=_("Loan Purpose"), null=True, on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


    class Meta:
        verbose_name = "Loan Purpose"


class LoanScale(models.Model):
    _id = models.IntegerField(default=0)
    scalemin = models.IntegerField(db_column='ScaleMin', blank=True, null=True)  # Field name made lowercase.
    scalemax = models.IntegerField(db_column='ScaleMax', blank=True, null=True)  # Field name made lowercase.
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Loan Scale"


class LoanShareAmount(models.Model):
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("Group"), on_delete=models.CASCADE)
    contract = models.ForeignKey("members.MemberGroup", verbose_name=_("Contact"), related_name="contact", on_delete=models.CASCADE)
    amount = models.FloatField(_("Amount"))
    payment_date = models.DateField(_("Payment Date"), auto_now=False, auto_now_add=False)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Loan Share Amount"

class LoanLinkSavingBook(models.Model):
    # loan = 
    # savings =
    loan_percentage = models.IntegerField(_("Loan Percentage"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Loan Link Saving Book"

class Periodicity(models.Model):
    name = models.CharField(_("Name"), default=None, max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Periodicity" 


class LoanPaymentPlan(models.Model):
    name = models.CharField(_("Name"), null=True, max_length=50)
    # loan_percentage = models.IntegerField(_("Loan Percentage"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


    def __str__(self):
        return self.name


    class Meta:
        verbose_name = "Loan Payment Plan"

class ScannedImage(models.Model):
    document = models.ImageField(_("Scanned Copy"), upload_to="scans/", height_field=None, width_field=None, max_length=None)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Saccned Images"


class GC_Settings(models.Model):
    min_percent = models.IntegerField(_("Minimum Percentage of Guarantors and Collaterals"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "GC Settings"

    def __str__(self):
        return "%s" % (self.min_percent)

class Figures_Settings(models.Model):
    min = models.FloatField(_("Minimum Vaue"), default=0.0)
    max = models.FloatField(_("Maximum Vaue"), default=0.0)
    actual = models.FloatField(_("Actual Vaue"), default=0.0)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Figures Settings"

scheme_rates = (
    ("All", "All"),
    ("Flat", "Flat(Fixed Pricinpal, Fixed Interest)"),
    ("Declining", "Fixed Principal"),
    ("Deline", "Declining Principal")
)


install_type = (
    ("All", "All"),
    ("Monthly", "Monthly"),
    ("Yearly", "Yearly"),
    ("quoterly", "quoterly")
)

client_type = (
    ("Individual", "Individaul"),
 
)

class LoanProduct(models.Model):
    product_title = models.CharField(_("Loan Product Title"), max_length=50)
    interest_rate_scheme = models.CharField(_("Interest Rate Scheme"), choices=scheme_rates, default="All", max_length=50)
    client_type = models.CharField(_("Client Type"), choices=client_type, default="Individual", max_length=50)
    loan_processing_fee = models.ForeignKey("loans.LoanEntryFee", verbose_name=_("Loan Processing Fee"), on_delete=models.CASCADE)
    periodicity = models.ForeignKey("parameters.Periodicity", verbose_name=_("Payment Period"), on_delete=models.CASCADE)
    payment_plan = models.ForeignKey("loans.LoanPaymentPlan", verbose_name=_("Payment Plan"), on_delete=models.CASCADE)
    use_loc = models.BooleanField(_("Use Line Of Credit"), default=False)

    installment_type = models.CharField(_("Installment Type"), choices=install_type, default="All", max_length=50)
    annual_interest_rate = models.ForeignKey("loans.Figures_Settings", verbose_name=_("Annual Interest Rate"), null=True, related_name="Annual Interest Rate+",on_delete=models.CASCADE)
    number_of_installments = models.ForeignKey("loans.Figures_settings", verbose_name=_("Number Of Intallments"), null=True, related_name="Number Of Intallments+", on_delete=models.CASCADE)
    grace_period = models.ForeignKey("loans.Figures_settings", verbose_name=_("Grace Period"), null=True, related_name="Grace Period+", on_delete=models.CASCADE)
    interest_over_grace_period = models.BooleanField(_("Change Interest Over Grace Period"), default=False)

    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line of Credit"), null=True, related_name="Line Of Credit+", on_delete=models.CASCADE)
    principal_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Principal Account"), null=True, related_name="Principal Account+", on_delete=models.CASCADE)
    
    interest_due_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Due Account"), null=True,  related_name="Interest Due Account+", on_delete=models.CASCADE)
    interest_income_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Income Account"), null=True,  related_name="Interest Income Account+",on_delete=models.CASCADE)
    accrued_penalty_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Accrued Penalty"),null=True,   related_name="Accrued Penalty+",on_delete=models.CASCADE)
    penalty_income_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Penalty Income"), null=True,  related_name="Penalty Income+",on_delete=models.CASCADE)
    interest_due_not_received = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Due But Not Received"), null=True, related_name="Interest Due But Not Received+", on_delete=models.CASCADE)
    interest_accrued_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Accrued Account"), null=True,  related_name="Interest Accrued Account+",on_delete=models.CASCADE)
    interest_accrued_but_not_due_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Accrued But Not Due Account"), null=True,  related_name="Interest Accrued But Not Due Account+",on_delete=models.CASCADE)

    use_guarantors_collaterals = models.BooleanField(_("Use Guarantors And Collaterals"), default=False)
    gc_setttings = models.ForeignKey("loans.GC_Settings", verbose_name=_("Guarantors & Collaterals Settings"), null=True, on_delete=models.CASCADE)
    
    late_fees_loan_amount = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On Loan Amount in % Per day"), null=True, related_name="Late Fees On Loan Amount+", on_delete=models.CASCADE)
    late_fees_overdue_principal = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On Overdue Principal in % Per day"), null=True, related_name="Late Fees On Overdue Principal+", on_delete=models.CASCADE)
    late_fees_overdue_interest = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On Overdue Interest in % Per day"), null=True, related_name="Late Fees On Overdue Interes+", on_delete=models.CASCADE)
    late_fees_olb = models.ForeignKey("loans.Figures_settings", verbose_name=_("Late Fees On OLB in % Per day"), null=True, related_name="Late Fees On OLB+", on_delete=models.CASCADE)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Loan Product"

class LoanContract(models.Model):
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), related_name="Member+",on_delete=models.CASCADE)
    requested_amount = models.FloatField(_("Amount Requested By the Member"))
    loan_product = models.ForeignKey("loans.LoanProduct", verbose_name=_("Loan Product"), related_name="Loan Product+",on_delete=models.CASCADE)
    requested_amount_words = models.CharField(_("Amount Requested in words "), max_length=50)
    loan_purpose = models.ForeignKey("loans.LoanPurpose", verbose_name=_("Purpose Of The Loan"), on_delete=models.CASCADE)
    loan_purpose_description = models.TextField(_("Describe The Purpose Of The Loan"))
    loan_payment_plan_description = models.TextField(_("Describe The Payment Plan Of The Loan"))
    amount_to_be_paid = models.FloatField(_("Monthly Installments Amount Expected"))
    amount_to_be_paid_words = models.CharField(_("Amount Requested in words "), max_length=50)
    guarantors = models.ManyToManyField("members.Member", verbose_name=_("Guarantors"), related_name="Loan Guarantors+")
    hard_copy_scans = models.ManyToManyField("loans.ScannedImage", verbose_name=_("Scans"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


    class Meta:
        verbose_name = "Loan Contract"
        icon_name = 'book'








class Loandisbursmentevents(models.Model):
    _id = models.ForeignKey('loans.Contractevents',models.DO_NOTHING, null=True, db_column='_id')
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    fees = models.DecimalField(max_digits=19, decimal_places=4)
    interest = models.DecimalField(max_digits=19, decimal_places=4)
    payment_method_id = models.IntegerField(blank=True, null=True)
    class Meta:
        verbose_name = "Loan Disburement Events"

class Loanentryfeeevents(models.Model):
    _id = models.IntegerField()
    fee = models.DecimalField(max_digits=19, decimal_places=4)
    disbursement_event_id = models.IntegerField()

    class Meta:
        verbose_name = "Loan Entry Fee Events"

class Loaninterestaccruingevents(models.Model):
    _id = models.IntegerField(primary_key=True)
    interest_prepayment = models.DecimalField(max_digits=19, decimal_places=4)
    accrued_interest = models.DecimalField(max_digits=19, decimal_places=4)
    rescheduled = models.BooleanField()
    installment_number = models.IntegerField()

    class Meta:
        verbose_name = "Loan Interest Accruing Events"




class Loanpenaltyaccrualevents(models.Model):
    _id = models.ForeignKey('loans.Contractevents', models.DO_NOTHING, null=True)
    penalty = models.DecimalField(max_digits=19, decimal_places=4)
    installment_number = models.IntegerField()

    class Meta:
        verbose_name = "Loan Penalty Accruing Events"


class Loanproductsentryfees(models.Model):
    id_entry_fee = models.ForeignKey('extras.Entryfees', models.DO_NOTHING, null=True)
    id_product = models.ForeignKey('loans.Packages', models.DO_NOTHING, null=True)
    cycle = models.ForeignKey('extras.Cycles', models.DO_NOTHING, blank=True, null=True)
    fee_index = models.IntegerField()

    class Meta:
        verbose_name = "Loan Product Entry Fee Events"


class Loanshareamounts(models.Model):
    person_id = models.IntegerField()
    group_id = models.IntegerField()
    contract_id = models.IntegerField()
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    event_id = models.IntegerField(blank=True, null=True)
    payment_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = "Loan Share Amount Accruing Events"


class Loantransitionevents(models.Model):
    _id = models.IntegerField()
    amount = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        verbose_name = "Loan Transition Event"


class Loanslinksavingsbook(models.Model):
    loan = models.OneToOneField('loans.Contracts', models.DO_NOTHING, blank=True, null=True)
    savings = models.ForeignKey('savings.Savingcontracts', models.DO_NOTHING, blank=True, null=True)
    loan_percentage = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = "Loan Link Savings Book"




class Contractassignhistory(models.Model):
    datechanged = models.DateTimeField(db_column='DateChanged')  # Field name made lowercase.
    loanofficerfrom = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING, related_name='Loan Officer From+', db_column='loanofficerFrom_id', null=True)  # Field name made lowercase.
    loanofficerto = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING, related_name='Loan Officer To+', db_column='loanofficerTo_id', null=True)  # Field name made lowercase.
    contract = models.ForeignKey('loans.Contracts', models.DO_NOTHING, null=True)

    class Meta:
        verbose_name = "Contract Assign History"



class Contractevents(models.Model):
    _id = models.OneToOneField('loans.Loaninterestaccruingevents', models.DO_NOTHING, db_column='_id', primary_key=True)
    event_type = models.CharField(max_length=4)
    contract = models.ForeignKey('loans.Contracts', models.DO_NOTHING,  blank=True, null=True)
    event_date = models.DateTimeField()
    user = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING,  blank=True, null=True)
    is_deleted = models.BooleanField()
    entry_date = models.DateTimeField(blank=True, null=True)
    is_exported = models.BooleanField()
    comment = models.CharField(max_length=4000, blank=True, null=True)
    teller = models.ForeignKey('extras.Tellers', models.DO_NOTHING, blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)
    cancel_date = models.DateTimeField(blank=True, null=True)
    doc1 = models.CharField(max_length=255, blank=True, null=True)
    doc2 = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = "Contracts Events"


class Contracts(models.Model):
    contract_code = models.CharField(max_length=255)
    branch_code = models.CharField(max_length=50)
    creation_date = models.DateTimeField()
    start_date = models.DateTimeField()
    close_date = models.DateTimeField()
    closed = models.BooleanField()
    project = models.ForeignKey('loans.Projects', models.DO_NOTHING, null=True)
    status = models.SmallIntegerField()
    credit_commitee_date = models.DateTimeField(blank=True, null=True)
    credit_commitee_comment = models.CharField(max_length=4000, blank=True, null=True)
    credit_commitee_code = models.CharField(max_length=100, blank=True, null=True)
    align_disbursed_date = models.DateTimeField(blank=True, null=True)
    loan_purpose = models.CharField(max_length=4000, blank=True, null=True)
    comments = models.CharField(max_length=4000, blank=True, null=True)
    nsg = models.ForeignKey('locations.Villages', models.DO_NOTHING, blank=True, null=True)
    activity = models.ForeignKey('loans.Loanpurpose',models.DO_NOTHING,  default=None )
    preferred_first_installment_date = models.DateTimeField(blank=True, null=True)
    created_by = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING, null=True)

    class Meta:
        verbose_name = 'Contracts'



class Writeoffevents(models.Model):
    _id = models.OneToOneField('loans.Contractevents', models.DO_NOTHING, db_column='_id', primary_key=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4)
    accrued_interests = models.DecimalField(max_digits=19, decimal_places=4)
    accrued_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    past_due_days = models.IntegerField()
    overdue_principal = models.DecimalField(max_digits=19, decimal_places=4)
    comment = models.CharField(max_length=255, blank=True, null=True)
    write_off_method = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name = 'WriteOffEvents'


class Writeoffoptions(models.Model):
    name = models.CharField(db_column='Name', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        verbose_name = 'WriteOffOptions'




class Traceuserlogs(models.Model):
    event_code = models.CharField(max_length=10, blank=True, null=True)
    event_date = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    event_description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TraceUserLogs'


class Trancheevents(models.Model):
    _id = models.OneToOneField('loans.Contractevents', models.DO_NOTHING, db_column='_id', primary_key=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    maturity = models.IntegerField()
    start_date = models.DateTimeField()
    interest_rate = models.DecimalField(max_digits=19, decimal_places=4)
    started_from_installment = models.IntegerField(blank=True, null=True)
    applied_new_interest = models.BooleanField()
    first_repayment_date = models.DateField()
    grace_period = models.IntegerField()
    payment_method_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TrancheEvents'




class RepRepaymentsData(models.Model):
    _id = models.IntegerField()
    branch_name = models.CharField(max_length=50, blank=True, null=True)
    load_date = models.DateTimeField(blank=True, null=True)
    event_id = models.IntegerField(blank=True, null=True)
    contract_code = models.CharField(max_length=255, blank=True, null=True)
    client_name = models.CharField(max_length=255, blank=True, null=True)
    district_name = models.CharField(max_length=255, blank=True, null=True)
    loan_officer_name = models.CharField(max_length=255, blank=True, null=True)
    loan_product_name = models.CharField(max_length=255, blank=True, null=True)
    early_fee = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    late_fee = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    principal = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    interest = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    total = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    written_off = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Rep_Repayments_Data'


class RepRescheduledLoansData(models.Model):
    _id = models.IntegerField()
    branch_name = models.CharField(max_length=50, blank=True, null=True)
    load_date = models.DateTimeField(blank=True, null=True)
    loan_officer = models.CharField(max_length=255, blank=True, null=True)
    client_name = models.CharField(max_length=255, blank=True, null=True)
    contract_code = models.CharField(max_length=255, blank=True, null=True)
    package_name = models.CharField(max_length=255, blank=True, null=True)
    loan_amount = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_rescheduled = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    maturity = models.IntegerField(blank=True, null=True)
    reschedule_date = models.DateTimeField(blank=True, null=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Rep_Rescheduled_Loans_Data'


class Repaymentevents(models.Model):
    _id = models.ForeignKey('loans.Contractevents', models.DO_NOTHING, db_column='_id')
    past_due_days = models.IntegerField()
    principal = models.DecimalField(max_digits=19, decimal_places=4)
    interests = models.DecimalField(max_digits=19, decimal_places=4)
    installment_number = models.IntegerField()
    commissions = models.DecimalField(max_digits=19, decimal_places=4)
    penalties = models.DecimalField(max_digits=19, decimal_places=4)
    payment_method_id = models.IntegerField(blank=True, null=True)
    calculated_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    written_off_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    unpaid_penalties = models.DecimalField(max_digits=19, decimal_places=4)
    bounce_fee = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'RepaymentEvents'


class Reschedulingofaloanevents(models.Model):
    _id = models.OneToOneField('loans.Contractevents', models.DO_NOTHING, db_column='_id', primary_key=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    interest = models.DecimalField(max_digits=19, decimal_places=4)
    nb_of_maturity = models.IntegerField()
    grace_period = models.IntegerField()
    charge_interest_during_grace_period = models.BooleanField()
    previous_interest_rate = models.DecimalField(max_digits=19, decimal_places=4)
    preferred_first_installment_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'ReschedulingOfALoanEvents'



class RepDisbursementsData(models.Model):
    _id = models.IntegerField()
    branch_name = models.CharField(max_length=50, blank=True, null=True)
    load_date = models.DateTimeField(blank=True, null=True)
    contract_code = models.CharField(max_length=255, blank=True, null=True)
    district = models.CharField(max_length=255, blank=True, null=True)
    loan_product = models.CharField(max_length=255, blank=True, null=True)
    client_name = models.CharField(max_length=255, blank=True, null=True)
    loan_cycle = models.IntegerField(blank=True, null=True)
    loan_officer = models.CharField(max_length=255, blank=True, null=True)
    disbursement_date = models.DateTimeField(blank=True, null=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    interest = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Rep_Disbursements_Data'




class RepActiveLoansData(models.Model):
    _id = models.IntegerField()
    branch_name = models.CharField(max_length=50, blank=True, null=True)
    load_date = models.DateTimeField(blank=True, null=True)
    break_down = models.CharField(max_length=150, blank=True, null=True)
    break_down_type = models.CharField(max_length=20, blank=True, null=True)
    contracts = models.IntegerField(blank=True, null=True)
    individual = models.IntegerField(blank=True, null=True)
    group = models.IntegerField(blank=True, null=True)
    corporate = models.IntegerField(blank=True, null=True)
    clients = models.IntegerField(blank=True, null=True)
    in_groups = models.IntegerField(blank=True, null=True)
    projects = models.IntegerField(blank=True, null=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    break_down_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Rep_Active_Loans_Data'

class Provisionevents(models.Model):
    _id = models.OneToOneField('loans.Contractevents', models.DO_NOTHING, db_column='_id', primary_key=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    rate = models.FloatField()
    overdue_days = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'ProvisionEvents'


class Provisioningrules(models.Model):
    _id = models.IntegerField(primary_key=True)
    number_of_days_min = models.IntegerField()
    number_of_days_max = models.IntegerField()
    provisioning_value = models.FloatField()
    provisioning_interest = models.FloatField()
    provisioning_penalty = models.FloatField()

    class Meta:
        managed = False
        db_table = 'ProvisioningRules'



class Penaltywriteoffevents(models.Model):
    _id = models.ForeignKey('loans.Contractevents', models.DO_NOTHING, db_column='_id')
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    installment_number = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'PenaltyWriteOffEvents'






class Nonaccrualinterestevents(models.Model):
    _id = models.ForeignKey('loans.Contractevents', models.DO_NOTHING, db_column='_id', blank=True, null=True)
    interest = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'NonAccrualInterestEvents'


class Nonaccrualpenaltyevents(models.Model):
    _id = models.IntegerField()
    penalty = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'NonAccrualPenaltyEvents'


class Overdueevents(models.Model):
    _id = models.OneToOneField('loans.Contractevents', models.DO_NOTHING, db_column='_id', primary_key=True)
    olb = models.DecimalField(max_digits=19, decimal_places=4)
    overdue_days = models.IntegerField()
    overdue_principal = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'OverdueEvents'


class Interestwriteoffevents(models.Model):
    _id = models.ForeignKey('loans.Contractevents', models.DO_NOTHING, db_column='_id')
    amount = models.DecimalField(max_digits=19, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'InterestWriteOffEvents'


class Installments(models.Model):
    expected_date = models.DateTimeField()
    interest_repayment = models.DecimalField(max_digits=19, decimal_places=4)
    capital_repayment = models.DecimalField(max_digits=19, decimal_places=4)
    contract = models.OneToOneField('loans.Credit', models.DO_NOTHING, primary_key=True)
    number = models.IntegerField()
    paid_interest = models.DecimalField(max_digits=19, decimal_places=4)
    paid_capital = models.DecimalField(max_digits=19, decimal_places=4)
    fees_unpaid = models.DecimalField(max_digits=19, decimal_places=4)
    paid_date = models.DateTimeField(blank=True, null=True)
    paid_fees = models.DecimalField(max_digits=19, decimal_places=4)
    comment = models.CharField(max_length=50, blank=True, null=True)
    pending = models.BooleanField()
    start_date = models.DateTimeField()
    olb = models.DecimalField(max_digits=19, decimal_places=4)
    commission = models.DecimalField(max_digits=19, decimal_places=4)
    paid_commission = models.DecimalField(max_digits=19, decimal_places=4)
    extra_amount_1 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    extra_amount_2 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    last_interest_accrual_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'Installments'
        unique_together = (('contract', 'number'),)

class Installmenthistory(models.Model):
    contract_id = models.IntegerField()
    event = models.ForeignKey('loans.Contractevents', models.DO_NOTHING)
    number = models.IntegerField()
    expected_date = models.DateTimeField()
    capital_repayment = models.DecimalField(max_digits=19, decimal_places=4)
    interest_repayment = models.DecimalField(max_digits=19, decimal_places=4)
    paid_interest = models.DecimalField(max_digits=19, decimal_places=4)
    paid_capital = models.DecimalField(max_digits=19, decimal_places=4)
    paid_fees = models.DecimalField(max_digits=19, decimal_places=4)
    fees_unpaid = models.DecimalField(max_digits=19, decimal_places=4)
    paid_date = models.DateTimeField(blank=True, null=True)
    delete_date = models.DateTimeField(blank=True, null=True)
    comment = models.CharField(max_length=50, blank=True, null=True)
    pending = models.BooleanField()
    start_date = models.DateTimeField()
    olb = models.DecimalField(max_digits=19, decimal_places=4)
    commission = models.DecimalField(max_digits=19, decimal_places=4)
    paid_commission = models.DecimalField(max_digits=19, decimal_places=4)
    extra_amount_1 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    extra_amount_2 = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    last_interest_accrual_date = models.DateField()

    class Meta:
        managed = False
        db_table = 'InstallmentHistory'


class Installmenttypes(models.Model):
    name = models.CharField(max_length=50)
    nb_of_days = models.IntegerField()
    nb_of_months = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'InstallmentTypes'




class Packages(models.Model):
    deleted = models.BooleanField()
    code = models.CharField(max_length=50)
    name = models.CharField(unique=True, max_length=100)
    client_type = models.CharField(max_length=1, blank=True, null=True)
    amount = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    interest_rate = models.DecimalField(max_digits=16, decimal_places=12, blank=True, null=True)
    interest_rate_min = models.DecimalField(max_digits=16, decimal_places=12, blank=True, null=True)
    interest_rate_max = models.DecimalField(max_digits=16, decimal_places=12, blank=True, null=True)
    installment_type = models.ForeignKey('loans.Installmenttypes', models.DO_NOTHING, db_column='installment_type')
    grace_period = models.IntegerField(blank=True, null=True)
    grace_period_min = models.IntegerField(blank=True, null=True)
    grace_period_max = models.IntegerField(blank=True, null=True)
    number_of_installments = models.IntegerField(blank=True, null=True)
    number_of_installments_min = models.IntegerField(blank=True, null=True)
    number_of_installments_max = models.IntegerField(blank=True, null=True)
    anticipated_total_repayment_penalties = models.FloatField(blank=True, null=True)
    anticipated_total_repayment_penalties_min = models.FloatField(blank=True, null=True)
    anticipated_total_repayment_penalties_max = models.FloatField(blank=True, null=True)
    exotic = models.ForeignKey('extras.Exotics', models.DO_NOTHING, blank=True, null=True)
    loan_type = models.SmallIntegerField()
    keep_expected_installment = models.BooleanField()
    charge_interest_within_grace_period = models.BooleanField()
    cycle = models.ForeignKey('extras.Cycles', models.DO_NOTHING, blank=True, null=True)
    non_repayment_penalties_based_on_overdue_interest = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_initial_amount = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_olb = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_overdue_principal = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_overdue_interest_min = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_initial_amount_min = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_olb_min = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_overdue_principal_min = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_overdue_interest_max = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_initial_amount_max = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_olb_max = models.FloatField(blank=True, null=True)
    non_repayment_penalties_based_on_overdue_principal_max = models.FloatField(blank=True, null=True)
    fundingline_id = models.IntegerField(db_column='fundingLine_id', blank=True, null=True)  # Field name made lowercase.
    currency = models.ForeignKey('extras.Currencies', models.DO_NOTHING)
    rounding_type = models.SmallIntegerField()
    grace_period_of_latefees = models.IntegerField()
    anticipated_partial_repayment_penalties = models.FloatField(blank=True, null=True)
    anticipated_partial_repayment_penalties_min = models.FloatField(blank=True, null=True)
    anticipated_partial_repayment_penalties_max = models.FloatField(blank=True, null=True)
    anticipated_partial_repayment_base = models.SmallIntegerField()
    anticipated_total_repayment_base = models.SmallIntegerField()
    number_of_drawings_loc = models.IntegerField(blank=True, null=True)
    amount_under_loc = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_under_loc_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_under_loc_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    maturity_loc = models.IntegerField(blank=True, null=True)
    maturity_loc_min = models.IntegerField(blank=True, null=True)
    maturity_loc_max = models.IntegerField(blank=True, null=True)
    activated_loc = models.BooleanField()
    allow_flexible_schedule = models.BooleanField()
    use_guarantor_collateral = models.BooleanField()
    set_separate_guarantor_collateral = models.BooleanField()
    percentage_total_guarantor_collateral = models.IntegerField()
    percentage_separate_guarantor = models.IntegerField()
    percentage_separate_collateral = models.IntegerField()
    use_compulsory_savings = models.BooleanField()
    compulsory_amount = models.IntegerField(blank=True, null=True)
    compulsory_amount_min = models.IntegerField(blank=True, null=True)
    compulsory_amount_max = models.IntegerField(blank=True, null=True)
    insurance_min = models.DecimalField(max_digits=18, decimal_places=2)
    insurance_max = models.DecimalField(max_digits=18, decimal_places=2)
    use_entry_fees_cycles = models.BooleanField()
    interest_scheme = models.IntegerField()
    script_name = models.CharField(max_length=255, blank=True, null=True)
    principal_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='principal_account', related_name="principal_account+", blank=True, null=True)
    interest_accrued_but_not_due_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='interest_accrued_but_not_due_account', related_name="interest_accrued_but_not_due_account+",  blank=True, null=True)
    interest_due_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='interest_due_account', related_name="interest_income_account+", blank=True, null=True)
    interest_due_but_not_received_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='interest_due_but_not_received_account', related_name="interest_due_but_not_received_account+", blank=True, null=True)
    interest_income_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='interest_income_account', related_name="interest_income_account+", blank=True, null=True)
    tax_on_interests_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='tax_on_interests_account', related_name="tax_on_interests_account+", blank=True, null=True)
    accrued_penalty_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='accrued_penalty_account', related_name="accrued_penalty_account+", blank=True, null=True)
    penalty_income_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='penalty_income_account', related_name="penalty_income_account+", blank=True, null=True)
    tax_on_penalty_account = models.ForeignKey('chartsoa.Account', models.DO_NOTHING, db_column='tax_on_penalty_account', related_name="tax_on_penalty_account+", blank=True, null=True)
    tax_value = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Packages'


class Packagesclienttypes(models.Model):
    client_type_id = models.IntegerField()
    package_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'PackagesClientTypes'



class Projects(models.Model):
    tiers = models.ForeignKey('members.Tiers', models.DO_NOTHING)
    status = models.SmallIntegerField()
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=255)
    aim = models.CharField(max_length=200)
    begin_date = models.DateTimeField()
    abilities = models.CharField(max_length=50, blank=True, null=True)
    experience = models.CharField(max_length=50, blank=True, null=True)
    market = models.CharField(max_length=50, blank=True, null=True)
    concurrence = models.CharField(max_length=50, blank=True, null=True)
    purpose = models.CharField(max_length=50, blank=True, null=True)
    corporate_name = models.CharField(max_length=50, blank=True, null=True)
    corporate_juridicstatus = models.CharField(db_column='corporate_juridicStatus', max_length=50, blank=True, null=True)  # Field name made lowercase.
    corporate_fiscalstatus = models.CharField(db_column='corporate_FiscalStatus', max_length=50, blank=True, null=True)  # Field name made lowercase.
    corporate_siret = models.CharField(max_length=50, blank=True, null=True)
    corporate_ca = models.DecimalField(db_column='corporate_CA', max_digits=19, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    corporate_nbofjobs = models.IntegerField(db_column='corporate_nbOfJobs', blank=True, null=True)  # Field name made lowercase.
    corporate_financialplantype = models.CharField(db_column='corporate_financialPlanType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    corporatefinancialplanamount = models.DecimalField(db_column='corporateFinancialPlanAmount', max_digits=19, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    corporate_financialplantotalamount = models.DecimalField(db_column='corporate_financialPlanTotalAmount', max_digits=19, decimal_places=4, blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(max_length=20, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    zipcode = models.CharField(db_column='zipCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    district_id = models.IntegerField(blank=True, null=True)
    home_phone = models.CharField(max_length=50, blank=True, null=True)
    personalphone = models.CharField(db_column='personalPhone', max_length=50, blank=True, null=True)  # Field name made lowercase.
    email = models.CharField(db_column='Email', max_length=50, blank=True, null=True)  # Field name made lowercase.
    hometype = models.CharField(max_length=50, blank=True, null=True)
    corporate_registre = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Projects'

class Projectpurposes(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'ProjectPurposes'







class Credit(models.Model):
    _id = models.OneToOneField('loans.Contracts', models.DO_NOTHING, db_column='_id', primary_key=True)
    package = models.ForeignKey('loans.Packages', models.DO_NOTHING)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    interest_rate = models.DecimalField(max_digits=16, decimal_places=12)
    installment_type = models.ForeignKey('loans.Installmenttypes', models.DO_NOTHING, db_column='installment_type')
    nb_of_installment = models.IntegerField()
    anticipated_total_repayment_penalties = models.FloatField()
    disbursed = models.BooleanField()
    loanofficer = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING)
    grace_period = models.IntegerField(blank=True, null=True)
    written_off = models.BooleanField()
    rescheduled = models.BooleanField()
    bad_loan = models.BooleanField()
    non_repayment_penalties_based_on_overdue_principal = models.FloatField()
    non_repayment_penalties_based_on_initial_amount = models.FloatField()
    non_repayment_penalties_based_on_olb = models.FloatField()
    non_repayment_penalties_based_on_overdue_interest = models.FloatField()
    fundingline = models.ForeignKey('chartsoa.Fundinglines', models.DO_NOTHING, db_column='fundingLine_id')  # Field name made lowercase.
    synchronize = models.BooleanField()
    interest = models.DecimalField(max_digits=19, decimal_places=4)
    grace_period_of_latefees = models.IntegerField()
    anticipated_partial_repayment_penalties = models.FloatField(blank=True, null=True)
    number_of_drawings_loc = models.IntegerField(blank=True, null=True)
    amount_under_loc = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    maturity_loc = models.IntegerField(blank=True, null=True)
    anticipated_partial_repayment_base = models.SmallIntegerField()
    anticipated_total_repayment_base = models.SmallIntegerField()
    schedule_changed = models.BooleanField()
    amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    ir_min = models.DecimalField(max_digits=16, decimal_places=12, blank=True, null=True)
    ir_max = models.DecimalField(max_digits=16, decimal_places=12, blank=True, null=True)
    nmb_of_inst_min = models.IntegerField(blank=True, null=True)
    nmb_of_inst_max = models.IntegerField(blank=True, null=True)
    loan_cycle = models.IntegerField(blank=True, null=True)
    insurance = models.DecimalField(max_digits=18, decimal_places=2)
    effective_interest_rate = models.DecimalField(max_digits=18, decimal_places=4)
    schedule_type = models.IntegerField()
    script_name = models.CharField(max_length=255, blank=True, null=True)
    initial_emi = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Credit'


class Creditentryfees(models.Model):
    credit = models.ForeignKey('loans.Credit', models.DO_NOTHING)
    entry_fee_id = models.IntegerField()
    fee_value = models.DecimalField(max_digits=18, decimal_places=4)

    class Meta:
        managed = False
        db_table = 'CreditEntryFees'


class Creditinsuranceevents(models.Model):
    _id = models.IntegerField()
    commission = models.DecimalField(max_digits=18, decimal_places=2)
    principal = models.DecimalField(max_digits=18, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'CreditInsuranceEvents'

