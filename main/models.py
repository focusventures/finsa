from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django_tenants.models import TenantMixin, DomainMixin

from django.core.validators import RegexValidator

counties = (
    ("Mombasa", "Mombasa"),
	("Kwale","Kwale"),
	("Kilifi","Kilifi"),	
	("Tana River",	"Tana River"),
	("Lamu", "Lamu"),	
	("Taita–Taveta","Taita–Taveta"),
	("Garissa",	"Garissa"),
	("Wajir","Wajir"),	
	("Mandera", "Mandera"),
	("Marsabit","Marsabit"),	
	("Isiolo","Isiolo"),	
	("Meru","Meru"),	
	("Tharaka-Nithi",	"Tharaka-Nithi"),
	("Embu","Embu"),	
	("Kitui","Kitui"),	
	("Machakos","Machakos"),	
	("Makueni","Makueni"),
	("Nyandarua","Nyandarua"),	
	("Nyeri","Nyeri"),
	("Kirinyaga","Kirinyaga"),	
	("Murang'a", "Murang'a"),
	("Kiambu","Kiambu"),	
	("Turkana", "Turkana"),
	("West Pokot",	"West Pokot"),
	("Samburu", "Samburu"),
	("Trans-Nzoia","Trans-Nzoia"),
	("Uasin Gishu",	 "Uasin Gishu"),	
	("Elgeyo-Marakwet",	"Elgeyo-Marakwet"),
	("Nandi", "Nandi"),	
	("Baringo", "Baringo"),
	("Laikipia", "Laikipia"),
	("Nakuru", "Nakuru"),	
    ("Narok", "Narok"),
	("Kajiado", "Kajiado"),
	("Kericho", "Kericho"),
	("Bomet", "Bomet"),	 
	("Kakamega","Kakamega"),
	("Vihiga", "Vihiga"),	
	("Bungoma", "Bungoma"),
	("Busia", "Busia"),
	("Siaya", "Siaya"),	 
	("Kisumu",	 "Kisumu"),
	("Homa Bay", "Homa Bay"),	
	("Migori",	 "Migori"),
	("Kisii", "Kisii"),
	("Nyamira",	"Nyamira"),
    ("Nairobi", "Nairobi"),
)

states = (
    ("kenya", "Kenya"),
)

class Tenant(TenantMixin):
    phone_regex = RegexValidator(regex=r'^\d{10}$',
                                 message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.")
    name = models.CharField(_('Organisation Name'), max_length=100, help_text="Provide name of the organisation i.e registered name", unique=True)
    paid_until =  models.DateField(null=True, blank=True)
    on_trial = models.BooleanField(null=True, blank=True)
    email = models.EmailField(_("Business Email"), null=True, max_length=254)
    phone_number = models.CharField(validators=[phone_regex], blank=True, max_length=15, unique=True)
    tell_phone = models.CharField(_("Tell Phone"), null=True, max_length=50)
    trial_start_date = models.DateField(_("Trial Start Date"), null=True, auto_now=False, auto_now_add=False)
    trial_end_date = models.DateField(_("Trial End Date"), null=True, auto_now=False, auto_now_add=False)
    created_on = models.DateField(auto_now_add=True)
    units_allowed = models.IntegerField(_("Number of units"), default=20)
    expires_on = models.DateField(_("Expiration Date"), auto_now=False, auto_now_add=False, null=True, blank=True)
    current_package = models.ForeignKey("main.Packages", null=True, blank=True,verbose_name=_("Choose Package"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    roomNumber = models.CharField(_("Room Number"), max_length=50, null=True, blank=True)
    house = models.CharField(_("House"), max_length=50, null=True, blank=True)
    streetName = models.CharField(_("Street Name"), max_length=50, null=True, blank=True)
    town = models.CharField(_("Town"), max_length=50, null=True, blank=True)
    city = models.CharField(_("City"), max_length=50, null=True, blank=True)
    county = models.CharField(_("County"), choices=counties, max_length=50, null=True, blank=True)
    state = models.CharField(_("State"), choices=states, max_length=50, null=True, blank=True)
    terms = models.BooleanField(_("I Agree With Terms Of Service Linked Below"), default=True, null=True, blank=True)



    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True

class Domain(DomainMixin):
    pass




class Packages(models.Model):
    package_name = models.CharField(_("One Part"), max_length=50)
    number_of_units = models.IntegerField(_("Number of Units One can list"))
    description = models.TextField(_("Package Description"))
    offer = models.IntegerField(_("Offer On Package"))
    offer_description = models.CharField(_("200"), max_length=50, null=True, blank=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)


    def __str__(self):
        return "{} - managing {} units".format(self.package_name, self.number_of_units)



class TenantPackages(models.Model):
    tenant = models.ForeignKey("main.Tenant", verbose_name=_("Tenant"), on_delete=models.CASCADE)
    package = models.ForeignKey("main.Packages", verbose_name=_("Packages"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)




class Queries(models.Model):

    phone_regex = RegexValidator(regex=r'^\d{10}$',
                                 message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.")

    phone_number = models.CharField(validators=[phone_regex], blank=True, max_length=15, unique=True)

    subject = models.CharField(_("Message"), max_length=200)
    message = models.TextField(_("Enter Your Message Here"))
    email = models.EmailField(_("Your Email Address"), max_length=254)
    full_name = models.CharField(_("Enter Your Name Here"), max_length=50)
    county = models.CharField(_("County"), choices=counties, null=True, blank=True,  max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)



    def __str__(self):
        return "{} - managing {} units".format(self.package_name, self.number_of_units)