# Generated by Django 3.0.4 on 2020-03-15 08:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tenant',
            name='trial_end_date',
            field=models.DateField(null=True, verbose_name='Trial End Date'),
        ),
        migrations.AddField(
            model_name='tenant',
            name='trial_start_date',
            field=models.DateField(null=True, verbose_name='Trial Start Date'),
        ),
    ]
