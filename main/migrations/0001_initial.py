# Generated by Django 3.0.4 on 2020-04-19 13:32

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django_tenants.postgresql_backend.base


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Packages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('package_name', models.CharField(max_length=50, verbose_name='One Part')),
                ('number_of_units', models.IntegerField(verbose_name='Number of Units One can list')),
                ('description', models.TextField(verbose_name='Package Description')),
                ('offer', models.IntegerField(verbose_name='Offer On Package')),
                ('offer_description', models.CharField(blank=True, max_length=50, null=True, verbose_name='200')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, null=True, verbose_name='Date Modified')),
            ],
        ),
        migrations.CreateModel(
            name='Tenant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('schema_name', models.CharField(db_index=True, max_length=63, unique=True, validators=[django_tenants.postgresql_backend.base._check_schema_name])),
                ('name', models.CharField(help_text='Provide name of the organisation i.e registered name', max_length=100, unique=True, verbose_name='Organisation Name')),
                ('paid_until', models.DateField(blank=True, null=True)),
                ('on_trial', models.BooleanField(blank=True, null=True)),
                ('email', models.EmailField(max_length=254, null=True, verbose_name='Business Email')),
                ('phone_number', models.CharField(blank=True, max_length=15, unique=True, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.", regex='^\\d{10}$')])),
                ('tell_phone', models.CharField(max_length=50, null=True, verbose_name='Tell Phone')),
                ('trial_start_date', models.DateField(null=True, verbose_name='Trial Start Date')),
                ('trial_end_date', models.DateField(null=True, verbose_name='Trial End Date')),
                ('created_on', models.DateField(auto_now_add=True)),
                ('units_allowed', models.IntegerField(default=20, verbose_name='Number of units')),
                ('expires_on', models.DateField(blank=True, null=True, verbose_name='Expiration Date')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, null=True, verbose_name='Date Modified')),
                ('roomNumber', models.CharField(blank=True, max_length=50, null=True, verbose_name='Room Number')),
                ('house', models.CharField(blank=True, max_length=50, null=True, verbose_name='House')),
                ('streetName', models.CharField(blank=True, max_length=50, null=True, verbose_name='Street Name')),
                ('town', models.CharField(blank=True, max_length=50, null=True, verbose_name='Town')),
                ('city', models.CharField(blank=True, max_length=50, null=True, verbose_name='City')),
                ('county', models.CharField(blank=True, choices=[('Mombasa', 'Mombasa'), ('Kwale', 'Kwale'), ('Kilifi', 'Kilifi'), ('Tana River', 'Tana River'), ('Lamu', 'Lamu'), ('Taita–Taveta', 'Taita–Taveta'), ('Garissa', 'Garissa'), ('Wajir', 'Wajir'), ('Mandera', 'Mandera'), ('Marsabit', 'Marsabit'), ('Isiolo', 'Isiolo'), ('Meru', 'Meru'), ('Tharaka-Nithi', 'Tharaka-Nithi'), ('Embu', 'Embu'), ('Kitui', 'Kitui'), ('Machakos', 'Machakos'), ('Makueni', 'Makueni'), ('Nyandarua', 'Nyandarua'), ('Nyeri', 'Nyeri'), ('Kirinyaga', 'Kirinyaga'), ("Murang'a", "Murang'a"), ('Kiambu', 'Kiambu'), ('Turkana', 'Turkana'), ('West Pokot', 'West Pokot'), ('Samburu', 'Samburu'), ('Trans-Nzoia', 'Trans-Nzoia'), ('Uasin Gishu', 'Uasin Gishu'), ('Elgeyo-Marakwet', 'Elgeyo-Marakwet'), ('Nandi', 'Nandi'), ('Baringo', 'Baringo'), ('Laikipia', 'Laikipia'), ('Nakuru', 'Nakuru'), ('Narok', 'Narok'), ('Kajiado', 'Kajiado'), ('Kericho', 'Kericho'), ('Bomet', 'Bomet'), ('Kakamega', 'Kakamega'), ('Vihiga', 'Vihiga'), ('Bungoma', 'Bungoma'), ('Busia', 'Busia'), ('Siaya', 'Siaya'), ('Kisumu', 'Kisumu'), ('Homa Bay', 'Homa Bay'), ('Migori', 'Migori'), ('Kisii', 'Kisii'), ('Nyamira', 'Nyamira'), ('Nairobi', 'Nairobi')], max_length=50, null=True, verbose_name='County')),
                ('state', models.CharField(blank=True, choices=[('kenya', 'Kenya')], max_length=50, null=True, verbose_name='State')),
                ('current_package', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='main.Packages', verbose_name='Choose Package')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TenantPackages',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, null=True, verbose_name='Date Modified')),
                ('package', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Packages', verbose_name='Packages')),
                ('tenant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Tenant', verbose_name='Tenant')),
            ],
        ),
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('domain', models.CharField(db_index=True, max_length=253, unique=True)),
                ('is_primary', models.BooleanField(db_index=True, default=True)),
                ('tenant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='domains', to='main.Tenant')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
