from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="index"),
    path('tenancy', views.tenancy, name="tenancy"),
    path('about', views.about, name="about_us"),
    path('services', views.services, name="our_services"),
    path('products', views.products, name="our_products"),
    path('training', views.training, name="training"),
    path('invest', views.invest, name="invest"),
    path('contact', views.contact, name="contact"),
    path('pricing', views.packages, name="pricing"),
    path('get_started', views.get_started, name="get_started"),
    path('terms_of_service/', views.terms, name="terms"),
    path('privacy_policy/', views.privacy, name="privacy"),
    path('queires/', views.aoe_query, name="queries"),
 
]