# from django.contrib import admin

# from .models import Tenant, Packages, TenantPackages, Domain


# admin.site.register(Packages)

# admin.site.register(TenantPackages)


# admin.site.register(Domain)

# from django.db import models
# from django.contrib import admin
# from django_tenants.admin import TenantAdminMixin


# @admin.register(Tenant)
# class TenantAdmin(TenantAdminMixin, admin.ModelAdmin):
#         list_display = ('name', 'paid_until')

from django.contrib.admin import AdminSite
from django.contrib import admin


class EventAdminSite(AdminSite):
    site_header = "FinGate Tenant Management Portal"
    site_title = "FinGate Tenant Management Portal"
    index_title = "Welcome to FinGate Tenant Management Portal"


from .models import Tenant, Domain



class TenantModelAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'paid_until', 'on_trial', 'email' , 'tell_phone', 'phone_number', 'trial_start_date',  'trial_end_date', 'created_on', 'units_allowed', 'expires_on','current_package','date_created','date_modified', 'roomNumber', 'house', 'streetName', 'town', 'city', 'county', 'state'
    ]
    order =  [
        'name', 'paid_until', 'on_trial', 'email' , 'tell_phone', 'phone_number', 'trial_start_date',  'trial_end_date', 'created_on', 'units_allowed', 'expires_on','current_package','date_created','date_modified', 'roomNumber', 'house', 'streetName', 'town', 'city', 'county', 'state'
    ]
    icon_name = 'people'
    search_fields =  [
        'name', 'paid_until', 'on_trial', 'email' , 'tell_phone', 'phone_number', 'trial_start_date',  'trial_end_date', 'created_on', 'units_allowed', 'expires_on','current_package','date_created','date_modified', 'roomNumber', 'house', 'streetName', 'town', 'city', 'county', 'state'
    ]
    list_filter =  [
        'name', 'paid_until', 'on_trial', 'email' , 'tell_phone', 'phone_number', 'trial_start_date',  'trial_end_date', 'created_on', 'units_allowed', 'expires_on','current_package','date_created','date_modified', 'roomNumber', 'house', 'streetName', 'town', 'city', 'county', 'state'
    ]

class DomainAdminModal(admin.ModelAdmin):
    list_display = ['Domain', 'Tenant']
event_admin_site = EventAdminSite(name='main_admin')
event_admin_site.register(Tenant, TenantModelAdmin)
event_admin_site.register(Domain)
