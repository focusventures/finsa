from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from .models import Packages, Domain, Tenant
from .forms import TenantForm
from authentication.models import CustomUser
from django_tenants.utils import tenant_context

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404




def tenancy(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    # logged_in = CustomUser.objects.all()
    schema = request.tenant.schema_name
    if schema != 'public':
        full_address = 'http://' + schema + '.fingate.co.ke'
        return HttpResponseRedirect(full_address)


    if request.method == 'POST':
        form = TenantForm( request.POST)
        if form.is_valid():

            tenant = form.save(commit = False)
            tenant.schema_name = request.POST.get('schema_name')
            tenant.creator = request.user
            tenant.user = request.user
            tenant.save()

            domain = Domain()
            domain.domain = tenant.schema_name + '.fingate.co.ke'
            domain.tenant = tenant
            domain.is_primary = True
            domain.save()


            with tenant_context(tenant):
                from dashboard.models import SnapShot
                from chartsoa.models import TypeOfAccount, PaymentMethod, Account
                from authentication.models import CustomUser
                from locations.models import Location
                from ostructure.models import Branch, Department
                from members.models import EconomicActivity


                user = CustomUser()
                user.phone_number = request.POST.get('phone_number')
                user.is_superuser = True
                user.is_staff = True
                user.has_dash_access = True
                user.save()
                user.set_password(request.POST.get('password'))
                user.save()
                snap = SnapShot()
                snap.creator = user
                snap.save()

                snap.users +=1
                snap.active_users += 1

                Branch.objects.bulk_create([
                    Branch(name="HQ",manager=user,town=tenant.town, house=tenant.house, 
                    room_number=tenant.roomNumber, county=tenant.county, phone_number=tenant.phone_number, 
                    city=tenant.city, creator=user),
                    Branch(name=tenant.town,manager=user,town=tenant.town, house=tenant.house, 
                    room_number=tenant.roomNumber, county=tenant.county, phone_number=tenant.phone_number, 
                    city=tenant.city, creator=user),
                ])

                TypeOfAccount.objects.bulk_create([
                    TypeOfAccount(title='Assets', description="Current Assets", account_number_range=100, creator=user),
                    TypeOfAccount(title='Liabilities', description="Liabilities", account_number_range=200, creator=user),
                    TypeOfAccount(title='Income', description="Income", account_number_range=300, creator=user),
                    TypeOfAccount(title='Expense', description="Expenses", account_number_range=400, creator=user),
                    TypeOfAccount(title='Equity', description="Equity", account_number_range=500, creator=user),
                ])

                Account.objects.bulk_create([
                    Account(name='Current Assets',description="current Assets", balance=0, account_number=100000100 , type=TypeOfAccount.objects.get(account_number_range=100),  creator=user),
                    Account(name='Fixed Assets',description="current Assets", balance=0, account_number=100000101 , type=TypeOfAccount.objects.get(account_number_range=100), creator=user),
                    Account(name='Current Liabilities',description="current Liabilities", balance=0, account_number=100000200 , type=TypeOfAccount.objects.get(account_number_range=200), creator=user),
                    Account(name='Income',description="Income", balance=0, account_number=100000300 , type=TypeOfAccount.objects.get(account_number_range=300), creator=user),
                    Account(name='Expense',description="Expense", balance=0, account_number=100000400 , type=TypeOfAccount.objects.get(account_number_range=400), creator=user),
                    Account(name='Equity',description="Equity", balance=0, account_number=100000500 , type=TypeOfAccount.objects.get(account_number_range=500), creator=user),
                ])

                Account.objects.bulk_create([
                    Account(name='Bank',description="Bank", balance=0, account_number=1000001001 , type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000100), creator=user),
                    Account(name='Petty Cash',description="Petty Cash", balance=0, account_number=1000001002 , type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000100), creator=user),
                    Account(name='Checkin Account',description="Checkin Account", balance=0, account_number=1000001003 , type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000100), creator=user),
                    Account(name='Short Term Investments',description="Short Term Investments", balance=0, account_number=1000001004 , type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000100),creator=user),
                    Account(name='Loan Receivables',description="Loan Receivables", balance=0, account_number=1000001005 , type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000100),creator=user),
                    Account(name='Accrued Interest Receveivable',description="Accrued Interest Receveivables", balance=0, account_number=1000001006 , type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000100), creator=user),
                    Account(name='Accumulated Loss',description="Accumulated Loss", balance=0, account_number=1000001007 , type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000100),creator=user),
                    Account(name='Accounts Recevable',description="Accounts Recevable", balance=0, account_number=1000001008 , type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000100), creator=user),
                ])
                Account.objects.bulk_create([
                    Account(name='Accounts Payable',description="Accounts Payable", balance=0, account_number=1000002001 , type=TypeOfAccount.objects.get(account_number_range=200), parent=Account.objects.get(account_number=100000200), creator=user),
                    Account(name='Accrued Expenses',description="Accrued Expenses", balance=0, account_number=1000002002 , type=TypeOfAccount.objects.get(account_number_range=200),parent=Account.objects.get(account_number=100000200), creator=user),
                    Account(name='Payroll Liabilities',description="Payroll Liabilities", balance=0, account_number=1000002003 , type=TypeOfAccount.objects.get(account_number_range=200),parent=Account.objects.get(account_number=100000200), creator=user),
                    Account(name='Accrued Interest Expense',description="Accrued Interest Expense", balance=0, account_number=1000002004 , type=TypeOfAccount.objects.get(account_number_range=200), parent=Account.objects.get(account_number=100000200),creator=user),
                    Account(name='Accrued Payroll Expense',description="Accrued Payroll Expense", balance=0, account_number=1000002005 , type=TypeOfAccount.objects.get(account_number_range=200), parent=Account.objects.get(account_number=100000200),creator=user),
                    Account(name='Accrued Payroll Taxes',description="Accrued Payroll Taxes", balance=0, account_number=1000002006 , type=TypeOfAccount.objects.get(account_number_range=200),parent=Account.objects.get(account_number=100000200), creator=user),
                    Account(name='Note Payable',description="Note Payable", balance=0, account_number=1000002007 , type=TypeOfAccount.objects.get(account_number_range=200), parent=Account.objects.get(account_number=100000200),creator=user),
                
                ])

                Account.objects.bulk_create([
                    Account(name='Common Shares',description="Common Shares", balance=0, account_number=1000005001 , type=TypeOfAccount.objects.get(account_number_range=500), parent=Account.objects.get(account_number=100000500), creator=user),
                    Account(name='Paid In Capital',description="Paid In Capital", balance=0, account_number=1000005002 , type=TypeOfAccount.objects.get(account_number_range=500),parent=Account.objects.get(account_number=100000500), creator=user),
                ])
                PaymentMethod.objects.bulk_create([
                    # PaymentMethod(name='Cash', account=Account.objects.get(id=1), creator=request.user),
                ])

                branches = Branch.objects.all()
                for branch in branches:
                    Account.objects.bulk_create([
                        Account(name='Branch Expenses',description="Petty Cash", balance=0, account_number=int( str(branch.id) + str(102)) , type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000400), creator=user),
                        Account(name='Checkin Account',description="Checkin Account", balance=0, account_number=int( str(branch.id) + str(103)) , type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000400), creator=user),
                        Account(name='Short Term Investments',description="Short Term Investments", balance=0, account_number=int( str(branch.id) + str(104)), type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000400),creator=user),
                        Account(name='Loan Receivables',description="Loan Receivables", balance=0, account_number=int( str(branch.id) + str(1005)), type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000300),creator=user),
                        Account(name='Accrued Interest Receveivable',description="Accrued Interest Receveivables", balance=0, account_number=int( str(branch.id) + str(106)), type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000300), creator=user),
                        Account(name='Accumulated Loss',description="Accumulated Loss", balance=0, account_number=int( str(branch.id) + str(107)), type=TypeOfAccount.objects.get(account_number_range=100), parent=Account.objects.get(account_number=100000200),creator=user),
                        Account(name='Accounts Recevable',description="Accounts Recevable", balance=0, account_number=int( str(branch.id) + str(108)), type=TypeOfAccount.objects.get(account_number_range=100),parent=Account.objects.get(account_number=100000300), creator=user),
                    
                    ])

                branch = Branch.objects.get(id=1)
                Department.objects.create(name="Finance",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Marketing",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Sales",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Loan Management",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Recovery",manager=user,branch=branch, creator=user)
                EconomicActivity.objects.create(name="Agriculture", creator=user)
                EconomicActivity.objects.create(name="Entrepreneurship", creator=user)
                EconomicActivity.objects.create(name="Fishing", creator=user)
                EconomicActivity.objects.create(name="Transport - Bodaboda", creator=user)
                EconomicActivity.objects.create(name="Transport - Matatu", creator=user)
                EconomicActivity.objects.create(name="Transport - Masafa Marefu", creator=user)

                branch = Branch.objects.get(id=2)
                Department.objects.create(name="Finance",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Marketing",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Sales",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Loan Management",manager=user,branch=branch, creator=user)
                Department.objects.create(name="Recovery",manager=user,branch=branch, creator=user)
                
                snap.departments += 16
                snap.locations += 1
                snap.branches += 1
                snap.save()
                full_address = 'http://' + tenant.schema_name + '.fingate.co.ke'
                return HttpResponseRedirect(full_address)
    else:
        form = TenantForm()
    context = {'form': form}
    return render(request, 'main/tenant.html', context)



def index(request):
    schema = request.tenant.schema_name
    if schema != 'public':
        if request.user.is_authenticated:
            full_address = 'http://' + schema + '.fingate.co.ke/dashboard/daily'
            return HttpResponseRedirect(full_address)

        tenant = Tenant.objects.get(schema_name__iexact=schema)
        with tenant_context(tenant):
            from settings.models import Index
            index = ""
            try:
                index = Index.objects.get(id=1)
            except Index.DoesNotExist:
                index = "No Results Fount. Go to settings and Add Index Log Details"
            context = {
                "name": request.tenant.name,
                "index": index,
                "login": 'http://' + schema + '.fingate.co.ke/auth/login',
                "admin": 'http://' + schema + '.fingate.co.ke/admin',
            }
            return render(request, 'main/tenant_index.html', context)

    return render(request, 'main/index.html')

def about(request):
    schema = request.tenant.schema_name
    if schema != 'public':
        tenant = Tenant.objects.get(schema_name__iexact=schema)
        with tenant_context(tenant):
            from settings.models import About
            about = ""
            try:
                about = About.objects.get(id=1)
            except About.DoesNotExist:
                about = "No Results Fount. Go to settings and Add About Log Details"
            context = {
                "name": request.tenant.name,
                "about": about,
                "login": 'http://' + schema + '.fingate.co.ke/auth/login',
                "admin": 'http://' + schema + '.fingate.co.ke/admin',
                }
            return render(request, 'main/tenant_about.html', context)

    return render(request, 'main/about.html')

def services(request):
    schema = request.tenant.schema_name
    if schema != 'public':
        tenant = Tenant.objects.get(schema_name__iexact=schema)
        with tenant_context(tenant):
            from settings.models import Services
            services = ""
            try:
                services = Services.objects.filter(is_active=True)
            except Services.DoesNotExist:
                services = "No Results Fount. Go to settings and Add Index Log Details"
            context = {
                "name": request.tenant.name,
                "services": services ,
                "login": 'http://' + schema + '.fingate.co.ke/auth/login',
                "admin": 'http://' + schema + '.fingate.co.ke/admin',
            }
            return render(request, 'main/tenant_services.html', context)
 
    return render(request, 'main/services.html')

def training(request):
   
    return render(request, 'main/training.html')


def packages(request):
    packages = Packages.objects.all()

    context = {
        "packages": packages
    }

    print(context)
    return render(request, 'main/pricing.html', context)


from .forms import QueryForm
from .models import Queries

from django.contrib import messages

def contact(request):
    schema = request.tenant.schema_name
    if schema != 'public':
        tenant = Tenant.objects.get(schema_name__iexact=schema)
        with tenant_context(tenant):
            from settings.models import Contacts
            contact = ""
            try:
                contact = Contacts.objects.all()
            except Contacts.DoesNotExist:
                contact = "No Results Fount. Go to settings and Add Contact Log Details"
            context = {
                "name": request.tenant.name,
                "contacts": contact,
                "login": 'http://' + schema + '.fingate.co.ke/auth/login',
                "admin": 'http://' + schema + '.fingate.co.ke/admin',
            }
            return render(request, 'main/tenant_contact.html', context)

    form = QueryForm()
    if request.method == 'POST':
        form = QueryForm(request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.save()
            messages.success(request, 'Message received. Thankyou for contacting us,  We will Get back to you as soon as possible')
            return redirect(reverse('queries'))

    context = {'form': form}
  
    return render(request, 'main/contact.html', context)


def products(request):
    return render(request, 'main/products.html')


def invest(request):

    return render(request, 'main/invest.html')



def get_started(request):

    return render(request, 'main/get_started.html')



def terms(request):
    
    return render(request, 'main/terms.html')




def privacy(request):
    
    return render(request, 'main/privacy.html')




def aoe_query(request):
    form = QueryForm()
    if request.method == 'POST':
        form = QueryForm(request.POST, request.FILES)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.save()

            messages.success(request, 'Message received. Thankyou for contacting us,  We will Get back to you as soon as possible')



            return redirect(reverse('queries'))

    context = {'form': form}
    return render(request, 'main/contact.html', context)