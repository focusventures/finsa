from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.layout import (Layout, Fieldset, Field, Row, Column, Button,
                                 ButtonHolder, Submit, Div)
from .models import Tenant, Queries
from django.core.validators import RegexValidator

class TenantForm(ModelForm):

    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="password",
        required=True)

    schema_name = forms.CharField(
        label="Database Name", help_text="Should be one name with no spaces",
        required=True)

    class Meta:
        model = Tenant
        fields = ('name', 'schema_name', 'email', 'current_package', 'county','phone_number', 'tell_phone',  'state','city', 'town','terms', 'streetName', 'house', 'roomNumber', )
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(TenantForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields["county"].required = True
        self.fields["town"].required = True
        self.fields["state"].required = True
        self.fields["city"].required = True
        self.fields["streetName"].required = True
        self.fields["house"].required = True
        self.fields["roomNumber"].required = True
        self.helper.layout = Layout(
            Row(
                Column('schema_name', css_class='form-group col-md-6 mb-0'),
                Column('name', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
             Row(
              Column('tell_phone', css_class='form-group col-md-6 mb-0'),
                Column('email', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

            Row(
                 Column('state', css_class='form-group col-md-4 mb-0'),
                Column('county', css_class='form-group col-md-4 mb-0'),
              
               Column('city', css_class='form-group col-md-4 mb-0'),
              
                css_class='form-row'
            ),

            Row(
                 Column('town', css_class='form-group col-md-3 mb-0'),
                Column('streetName', css_class='form-group col-md-3 mb-0'),
                 Column('house', css_class='form-group col-md-3 mb-0'),
                Column('roomNumber', css_class='form-group col-md-3 mb-0'),

              
                css_class='form-row'
            ),

            Row(
              
              
                css_class='form-row'
            ),
            # Row(
            #     Column('current_package', css_class='form-group col-md-7 mb-0'),
        
              
            #     css_class='form-row d-flex align-items-center'
            # ), 

            Div(
                Row(
                        css_class='form-row hr'
                ),
                Row(
                Column('phone_number', css_class='form-group col-md-6 mb-0', help_text="These are the credentials you will use to login"),

                Column('password', css_class='form-group col-md-4 mb-0'),
              
              
                css_class='form-row'
            ),
            ),

              Div(
                Row(
                        css_class='form-row hr'
                ),
                Row(
                Column('terms', css_class='form-group col-md-6 mb-0'),

              
              
                css_class='form-row'
            ),
            ),
            Row(
                Submit('Create', 'Create', css_class="btn btn-success m-2"),
                
                css_class='form-row text-center'
            ),

            
         
            
        )
     


class QueryForm(ModelForm):
    
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="password",
        required=True)

    schema_name = forms.CharField(
        label="Database Name", help_text="Should be one name with no spaces",
        required=True)

    class Meta:
        model = Queries
        fields = ('full_name', 'subject', 'message', 'email', 'phone_number' )
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(QueryForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields["full_name"].required = True
        self.fields["subject"].required = True
        self.fields["message"].required = True
        self.fields["email"].required = True
        self.fields["phone_number"].required = True
        # self.fields["county"].required = True
        self.helper.layout = Layout(
            Row(
                Column('phone_number', css_class='form-group col-md-6 mb-0'),
                Column('email', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
             Row(
              Column('subject', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),

            Row(
                 Column('message', css_class='form-group col-md-12 mb-0'),
       
              
                css_class='form-row'
            ),

         
        
       

            Row(
                Submit('Create', 'Send Query', css_class="btn btn-success m-2"),
                
                css_class='form-row text-center'
            ),

            
         
            
        )