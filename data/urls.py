from django.urls import path

from . import views



urlpatterns = [
     path('import', views.data_import, name="data_imp"),
     path('import/csv', views.data_import_csv, name="data_import"),
     path('import/groups', views.import_group, name="group_import"),
     path('import/groups/csv', views.data_import_groups, name="data_import_groups"),
     path('export', views.data_export, name="data_exp" ),

]
