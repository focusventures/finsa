from django import forms
from django.forms import ModelForm, Form
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button

from .models import DataUpload
datatype = (
    ('Members', "Members"),
    ('Share Capital', 'Share Capital'),
    
)
class DataImportForm(Form):
    data_type = forms.ChoiceField(choices=datatype, required=True)
    csv_file = forms.FileField(max_length=500, required=True)
    



    # class Meta:
    #     model = DataUpload
    #     # fields = ['data_type', 'csv_file']
    #     exclude = ( )
    

    def __init__(self, *args, **kwargs):
        super(DataImportForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('csv_file', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),

            Row(
                Column('data_type', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
       
          
            Row(
                Submit('submit', 'Upload', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )