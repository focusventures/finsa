from django.shortcuts import render, reverse, redirect
from authentication.models import CustomUser

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from members.models import Member, MemberGroup
from shares.models import ShareCapital

from savings.models import SavingAccount, CurrentAccount, Savingcontracts, Savingproducts
from financial.models import MemberCustomAccount, GroupCustomAccount

from ostructure.models import Branch
from parameters.models import Events

from loans.models import LoanContract, LoanProduct, LoanPurpose

from data.forms import DataImportForm
import io

@login_required()
def data_import(request):
    context = {
        'form': DataImportForm()
    }
    return render(request, 'data/import.html', context)

import csv
import datetime
from data.forms import DataImportForm
from data.models import DataUpload

@login_required()
def data_import_csv(request):
    date = datetime.datetime.now()


    if request.method == 'POST':
        form  = DataImportForm(request.POST)

        csv_file = request.FILES['csv_file']

        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'THIS IS NOT A CSV FILE')
            return redirect(reverse('data_imp'))

        data_set = csv_file.read().decode('UTF-8')

        io_string = io.StringIO(data_set)
        next(io_string)
        for row in csv.reader(io_string, delimiter=',', quotechar="|"):
            print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
            print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
            print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
            print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
            print(row)
            
            member = Member()
            member.account_number = row[0]
            member.sex = row[1]
            member.first_name = row[2]
            member.middle_name = row[3]
            member.last_name = row[4]
            member.phone_number = row[11]
            member.id_number = row[12]
            member.current_occupation = ''
            member.group = MemberGroup.objects.get(id=row[13])

            # member.next_of_fullname = row[20]
            # member.next_of_id_number = row[22]
            # member.next_of_relation = row[21]
            # member.next_of_address = row[23]
            member.creator = request.user
            member.date_created = date
            member.save()

            share = ShareCapital()
            share.member = member
            share.creator = request.user
            share.date_created = date
            share.account = '030-' + member.account_number
            share.share_bal = row[5]
            share.save()

            contract = Savingcontracts()
            contract.product = Savingproducts.objects.get(id=1)
            contract.creator = request.user
            contract.entry_fees = 0.00
            contract.initial_amount = row[6]
            
            contract.start_date = date
            contract.date_created = date
            contract.member = member
            contract.product = Savingproducts.objects.get(id=1)
            contract.savings_officer = request.user
            contract.creation_date = date
            contract.save()

            savings = SavingAccount()
            savings.acc_balance = row[6]
            savings.acc_number = '020-' + member.account_number
            savings.creator = request.user
            savings.date_created = date
            savings.saving_contract = contract
            savings.member = member
            savings.save()

            member.has_saving_acc = True







            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="shares_deposits")
            bbf_account.acc_balance = row[5]
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()

            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="bbf")
            bbf_account.acc_balance = row[7]
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()


            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="loan_repayment")
            bbf_account.acc_balance = 0.00
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()


            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="interest_on_loans")
            bbf_account.acc_balance = 0.00
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()

            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="entrance_fee")
            bbf_account.acc_balance = row[9]
            bbf_account.acc_number =str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()

            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="savings")
            bbf_account.acc_balance = row[6]
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()


            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="loan_form")
            bbf_account.acc_balance = 0.00
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()


            bbf_account = MemberCustomAccount()
            event = Events.objects.get(code="sundry_debtors")
            bbf_account.acc_balance = row[9]
            bbf_account.acc_number = str(event.id) + member.account_number
            bbf_account.creator = request.user
            bbf_account.date_created = date
            bbf_account.member = member
            bbf_account.acc_type = event
            bbf_account.save()







            current_account = CurrentAccount()
            current_account.acc_balance = 0.00
            current_account.acc_number = '00-' + member.account_number
            current_account.creator = request.user
            current_account.date_created = date
            current_account.member = member
            current_account.save()

            member.birth_date = date
            member.county = "Tharaka-Nithi"
            member.branch = Branch.objects.get(id=1)
            member.employer = ''

            member.save()

            from decimal import Decimal

            if int(row[8]) > 0:
                loan = LoanContract()
                loan.member = member
                loan.loan_product = LoanProduct.objects.get(id=1)
                loan.requested_amount = row[8]
                loan.approved_amount = row[8]
                loan.interest = row[9]
                loan.balance = Decimal(row[8]) + Decimal(row[9])
                loan.creator = request.user
                loan.date_approved = datetime.datetime.now()
                loan.date_created = datetime.datetime.now()
                loan.amount_to_be_paid_words = ''
                loan.loan_purpose = LoanPurpose.objects.get(id=1)
                loan.approving_officer = CustomUser.objects.get(id=1)
                loan.confirming_officer = CustomUser.objects.get(id=1)
                loan.date_approved = datetime.datetime.now()
                loan.date_disbursed = datetime.datetime.now()
                loan.is_approved = True
                loan.is_confirmed = True
                loan.is_disbursed = True
                loan.requested_amount_words = ''
                loan.save()

      


    return redirect(reverse('members'))

@login_required()
def data_export(request):

    return render(request, 'data/export.html')

@login_required()
def import_group(request):
    context = {
        'form': DataImportForm()
    }
    return render(request, 'data/groups.html', context)

@login_required()
def data_import_groups(request):
    date = datetime.datetime.now()


    if request.method == 'POST':
        form  = DataImportForm(request.POST)

        csv_file = request.FILES['csv_file']

        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'THIS IS NOT A CSV FILE')
            return redirect(reverse('data_imp'))

        data_set = csv_file.read().decode('UTF-8')

        io_string = io.StringIO(data_set)
        next(io_string)
        for row in csv.reader(io_string, delimiter=',', quotechar="|"):
            print(row)
            member = MemberGroup()
            member.id = row[0]
            member.group_name = row[1]
            member.county = row[2]
            member.town = row[3]
            member.sub_county = row[4]
            member.group_officer = row[11]
            member.meeting_day = row[12]
            member.branch = Branch.objects.get(id=1)
            member.date_created = MemberGroup.objects.get(id=row[13])
            member.creator = request.user
            member.date_created = date
            member.modified_by = request.user
            member.save()

    return redirect(reverse('groups_list'))
# from parameters.models import Events
# @login_required()
# def data_import_groups(request):
#     date = datetime.datetime.now()


#     if request.method == 'POST':
#         form  = DataImportForm(request.POST)

#         csv_file = request.FILES['csv_file']

#         if not csv_file.name.endswith('.csv'):
#             messages.error(request, 'THIS IS NOT A CSV FILE')
#             return redirect(reverse('data_imp'))

#         data_set = csv_file.read().decode('UTF-8')

#         io_string = io.StringIO(data_set)
#         next(io_string)
#         for row in csv.reader(io_string, delimiter=',', quotechar="|"):
#             print(row)
#             member = Events()
            

#             member.creator = request.user
#             member.date_created = date
#             member.modified_by = request.user
#             member.save()

#     return redirect(reverse('groups_list'))

def upload_csv(request):
    data = {}
    if "GET" == request.method:
        return render(request, "myapp/upload_csv.html", data)

    try:
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request,'File is not CSV type')
            return HttpResponseRedirect(reverse("myapp:upload_csv"))

        if csv_file.multiple_chunks():
            messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return HttpResponseRedirect(reverse("myapp:upload_csv"))
 
        file_data = csv_file.read().decode("utf-8")
 
        lines = file_data.split("\n")

        for line in lines:
            fields = line.split(",")
            data_dict = {}
            data_dict["name"] = fields[0]
            data_dict["start_date_time"] = fields[1]
            data_dict["end_date_time"] = fields[2]
            data_dict["notes"] = fields[3]
            try:
                form = EventsForm(data_dict)
                if form.is_valid():
                    form.save()
                else:
                    logging.getLogger("error_logger").error(form.errors.as_json())
            except Exception as e:
                logging.getLogger("error_logger").error(repr(e))
                pass
 
    except Exception as e:
        logging.getLogger("error_logger").error("Unable to upload file. "+repr(e))
        messages.error(request,"Unable to upload file. "+repr(e))
 
    return HttpResponseRedirect(reverse("myapp:upload_csv"))


