from django.db import models
from django.utils.translation import ugettext_lazy as _

datatype = (
    ('Members', "Members"),
    
)

class DataUpload(models.Model):
    csv_file = models.FileField(_("Csv File"), upload_to='data/', max_length=100)
    data_type = models.CharField(_(""), choices=datatype, max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)

