from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.layout import (Layout, Fieldset, Field, Row, Column, Button,
                                 ButtonHolder, Submit, Div)
from .models import TypeOfAccount, Account, PaymentMethod

class TypeForm(ModelForm):
    class Meta:
        model = TypeOfAccount
        fields = ('title', 'description',
        
        )
         

    def __init__(self, *args, **kwargs):
        super(TypeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        # self.fields['parent'].required = False
        self.helper.layout = Layout(
            Row(
                Column('title', css_class='form-group col-md-6 mb-0'),
               
                css_class='form-row'
            ),
            Row(
                Column('description', css_class='form-group col-md-12 mb-0'),
                css_class='form-row'
            ),
             
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                css_class='form-row text-right'
            ),
         
            
        )

class PaymentMethodForm(ModelForm):
    class Meta:
        model = PaymentMethod
        fields = ['name', 'account']

    def __init__(self, *args, **kwargs):
        super(PaymentMethodForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
          
            Row(
                Column('account', css_class='form-group col-md-6 mb-0'),
               
              
                css_class='form-row'
            ),
          
             
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )




class BankingForm(ModelForm):
    class Meta:
        model = PaymentMethod
        fields = ['name', 'account']

    def __init__(self, *args, **kwargs):
        super(BankingForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                Column('account', css_class='form-group col-md-6 mb-0'),
               
                css_class='form-row'
            ),
          

          
             
            Row(
                Submit('submit', 'Send To Bank', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ['type', 'balance', 'description', 'name', 'opening_balance', 'transaction_type'
        
        ]
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(AccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
               Column('type', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
             Row(
                Column('opening_balance', css_class='form-group col-md-6 mb-0'),
               
                 Column('balance', css_class='form-group col-md-6 mb-0'),
               
              
                css_class='form-row'
            ),
           Row(
                Column('transaction_type', css_class='form-group col-md-6 mb-0'),
        
              
                css_class='form-row'
            ),
             
            Row(
                Column('description', css_class='form-group col-md-12 mb-0'),
        
              
                css_class='form-row'
            ),
             
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



from .models import Payments, Expenses
from bootstrap_modal_forms.forms import BSModalForm

class PaymentForm(BSModalForm):
    class Meta:
        model = Payments
        fields = ['amount', 'paymentmethod', 'invoice']


    def __init__(self, *args, **kwargs):
        super(PaymentForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('amount', css_class='form-group col-md-6 mb-0'),
               Column('paymentmethod', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

             Row(
                Column('invoice', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

            Div(
                   Row(
                Submit('submit', 'Submit & close', css_class="btn btn-success m-2"),
                Submit('submit', 'Submit & Print', css_class="btn btn-warning m-2"),
                
                css_class='form-row text-right'
            ),
                css_class="modal-footer"
            )
        
         
         
            
        )



class ExpensesForm(BSModalForm):
    class Meta:
        model = Expenses
        fields = ['amount', 'account', 'clas']


    def __init__(self, *args, **kwargs):
        super(ExpensesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('amount', css_class='form-group col-md-6 mb-0'),
               Column('account', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

             Row(
                Column('clas', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

          

            Div(
                   Row(
                Submit('submit', 'Submit', css_class="btn btn-warning m-2"),
                
                css_class='form-row text-right'
            ),
                css_class="modal-footer"
            )
        
         
         
            
        )



from .models import Budget, Estimates

class BudgetForm(BSModalForm):
    class Meta:
        model = Budget
        fields = ['financial_year',]


    def __init__(self, *args, **kwargs):
        super(BudgetForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('financial_period', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),

        
          

            Div(
                   Row(
                Submit('submit', 'Submit', css_class="btn btn-warning m-2"),
                
                css_class='form-row text-right'
            ),
                css_class="modal-footer"
            )
        
         
         
            
        )


from financial.models import GroupTransactions


class GroupTransactionCancelForm(ModelForm):
    date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))
    class Meta:
        model = GroupTransactions
        fields = ('type', 'amount','date'
        
        )
         
    def __init__(self, *args, **kwargs):
        super(GroupTransactionCancelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('type', css_class='form-group col-md-4 mb-0'),
                Column('amount', css_class='form-group col-md-4 mb-0'),
                Column('date', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
 
             
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                css_class='form-row text-right'
            ),
         
            
        )



from chartsoa.models import SecondaryAccount
class SecondaryPaymentMethodForm(ModelForm):
    class Meta:
        model = PaymentMethod
        fields = ['name', 'account']

    def __init__(self, *args, **kwargs):
        super(SecondaryPaymentMethodForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
          
            Row(
                Column('account', css_class='form-group col-md-6 mb-0'),
               
              
                css_class='form-row'
            ),
          
             
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )




class SecondaryBankingForm(ModelForm):
    class Meta:
        model = PaymentMethod
        fields = ['name', 'account']

    def __init__(self, *args, **kwargs):
        super(SecondaryBankingForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                Column('account', css_class='form-group col-md-6 mb-0'),
               
                css_class='form-row'
            ),
          

          
             
            Row(
                Submit('submit', 'Send To Bank', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



class SecondaryAccountForm(ModelForm):
    class Meta:
        model = SecondaryAccount
        fields = ['type', 'balance', 'description', 'name', 'opening_balance', 'transaction_type'
        
        ]
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(SecondaryAccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
               Column('type', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
             Row(
                Column('opening_balance', css_class='form-group col-md-6 mb-0'),
               
                 Column('balance', css_class='form-group col-md-6 mb-0'),
               
              
                css_class='form-row'
            ),
           Row(
                Column('transaction_type', css_class='form-group col-md-6 mb-0'),
        
              
                css_class='form-row'
            ),
             
            Row(
                Column('description', css_class='form-group col-md-12 mb-0'),
        
              
                css_class='form-row'
            ),
             
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )



