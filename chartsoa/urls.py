from django.urls import path

from . import views

from . import sec_views as sec_views



urlpatterns = [


    path('budget_list', views.budgetlist, name="budget_list"),
    path('budget/aoe', views.budgetaoe, name="budget_aoe"),
    path('budget/edit/<int:pk>', views.budgetedit, name="budget_edit"),
    path('budget/delete/<int:pk>', views.budgetdelete, name="budget_delete"),
    path('budget/details/<int:pk>', views.detailsbudget, name="budget_details"),

    # path('budget/process/<int:pk>', views.process_estimate, name="process_estimate"),

    path('loc_list', views.loclist, name="loc_list"),
    path('loc/aoe', views.locaoe, name="loc_aoe"),
    path('loc/edit/<int:pk>', views.locedit, name="loc_edit"),
    path('loc/delete/<int:pk>', views.locdelete, name="loc_delete"),
    path('loc/details/<int:pk>', views.detailsloc, name="loc_details"),
    path('loc/finance/<int:pk>', views.financeloc, name="loc_finance"),
    path('loc/transfer/<int:pk>', views.bankingloc, name="loc_banking"),
    path('loc/reversal/<int:pk>', views.cancel, name="loc_cancel"),
    # path('loc/reverse/<int:pk>', views.cancel, name="loc_reversal"),

    path('loc/payment/reversal/<int:pk>', views.reverse_payment, name="payment_reversal"),

    path('coa_list', views.list, name="coa_list"),
    path('aoe', views.aoe, name="coa_aoe"),
    path('coa/edit/<int:pk>', views.coaedit, name="coa_edit"),
    path('coa/delete/<int:pk>', views.coadelete, name="coa_delete"),
    path('coa/details/<int:pk>', views.detailscoa, name="coa_details"),


    path('types', views.types, name="types_list"),
    path('typesaoe', views.aoetype, name="types_aoe"),
    path('typesaoe/<int:pk>', views.typeedit, name="types_edit"),
    path('typesaoe/delete', views.typedelete, name="types_delete"),
    path('typesview/<int:pk>', views.detailstype, name="types_details"),

    path('methods', views.methods, name="methods_list"),
    path('methodsaoe', views.aoemethod, name="methods_aoe"),
    path('methodsaoe/<int:pk>', views.methodedit, name="methods_edit"),
    path('methodsaoe/delete', views.methoddelete, name="methods_delete"),

    path('receipt', views.ReceiptView.as_view(), name="receipt"),
    path('transactions', views.transactions, name="transactions"),

    path('statement', views.StatementView.as_view(), name="statement"),
    path('invoice', views.InvoiceView.as_view(), name="invoice"),
    
    path('expenses/list', views.expenses, name="expenses_list"),
    path('expenses', views.ExpenseView.as_view(), name="expense"),

    path('running/trialbalance', views.trial, name='running_trialbalance'),
    path('running/cashbook', views.cashbook, name='running_cashbook'),
    path('running/surplus_and_loss_account', views.surplus_loss, name='running_surplus_and_loss'),
    path('running/balancesheet', views.balancesheet, name='running_balancesheet'),

    path('sec/loc_list', sec_views.loclist, name="sec_loc_list"),
    path('sec/loc/aoe', sec_views.locaoe, name="sec_loc_aoe"),
    path('sec/loc/edit/<int:pk>', sec_views.locedit, name="sec_loc_edit"),
    path('sec/loc/delete/<int:pk>', sec_views.locdelete, name="sec_loc_delete"),
    path('sec/loc/details/<int:pk>', sec_views.detailsloc, name="sec_loc_details"),
    path('sec/loc/finance/<int:pk>', sec_views.financeloc, name="sec_loc_finance"),
    path('sec/loc/transfer/<int:pk>', sec_views.bankingloc, name="sec_loc_banking"),
    path('sec/loc/reversal/<int:pk>', sec_views.cancel, name="sec_loc_cancel"),
    # path('loc/reverse/<int:pk>', views.cancel, name="loc_reversal"),

    path('sec/loc/payment/reversal/<int:pk>', sec_views.reverse_payment, name="sec_payment_reversal"),

    path('sec/coa_list', sec_views.list, name="sec_coa_list"),
    path('sec/aoe', sec_views.aoe, name="sec_coa_aoe"),
    path('sec/coa/edit/<int:pk>', sec_views.coaedit, name="sec_coa_edit"),
    path('sec/coa/delete/<int:pk>', sec_views.coadelete, name="sec_coa_delete"),
    path('sec/coa/details/<int:pk>', sec_views.detailscoa, name="sec_coa_details"),


    path('sec/running/trialbalance', sec_views.trial, name='sec_running_trialbalance'),
    path('sec/running/cashbook', sec_views.cashbook, name='sec_running_cashbook'),
    path('sec/running/surplus_and_loss_account', sec_views.surplus_loss, name='sec_running_surplus_and_loss'),
    path('sec/running/balancesheet', sec_views.balancesheet, name='sec_running_balancesheet'),
]
