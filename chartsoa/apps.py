from django.apps import AppConfig


class ChartsoaConfig(AppConfig):
    name = 'chartsoa'
    icon_name = 'book'
    
    def ready(self):
        import chartsoa.signals
