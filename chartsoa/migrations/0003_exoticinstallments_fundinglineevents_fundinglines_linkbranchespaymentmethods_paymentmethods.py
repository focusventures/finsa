# Generated by Django 3.0.4 on 2020-04-13 21:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chartsoa', '0002_auto_20200319_1244'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exoticinstallments',
            fields=[
                ('number', models.IntegerField(primary_key=True, serialize=False)),
                ('principal_coeff', models.FloatField()),
                ('interest_coeff', models.FloatField(blank=True, null=True)),
            ],
            options={
                'db_table': 'ExoticInstallments',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Fundinglineevents',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=200)),
                ('amount', models.DecimalField(decimal_places=4, max_digits=19)),
                ('direction', models.SmallIntegerField()),
                ('deleted', models.BooleanField()),
                ('creation_date', models.DateTimeField()),
                ('type', models.SmallIntegerField()),
                ('user_id', models.IntegerField(blank=True, null=True)),
                ('contract_event_id', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'FundingLineEvents',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Fundinglines',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('begin_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('amount', models.DecimalField(decimal_places=0, max_digits=18)),
                ('purpose', models.CharField(max_length=50)),
                ('deleted', models.BooleanField()),
            ],
            options={
                'db_table': 'FundingLines',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Linkbranchespaymentmethods',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('branch_id', models.IntegerField()),
                ('deleted', models.BooleanField()),
                ('date', models.DateTimeField(blank=True, null=True)),
                ('account_id', models.IntegerField()),
            ],
            options={
                'db_table': 'LinkBranchesPaymentMethods',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Paymentmethods',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=50, null=True)),
                ('description', models.CharField(blank=True, max_length=250, null=True)),
                ('pending', models.BooleanField(blank=True, null=True)),
                ('accountnumber', models.CharField(blank=True, db_column='accountNumber', max_length=32, null=True)),
            ],
            options={
                'db_table': 'PaymentMethods',
                'managed': False,
            },
        ),
    ]
