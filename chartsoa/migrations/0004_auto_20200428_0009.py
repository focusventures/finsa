# Generated by Django 3.0.4 on 2020-04-27 21:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chartsoa', '0003_exoticinstallments_fundinglineevents_fundinglines_linkbranchespaymentmethods_paymentmethods'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='balance',
            field=models.DecimalField(decimal_places=2, max_digits=20, verbose_name='Account Balance'),
        ),
    ]
