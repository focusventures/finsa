from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Sum, Aggregate
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from .forms import TypeForm, AccountForm,PaymentForm, PaymentForm, PaymentMethodForm, ExpensesForm
from .models import TypeOfAccount, Account, PaymentMethod, Payments, Expenses, AccountArchive, AccountEvent



from django.shortcuts import render

from django.db.models import Sum, Aggregate
from django.db.models.functions import Coalesce

from parameters.models import LOCArchive, LOCEvent, Month, Periods
from financial.models import GroupTransactions
from loans.models import LoanStatus
from django.contrib import messages


from financial.models import GroupCustomAccount
from chartsoa.models import Account, AccountArchive
from financial.models import Transactions

@login_required()
def transactions(request):
    transactions = Transactions.objects.filter(Q(amount__gte = 0) &  Q(fees__isnull=False)).select_related('fees')

    context = {
        "transactions": transactions
    }
    return render(request, 'chartsoa/transactions.html', context)



@login_required()
def expenses(request):
    posts_list = Expenses.objects.all().select_related('account')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'chartsoa/expenses.html', { "expenses": posts })

@login_required()
def methods(request):
    posts_list = PaymentMethod.objects.all().select_related('account')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'chartsoa/methods.html', { "methods": posts })


@login_required()
def aoemethod(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = PaymentMethodForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('methods_list'))
    else:
        form = PaymentMethodForm()
    context = {'form': form}
    return render(request, 'chartsoa/methodaoe.html', context)


@login_required()
def methodedit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    object = PaymentMethod.objects.get(id=pk)
    if request.method == 'POST':
        form = PaymentMethodForm(request.POST, instance=object)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('methods_list'))
    else:

        form = PaymentMethodForm(instance=object)
      
    context = {'form': form}
    return render(request, 'chartsoa/methodaoe.html', context)

@login_required()
def methoddelete(request, pk):
    object = PaymentMethod.objects.get(id=pk).delete()
    return redirect('/')

@login_required()
def detailsmethod(request, pk):
    
    result = PaymentMethod.objects.get(id=pk)
    context = {
        # "shop": shop,
        "type": result
    }  
    return render(request, 'chartsoa/detailsmethod.html', context)



@login_required()
def types(request):
    posts_list = TypeOfAccount.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return render(request, 'chartsoa/types.html', { "types": posts })


@login_required()
def aoetype(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = TypeForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('types_list'))
    else:
        # pk = request.GET.get('pk')
        # if pk != '':
        #     object = TypeOfAccount.objects.get(id=pk)
        #     form = TypeForm(instance=object)
        # else:
        form = TypeForm()
    context = {'form': form}
    return render(request, 'chartsoa/aoetype.html', context)

@login_required()
def typeedit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    type_ = TypeOfAccount.objects.get(id=pk)
    if request.method == 'POST':
        form = TypeForm(request.POST, instance=type_)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('types_list'))
    else:
        form = TypeForm(instance=type_)
      
    context = {'form': form}
    return render(request, 'chartsoa/aoetype.html', context)

@login_required()
def typedelete(request, pk):
    object = TypeOfAccount.objects.get(id=pk).delete()
    return redirect('/')

@login_required()
def detailstype(request, pk):
    
    result = TypeOfAccount.objects.get(id=pk)
    context = {
        # "shop": shop,
        "type": result
    }  
    return render(request, 'chartsoa/detailstype.html', context)

@login_required()
def list(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    
    month = Month.objects.get(id=month.id)
    year = Periods.objects.get(id=year.id)

    posts_list = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month)).select_related('type', 'transaction_type').select_related('creator')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    posts = Account.objects.filter(Q(financial_year=year) & Q(financial_month=month)).select_related('type', 'transaction_type').select_related('creator')
    context = {
        "accounts": posts
    }

    return render(request, 'chartsoa/list.html', context)


@login_required()
def aoe(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('coa_list'))
    else:
        form = AccountForm()
    context = {'form': form}
    return render(request, 'chartsoa/aoe.html', context)

@login_required()
def coaedit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    object = Account.objects.get(id=pk)
    if request.method == 'POST':
        form = AccountForm(request.POST, instance=object)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('coa_details', args=(pk,) ))
    else:
        
        form = AccountForm(instance=object)
      
    context = {'form': form}
    return render(request, 'chartsoa/aoetype.html', context)


@login_required()
def coadelete(request, pk):
    object = Account.objects.get(id=pk).delete()
    return redirect('/')

from financial.models import GroupTransactions
@login_required()
def detailscoa(request, pk):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    account_ = Account.objects.filter(id=pk)
    events = AccountEvent.objects.filter(Q(account=account_.first()) & Q(month=month)).select_related('transaction').order_by('id')
    dr = AccountEvent.objects.filter(Q(account=account_.first()) & Q(month=month)).select_related('transaction').aggregate(Sum('transaction__dr_amount'))
    cr = AccountEvent.objects.filter(Q(account=account_.first()) & Q(month=month)).select_related('transaction').aggregate(Sum('transaction__cr_amount'))

    context={
        'accounts': account_,
        # 'form': form,
        'events': events,
        'pk':pk,
        'dr': dr,
        'cr': cr
    }	
    return render(request, 'chartsoa/details.html', context)



from django.urls import reverse_lazy
from .forms import PaymentForm
from .models import Payments
from bootstrap_modal_forms.generic import BSModalCreateView

class PaymentView(BSModalCreateView):
    template_name = 'chartsoa/payment.html'
    form_class = PaymentForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')


class ReceiptView(BSModalCreateView):
    template_name = 'chartsoa/receipt.html'
    form_class = PaymentForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')



class InvoiceView(BSModalCreateView):
    template_name = 'chartsoa/invoice.html'
    form_class = PaymentForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')



class StatementView(BSModalCreateView):
    template_name = 'chartsoa/statement.html'
    form_class = PaymentForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')



class ExpenseView(BSModalCreateView):
    template_name = 'chartsoa/expense.html'
    form_class = ExpensesForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')






from parameters.models import LOC, LOCEvent
from parameters.forms import LOCForm
import datetime

@login_required()
def loclist(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    month = Month.objects.get(id=month.id)
    year = Periods.objects.get(id=year.id)

    posts_list = LOC.objects.filter(Q(financial_year=year) & Q(financial_month=month)).select_related('creator')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    posts = LOC.objects.filter(Q(financial_year=year) & Q(financial_month=month)).select_related('creator')
    context = {
        "accounts": posts
    }

    return render(request, 'chartsoa/loc/list.html', context)


@login_required()
def locaoe(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = LOCForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.remander_amount = atype.initial_amount
            atype.creator = request.user
            atype.save()
            return redirect(reverse('loc_list'))
    else:
        form = LOCForm()
    context = {'form': form}
    return render(request, 'chartsoa/loc/aoe.html', context)

@login_required()
def locedit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    object = LOC.objects.get(id=pk)
    if request.method == 'POST':
        form = LOCForm(request.POST, instance=object)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.modified_by = request.user
            atype.date_modified = datetime.datetime.now()
            atype.save()
            return redirect(reverse('loc_details', args=(pk,) ))
    else:
        
        form = LOCForm(instance=object)
      
    context = {'form': form}
    return render(request, 'chartsoa/loc/aoe.html', context)


@login_required()
def locdelete(request, pk):
    object = LOC.objects.get(id=pk).delete()
    return redirect('loc_list')

@login_required()
def detailsloc(request, pk):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    account_ = LOC.objects.filter(id=pk)

    events =  LOCEvent.objects.filter(Q(loc_id=account_.first().id) & Q (amount__gt= 0) & Q(financial_month=month)).select_related('transaction', 'slip',).order_by('id')
    debits = LOCEvent.objects.filter(Q(loc_id=account_.first().id) & Q(cancelled=False) & Q(financial_month=month)).aggregate(Sum('transaction__dr_amount'))
    credits = LOCEvent.objects.filter(Q(loc_id=account_.first().id) & Q(cancelled=False) & Q(financial_month=month)).aggregate(Sum('transaction__cr_amount'))

    context={
        'accounts': account_,
        'events': events,
        'debits': debits,
        'credits': credits,
        'account': account_.first(),
        'pk':pk
    }	
    return render(request, 'chartsoa/loc/details.html', context)

from .forms import BankingForm

@login_required()
def financeloc(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = LOCForm(pk, request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.remander_amount = atype.initial_amount
            atype.creator = request.user
            atype.save()
            return redirect(reverse('loc_list'))
    else:
        form = LOCForm(pk)
    context = {'form': form}
    return render(request, 'chartsoa/loc/financing.html', context)

from parameters.models import LOCBanking
from parameters.forms import LOCBankingForm
from financial.models import Transactions, GroupTransactions
from parameters.models import Events, Month, Periods
import decimal

@login_required()
def bankingloc(request, pk):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    posts_list = LOC.objects.filter(id=pk).select_related('creator', 'type')
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = LOCBankingForm(request.POST)
        if form.is_valid():
            if posts_list.first().is_cash:
                atype = form.save(commit = False)

                atype.loc = posts_list.first()
                
                atype.creator = request.user
                atype.save()

                event, new = Events.objects.get_or_create(code ='banking')
                if new:
                    event.title = "Banking"
                    event.save()

                tranaction_dep = GroupTransactions()
                tranaction_dep.amount = atype.amount
                tranaction_dep.description = event.title
                tranaction_dep.type = event
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date
                tranaction_dep.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction_dep.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction_dep.date_created = datetime.datetime.now()
                tranaction_dep.code = '978'
                tranaction_dep.double_entry = 'Dr'
                tranaction_dep.actual_amount = atype.amount
                tranaction_dep.cr_amount = atype.amount
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date
                tranaction_dep.save()

                chart_oa = Account.objects.get(Q(transaction_type=event) & Q(financial_year=year) & Q(financial_month=month))
                chart_oa.balance += decimal.Decimal(atype.amount)
                chart_oa.save()

                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = tranaction_dep.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()


                loc = LOC.objects.get(Q(is_cash=True) & Q(is_active=True))
                loc.remainder_amount -= tranaction_dep.amount
                loc.save()


                tranaction_dep.loc = loc
                tranaction_dep.save()

                # coa_event = AccountEvent(
                #     account = chart_oa,
                #     transaction = tranaction_dep,
                #     amount = atype.amount,
                #     coa_balance = chart_oa.balance,
                #     month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                #     financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                #     date_created = datetime.datetime.now(),
                #     creator = request.user
                # )
                # coa_event.save()

                loc_event = LOCEvent()
                loc_event.loc = loc
                loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                loc_event.transaction = tranaction_dep
                loc_event.description = tranaction_dep.description
                loc_event.amount = tranaction_dep.amount
                loc_event.date = atype.date
                loc_event.remainder = loc.remainder_amount
                loc_event.type = 'Cr'
                loc_event.creator = request.user
                loc_event.date_created = datetime.datetime.now()
                loc_event.save()



                tranaction = GroupTransactions()
                tranaction.amount = atype.amount
                tranaction.description = event.title
                tranaction.type = event
                tranaction.creator = request.user
                tranaction.date = datetime.datetime.now()
                tranaction.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction.date_created = datetime.datetime.now()
                tranaction.code = '978'
                tranaction.double_entry = 'Dr'
                tranaction.actual_amount = atype.amount
                tranaction.dr_amount = atype.amount
                tranaction.creator = request.user
                tranaction.date = atype.date
                tranaction.loc_account = 'bank'

                loc = LOC.objects.filter(Q(is_bank=True) & Q(is_active=True)).select_related('type').first()
                loc.remainder_amount += tranaction.amount
                loc.save()

 

                tranaction.loc = loc
                tranaction.save()


                ev = Events.objects.get(id=loc.type.id)
                chart_ = Account.objects.filter(Q(transaction_type=ev) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_.balance += decimal.Decimal(tranaction_dep.amount)
                chart_.save()
                coa_event = AccountEvent(
                    account = chart_,
                    transaction = tranaction,
                    amount = tranaction.amount,
                    coa_balance = chart_.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                event = LOCEvent()
                event.loc = loc
                event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                event.transaction = tranaction
                event.description = tranaction.description
                event.amount = tranaction.amount
                event.remainder = loc.remainder_amount
                event.loc_account = 'bank'
                event.type = 'Dr'
                event.date = atype.date
                event.creator = request.user
                event.date_created = datetime.datetime.now()
                event.save()


                # original = GroupTransactions.objects.filter(id=posts_list.first().transaction.id).select_related('payment_method').first()
                # method = PaymentMethod.objects.filter(id=original.payment_method.id).select_related('account')
                e = Events.objects.get(id=posts_list.first().type.id)
                chart_oa = Account.objects.filter(Q(transaction_type=e) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_oa.balance -= decimal.Decimal(tranaction_dep.amount)
                chart_oa.save()
                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = tranaction_dep.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()



            if posts_list.first().is_bank:
                atype = form.save(commit = False)

                atype.loc = posts_list.first()
                
                atype.creator = request.user
                atype.save()
                event, new = Events.objects.get_or_create(code ='cash_from_bank')
                if new:
                    event.title = "Cash From Bank"
                    event.save()

                tranaction_dep = GroupTransactions()
                tranaction_dep.amount = atype.amount
                tranaction_dep.description = event.title
                tranaction_dep.type = event
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date
                tranaction_dep.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction_dep.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction_dep.date_created = datetime.datetime.now()
                tranaction_dep.code = '978'
                tranaction_dep.double_entry = 'Cr'
                tranaction_dep.actual_amount = atype.amount
                tranaction_dep.cr_amount = atype.amount
                tranaction_dep.creator = request.user
                tranaction_dep.date = atype.date
                tranaction_dep.save()

                chart_oa = Account.objects.get(Q(transaction_type=event) & Q(financial_year=year) & Q(financial_month=month))
                chart_oa.balance += decimal.Decimal(atype.amount)
                chart_oa.save()

                coa_event = AccountEvent(
                    account = chart_oa,
                    transaction = tranaction_dep,
                    amount = tranaction_dep.amount,
                    coa_balance = chart_oa.balance,
                    month = month,
                    financial_year = year,
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()


                loc = LOC.objects.filter(Q(is_bank=True) & Q(is_active=True)).select_related('type').first()
                loc.remainder_amount -= tranaction_dep.amount
                loc.save()

                tranaction_dep.loc = loc
                tranaction_dep.save()

                loc_event = LOCEvent()
                loc_event.loc = loc
                loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                loc_event.transaction = tranaction_dep
                loc_event.description = tranaction_dep.description
                loc_event.amount = tranaction_dep.amount
                loc_event.date = atype.date
                loc_event.remainder = loc.remainder_amount
                loc_event.type = 'Cr'
                loc_event.creator = request.user
                loc_event.date_created = datetime.datetime.now()
                loc_event.save()

                b_chart_oa = Account.objects.get(Q(transaction_type=loc.type) & Q(financial_year=year) & Q(financial_month=month))
                b_chart_oa.balance -= decimal.Decimal(atype.amount)
                b_chart_oa.save()


                coa_event = AccountEvent(
                    account = b_chart_oa,
                    transaction = tranaction_dep,
                    amount = atype.amount,
                    coa_balance = chart_oa.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                # loc_event = LOCEvent()
                # loc_event.loc = loc
                # loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                # loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                # loc_event.transaction = tranaction_dep
                # loc_event.description = tranaction_dep.description
                # loc_event.amount = tranaction_dep.amount
                # loc_event.date = atype.date
                # loc_event.remainder = loc.remainder_amount
                # loc_event.type = 'Cr'
                # loc_event.creator = request.user
                # loc_event.date_created = datetime.datetime.now()
                # loc_event.save()

                tranaction = GroupTransactions()
                tranaction.amount = atype.amount
                tranaction.description = event.title
                tranaction.type = event
                tranaction.creator = request.user
                tranaction.date = atype.date
                tranaction.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                tranaction.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                tranaction.code = '978'
                tranaction.double_entry = 'Dr'
                tranaction.date_created = datetime.datetime.now()
                tranaction.actual_amount = atype.amount
                tranaction.dr_amount = atype.amount
                tranaction.creator = request.user
                tranaction.date = atype.date
                tranaction.loc_account = 'bank'

                loc = LOC.objects.filter(Q(is_cash=True) & Q(is_active=True)).select_related('type').first()
                loc.remainder_amount += tranaction.amount
                loc.save()

                tranaction.loc = loc
                tranaction.save()



                ev = Events.objects.get(id=loc.type.id)
                chart_ = Account.objects.filter(Q(transaction_type=ev) & Q(financial_year=year) & Q(financial_month=month)).first()
                chart_.balance += decimal.Decimal(tranaction_dep.amount)
                chart_.save()
                coa_event = AccountEvent(
                    account = chart_,
                    transaction = tranaction,
                    amount = tranaction.amount,
                    coa_balance = chart_.balance,
                    month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                    financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                    date_created = datetime.datetime.now(),
                    creator = request.user
                )
                coa_event.save()

                event = LOCEvent()
                event.loc = loc
                event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
                event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
                event.transaction = tranaction
                event.description = tranaction.description
                event.amount = tranaction.amount
                event.remainder = loc.remainder_amount
                event.loc_account = 'bank'
                event.type = 'Dr'
                event.date = atype.date
                event.creator = request.user
                event.date_created = datetime.datetime.now()
                event.save()



                # e = Events.objects.get(id=posts_list.first().type.id)
                # chart_oa = Account.objects.filter(Q(transaction_type=e) & Q(financial_year=year) & Q(financial_month=month)).first()
                # chart_oa.balance -= decimal.Decimal(tranaction_dep.amount)
                # chart_oa.save()
                # coa_event = AccountEvent(
                #     account = chart_oa,
                #     transaction = tranaction_dep,
                #     amount = tranaction_dep.amount,
                #     coa_balance = chart_oa.balance,
                #     month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                #     financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                #     date_created = datetime.datetime.now(),
                #     creator = request.user
                # )
                # coa_event.save()


            return redirect(reverse('loc_list'))
    else:
        form = LOCBankingForm()
      
    context = {'form': form, "accounts": posts_list, "events": LOCBanking.objects.filter(loc=posts_list.first()), 'loc':posts_list.first()}
    return render(request, 'chartsoa/loc/banking.html', context)



from chartsoa.forms import GroupTransactionCancelForm
@login_required()
def cancelloc(request, pk):
    posts_list = LOC.objects.filter(id=pk).select_related('creator')

    if request.method == 'POST':
        form = GroupTransactionCancelForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.loc = posts_list.first()
            atype.creator = request.user
            atype.description = ''
            atype.creator = request.user
            atype.date = datetime.datetime.now()
            atype.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
            atype.financial_period = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
            atype.date_created = datetime.datetime.now()
            atype.code = ''
            atype.double_entry = 'Cr'
            atype.date_created = datetime.datetime.now()
            atype.actual_amount = atype.amount
            atype.creator = request.user
            atype.date = datetime.datetime.now()
            atype.loc_account = form.cleaned_data.get('type').title
            atype.save()

            chart_oa = Account.objects.get(type=form.cleaned_data.get('type'))
            chart_oa.balance -= decimal.Decimal(atype.amount)
            chart_oa.save()

            loc = LOC.objects.filter(id=3).first()
            loc.remainder_amount -= atype.amount
            loc.save()

            loc_event = LOCEvent()
            loc_event.loc = loc
            loc_event.financial_month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
            loc_event.financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
            loc_event.transaction = atype
            loc_event.description = atype.description
            loc_event.amount = atype.amount
            loc_event.remainder = loc.remainder_amount
            loc_event.type = 'Cr'
            loc_event.creator = request.user
            loc_event.date_created = datetime.datetime.now()
            loc_event.save()

            loc = LOC.objects.filter(id=1).first()
            loc.remainder_amount += atype.amount
            loc.save()

            return redirect(reverse('loc_list'))
    else:
        form = GroupTransactionCancelForm()
      
    context = {'form': form, "accounts": posts_list}
    return render(request, 'chartsoa/loc/cancel.html', context)




from chartsoa.models import Budget, Estimates
from chartsoa.forms import BudgetForm
from django.forms import modelformset_factory

@login_required()
def budgetlist(request):
    posts_list = Budget.objects.all().select_related('creator')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    posts = Budget.objects.all().select_related('creator')
    context = {
        "accounts": posts
    }

    return render(request, 'chartsoa/budget/list.html', context)


@login_required()
def budgetaoe(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    if request.method == 'POST':
        form = BudgetForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('loc_list'))
    else:
        form = BudgetForm()
    context = {'form': form}
    return render(request, 'chartsoa/budget/aoe.html', context)


@login_required()
def budgetedit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """

    TransactionsModelFormset = modelformset_factory(
    Estimates,
    fields=('event', 'estimate' ),
    extra=1
    )
    object = Budget.objects.get(id=pk)
    formset = TransactionsModelFormset(queryset=Estimates.objects.filter(budget=object),)

    if request.method == "POST":
        TransactionsModelFormset = modelformset_factory(
            Estimates,
            fields=('event','estimate', ),
            extra=1
        )
        formset = TransactionsModelFormset(request.POST)
        if formset.is_valid():
            for form in formset:
                if form.cleaned_data.get('estimate'):
                    
                    estimate = form.save(commit=False)
                    estimate.budget = object
                    estimate.creator = request.user
                    estimate.date_created = datetime.datetime.now()
                    estimate.save()

                    object.total += estimate.estimate
                    object.save()


        return redirect(reverse('budget_details', args=(pk,) ))
 
        
    form = BudgetForm(instance=object)
      
    context = {'form': form, 'formset': formset, 'budget': Budget.objects.get(id=pk)}
    return render(request, 'chartsoa/budget/edit.html', context)


@login_required()
def budgetdelete(request, pk):
    object = Budget.objects.get(id=pk).delete()
    return redirect('loc_list')

@login_required()
def detailsbudget(request, pk):
    account_ = Budget.objects.filter(id=pk)
    events =  Estimates.objects.filter(budget=account_.first())
   

    context={
        'accounts': account_,
        # 'form': form,
        'events': events,
        'pk':pk
    }	
    return render(request, 'chartsoa/budget/details.html', context)




import pdb
from chartsoa.forms import GroupTransactionCancelForm
@login_required()
def cancel(request, pk):
    loc_event = LOCEvent.objects.filter(id=pk).select_related('transaction', 'loc').first()
    loc = LOC.objects.filter(id=loc_event.loc.id).select_related('type').first()
    instance = GroupTransactions.objects.filter(id=loc_event.transaction.id).select_related('type', 'payment_method').first()


    detatched = False
    try:
        payment_m = PaymentMethod.objects.filter(id=instance.payment_method.id).select_related('account').first()
    except AttributeError:
        detatched = True

    if detatched:
        loc.remainder_amount -= int(instance.amount)
        loc.save()

        loc_event.cancelled = True
        loc_event.save()

        archive = GroupTransactions()
        archive.financial_month = instance.financial_month or None
        archive.financial_year = instance.financial_year or None
        
        if instance.dr_amount > 0:
            archive.cr_amount = instance.dr_amount
        if instance.cr_amount > 0:
            archive.dr_amount = instance.cr_amount

        archive.type = instance.type or None
        archive.action = instance.action or None
        archive.acc = instance.acc or None
        archive.payment_method = instance.payment_method or None
        archive.slip = instance.slip or None
        archive.session = instance.session or None
        archive.code = instance.code or None
        archive.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        archive.expense = instance.expense or None
        archive.fees = instance.fees or None
        archive.member = instance.member or None
        archive.group = instance.group or None
        archive.receiver = instance.receiver or None
        archive.receiving_member = instance.receiving_member or None
        archive.date = instance.date or None
        archive.processed = instance.processed or None
        archive.amount = instance.amount or None
        archive.coa = instance.coa or None
        archive.actual_amount = instance.actual_amount or 0.00
        archive.creator = request.user
        archive.date_modified = instance.date_modified or None
        archive.date_created = datetime.datetime.now()
        archive.modified_by = instance.modified_by or None
        archive.save()

        loc_e = LOCEvent()
        loc_e.financial_month = instance.financial_month
        loc_e.financial_year = instance.financial_year 
        loc_e.loc_account = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        loc_e.type = instance.type
        loc_e.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        loc_e.loc = loc
        loc_e.slip = instance.slip
        loc_e.payment_method = instance.payment_method or None
        loc_e.transaction = archive
        loc_e.amount = instance.amount
        loc_e.remainder = loc.remainder_amount
        loc_e.cancelled = True
        loc_e.date_created = datetime.datetime.now()
        loc_e.creator = request.user
        loc_e.date_modified = datetime.datetime.now()
        loc_e.modified_by = request.user
        loc_e.save()

        # Update main banking or cash from bank event
        event = Events.objects.get(id=instance.type.id)
        chart_oa = Account.objects.filter(transaction_type=event).first()
        chart_oa.balance -= decimal.Decimal(archive.amount)
        chart_oa.save()
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = instance.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()


        event_loc_ = Events.objects.filter(id=loc.type.id).first()
        chart_oa = Account.objects.filter(transaction_type=event_loc_).first()
        chart_oa.balance -= decimal.Decimal(archive.amount)
        chart_oa.save()
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = instance.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()

        if loc.is_bank:
            reversal_loc = LOC.objects.filter(is_cash=True).select_related('type').first()

        if loc.is_cash:
            reversal_loc = LOC.objects.filter(is_bank=True).select_related('type').first()

        r_event = Events.objects.get(id=reversal_loc.type.id)

        reversal_loc.remainder_amount += int(instance.amount)
        reversal_loc.save()



        archive = GroupTransactions()
        archive.financial_month = instance.financial_month or None
        archive.financial_year = instance.financial_year or None
        
        # if instance.dr_amount > 0:
        archive.cr_amount = instance.cr_amount
        # if instance.cr_amount > 0:
        #     archive.dr_amount = instance.cr_amount

        archive.type = r_event or None
        archive.action = r_event or None
        archive.acc = instance.acc or None
        archive.payment_method = instance.payment_method or None
        archive.slip = instance.slip or None
        archive.session = instance.session or None
        archive.code = instance.code or None
        archive.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        archive.expense = instance.expense or None
        archive.fees = instance.fees or None
        archive.member = instance.member or None
        archive.group = instance.group or None
        archive.receiver = instance.receiver or None
        archive.receiving_member = instance.receiving_member or None
        archive.date = instance.date or None
        archive.processed = instance.processed or None
        archive.amount = instance.amount or None
        archive.coa = instance.coa or None
        archive.actual_amount = instance.actual_amount or 0.00
        archive.creator = request.user
        archive.date_modified = instance.date_modified or None
        archive.date_created = datetime.datetime.now()
        archive.modified_by = instance.modified_by or None
        archive.save()

        loc_e = LOCEvent()
        loc_e.financial_month = instance.financial_month 
        loc_e.financial_year = instance.financial_year 
        loc_e.loc_account =  "Reversal for " + str(loc_event.id) + ' ' + str(instance.type )
        loc_e.type = r_event
        loc_e.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        loc_e.loc = reversal_loc
        loc_e.slip = instance.slip
        loc_e.payment_method = instance.payment_method
        loc_e.transaction = archive
        loc_e.amount = instance.amount
        loc_e.remainder = reversal_loc.remainder_amount
        loc_e.date_created = datetime.datetime.now()
        loc_e.creator = request.user
        loc_e.date_modified = datetime.datetime.now()
        loc_e.modified_by = request.user
        loc_e.cancelled = True
        loc_e.save()

        chart_oa = Account.objects.filter(transaction_type=r_event).first()
        chart_oa.balance += decimal.Decimal(archive.amount)
        chart_oa.save()
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = archive.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()

    else:
        loc.remainder_amount -= int(instance.amount)
        loc.save()
        loc_event.cancelled = True
        loc_event.save()

        archive = GroupTransactions()
        archive.financial_month = instance.financial_month or None
        archive.financial_year = instance.financial_year or None
        
        if instance.dr_amount > 0:
            archive.cr_amount = instance.dr_amount
        if instance.cr_amount > 0:
            archive.dr_amount = instance.cr_amount

        archive.type = instance.type or None
        archive.action = instance.action or None
        archive.acc = instance.acc or None
        archive.payment_method = instance.payment_method or None
        archive.slip = instance.slip or None
        archive.session = instance.session or None
        archive.code = instance.code or None
        archive.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) or None
        archive.expense = instance.expense or None
        archive.fees = instance.fees or None
        archive.member = instance.member or None
        archive.group = instance.group or None
        archive.receiver = instance.receiver or None
        archive.receiving_member = instance.receiving_member or None
        archive.date = instance.date or None
        archive.processed = instance.processed or None
        archive.amount = instance.amount or None
        archive.coa = instance.coa or None
        archive.actual_amount = instance.actual_amount or 0.00
        archive.creator = request.user
        archive.date_modified = instance.date_modified or None
        archive.date_created = datetime.datetime.now()
        archive.modified_by = instance.modified_by or None
        archive.save()


        loc_e = LOCEvent()
        loc_e.financial_month = instance.financial_month 
        loc_e.financial_year = instance.financial_year 
        loc_e.loc_account = "Cash"
        loc_e.type = instance.type
        loc_e.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        loc_e.loc = loc
        loc_e.slip = instance.slip
        loc_e.payment_method = instance.payment_method
        loc_e.transaction = archive
        loc_e.amount = instance.amount
        loc_e.remainder = loc.remainder_amount
        loc_e.cancelled = True
        loc_e.date_created = datetime.datetime.now()
        loc_e.creator = request.user
        loc_e.date_modified = datetime.datetime.now()
        loc_e.modified_by = request.user
        loc_e.save()

        event = Events.objects.get(id=instance.type.id)

        if event.is_reducing:
            chart_oa = Account.objects.filter(transaction_type=event).first()
            chart_oa.balance += decimal.Decimal(archive.amount)
            chart_oa.save()
        else:
            chart_oa = Account.objects.filter(transaction_type=event).first()
            chart_oa.balance -= decimal.Decimal(archive.amount)
            chart_oa.save()            
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = archive.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()
        payment_m = PaymentMethod.objects.filter(id=instance.payment_method.id).select_related('account').first()

        chart_oa = Account.objects.filter(id=payment_m.account.id).first()
        chart_oa.balance -= decimal.Decimal(archive.amount)
        chart_oa.save()
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = archive.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()


        if event.secondary_event is not None:
            secondary_event = Events.objects.get(id=event.secondary_event.id)
            chart_oa_secondary = Account.objects.filter(transaction_type=secondary_event).first()

            if instance.cr_amount > 0:
                chart_oa_secondary.balance += instance.cr_amount

            chart_oa_secondary.save()

            coa_event_sec = AccountEvent(
                account = chart_oa_secondary,
                transaction = archive,
                amount = archive.amount,
                coa_balance = chart_oa_secondary.balance,
                month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
                financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
                date_created = datetime.datetime.now(),
                creator = request.user
            )

            coa_event_sec.save()

    return redirect(reverse('loc_details', args=(loc.id,) ))






from chartsoa.forms import GroupTransactionCancelForm
@login_required()
def reverse_payment(request, pk):
    loc_event = LOCEvent.objects.filter(id=pk).select_related('transaction', 'loc').first()
    loc = LOC.objects.filter(id=loc_event.loc.id).select_related('type').first()
    instance = GroupTransactions.objects.filter(id=loc_event.transaction.id).select_related('type', 'payment_method').first()


    detatched = False
    try:
        payment_m = PaymentMethod.objects.filter(id=instance.payment_method.id).select_related('account').first()
    except AttributeError:
        detatched = True

    if not detatched:

        loc.remainder_amount += int(instance.amount)
        loc.save()

        loc_event.cancelled = True
        loc_event.save()

        archive = GroupTransactions()
        archive.financial_month = instance.financial_month or None
        archive.financial_year = instance.financial_year or None
        
        if instance.dr_amount > 0:
            archive.cr_amount = instance.dr_amount
        if instance.cr_amount > 0:
            archive.dr_amount = instance.cr_amount

        archive.type = instance.type or None
        archive.action = instance.action or None
        archive.acc = instance.acc or None
        archive.payment_method = instance.payment_method or None
        archive.slip = instance.slip or None
        archive.session = instance.session or None
        archive.code = instance.code or None
        archive.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        archive.expense = instance.expense or None
        archive.fees = instance.fees or None
        archive.member = instance.member or None
        archive.group = instance.group or None
        archive.receiver = instance.receiver or None
        archive.receiving_member = instance.receiving_member or None
        archive.date = instance.date or None
        archive.processed = instance.processed or None
        archive.amount = instance.amount or None
        archive.coa = instance.coa or None
        archive.actual_amount = instance.actual_amount or 0.00
        archive.creator = request.user
        archive.date_modified = instance.date_modified or None
        archive.date_created = datetime.datetime.now()
        archive.modified_by = instance.modified_by or None
        archive.save()

        loc_e = LOCEvent()
        loc_e.financial_month = instance.financial_month
        loc_e.financial_year = instance.financial_year 
        loc_e.loc_account = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        loc_e.type = instance.type
        loc_e.description = "Reversal for " + str(loc_event.id) + ' ' + str(instance.type ) 
        loc_e.loc = loc
        loc_e.slip = instance.slip
        loc_e.payment_method = instance.payment_method or None
        loc_e.transaction = archive
        loc_e.amount = instance.amount
        loc_e.remainder = loc.remainder_amount
        loc_e.cancelled = True
        loc_e.date_created = datetime.datetime.now()
        loc_e.creator = request.user
        loc_e.date_modified = datetime.datetime.now()
        loc_e.modified_by = request.user
        loc_e.save()

        # Update main banking or cash from bank event
        event = Events.objects.get(id=instance.type.id)
        chart_oa = Account.objects.filter(transaction_type=event).first()
        chart_oa.balance -= decimal.Decimal(archive.amount)
        chart_oa.save()
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = instance.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()


        event_loc_ = Events.objects.filter(id=loc.type.id).first()
        chart_oa = Account.objects.filter(transaction_type=event_loc_).first()
        chart_oa.balance += decimal.Decimal(archive.amount)
        chart_oa.save()
        coa_event = AccountEvent(
            account = chart_oa,
            transaction = archive,
            amount = instance.amount,
            coa_balance = chart_oa.balance,
            month = Month.objects.get(id=request.parameters["settings"]["financial_month"]),
            financial_year = Periods.objects.get(id=request.parameters["settings"]["financial_period"]),
            date_created = datetime.datetime.now(),
            creator = request.user
        )
        coa_event.save()
    return redirect(reverse('loc_details', args=(loc.id,) ))









from parameters.models import LOC, LOCArchive, LOCEventArchive



from decimal import Decimal
from chartsoa.models import Budget, Estimates
from django.db.models import Q
from chartsoa.models import Account
from parameters.models import Events





from parameters.models import LOC, LOCArchive, LOCEventArchive


def cashbook(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    transactions = LOCEvent.objects.filter(financial_month=month).select_related( 'slip', 'payment_method', 'transaction', 'loc').order_by('id')
    try:
        bank = LOC.objects.get(Q(is_bank=True) & Q(is_active=True))
    except LOC.DoesNotExist:
        messages.success(request, 'The Archives necessary to generate a cashook are not available. Possible reason are EOM operations are not done for the specific month')

    try:
        cash = LOC.objects.get(Q(is_cash=True) & Q(is_active=True))
    except LOC.DoesNotExist:
        messages.success(request, 'The Archives necessary to generate a cashook are not available. Possible reason are EOM operations are not done for the specific month')


    dr_bank_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_bank=True) & Q(cancelled=False)).aggregate(Sum('transaction__dr_amount'))
    cr_bank_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_bank=True) & Q(cancelled=False)).aggregate(Sum('transaction__cr_amount'))
    dr_cash_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_cash=True) & Q(cancelled=True)).aggregate(Sum('transaction__dr_amount'))
    cr_cash_total = LOCEvent.objects.filter(Q(financial_month=month) & Q(loc__is_cash=True) & Q(cancelled=False)).aggregate(Sum('transaction__cr_amount'))

    context = {
        'transactions': transactions,
        'bank': bank,
        'cash': cash,
        'year': year,
        'month': month,
        'dr_bank': dr_bank_total,
        'cr_bank': cr_bank_total,
        'dr_cash': cr_cash_total,
        'cr_cash': dr_cash_total

    }
    return render(request, 'chartsoa/cashbook.html', context)






def balancesheet(request):
    if request.method == 'POST':
        year = request.POST.get('financial_year')
        month = request.POST.get('financial_month')
        month = Month.objects.get(id=month)
        year = Periods.objects.get(id=year)

        budget = Budget.objects.filter(id=1)[0]
        estimates = Estimates.objects.filter(budget=budget).select_related('event')

        current_assets_events = Events.objects.filter(balance_sheet_item='c_assets')
        current_assets = []

        for event in current_assets_events:
            received = GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount'))
            budgeted = Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first()

            e = {
                'event': event,
                'received': received,
                'budgeted': budgeted,
                'variance': 0.00
            }

            current_assets.append(e)

        fixed_assets_events = Events.objects.filter(type='f_assets')
        fixed_assets = []

        for event in fixed_assets_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            fixed_assets.append(e)



        current_liablities_events = Events.objects.filter(type='c_liablity')
        current_liablities = []

        for event in current_liablities_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            current_liablities.append(e)

        long_liablities_events = Events.objects.filter(type='l_liablity')
        long_liablities = []

        for event in long_liablities_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            long_liablities.append(e)

        owners_equity_events = Events.objects.filter(type='equity')
        owners_equity = []

        for event in owners_equity_events:
            e = {
                'event': event,
                'received': GroupTransactions.objects.filter(Q(financial_month=month) & Q(type=event)).aggregate(Sum('amount')),
                'budgeted': Estimates.objects.filter(Q(budget=budget) & Q(event=event)).select_related('event').first(),
                'variance': 0.00
            }

            owners_equity.append(e)
        
        

  

        context = {
            'form': ReportForm,
            'current_assets': current_assets,
            'current_liablities': current_liablities,
            'fixed_assets': fixed_assets,
            'long_liablities': long_liablities,
            'owners_equity': owners_equity,


        }
        return render(request, 'reports/balancesheet.html', context)


    context = {
        'form': ReportForm,

    }
    return render(request, 'reports/balancesheet.html', context)



from decimal import Decimal
from chartsoa.models import Budget, Estimates
from django.db.models import Q
from parameters.models import Events


def trial(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    month = Month.objects.get(id=month.id)
    year = Periods.objects.get(id=year.id)

    income_events = Events.objects.filter(Q(trial_balance_placement='cr') & Q(is_trial_balance_account=True))
    income = []

    income_total = Decimal(0.00)
    for event in income_events:
        account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month)).first()
        try:
            account_balance = account.balance
        except AttributeError:
            account_balance =  Decimal(0.00)

        income_total += account_balance
        e = {
            'event': event,
            'account': account,
            }
        income.append(e)

    expenses_events = Events.objects.filter(Q(trial_balance_placement='dr') & Q(is_trial_balance_account=True))
    expenses = []

    expenses_total = Decimal(0.00)
    for event in expenses_events:
        account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month)).first()
        try:
            account_balance = account.balance
        except AttributeError:
            account_balance =  Decimal(0.00)
        expenses_total += account_balance
        e = {
            'event': event,
            'account': account,
            }
        expenses.append(e)

    context = {
        'month': month,
        'year': year,
        'income': income,
        'expenses': expenses,
        'income_total' : income_total,
        'expenses_total' : expenses_total,
    }
    return render(request, 'chartsoa/trial_.html', context)




def surplus_loss(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    month = Month.objects.get(id=month.id)
    year = Periods.objects.get(id=year.id)

    budget = Budget.objects.get(financial_year=year)

    total = Decimal(0.00)
    income_events = Events.objects.filter(is_income=True)

    incomes_list = []
    income_budget_total = Decimal(0.00)
    income_variance_total = Decimal(0.00)
    income_total = Decimal(0.00)

    for event in income_events:
        account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month)).first()
        estimate = Estimates.objects.filter(event=event).first()

        try:
            estimated_amount = estimate.estimate 
        except AttributeError:
            estimated_amount =  Decimal(0.00)

        try:
            account_opening_balance = account.opening_balance
        except AttributeError:
            account_opening_balance = Decimal(0.00)

        try:
            account_balance = account.balance
        except AttributeError:
            account_balance =  Decimal(0.00)

        diff = account_balance - account_opening_balance

        if diff < 0:
            diff = diff * -1
        
        income_total += account_balance
        income_budget_total += estimated_amount
        income_variance_total += estimated_amount - diff

        e = {
            'event': event,
            'account': account,
            'estimate': estimated_amount,
            'balance': diff,
            'variance': estimated_amount - diff
            }
        incomes_list.append(e)


    expenses_events = Events.objects.filter(is_expense=True)

    expenses = []
    expenses_variance_total = Decimal(0.00)
    expenses_budget_total = Decimal(0.00)

    expenses_total = Decimal(0.00)
    for event in expenses_events:
        account = Account.objects.filter(Q(transaction_type=event) & Q(financial_month=month)).first()
        estimate = Estimates.objects.filter(event=event).first()

        try:
            estimated_amount = estimate.estimate 
        except AttributeError:
            estimated_amount =  Decimal(0.00)

        try:
            account_opening_balance = account.opening_balance
        except AttributeError:
            account_opening_balance =  Decimal(0.00)

        try:
            account_balance = account.balance
        except AttributeError:
            account_balance =  Decimal(0.00)

        diff = account_balance - account_opening_balance

        if diff < 0:
            diff = diff * -1

        expenses_total += diff
        expenses_variance_total += estimated_amount - diff
        expenses_budget_total += estimated_amount

        e = {
            'event': event,
            'account': account,
            'estimate': estimated_amount,
            'balance': diff,
            'variance': estimated_amount - diff

            }
        expenses.append(e)

    context = {
        'budget': budget,
        'month': month,
        'year': year,
        'income_list': incomes_list,
        'expenses': expenses,
        'income_total' : income_total,
        'expenses_total' : expenses_total,
        'total': total,
        'budget': budget,
        'month': month,
        'year': year,
        'income': incomes_list,
        'expenses': expenses,
        'income_variance_total' : income_variance_total,
        'income_budget_total' : income_budget_total,
        'income_total' : income_total,
        'expenses_variance_total' : expenses_variance_total,
        'expenses_budget_total' : expenses_budget_total,
        'expenses_total' : expenses_total,
    }
    return render(request, 'chartsoa/surplus&loss.html', context)





