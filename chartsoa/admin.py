from django.contrib import admin

from .models import Invoice, Payments, TypeOfAccount, Account, PaymentMethod, Expenses, AccountArchive, AccountEvent, Budget, Estimates, SecondaryAccount, SecondaryAccountEvent


class ModelAdminTypeOfAccount(admin.ModelAdmin):
    list_display = (
        'title', 'description', 'account_number_range', 'last_assigned','date_created', 'creator', 'date_modified', 'modified_by'
    )
    search_fields = (
        'title', 'description',     )



class AccountModelAdmin(admin.ModelAdmin):
    field = ['parent', 'name', 'account_number', 'type', 'description','opening_balance','balance', 'creator', 'date_created', 'date_modified', 'modified_by']
    list_display = [ 'name', 'account_number', 'transaction_type','opening_balance','balance', 'creator', 'date_created', 'date_modified', 'modified_by']
    ordering = ['parent', 'name', 'account_number', 'type', 'description','opening_balance','balance', 'creator', 'date_created', 'date_modified', 'modified_by']
    icon_name = 'money'

    search_fields =[ 'name', 'account_number', 'description','balance',]
    list_filter = ['parent', 'name', 'account_number', 'type', 'description','opening_balance','balance', 'creator', 'date_created', 'date_modified', 'modified_by']



class AccountArchiveModelAdmin(admin.ModelAdmin):
    field = ['parent', 'name', 'account_number', 'type', 'description','balance', 'creator', 'date_created', 'date_modified', 'modified_by']
    list_display = ['parent', 'name', 'account_number', 'type', 'description','balance', 'creator', 'date_created', 'date_modified', 'modified_by']
    ordering = ['parent', 'name', 'account_number', 'type', 'description','balance', 'creator', 'date_created', 'date_modified', 'modified_by']
    icon_name = 'money'

    search_fields =['parent', 'name', 'account_number', 'description','balance', ]
    list_filter = ['parent', 'name', 'account_number', 'type', 'description','balance', 'creator', 'date_created', 'date_modified', 'modified_by']



class AccountEventModelAdmin(admin.ModelAdmin):
    field = ['account', 'transaction', 'amount', 'coa_balance', 'month','financial_year', 'creator', 'date_created', 'date_modified', 'modified_by']
    list_display = ['id','account', 'transaction', 'amount', 'coa_balance', 'month','financial_year', 'creator', 'date_created', 'date_modified', 'modified_by']
    ordering = ['account', 'transaction', 'amount', 'coa_balance', 'month','financial_year', 'creator', 'date_created', 'date_modified', 'modified_by']
    icon_name = 'money'

    search_fields =['id',  'amount', ]
    list_filter = ['account', 'transaction', 'amount', 'coa_balance', 'month','financial_year', 'creator', 'date_created', 'date_modified', 'modified_by']


admin.site.register(TypeOfAccount, ModelAdminTypeOfAccount)
admin.site.register(Account, AccountModelAdmin)
admin.site.register(AccountArchive, AccountArchiveModelAdmin)
admin.site.register(PaymentMethod)
admin.site.register(SecondaryAccount, AccountModelAdmin)
admin.site.register(SecondaryAccountEvent, AccountEventModelAdmin)
# admin.site.register(Expenses)
admin.site.register(AccountEvent, AccountEventModelAdmin)
admin.site.register(Budget)
admin.site.register(Estimates)