from django.db import models
from django.utils.translation import ugettext_lazy as _


class TypeOfAccount(models.Model):
    title = models.CharField(_("Type Name"), max_length=50)
    description = models.TextField(_("Type Description"))
    account_number_range = models.IntegerField(_("Account Number Range"), default=0)
    last_assigned = models.IntegerField(_("Account Number Range"), default=0)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Type Of Account Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Type Of Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)


    def __str__(self):
        return "{}".format(self.title)

t_types = (
    ("deposit_savings","Savings" ),
    ("pay_loan","Loan Repayment" ),
    ("share_capital","Share Capital" ),
    ("loan_interest","Interest On Member Loans" ),
    ("entrance_fee","Entrance Fee"),
    ("loan_form", "Loan Form"),
    ("sumndry", "Sumndry Debtors"),
    ("bbf","BBF" ),

)

class Account(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    parent = models.ForeignKey("chartsoa.Account", verbose_name=_("Parent Account"), null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(_("Account Name"), max_length=50)
    account_number = models.CharField(_("Account Number"), null=True, blank=True, max_length=50)
    type = models.ForeignKey("chartsoa.TypeOfAccount", verbose_name=_("Account Type"), on_delete=models.CASCADE)
    # transaction_type = models.CharField(_("Transaction Type"), choices=t_types, max_length=50, null=True, blank=True)
    transaction_type = models.ForeignKey("parameters.Events", verbose_name=_(" Event Type"), on_delete=models.DO_NOTHING, null=True)
    description = models.TextField(_("Description"))
    year_opening_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    balance = models.DecimalField(_("Account Remainder"), max_digits=20, decimal_places=2, default=0.00)
    opening_balance = models.DecimalField(_("Account Opening Balance"), max_digits=20, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} - {}".format(self.name, self.account_number, self.type)


    @property
    def variance(self):
        return self.balance - self.opening_balance


    class Meta:
        verbose_name = "Accounts"
        verbose_name_plural = 'Accounts'

class AccountArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    parent = models.ForeignKey("chartsoa.Account", verbose_name=_("Parent Account"), null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(_("Account Name"), max_length=50)
    account_number = models.CharField(_("Account Number"), null=True, blank=True, max_length=50)
    type = models.ForeignKey("chartsoa.TypeOfAccount", verbose_name=_("Account Type"), on_delete=models.CASCADE)
    # transaction_type = models.CharField(_("Transaction Type"), choices=t_types, max_length=50, null=True, blank=True)
    transaction_type = models.ForeignKey("parameters.Events", verbose_name=_(" Event Type"), on_delete=models.DO_NOTHING, null=True)
    description = models.TextField(_("Description"))
    balance = models.DecimalField(_("Account Remainder"), max_digits=20, decimal_places=2, default=0.00)
    opening_balance = models.DecimalField(_("Account Opening Balance"), max_digits=20, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} - {}".format(self.name, self.account_number, self.type)


    class Meta:
        verbose_name = "Accounts Archive"
        verbose_name_plural = 'Accounts Archive'

class AccountEvent(models.Model):
    account = models.ForeignKey("chartsoa.Account", verbose_name=_("Chart Of Account"), on_delete=models.DO_NOTHING)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_("Transaction"), on_delete=models.DO_NOTHING)
    amount = models.DecimalField(_("Amount Transacted"), max_digits=10, decimal_places=2, null=True, blank=True)
    coa_balance = models.DecimalField(_("Chart Of Account Balance"), max_digits=10, decimal_places=2, default=0.00)
    month = models.ForeignKey("parameters.Month", verbose_name=_("Active Month"), on_delete=models.DO_NOTHING, default=None)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING, default=None)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

class AccountEventArchive(models.Model):
    account = models.ForeignKey("chartsoa.AccountArchive", verbose_name=_("Chart Of Account"), on_delete=models.DO_NOTHING)
    transaction = models.ForeignKey("financial.GroupTransactions", verbose_name=_("Transaction"), on_delete=models.DO_NOTHING)
    amount = models.DecimalField(_("Amount Transacted"), max_digits=10, decimal_places=2, null=True, blank=True)
    coa_balance = models.DecimalField(_("Chart Of Account Balance"), max_digits=10, decimal_places=2, default=0.00)
    month = models.ForeignKey("parameters.Month", verbose_name=_("Active Month"), on_delete=models.DO_NOTHING, default=None)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING, default=None)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)




class PaymentMethod(models.Model):
    account = models.ForeignKey("chartsoa.Account", verbose_name=_("Chart of Account"), default=None, on_delete=models.CASCADE)
    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line Of Credit"), null=True, blank=True, on_delete=models.DO_NOTHING)
    is_cash = models.BooleanField(_("Is Cash"), default=False)
    is_bank = models.BooleanField(_("Is Bank"), default=False)
    name = models.CharField(_("Payment Method Title"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Method Creator+",verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", related_name="Payment Method Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.name)

decs = (
    ("Rent Payment", "Rent Payment"),
    ("Damage", "Damage")
)
class Payments(models.Model):
    
    amount = models.IntegerField(_("Amount Paid"), default="0")
    date = models.DateField(_("Payment Date"), null=True, blank=True,  auto_now=False, auto_now_add=False)
    invoice = models.ForeignKey("chartsoa.Invoice", verbose_name=_("Invoice"), null=True, blank=True, on_delete=models.CASCADE)
    description = models.CharField(_("Description Of the Payment"), choices=decs,  default="Rent Payment", max_length=50)
    paymentmethod = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Payment Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)


class Invoice(models.Model):
    amount = models.IntegerField(_("Amount Paid"), default=0)
    invoice_number = models.IntegerField(_("Invoice Number"), default=0)
    paymentmethod = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Payment Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

class Expenses(models.Model):
    amount = models.IntegerField(_("Amount"), default="0")
    account = models.ForeignKey("chartsoa.Account", verbose_name=_("Account"), on_delete=models.CASCADE)
    clas = models.ForeignKey("chartsoa.Account", related_name="Exepnse Class+", null=True, blank=True, verbose_name=_("Class"), on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Payment Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)


class Paymentmethods(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=250, blank=True, null=True)
    is_cash = models.BooleanField(_("Is Cash"), default=False)
    is_bank = models.BooleanField(_("Is Bank"), default=False)
    pending = models.BooleanField(blank=True, null=True)
    accountnumber = models.CharField(db_column='accountNumber', max_length=32, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PaymentMethods'


class Linkbranchespaymentmethods(models.Model):
    branch_id = models.IntegerField()
    payment_method = models.ForeignKey('chartsoa.Paymentmethods', models.DO_NOTHING)
    deleted = models.BooleanField()
    date = models.DateTimeField(blank=True, null=True)
    account_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'LinkBranchesPaymentMethods'



class Fundinglineevents(models.Model):
    code = models.CharField(max_length=200)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    direction = models.SmallIntegerField()
    fundingline = models.ForeignKey('chartsoa.Fundinglines', models.DO_NOTHING)
    deleted = models.BooleanField()
    creation_date = models.DateTimeField()
    type = models.SmallIntegerField()
    user_id = models.IntegerField(blank=True, null=True)
    contract_event_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'FundingLineEvents'


class Fundinglines(models.Model):
    name = models.CharField(max_length=50)
    begin_date = models.DateTimeField()
    end_date = models.DateTimeField()
    amount = models.DecimalField(max_digits=18, decimal_places=0)
    purpose = models.CharField(max_length=50)
    deleted = models.BooleanField()
    currency = models.ForeignKey('extras.Currencies', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'FundingLines'

class Exoticinstallments(models.Model):
    number = models.IntegerField(primary_key=True)
    principal_coeff = models.FloatField()
    interest_coeff = models.FloatField(blank=True, null=True)
    exotic = models.ForeignKey('extras.Exotics', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'ExoticInstallments'
        unique_together = (('number', 'exotic'),)




class Budget(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Period"), on_delete=models.DO_NOTHING)
    total = models.DecimalField(_("Total"), max_digits=20, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Payment Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)


    def __str__(self):
        return "{}".format(self.financial_year)

    class Meta:
        verbose_name = "Budget"
        verbose_name_plural = 'Budget'




class Estimates(models.Model):
    budget = models.ForeignKey("chartsoa.Budget", verbose_name=_("Budget"), on_delete=models.DO_NOTHING)
    event = models.ForeignKey("parameters.Events", verbose_name=_("Events"), on_delete=models.DO_NOTHING)
    estimate = models.DecimalField(_("Estimate"), max_digits=20, decimal_places=2)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Item Created By+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    date_created = models.DateField(_("Date Created"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{} - {} -{}".format(self.budget, self.event, self.estimate)

    class Meta:
        verbose_name = "Budget Estimates"
        verbose_name_plural = 'Budget Estimates'




class SecondaryAccount(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    parent = models.ForeignKey("chartsoa.SecondaryAccount", verbose_name=_("Parent Secondary Account"), null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(_("Account Name"), max_length=50)
    account_number = models.CharField(_("Account Number"), null=True, blank=True, max_length=50)
    type = models.ForeignKey("chartsoa.TypeOfAccount", verbose_name=_("Account Type"), on_delete=models.CASCADE)
    transaction_type = models.ForeignKey("parameters.Events", verbose_name=_(" Event Type"), on_delete=models.DO_NOTHING, null=True)
    description = models.TextField(_("Description"))
    balance = models.DecimalField(_("Account Remainder"), max_digits=20, decimal_places=2, default=0.00)
    opening_balance = models.DecimalField(_("Account Opening Balance"), max_digits=20, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} - {}".format(self.name, self.account_number, self.type)


    class Meta:
        verbose_name = " Secondary Account"
        verbose_name_plural = 'Secondary Accounts'

class SecondaryAccountArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    parent = models.ForeignKey("chartsoa.SecondaryAccount", verbose_name=_("Parent Account"), null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(_("Account Name"), max_length=50)
    account_number = models.CharField(_("Account Number"), null=True, blank=True, max_length=50)
    type = models.ForeignKey("chartsoa.TypeOfAccount", verbose_name=_("Account Type"), on_delete=models.CASCADE)
    transaction_type = models.ForeignKey("parameters.Events", verbose_name=_(" Event Type"), on_delete=models.DO_NOTHING, null=True)
    description = models.TextField(_("Description"))
    balance = models.DecimalField(_("Account Remainder"), max_digits=20, decimal_places=2, default=0.00)
    opening_balance = models.DecimalField(_("Account Opening Balance"), max_digits=20, decimal_places=2, default=0.00)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} - {}".format(self.name, self.account_number, self.type)


    class Meta:
        verbose_name = "Secondary Accounts Archive"
        verbose_name_plural = 'Secondary Accounts Archive'

class SecondaryAccountEvent(models.Model):
    account = models.ForeignKey("chartsoa.SecondaryAccount", verbose_name=_("Chart Of Account"), on_delete=models.DO_NOTHING)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_("Transaction"), on_delete=models.DO_NOTHING)
    amount = models.DecimalField(_("Amount Transacted"), max_digits=10, decimal_places=2, null=True, blank=True)
    coa_balance = models.DecimalField(_("Chart Of Account Balance"), max_digits=10, decimal_places=2, default=0.00)
    month = models.ForeignKey("parameters.Month", verbose_name=_("Active Month"), on_delete=models.DO_NOTHING, default=None)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING, default=None)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)

class SecondaryAccountEventArchive(models.Model):
    account = models.ForeignKey("chartsoa.SecondaryAccount", verbose_name=_("Chart Of Account"), on_delete=models.DO_NOTHING)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_("Transaction"), on_delete=models.DO_NOTHING)
    amount = models.DecimalField(_("Amount Transacted"), max_digits=10, decimal_places=2, null=True, blank=True)
    coa_balance = models.DecimalField(_("Chart Of Account Balance"), max_digits=10, decimal_places=2, default=0.00)
    month = models.ForeignKey("parameters.Month", verbose_name=_("Active Month"), on_delete=models.DO_NOTHING, default=None)
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Financial Year"), on_delete=models.DO_NOTHING, default=None)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Account  Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False, blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Account Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)



class DirectLedgerPosting(models.Model):
    reason = models.ForeignKey("parameters.Events", verbose_name=_("Reason"), related_name="Reason+", on_delete=models.DO_NOTHING, null=True)
    affecting = models.ForeignKey("parameters.Events", verbose_name=_(" Ledger Affected"), related_name="Affecting+", on_delete=models.DO_NOTHING, null=True)
    amount = models.DecimalField(_("Amount "), max_digits=5, decimal_places=2)

