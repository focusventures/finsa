from django.shortcuts import render
from shares.models import ShareCapital, ShareCapitalEvents
from members.models import Member

def home(request):
    return render(request, 'shares/home.html')


def distribution(request):
    return render(request, 'shares/distriibution.html')



def formulae(request):
    return render(request, 'shares/formulae.html')


def accounts(request):
    accounts = ShareCapital.objects.all().select_related("member")

    context = {
        "accounts": accounts
    }
    return render(request, 'shares/accounts.html', context)



def accounts_movement(request, pk):
    member = Member.objects.get(id=pk)
    account = ShareCapital.objects.filter(member=pk)
    events = ShareCapitalEvents.objects.filter(share_acc__member__id=member.id)

    context = {
        "account": account,
        "member": member,
        "events": events
    }
    return render(request, 'shares/account_details.html', context)


def dividends(request):
    return render(request, 'shares/dividends.html')