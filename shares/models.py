from django.db import models
from django.utils.translation import ugettext_lazy as _

class ShareCapital(models.Model):
    account = models.CharField(_("Shares capital Account"), max_length=50, null=True, blank=True)
    share_bal = models.DecimalField(_("Share Capital"), max_digits=20, decimal_places=2, default=0.00)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "Share Capital"
        verbose_name_plural = "Share Capital"



    def __str__(self):
        return "%s %s" % (self.member, self.share_bal)


activities = (
    ("Deposit", "Deposit"),
    ("Transfer", "Transfer"),
    ("Loan Repayment", "Loan Repayment"),
)

class ShareCapitalEvents(models.Model):
    share_acc = models.ForeignKey("shares.ShareCapital", verbose_name=_("Share Capital Account"), on_delete=models.CASCADE)
    activity = models.CharField(_("Type Of Activity"), choices=activities, max_length=50)
    share_bal = models.DecimalField(_("Share Capital"), max_digits=10, decimal_places=2)
    member = models.ForeignKey("members.Member", verbose_name=_("Mmember"), on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)


    class Meta:
        verbose_name = "Share Capital Events"
        verbose_name_plural = "Share Capital Events"



    def __str__(self):
        return "%s %s %s" % (self.share_acc, self.activity, self.share_bal)

