from django.urls import path

from . import views


urlpatterns = [
    path('', views.home, name="share_home"),
    path('distribution', views.distribution, name="share_distribution"),
    path('accounts', views.accounts, name="share_accounts"),
    path('accounts_movement/<int:pk>', views.accounts_movement, name="share_accounts_movement"),
    path('dividends', views.dividends, name="dividends"),

]