from django.apps import AppConfig


class SharesConfig(AppConfig):
    name = 'shares'
    icon_name = 'book'
