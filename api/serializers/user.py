from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets
from authentication.models import CustomUser

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['url', 'username', 'email', 'is_staff']


