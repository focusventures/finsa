from django.db.models import Sum
from django.http import JsonResponse
from django.contrib.auth.models import User
from authentication.models import CustomUser
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny

from django.shortcuts import get_object_or_404
from django.db.models import Q

from rest_framework.decorators import action, permission_classes, api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import  IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework import viewsets

from django.db.models import Count

import json 

from authentication.models import CustomUser
from ..serializers import *

from rest_framework_jwt.settings import api_settings

from django.forms.models import model_to_dict

from rest_framework.permissions import AllowAny
from rest_framework.views import APIView


@api_view(['GET'])
def myProfile(request):
    profile = CustomUser.objects.get(phone_number=request.user)
    return Response(json.loads(profile))

@api_view(['GET'])
def currentUser(request):
    profile = CustomUser.objects.get(phone_number=request.user)
    profileData = UserSerializer(profile, context={'request': request})
    return Response(profileData.data)

@api_view(['GET'])
def balance(request):
    profile = CustomUser.objects.get(phone_number=request.user)
    return Response(profile.get_user_balance(request))

@api_view(['POST'])
def register(request):
    if request.method == 'POST': 
        data = json.loads(request.body)

        username = data['username']
        phone_number = data['phoneNumber']
        email = data['email']
        password = data['password']
     
        user = CustomUser.objects.create_user(         
            phone_number=phone_number,           
            password = password,
            username = username,
            alias = username

            )
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
       
        return Response({"token":token})




class Logout(APIView):
    def post(self, request, format=None):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)


def index(request):
    return Response("Hello Index")