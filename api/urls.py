from django.urls import path, include
from authentication.models import CustomUser
from rest_framework import routers, serializers, viewsets
from .viewsets import *

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)




urlpatterns = [
    path('', include(router.urls)),
    path(r'profile', currentUser, name='customer-profile'),
    path(r'register', register, name='customer-register'),
    path(r'token/', obtain_jwt_token, name='token_obtain_pair'),
    path(r'token/refresh/', refresh_jwt_token, name='token_refresh'),
    path(r'token/verify/', verify_jwt_token, name='token_verify'),
    path(r'logout', Logout.as_view()),
]
