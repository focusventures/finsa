from django.apps import AppConfig


class MembersConfig(AppConfig):
    name = 'members'
    icon_name = 'people'



    def ready(self):
        import members.signals



