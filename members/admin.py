from django.contrib import admin

# Register your models here.
from django.contrib.sites.models import Site
admin.site.unregister(Site)

from django.contrib.auth.models import User, Group
# admin.site.unregister(Group)
from .models import Member, MemberGroup



from utils.mixins.export import ExportCsvMixin
class MemberInline(admin.TabularInline):
    '''Tabular Inline View for '''

    model = Member
    # min_num = 3
    # max_num = 20
    # extra = 1
    # raw_id_fields = (,))

class MemberModelAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = [
        'first_name', 'middle_name', 'last_name','account_number', 'business_name' , 'id_number', 'phone_number', 'current_occupation',  'sex', 'birth_date', 'nationality', 'avatar','id_copy','other_document',
    ]
    order = ['date_created',]
    icon_name = 'people'
    search_fields = [
        'first_name','account_number', 'middle_name', 'last_name', 'business_name' , 'id_number', 'phone_number', 'current_occupation',  'sex', 

    ]
    list_filter = [
        'first_name', 'middle_name', 'last_name', 'business_name' , 'id_number', 'phone_number', 'current_occupation',  'sex', 

    ]
    inline = [ MemberInline]
    list_per_page = 15

    actions = ["export_as_csv"]






    # fieldsets = (
    #     (
    #         ' Bio', {
    #             'fields': (('first_name', 'middle_name', 'last_name',), 'id_number')
    #         }
    #     ),

    #     (
    #         'Contact Info', {
    #             'fields': (('phone_number', 'tell_phone',),)
    #         }
    #     ),
    #     (
    #         'Group Info', {
    #             'fields': (('group',))
    #         }
    #     ),
    #     (
    #         'Economic Info', {
    #             'fields': (('economic_activity',))
    #         }
    #     ),
    #     (
    #         'Address Info', {
    #             'fields': (('city','town','streetName',),)
    #         }
    #     ),
    #     (
    #         'File & Pictures Info', {
    #             'fields': (('avatar','id_copy','other_document',),)
    #         }
    #     ),
    # )







admin.site.register(Member, MemberModelAdmin)

class MemberGroupInline(admin.TabularInline):
    '''Tabular Inline View for '''

    model = MemberGroup
    
class MemberGroupModelAdmin(admin.ModelAdmin, ExportCsvMixin):
    list_display = ['id','group_name', 'county', 'town','sub_county', 'date_created', 'creator', 'date_modified', 'modified_by' ]
    ordering = ['id']
    icon_name = 'group'

    search_fields = [
      'group_name', 'county', 'town','sub_county', 

    ]
    list_filter = [
        'group_name', 'county', 'town','sub_county', 'date_created', 'creator', 'date_modified', 'modified_by'

    ]


    fieldsets = (
        (
            'Group Details', {
                'fields': ('group_name',)
            }
        ),

        (
            'Location Info', {
                'fields': (('county', 'town','sub_county',),)
            }
        ),
       
        (
            'System Specific Info', {
                'fields': (( 'creator', 'modified_by',),)
            }
        ),
    )

    inline = [ MemberGroupInline]
    list_per_page = 15

    actions = ["export_as_csv"]

    
admin.site.register(MemberGroup, MemberGroupModelAdmin)
