from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Member, MemberGroup
from dashboard.models import SnapShot


@receiver(post_save, sender=Member)
def update_snapshot_on_save(sender, instance, created, **kwargs):
    print("ppppppppppppppppppppppppppppppppppp")
    for i in range(20):
        print("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")

    if created:
        snapshot = SnapShot.objects.all()[0]
        snapshot.members += 1
        snapshot.active_members += 1
        snapshot.save()




@receiver(post_delete, sender=Member)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.members -= 1
    snapshot.active_members -= 1
    snapshot.save()



@receiver(post_save, sender=MemberGroup)
def update_snapshot_on_save(sender, instance, created, **kwargs):

    if created:
        snapshot = SnapShot.objects.all()[0]
        snapshot.member_groups += 1
        snapshot.save()




@receiver(post_delete, sender=MemberGroup)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.member_groups -= 1
    snapshot.save()