from django.urls import path

from . import views



urlpatterns = [
    path('seacrh', views.search, name="search"),
    path('list', views.list, name="members_list"),
    path('details/<int:pk>', views.details, name="members_details"),
    path('aoe', views.aoe, name="members_aoe"),
    path('edit/<int:pk>', views.edit, name="members_edit"),

    path('groups', views.grouplist, name="groups_list"),
    path('group_details/<int:pk>', views.groupdetails, name="groups_details"),
    path('group_aoe', views.groupaoe, name="groups_aoe"),
    path('group_edit/<int:pk>', views.groupedit, name="groups_edit"),
    path('group_edit/<int:pk>', views.groupedit, name="groups_delete"),
    path('group_remove_member/<int:group>/<int:pk>', views.group_remove_member, name="group_remove_member"),
    
    path('economic_activities', views.econlist, name="econ_act_list"),
    path('economic_activity/aoe', views.econaoe, name="econ_act_aoe"),
    path('economic_activity/edit/<int:pk>', views.econedit, name="econ_act_edit"),


    path('search_member', views.search_member, name="search_member")
]
