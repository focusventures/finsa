# Generated by Django 3.0.4 on 2020-03-31 02:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_membergroup'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='members.MemberGroup', verbose_name='MemberGroup'),
        ),
    ]
