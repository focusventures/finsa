from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from .models import Member, MemberGroup, EconomicActivity

class MemberForm(ModelForm):
    date_of_birth = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))
    phone_number = forms.RegexField(regex=r'^\+?1?\d{9,10}$',
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': 'phone number'}),
                                        help_text=(
                                        "Phone number must be entered in the format: ''. Up to 10 digits allowed. format is 0710121314"))

    class Meta:
        model = Member
        fields = ['first_name', 'middle_name', 'last_name','avatar','id_copy','other_document','group','economic_activity', 'gender', 'marrital_status', 'email', 'date_of_birth','account_number',
        'phone_number', 'id_number', 'current_occupation', 'tell_phone', 'witness', 'next_of_address', 'next_of_id_number', 'next_of_relation', 'next_of_fullname',
       'city', 'streetName', 'house', 'roomNumber', 'county', 'town', 'branch', 'payroll_no', 'department', 'employer', 'date_of_employment']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(MemberForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['id_number'].required = False
        self.fields['phone_number'].required = False
        self.fields['account_number'].required = True
        self.helper.layout = Layout(
            Row(
                Column('first_name', css_class='form-group col-md-4 mb-0'),
                Column('middle_name', css_class='form-group col-md-4 mb-0'),
                Column('last_name', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('phone_number', css_class='form-group col-md-4 mb-0'),
                Column('id_number', css_class='form-group col-md-4 mb-0'),
                                Column('account_number', css_class='form-group col-md-4 mb-0'),

              
                css_class='form-row'
            ),

                      Row(
                Column('email', css_class='form-group col-md-6 mb-0'),
                Column('date_of_birth', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
              Row(
                Column('gender', css_class='form-group col-md-4 mb-0'),
                Column('marrital_status', css_class='form-group col-md-4 mb-0'),

                css_class='form-row'
            ),

               Row(
                Column('avatar', css_class='form-group col-md-4 mb-0 no-print'),
                Column('id_copy', css_class='form-group col-md-4 mb-0 no-print'),
                Column('other_document', css_class='form-group col-md-4 mb-0 no-print'),

                css_class='form-row'
            ),
       
          
            Row(
                Column('branch', css_class='form-group col-md-6 mb-0'),
                        Column('group', css_class='form-group col-md-6 mb-0'),

              
                css_class='form-row'
            ),
         
        
        
            TabHolder(




                Tab('Employment Information',
                    Row(
                        Column('employer', css_class='form-group col-md-6 mb-0'),
                                                Column('department', css_class='form-group col-md-6 mb-0'),

                        css_class='form-row '
                        ),
                   
                    Row(
                        Column('date_of_employment', css_class='form-group col-md-6 mb-0'),
                                                Column('payroll_no', css_class='form-group col-md-6 mb-0'),

              
                        css_class='form-row'
                    ),

                 

                      Row(
                Column('current_occupation', css_class='form-group col-md-12 mb-0'),

                                Column('economic_activity', css_class='form-group col-md-6 mb-0'),

        
              
                css_class='form-row'
            ),
                ),



                 Tab('Residence Information',
                     Row(
                Column('county', css_class='form-group col-md-4 mb-0'),
                Column('city', css_class='form-group col-md-4 mb-0'),
                Column('town', css_class='form-group col-md-4 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('streetName', css_class='form-group col-md-4 mb-0'),
                Column('house', css_class='form-group col-md-4 mb-0'),
                Column('roomNumber', css_class='form-group col-md-4 mb-0'),
                
              
                css_class='form-row'
            ),
                ),

                
                    Tab('Next Of Kin Information',
                    
                    Row(
                        Column('next_of_fullname', css_class='form-group col-md-8 mb-0'),
                        Column('next_of_relation', css_class='form-group col-md-4 mb-0'),

                        css_class='form-row'
                    ),

                    Row(
                        Column('next_of_id_number', css_class='form-group col-md-6 mb-0'),
                        Column('next_of_address', css_class='form-group col-md-6 mb-0'),

                        css_class='form-row'
                    ), 

                    Row(
                        Column('witness', css_class='form-group col-md-6 mb-0'),

                        css_class='form-row'
                    ),     
                  
                    ),

                css_class='bg-info'
            ),
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right no-print'
            ),
         
            
        )








    
        #     TabHolder(
        #         Tab('Product Info',
        #             Row(
        #                 Column('product_title', css_class='form-group col-md-4 mb-0'),
        #                 css_class='form-row '
        #                 ),
        #             Row(
        #                 Column('installment_type', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             Row(
        #                 Column('annual_interest_rate', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),

        #             Row(
        #                 Column('periodicity', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('payment_plan', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             ),
        #         Tab('Fees Info',
                    
        #             Row(
        #                 Column('loan_processing_fee', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),

                    
                  
        #             ),
        #         Tab('Loan Commissions Info',
        #             Row(
        #                 Column('periodicity', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('payment_plan', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
                   
        #             ),
        #         Tab('Line Of Credit Info',
        #             Row(
        #                 Column('loc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
                    
        #             ),

        #          Tab('Accounting Info',
        #             Row(
        #                 Column('principal_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('interest_due_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),

        #             Row(
        #                 Column('interest_income_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('accrued_penalty_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             Row(
        #                 Column('penalty_income_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
        #             Row(
        #                 Column('interest_due_not_received', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
        #             Row(
        #                 Column('interest_accrued_acc', css_class='form-group col-md-6 mb-0'),
              
        #                 css_class='form-row'
        #             ),
            
            
                   


                   
        #             ),
        #     ),
        #     ButtonHolder(
        #         Submit('submit', 'Addd Product',
        #                css_class='float-right btn-warning mr-3')
        #     )

    

































class MemberGroupForm(ModelForm):
    meeting_day = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))
    class Meta:
        model = MemberGroup
        fields = ['group_name', 'county', 'town', 'sub_county', 'branch', 'group_officer', 'meeting_day']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(MemberGroupForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('group_name', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Column('branch', css_class='form-group col-md-6 mb-0'),
                Column('group_officer', css_class='form-group col-md-6 mb-0'),

              
                css_class='form-row'
            ),
            Row(
                Column('county', css_class='form-group col-md-6 mb-0'),
                Column('sub_county', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
             Row(
                Column('meeting_day', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
            Row(
                Column('town', css_class='form-group col-md-8 mb-0'),
              
                css_class='form-row'
            ),
         

       
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )







class EconomicActivityForm(ModelForm):
    class Meta:
        model = EconomicActivity
        fields = ['name', ]
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(EconomicActivityForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )