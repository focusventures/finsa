from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from .forms import MemberForm, MemberGroupForm, EconomicActivityForm

from .models import Member, MemberGroup, EconomicActivity

from django.db.models import Q
from dashboard.models import SnapShot
from chartsoa.models import Payments, Invoice
from extras.models import Notes, Letters

from extras.forms import NoteForm, LetterForm
from savings.models import CurrentAccount
from members.models import MemberGroup
from loans.models import LoanContract
from savings.models import Savingcontracts
# from projects.models import Projects

from authentication.models import CustomUser
from members.models import MemberGroup
from ostructure.models import Branch
from shares.models import ShareCapital, ShareCapitalEvents

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse


from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.csrf import csrf_exempt
import json



    # page = request.GET.get('page', 1)
    # paginator = Paginator(posts_list, 10) 
  
    # try:
    #     posts = paginator.page(page)

    # except PageNotAnInteger:
    #     posts = paginator.page(1)
    # except EmptyPage:
    #     posts = paginator.page(paginator.num_pages)

@csrf_exempt
@login_required()
def search_member(request):
    if request.method == "POST":
        data = json.loads(request.body)
        members = list(Member.objects.filter(Q(account_number__icontains=data['term']) | Q(id_number__icontains=data['term']) | Q(first_name__icontains=data['term']) | Q(middle_name__icontains=data['term']) | Q(last_name__icontains=data['term']) | Q(phone_number__icontains=data['term'])).select_related('group').values())
        data = { 'members': members, }
        return JsonResponse(data, safe=False)


@login_required()
def search(request):
    members = []
    groups = []
    users = []
    branches = []

    if request.method == "GET":
        term = request.GET['search_term']
        if True:
            members = Member.objects.filter(Q(account_number__icontains=term) | Q(id_number__icontains=term) | Q(first_name__icontains=term) | Q(middle_name__icontains=term) | Q(last_name__icontains=term) | Q(phone_number__icontains=term)).select_related('group')
            groups = MemberGroup.objects.filter(Q(group_name__icontains=term) | Q(county__icontains=term))
            users = CustomUser.objects.filter(Q(phone_number__icontains=term) | Q(username__icontains=term))
            branches = Branch.objects.filter(Q(name__icontains=term))

    context = {
        'term': term,
        'members': members,
        'groups': groups,
        'users': users,
        'branches': branches
    }

    return render(request, 'members/search.html', context)

@login_required()
def list(request):
    members = []
    if True:
        # members = Member.objects.filter(branch__name__icontains="meru")

        members_list = Member.objects.all()


        
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)

        
    else:
        
        members_list = Member.objects.filter(branch__name__icontains="meru")
        page = request.GET.get('page', 1)
        paginator = Paginator(members_list, 12) 
  
        try:
            members = paginator.page(page)

        except PageNotAnInteger:
            members = paginator.page(1)
        except EmptyPage:
            members = paginator.page(paginator.num_pages)
    context = {
        'members': members
    }
    return render(request, 'members/list.html', context)

from financial.models import MemberCustomAccount
import datetime

@login_required()
def aoe(request):
    form = MemberForm()
    if request.method == 'POST':
        form = MemberForm(request.POST, request.FILES)
        # pdb.set_trace()
        if form.is_valid():
            member = form.save(commit=False)
            member.creator = request.user
            member.save()

            acc = CurrentAccount()
            acc.member = member

            acc.acc_number = '010' + str(member.account_number)
            acc.creator = request.user
            acc. acc_balance = 0.00
            acc.save()

            member.current_acc = acc
            member.has_current_acc = True
            member.save()

            sharecapital = ShareCapital()
            sharecapital.member = member
            sharecapital.account = '030' + member.account_number
            sharecapital.share_bal = 0.00
            sharecapital.creator = request.user
            sharecapital.save()

            # bbf_account = MemberCustomAccount()
            # bbf_account.acc_balance = 0.000
            # bbf_account.acc_number = '040-' + member.account_number
            # bbf_account.creator = request.user
            # bbf_account.date_created = datetime.datetime.now()
            # bbf_account.member = member
            # bbf_account.acc_type = 'bbf'
            # bbf_account.save()


            # bbf_account = MemberCustomAccount()
            # bbf_account.acc_balance = 0.00
            # bbf_account.acc_number = '100-' + member.account_number
            # bbf_account.creator = request.user
            # bbf_account.date_created = datetime.datetime.now()
            # bbf_account.member = member
            # bbf_account.acc_type = 'pay_loan'
            # bbf_account.save()


            # bbf_account = MemberCustomAccount()
            # bbf_account.acc_balance = 0.00
            # bbf_account.acc_number = '090-' + member.account_number
            # bbf_account.creator = request.user
            # bbf_account.date_created = datetime.datetime.now()
            # bbf_account.member = member
            # bbf_account.acc_type = 'loan_interest'
            # bbf_account.save()

            snapshot = SnapShot.objects.all()[0]
            snapshot.members += 1
            snapshot.active_members += 1
            snapshot.save()
            return redirect(reverse('members_details', args=(member.id,)))
    else:
       form = MemberForm()
    context = {'form': form}
    return render(request, 'members/aoe.html', context)


@login_required()
def edit(request, pk):
    form = MemberForm()
    if request.method == 'POST':
        form = MemberForm(request.POST, request.FILES, instance=Member.objects.get(id=pk))
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('members_details', args=(pk,)))
    else:
       form = MemberForm(instance=Member.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'members/aoe.html', context)


from savings.models import SavingAccount, CurrentAccount, Savingcontracts
from financial.models import Transactions
from financial.forms import TransactionsForm
from members.models import MemberGroupEvents, MemberGroup
from parameters.models import Events
from loans.models import InternalGuarantors
@login_required()
def details(request, pk):
    member = Member.objects.get(id=pk)
    saving_acc =SavingAccount.objects.filter(member__id__exact=member.id).first()
    current_acc =CurrentAccount.objects.filter(member__id__exact=member.id).first()
    notes = Notes.objects.filter(member__id=pk)
    letters = Letters.objects.filter(member__id=pk)
    loans = LoanContract.objects.filter(member__id__exact=member.id)
    savings = SavingAccount.objects.filter(member__id__exact=member.id)
    savings_contracts = Savingcontracts.objects.filter(member__id=member.id)
    transactions = Transactions.objects.filter(member__id=member.id)
    projects = ""
    loans_group = ""
    groupevents = MemberGroupEvents.objects.filter(member=member)
    group = MemberGroup.objects.filter(id=member.group.id)
    sharecapital = ShareCapital.objects.filter(member=member).first()
    sharecapitalevents = ShareCapitalEvents.objects.filter(member=member)
    sms = ""
    email = ""
    event = ""
    letter_form = LetterForm
    note_form = NoteForm
    events = Events.objects.filter(can_create_accounts=True)
    gurantees = InternalGuarantors.objects.filter(member=member)

    accounts = []

    for e in events:
        accounts.append({
            'event': e,
            'account':MemberCustomAccount.objects.filter(Q(member=member) & Q(acc_type=e)).first()
        })
  

    context = {
        "tenant": member,
        'events': events,
        'accounts': accounts,
        'guarantees': gurantees,
        'saving_acc': saving_acc,
        'current_acc': current_acc,
        'savings': savings,
        'saving_contracts': savings_contracts,
        'requests': loans,
        'transactions': transactions,
        "notes": notes,
        "letters": letters,
        "projects": projects,
        "loans_guaranteed": loans_group,
        "groupsevents": groupevents,
        "group": group,
        "projects": projects,
        "sms": sms,
        "sharecapital": sharecapital,
        'sharecapitalevents':sharecapitalevents,
        "email": email,
        "event": event,
        "letter_form": LetterForm,
        "note_form": note_form,
        "transaction_form": TransactionsForm()
    
    
    }
    
    return render(request, 'members/details.html', context)





@login_required()
def grouplist(request):
    groups_list = []
    if True:
        groups_list = MemberGroup.objects.all().select_related('creator', 'modified_by')
        page = request.GET.get('page', 1)
        paginator = Paginator(groups_list, 12) 
  
        try:
            groups = paginator.page(page)

        except PageNotAnInteger:
            groups = paginator.page(1)
        except EmptyPage:
            groups = paginator.page(paginator.num_pages)

    else:
        
        groups_list = MemberGroup.objects.filter(branch__name__icontains="meru").select_related('creator', 'modified_by')

        page = request.GET.get('page', 1)
        paginator = Paginator(groups_list, 12) 
  
        try:
            groups = paginator.page(page)

        except PageNotAnInteger:
            groups = paginator.page(1)

        except EmptyPage:
            groups = paginator.page(paginator.num_pages)

    context = {
        'groups': groups
    }
    
    return render(request, 'members/groups.html', context)


@login_required()
def groupaoe(request):
    form = MemberForm()
    if request.method == 'POST':
        form = MemberGroupForm(request.POST)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('groups_list'))
    else:
       form = MemberGroupForm()
    context = {'form': form}
    return render(request, 'members/groupaoe.html', context)


@login_required()
def groupedit(request, pk):
    form = MemberGroupForm()
    if request.method == 'POST':
        form = MemberGroupForm(request.POST, instance=MemberGroup.objects.get(id=pk))
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.modified_by = request.user

            bulding.save()
            return redirect(reverse('groups_details', args=(bulding.id,)))
    else:
       form = MemberGroupForm(instance=MemberGroup.objects.get(id=pk))
    context = {'form': form}
    return render(request, 'members/groupaoe.html', context)

@login_required()
def groupdetails(request, pk):
    group = MemberGroup.objects.get(id=pk)
    members = Member.objects.filter(group__id=pk)
    context = {
        "group": group,
        "members": members,
    }
    return render(request, 'members/groupdetails.html', context)


def group_remove_member(request,group, pk):
    member = Member.objects.get(id=pk)
    member.group = None
    member.save()

    return redirect(reverse('groups_details', args=(group,)))


@login_required()
def econlist(request):
    econ_act = EconomicActivity.objects.all()
    context = {
        'activities': econ_act
    }
    
    return render(request, 'members/activities.html', context)


@login_required()
def econaoe(request):
    form = MemberForm()
    if request.method == 'POST':
        form = EconomicActivityForm(request.POST)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()


            return redirect(reverse('econ_act_list'))
    else:
       form = EconomicActivityForm()
    context = {'form': form}
    return render(request, 'members/activityaoe.html', context)


@login_required()
def econedit(request, pk):
    form = EconomicActivityForm()
    activity = EconomicActivity.objects.get(id=pk)
    if request.method == 'POST':
        form = EconomicActivityForm(request.POST, instance=activity)
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('econ_act_list'))
    else:
       form = EconomicActivityForm(instance=activity)
    context = {'form': form}
    return render(request, 'members/activityaoe.html', context)
