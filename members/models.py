from django.db import models
from django.utils.translation import ugettext_lazy as _
from utils.templates import InvoiceTemplate
from stdimage import StdImageField

from django.core.validators import RegexValidator



from authentication.models import CustomUser
from django.db import connection


def get_tenant_specific_upload_folder(instance, filename):
    upload_folder = 'media/{0}/{}1'.format(
        connection.tenant,
        filename
    )
    return upload_folder

counties = (
    ("Mombasa", "Mombasa"),
	("Kwale","Kwale"),
	("Kilifi","Kilifi"),	
	("Tana River",	"Tana River"),
	("Lamu", "Lamu"),	
	("Taita–Taveta","Taita–Taveta"),
	("Garissa",	"Garissa"),
	("Wajir","Wajir"),	
	("Mandera", "Mandera"),
	("Marsabit","Marsabit"),	
	("Isiolo","Isiolo"),	
	("Meru","Meru"),	
	("Tharaka-Nithi",	"Tharaka-Nithi"),
	("Embu","Embu"),	
	("Kitui","Kitui"),	
	("Machakos","Machakos"),	
	("Makueni","Makueni"),
	("Nyandarua","Nyandarua"),	
	("Nyeri","Nyeri"),
	("Kirinyaga","Kirinyaga"),	
	("Murang'a", "Murang'a"),
	("Kiambu","Kiambu"),	
	("Turkana", "Turkana"),
	("West Pokot",	"West Pokot"),
	("Samburu", "Samburu"),
	("Trans-Nzoia","Trans-Nzoia"),
	("Uasin Gishu",	 "Uasin Gishu"),	
	("Elgeyo-Marakwet",	"Elgeyo-Marakwet"),
	("Nandi", "Nandi"),	
	("Baringo", "Baringo"),
	("Laikipia", "Laikipia"),
	("Nakuru", "Nakuru"),	
    ("Narok", "Narok"),
	("Kajiado", "Kajiado"),
	("Kericho", "Kericho"),
	("Bomet", "Bomet"),	 
	("Kakamega","Kakamega"),
	("Vihiga", "Vihiga"),	
	("Bungoma", "Bungoma"),
	("Busia", "Busia"),
	("Siaya", "Siaya"),	 
	("Kisumu",	 "Kisumu"),
	("Homa Bay", "Homa Bay"),	
	("Migori",	 "Migori"),
	("Kisii", "Kisii"),
	("Nyamira",	"Nyamira"),
    ("Nairobi", "Nairobi"),


)


gender = (
    ("MALE", "MALE"),
    ("FEMALE", "FEMALE"),
)

status = (
    ("MARRIED", "MARRIED"),
    ("SINGLE", "SINGLE"),
)

sex = (
    ("Male", "MALE"),
    ("Female", "FEMALE"),
)




def get_tenant_specific_upload_folder_avatar(instance, filename):
    upload_folder = '{0}/avatars/{1}'.format(
        connection.tenant.schema_name,
        filename
    )
    return upload_folder



def get_tenant_specific_upload_folder_signature(instance, filename):
    upload_folder = '{0}/signatures/{1}'.format(
        connection.tenant.schema_name,
        filename
    )
    return upload_folder


def get_tenant_specific_upload_folder_document(instance, filename):
    upload_folder = '{0}/ducoments/{1}'.format(
        connection.tenant.schema_name,
        filename
    )
    return upload_folder



class Member(models.Model):
    phone_regex = RegexValidator(regex=r'^\d{10}$',
                                 message="Phone number must be entered in the format: '0721XXXXXX'. Up to 10 digits allowed.")
    
    first_name = models.CharField(max_length=100)
    sex = models.CharField(max_length=10, choices=sex, default="Female")
    identification_data = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=100)
    birth_date = models.DateTimeField(blank=True, null=True)
    image_path = models.CharField(max_length=500, blank=True, null=True)
    father_name = models.CharField(max_length=200, blank=True, null=True)
    birth_place = models.CharField(max_length=50, blank=True, null=True)
    nationality = models.CharField(max_length=50, blank=True, null=True)
    loan_officer_id = models.IntegerField(blank=True, null=True)
    first_name = models.CharField(_("First Name"), max_length=50, null=True, blank=True,)
    middle_name = models.CharField(_("Middle Name"), max_length=50, null=True, blank=True,)
    last_name = models.CharField(_("Last Name"), max_length=50, null=True, blank=True,)
    account_number = models.CharField(_("Member Number"), max_length=50, null=True, blank=True,)
    phone_number = models.CharField(_("Phone Number"), max_length=50, null=True, blank=True,)
    id_number = models.CharField(_("National Id Number"), max_length=50, null=True, blank=True)
    current_occupation = models.CharField(_("Job Description"), max_length=50, null=True, blank=True,)
    economic_activity = models.ForeignKey("members.EconomicActivity", null=True, verbose_name=_("Economic Activity"), on_delete=models.DO_NOTHING)
    tell_phone = models.CharField(_("Tell Phone"), max_length=50, null=True, blank=True,)
    is_business = models.BooleanField(_("Company"), null=True, blank=True,)
    nature_of_business = models.TextField(_("Nature Of Business"), null=True, blank=True,)
    business_name = models.CharField(_("Company Name"), max_length=50, null=True, blank=True,)
    date_created = models.DateField(_("Date created"), auto_now=True)
    group = models.ForeignKey("members.MemberGroup", verbose_name=_("MemberGroup"), null=True, blank=True, on_delete=models.DO_NOTHING)
    creator = models.ForeignKey("authentication.CustomUser", related_name="Member Modifier+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True, null=True, blank=True)
    modified_by = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Member Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)
    roomNumber = models.CharField(_("Room Number"), max_length=50, null=True, blank=True)
    house = models.CharField(_("House"), max_length=50, null=True, blank=True)
    streetName = models.CharField(_("Street Name"), max_length=50, null=True, blank=True)
    town = models.CharField(_("Town"), max_length=50, null=True, blank=True)
    city = models.CharField(_("City"), max_length=50, null=True, blank=True)
    branch = models.ForeignKey("ostructure.Branch", verbose_name=_("Member Branch"),null=True, on_delete=models.DO_NOTHING)
    county = models.CharField(_("County"), choices=counties, max_length=50, null=True, blank=True)
    state = models.CharField(_("State"), max_length=50, null=True, blank=True)
    has_group_acc = models.BooleanField(_("Has Group Account"), default=False)
    has_project_acc = models.BooleanField(_("Has A Project Account"), default=False)
    has_saving_acc = models.BooleanField(_("Has Saving Account"), default=False)
    has_active_loan = models.BooleanField(_("Has Active Loan"), default=False)
    has_current_acc = models.BooleanField(_("Has A current Account"), default=False)
    saving_acc = models.ForeignKey("savings.SavingAccount", verbose_name=_("Saving Account"), related_name="Savings Account+", on_delete=models.DO_NOTHING, null=True, blank=True)
    current_acc = models.ForeignKey("savings.CurrentAccount", related_name="Current Account+", verbose_name=_("Current Account"), on_delete=models.DO_NOTHING, null=True, blank=True)
    date_of_birth = models.DateField(_("Date Of Birth"), default=None, auto_now=False, auto_now_add=False, null=True, blank=True)
    gender = models.CharField(_("Gender"), choices=gender, default="FEMALE", max_length=50)
    marrital_status = models.CharField(_("Marital Status"), default="SINGLE", choices=status, max_length=50)
    email = models.EmailField(_("Member Email"), null=True, blank=True, max_length=254)
    has_group_acc = models.BooleanField(_("Has Group Account"), default=False)
    has_project_acc = models.BooleanField(_("Has A Project Account"), default=False)
    avatar = StdImageField(_("Member Picture"), upload_to=get_tenant_specific_upload_folder_avatar,
                          variations={'thumbnail': {'width': 300, 'height': 200}}, null=True, blank=True, default='user.jpg')
    
    id_copy = StdImageField(_("Copy of Id"), upload_to=get_tenant_specific_upload_folder_signature,
                          variations={'thumbnail': {'width': 300, 'height': 200}}, null=True, blank=True, default='user.jpg')
    other_document = StdImageField(_("Other Documents"), upload_to=get_tenant_specific_upload_folder_document,
                          variations={'thumbnail': {'width': 100, 'height': 75}}, null=True, blank=True, default='user.jpg')


    payroll_no = models.CharField(_("Payroll Number"), max_length=50, null=True, blank=True)
    date_of_employment = models.DateField(_("Date Of Employemnt"), auto_now=False, auto_now_add=False, null=True, blank=True)
    department = models.CharField(_("Department"), max_length=50, null=True, blank=True)
    employer = models.CharField(_("Employer"), max_length=50, null=True, blank=True)


    next_of_fullname = models.CharField(_("Nominated Next Of Kin Full Name"), max_length=100, null=True, blank=True)
    next_of_relation = models.CharField(_("Relation Ship With Next Of Kin"), max_length=50, null=True, blank=True)
    next_of_id_number = models.CharField(validators=[phone_regex], blank=True, max_length=15, null=True)
    next_of_address = models.CharField(_("Next Of Kin Address"), max_length=50, null=True, blank=True)
    witness = models.CharField(_("Witness"), max_length=50, null=True, blank=True)


    def __str__(self):
        return "%s %s %s %s" % (self.account_number, self.first_name, self.middle_name, self.last_name)

 


    @property
    def get_names(self):
        return "{} {}".format(self.first_name, self.last_name)


    
    def get_savings(self):

        try:
            savings = self.saving_acc.acc_balance

        except AttributeError:
            savings = 0

        return savings

    @property
    def get_building(self):
        return "%s" %('Hello')

    @property
    def get_arrears(self):
        return "%s" %('Hello')

    @property
    def get_overpayments(self):
        return "%s" %('Hello')

    @property
    def get_c_arrears(self):
        return "%s" %('Hello')

    @property
    def get_c_overpayments(self):
        return "%s" %('Hello')

    @property
    def get_unit(self):
        print(self.current_unit)
        return "%s" %(self.current_unit)


    @property
    def get_payment_status(self):
        return "%s" %('Hello')

    






class MemberProjections(models.Model):
    is_current = models.BooleanField(_("Projection is Current"), default=True)
    amount_received = models.FloatField(_("Amount Already Paid"), default=0.0)
    year = models.DateField(_("Year of Projection"), auto_now=False, blank=True, null=True, auto_now_add=False)
    member = models.ForeignKey("members.Member", verbose_name=_("Member Associated"), on_delete=models.CASCADE)
    amount_expected = models.FloatField(_("Amount Expected to be received"), default=0.0)
    previous_projection = models.ForeignKey("members.MemberProjections", blank=True, null=True, verbose_name=_("Proction Before Recalculation"), on_delete=models.DO_NOTHING)
    is_recalculated = models.BooleanField(_("Values Are recalculated"), default=False)
    reason_for_recalculation = models.CharField(_("Reason For Projection Recalculation"), blank=True, null=True, max_length=200)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)



class MemberGroup(models.Model):
    # id = models.IntegerField(_("Id"), primary_key=True)
    group_name = models.CharField(_("Group Name"),  max_length=50)
    county = models.CharField(_("County"), choices=counties, default="Tharaka-Nithi", max_length=50)
    town = models.CharField(_("Town"), default="Chuka", blank=True, null=True, max_length=50)
    sub_county = models.CharField(_("Sub County"),default="Chuka", max_length=50)
    group_officer = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="Group Officer+", verbose_name=_("Group Officer"), on_delete=models.DO_NOTHING)
    meeting_day = models.DateField(_("Meeting Day"), auto_now=False, auto_now_add=False, null=True, blank=True)
    branch = models.ForeignKey("ostructure.Branch", verbose_name=_("Group Branch"),null=True, on_delete=models.DO_NOTHING)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{}".format(self.group_name)


    @property
    def get_names(self):
        return "%s - %s" % (self.group_name, self.branch) 

    



class MemberGroupEvents(models.Model):
    group_name = models.ForeignKey("members.MemberGroup", verbose_name=_("Group"), on_delete=models.DO_NOTHING)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), on_delete=models.DO_NOTHING)
    particulars = models.CharField(_("Description"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)




class EconomicActivity(models.Model):
    name = models.CharField(_("Name"),  max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    def __str__(self):
        return "{}".format(self.name)




class Persongroupbelonging(models.Model):
    person = models.OneToOneField('members.Member', models.DO_NOTHING, primary_key=True)
    group = models.ForeignKey('members.Groups', models.DO_NOTHING)
    is_leader = models.BooleanField()
    currently_in = models.BooleanField()
    joined_date = models.DateTimeField()
    left_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'PersonGroupBelonging'
        unique_together = (('person', 'group'),)


# class Persons(models.Model):
#     id = models.OneToOneField('members.Tiers', models.DO_NOTHING, db_column='id', primary_key=True)
#     first_name = models.CharField(max_length=100)
#     sex = models.CharField(max_length=1)
#     identification_data = models.CharField(max_length=200, blank=True, null=True)
#     last_name = models.CharField(max_length=100)
#     birth_date = models.DateTimeField(blank=True, null=True)
#     activity = models.ForeignKey('members.Economicactivities', models.DO_NOTHING, blank=True, null=True)
#     image_path = models.CharField(max_length=500, blank=True, null=True)
#     father_name = models.CharField(max_length=200, blank=True, null=True)
#     birth_place = models.CharField(max_length=50, blank=True, null=True)
#     nationality = models.CharField(max_length=50, blank=True, null=True)
#     loan_officer_id = models.IntegerField(blank=True, null=True)

#     class Meta:
#         managed = False
#         db_table = 'Persons'


class Personsphotos(models.Model):
    person_id = models.IntegerField()
    picture_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'PersonsPhotos'

class Groups(models.Model):
    id = models.OneToOneField('members.Tiers', models.DO_NOTHING, db_column='id', primary_key=True)
    name = models.CharField(max_length=50)
    establishment_date = models.DateTimeField(blank=True, null=True)
    comments = models.CharField(max_length=500, blank=True, null=True)
    meeting_day = models.IntegerField(blank=True, null=True)
    loan_officer_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Groups'



class Corporateeventstype(models.Model):
    id = models.IntegerField(primary_key=True)
    code = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CorporateEventsType'


class Corporatepersonbelonging(models.Model):
    corporate = models.OneToOneField('members.Corporates', models.DO_NOTHING, primary_key=True)
    person = models.ForeignKey('members.Member', models.DO_NOTHING)
    position = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'CorporatePersonBelonging'
        unique_together = (('corporate', 'person'),)


class Corporates(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField()
    deleted = models.BooleanField()
    sigle = models.CharField(max_length=50, blank=True, null=True)
    small_name = models.CharField(max_length=50, blank=True, null=True)
    volunteer_count = models.IntegerField(blank=True, null=True)
    agrement_date = models.DateTimeField(blank=True, null=True)
    agrement_solidarity = models.BooleanField(blank=True, null=True)
    employee_count = models.IntegerField(blank=True, null=True)
    siret = models.CharField(max_length=50, blank=True, null=True)
    activity = models.ForeignKey('members.EconomicActivity', models.DO_NOTHING, blank=True, null=True)
    date_create = models.DateTimeField(blank=True, null=True)
    fiscal_status = models.CharField(max_length=50, blank=True, null=True)
    registre = models.CharField(max_length=50, blank=True, null=True)
    legalform = models.CharField(db_column='legalForm', max_length=50, blank=True, null=True)  # Field name made lowercase.
    insertiontype = models.CharField(db_column='insertionType', max_length=50, blank=True, null=True)  # Field name made lowercase.
    loan_officer_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Corporates'

class Tiers(models.Model):
    id = models.OneToOneField('members.Corporates', models.DO_NOTHING, db_column='id', primary_key=True)
    client_type_code = models.CharField(max_length=1)
    scoring = models.FloatField(blank=True, null=True)
    loan_cycle = models.IntegerField()
    active = models.BooleanField()
    bad_client = models.BooleanField()
    district = models.ForeignKey('locations.Districts', models.DO_NOTHING)
    city = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True)
    secondary_district = models.ForeignKey('locations.Districts', models.DO_NOTHING, related_name="Secondary Districts+", blank=True, null=True)
    secondary_city = models.CharField(max_length=50, blank=True, null=True)
    secondary_address = models.CharField(max_length=500, blank=True, null=True)
    creation_date = models.DateTimeField(blank=True, null=True)
    home_phone = models.CharField(max_length=50, blank=True, null=True)
    personal_phone = models.CharField(max_length=50, blank=True, null=True)
    secondary_home_phone = models.CharField(max_length=50, blank=True, null=True)
    secondary_personal_phone = models.CharField(max_length=50, blank=True, null=True)
    e_mail = models.CharField(max_length=50, blank=True, null=True)
    secondary_e_mail = models.CharField(max_length=50, blank=True, null=True)
    status = models.SmallIntegerField()
    zipcode = models.CharField(db_column='zipCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    secondary_zipcode = models.CharField(db_column='secondary_zipCode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    branch = models.ForeignKey('ostructure.Branch', models.DO_NOTHING)
    created_by = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING, db_column='created_by')
    currentaccount = models.CharField(db_column='CurrentAccount', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Tiers'



