from django.urls import path

from . import views



urlpatterns = [
    path('list', views.list, name="locations_list"),
    path('details/<int:pk>', views.details, name="locations_details"),
    path('aoe', views.aoe, name="locations_aoe"),
    path('edit/<int:pk>', views.edit, name="locations_edit"),
    path('delete/<int:pk>', views.delete, name="locations_delete"),

]
