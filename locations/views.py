from django.shortcuts import render, redirect, reverse
from .forms import LocationForm
from .models import Location

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def list(request):
    if not request.user.has_perm('locations.location_read'):
        return redirect('members_list')
        
    posts_list = Location.objects.all().select_related( 'sub_county', 'creator','village', 'location', 'sub_location')
    page = request.GET.get('page', 1)
    paginator = Paginator(posts_list, 12) 
  
    try:
        posts = paginator.page(page)

    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    context = {
        "clients": posts
    }
    return render(request, 'locations/list.html', context)



def aoe(request):
    """
    :param request:
    :return: admission form to
    logged in user.
    """

    if request.method == 'POST':
        form = LocationForm(request.POST)
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creator = request.user
            atype.save()
            return redirect(reverse('locations_list'))
    else:
        form = LocationForm()
    context = {'form': form}
    return render(request, 'locations/aoe.html', context)


def edit(request, pk):
    """
    :param request:
    :return: admission form to
    logged in user.
    """
    location = Location.objects.get(id=pk)
    if request.method == 'POST':
        form = LocationForm(request.POST,instance=location )
        if form.is_valid():
            atype = form.save(commit = False)
            atype.creater = request.user
            atype.save()
            return redirect(reverse('locations_list'))
    else:
        form = LocationForm(instance=location)
      
    context = {'form': form}
    return render(request, 'locations/aoe.html', context)

def delete(request, pk):
    object = Location.objects.get(id=pk).delete()
    return redirect('/')


def details(request, pk):
    
    posts = Location.objects.filter(id=pk)
    context = {
        # "shop": shop,
        "account": posts
    }  
    return render(request, 'locations/details.html', context)


