from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Location
from dashboard.models import SnapShot


@receiver(post_save, sender=Location)
def update_snapshot_on_save(sender, instance, created, **kwargs):

    if created:
        snapshot = SnapShot.objects.all()[0]
        snapshot.locations += 1
        snapshot.save()




@receiver(post_save, sender=Location)
def update_snapshot_on_delete(sender, instance, **kwargs):
    snapshot = SnapShot.objects.all()[0]
    snapshot.locations -= 1
    snapshot.save()


