from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.layout import (Layout, Fieldset, Field, Row, Column, Button,
                                 ButtonHolder, Submit, Div)
from .models import Location, Districts, Provinces, City

class LocationForm(ModelForm):
    class Meta:
        model = Location
        fields = ['name','category', 'location', 
        'town', 'county', 'sub_county', 'sub_location', 'village',
       
        
        ]
        exclude = ()  

    def __init__(self, *args, **kwargs):
        super(LocationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-6 mb-0'),
                Column('category', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
             Row(
                Column('county', css_class='form-group col-md-6 mb-0'),
                Column('sub_county', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('location', css_class='form-group col-md-6 mb-0'),
                 Column('sub_location', css_class='form-group col-md-6 mb-0'),

                css_class='form-row'
            ),
          
            Row(
                Column('town', css_class='form-group col-md-6 mb-0'),
             
              
                css_class='form-row'
            ),

             Row(
                Column('village', css_class='form-group col-md-6 mb-0'),
             
              
                css_class='form-row'
            ),
       
            

       
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )




class DistrictForm(ModelForm):
    class Meta:
        model = Districts
        fields = '__all__'



class ProvinceForm(ModelForm):
    class Meta:
        model = Provinces
        fields = '__all__'



class CityForm(ModelForm):
    class Meta:
        model = City
        fields = '__all__'


    