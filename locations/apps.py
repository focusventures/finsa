from django.apps import AppConfig


class LocationsConfig(AppConfig):
    name = 'locations'
    icon_name = 'place' 
    def ready(self):
        import locations.signals
