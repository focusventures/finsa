from django.db import models
from django.utils.translation import ugettext_lazy as _


counties = (
    ("Mombasa", "Mombasa"),
	("Kwale","Kwale"),
	("Kilifi","Kilifi"),	
	("Tana River",	"Tana River"),
	("Lamu", "Lamu"),	
	("Taita–Taveta","Taita–Taveta"),
	("Garissa",	"Garissa"),
	("Wajir","Wajir"),	
	("Mandera", "Mandera"),
	("Marsabit","Marsabit"),	
	("Isiolo","Isiolo"),	
	("Meru","Meru"),	
	("Tharaka-Nithi",	"Tharaka-Nithi"),
	("Embu","Embu"),	
	("Kitui","Kitui"),	
	("Machakos","Machakos"),	
	("Makueni","Makueni"),
	("Nyandarua","Nyandarua"),	
	("Nyeri","Nyeri"),
	("Kirinyaga","Kirinyaga"),	
	("Murang'a", "Murang'a"),
	("Kiambu","Kiambu"),	
	("Turkana", "Turkana"),
	("West Pokot",	"West Pokot"),
	("Samburu", "Samburu"),
	("Trans-Nzoia","Trans-Nzoia"),
	("Uasin Gishu",	 "Uasin Gishu"),	
	("Elgeyo-Marakwet",	"Elgeyo-Marakwet"),
	("Nandi", "Nandi"),	
	("Baringo", "Baringo"),
	("Laikipia", "Laikipia"),
	("Nakuru", "Nakuru"),	
    ("Narok", "Narok"),
	("Kajiado", "Kajiado"),
	("Kericho", "Kericho"),
	("Bomet", "Bomet"),	 
	("Kakamega","Kakamega"),
	("Vihiga", "Vihiga"),	
	("Bungoma", "Bungoma"),
	("Busia", "Busia"),
	("Siaya", "Siaya"),	 
	("Kisumu",	 "Kisumu"),
	("Homa Bay", "Homa Bay"),	
	("Migori",	 "Migori"),
	("Kisii", "Kisii"),
	("Nyamira",	"Nyamira"),
    ("Nairobi", "Nairobi"),
)


level = (
    ("county", 'county'),
    ('sub county', 'sub county'),
    ('location', 'location'),
    ('sub location', 'sub location'),
    ('village', 'Village')
)
class Location(models.Model):
    name = models.CharField(_("Location Name"),  max_length=50)
    sub_county = models.ForeignKey("locations.Location",blank=True, null=True, verbose_name=_("Sub County"), related_name="Sub County+", on_delete=models.DO_NOTHING)
    location = models.ForeignKey("locations.Location", blank=True, null=True, verbose_name=_("Location"), related_name="Location+", on_delete=models.DO_NOTHING)
    sub_location = models.ForeignKey("locations.Location", blank=True, null=True, verbose_name=_("Sub Location"), related_name="Sub Location+",  on_delete=models.DO_NOTHING)
    village = models.ForeignKey("locations.Location",blank=True, null=True, verbose_name=_("Village"), related_name="Village+", on_delete=models.DO_NOTHING)
    category = models.CharField(_("Level of Administration"), choices=level,  max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    town = models.CharField(_("Town"), blank=True, null=True,max_length=50)
    county = models.CharField(_("County"), choices=counties,  max_length=50)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Building Creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=False,  blank=True, null=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  blank=True, null=True,  related_name="Building Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


    def __str__(self):
        return self.name
        
class Districts(models.Model):
    name = models.CharField(max_length=50)
    province = models.ForeignKey('locations.Provinces', models.DO_NOTHING)
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'Districts'


class Provinces(models.Model):
    name = models.CharField(max_length=50)
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'Provinces'

class City(models.Model):
    name = models.CharField(max_length=100)
    district = models.ForeignKey('locations.Districts', models.DO_NOTHING)
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'City'


class Villages(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    establishment_date = models.DateTimeField()
    loan_officer = models.ForeignKey('authentication.CustomUser', models.DO_NOTHING, db_column='loan_officer')
    meeting_day = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Villages'


class Villagesattendance(models.Model):
    village = models.ForeignKey('locations.Villages', models.DO_NOTHING)
    person_id = models.IntegerField()
    date = models.DateTimeField()
    attended = models.BooleanField()
    comment = models.CharField(max_length=1000, blank=True, null=True)
    loan_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'VillagesAttendance'


class Villagespersons(models.Model):
    village = models.OneToOneField('locations.Villages', models.DO_NOTHING, primary_key=True)
    person_id = models.IntegerField()
    joined_date = models.DateTimeField()
    left_date = models.DateTimeField(blank=True, null=True)
    is_leader = models.BooleanField()
    currently_in = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'VillagesPersons'
        unique_together = (('village', 'person_id'),)




