# Generated by Django 3.0.4 on 2020-03-28 18:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        # ('members', '0001_initial'),
        # migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Notes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=50, null=True, verbose_name='Subject of Text')),
                ('message', models.TextField(blank=True, null=True, verbose_name='Note Message ')),
                ('do_date', models.DateField(auto_now=True, verbose_name='Do Date')),
                ('is_read', models.BooleanField(default=False, verbose_name='Is Read')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Listing Creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Listing Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
                # ('tenant', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='members.Member', verbose_name='Member')),
            ],
        ),
        migrations.CreateModel(
            name='Letters',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=50, null=True, verbose_name='Subject of Text')),
                ('message', models.TextField(blank=True, null=True, verbose_name='Note Message ')),
                ('do_date', models.DateField(auto_now=True, verbose_name='Do Date')),
                ('is_read', models.BooleanField(default=False, verbose_name='Is Read')),
                ('is_sent', models.BooleanField(default=False, verbose_name='Is sent')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Listing Creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Listing Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
                # ('tenant', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='members.Member', verbose_name='Member')),
            ],
        ),
    ]
