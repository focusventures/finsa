from django.apps import AppConfig


class ExtrasConfig(AppConfig):
    name = 'extras'
    icon_name = 'more'
    def ready(self):
        import extras.signals

