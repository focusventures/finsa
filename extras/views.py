from django.shortcuts import render
from django.urls import reverse_lazy
from .forms import NoteForm, LetterForm
from .models import Notes, Letters
from bootstrap_modal_forms.generic import BSModalCreateView
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required




class LetterView(BSModalCreateView):
    template_name = 'extras/letter.html'
    form_class = LetterForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')




class NoteView(BSModalCreateView):
    template_name = 'extras/note.html'
    form_class = NoteForm
    success_message = 'Success: Payment Was Made Successfully.'
    success_url = reverse_lazy('tenants_list')


@login_required()
def add_note(request, pk):
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('members_details', args=(pk,)))
    return redirect(reverse('members_details', args=(pk,)))


@login_required()
def edit_note(request,id, pk):
    form = NoteForm(request.POST, instance=Notes.objects.get(id=pk))
    if request.method == 'POST':
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.modified_by = request.user
            bulding.save()
            return redirect(reverse('groups_details', args=(id,)))
    return redirect(reverse('members_details', args=(id,)))







@login_required()
def add_letter(request, pk):
    form = LetterForm()
    if request.method == 'POST':
        form = LetterForm(request.POST)
        # pdb.set_trace()
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.creator = request.user
            bulding.save()
            return redirect(reverse('members_details', args=(pk,)))
    return redirect(reverse('members_details', args=(pk,)))


@login_required()
def edit_letter(request,id, pk):
    form = LetterForm(request.POST, instance=Letters.objects.get(id=pk))
    if request.method == 'POST':
        if form.is_valid():
            bulding = form.save(commit=False)
            bulding.modified_by = request.user
            bulding.save()
            return redirect(reverse('groups_details', args=(id,)))
    return redirect(reverse('members_details', args=(id,)))

