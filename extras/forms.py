from .models import Notes, Letters
from bootstrap_modal_forms.forms import BSModalForm

from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.layout import (Layout, Fieldset, Field, Row, Column, Button,
                                 ButtonHolder, Submit, Div)

class NoteForm(BSModalForm):
    do_date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Notes
        fields = ['subject', 'message', 'do_date', ]

    def __init__(self, *args, **kwargs):
        super(NoteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('subject', css_class='form-group col-md-9 mb-0'),
                Column('do_date', css_class='form-group col-md-3 mb-0'),

                css_class='form-row'
            ),
         
            Row(
                Column('message', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
           
         

       
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )
     


class LetterForm(BSModalForm):
    do_date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'} ))

    class Meta:
        model = Letters
        fields = ['subject', 'message', 'do_date', ]

    def __init__(self, *args, **kwargs):
        super(LetterForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            Row(
                Column('subject', css_class='form-group col-md-9 mb-0'),
                Column('do_date', css_class='form-group col-md-3 mb-0'),

                css_class='form-row'
            ),
         
            Row(
                Column('message', css_class='form-group col-md-12 mb-0'),
              
                css_class='form-row'
            ),
           
         

       
          
            Row(
                Submit('submit', 'Submit', css_class="btn btn-success m-2"),
                 Button('cancel', 'Cancel', css_class="btn btn-danger m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )
