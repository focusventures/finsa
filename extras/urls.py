from django.urls import path

from . import views



urlpatterns = [

    path('notes', views.NoteView.as_view(), name="note"),
    path("notes/add/<int:pk>", views.add_note, name="add_note"),
    path("notes/edit/<int:pk>", views.edit_note, name="edit_note"),
    path("letter/add/<int:pk>", views.add_letter, name="add_letter"),
    path("letter/edit/<int:pk>", views.edit_letter, name="edit_letter"),
    path('letter', views.LetterView.as_view(), name="letter"),
    # path('vacate', views.VacateView.as_view(), name="vacate"),




]
