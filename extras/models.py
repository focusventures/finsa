from django.db import models
from django.utils.translation import ugettext_lazy as _

class Notes(models.Model):
    subject = models.CharField(_("Subject of Text"), null=True, max_length=50)
    message = models.TextField(_("Note Message "), blank=True, null=True)
    do_date = models.DateField(_("Do Date"), auto_now=False, auto_now_add=False)
    is_read = models.BooleanField(_("Is Read"), default=False)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), default=None, on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  null=True,  related_name="Listing Creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",   null=True,   blank=True, related_name="Listing Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Note'
        verbose_name_plural = 'Notes'


class Letters(models.Model):
    subject = models.CharField(_("Subject of Text"), null=True, max_length=50)
    message = models.TextField(_("Note Message "), blank=True, null=True)
    do_date = models.DateField(_("Do Date"), auto_now=False, auto_now_add=False)
    is_read = models.BooleanField(_("Is Read"), default=False)
    is_sent = models.BooleanField(_("Is sent"), default=False)
    member = models.ForeignKey("members.Member", verbose_name=_("Member"), null=True, on_delete=models.CASCADE)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser", null=True, blank=True, related_name="Listing Creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  null=True, blank=True, related_name="Listing Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Letter'
        verbose_name_plural = 'Letters'



class Economicactivities(models.Model):
    name = models.CharField(max_length=100)
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'EconomicActivities'
        verbose_name = 'Economic Activity'
        verbose_name_plural = 'Economic Activities'


class Economicactivityloanhistory(models.Model):
    contract_id = models.IntegerField()
    person_id = models.IntegerField()
    group_id = models.IntegerField(blank=True, null=True)
    economic_activity_id = models.IntegerField()
    deleted = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'EconomicActivityLoanHistory'
        verbose_name = 'Economic Activity History'
        verbose_name_plural = 'Economic Activity History'


class Entryfees(models.Model):
    name_of_fee = models.CharField(max_length=100)
    min = models.DecimalField(max_digits=18, decimal_places=4, blank=True, null=True)
    max = models.DecimalField(max_digits=18, decimal_places=4, blank=True, null=True)
    rate = models.BooleanField(blank=True, null=True)
    is_deleted = models.BooleanField()
    max_sum = models.DecimalField(max_digits=18, decimal_places=4, blank=True, null=True)
    account_number = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EntryFees'


class Eventattributes(models.Model):
    event_type = models.ForeignKey('extras.Eventtypes', models.DO_NOTHING, db_column='event_type')
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'EventAttributes'


class Eventtypes(models.Model):
    event_type = models.CharField(primary_key=True, max_length=4)
    description = models.CharField(max_length=50)
    sort_order = models.IntegerField(blank=True, null=True)
    accounting = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'EventTypes'



class Tellers(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=100, blank=True, null=True)
    account_id = models.IntegerField()
    deleted = models.BooleanField()
    branch = models.ForeignKey('ostructure.Branch', models.DO_NOTHING)
    currency = models.ForeignKey('extras.Currencies', models.DO_NOTHING)
    user_id = models.IntegerField()
    amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdrawal_amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdrawal_amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Tellers'



class Tellerevents(models.Model):
    teller = models.ForeignKey('extras.Tellers', models.DO_NOTHING)
    event_code = models.CharField(max_length=4)
    amount = models.DecimalField(max_digits=19, decimal_places=4)
    date = models.DateTimeField()
    is_exported = models.BooleanField()
    description = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'TellerEvents'


class Latedaysrange(models.Model):
    min = models.IntegerField(db_column='Min')  # Field name made lowercase.
    max = models.IntegerField(db_column='Max')  # Field name made lowercase.
    label = models.CharField(db_column='Label', max_length=15, blank=True, null=True)  # Field name made lowercase.
    color = models.CharField(db_column='Color', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LateDaysRange'



class Cycleobjects(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'CycleObjects'


class Cycleparameters(models.Model):
    loan_cycle = models.IntegerField()
    min = models.DecimalField(max_digits=19, decimal_places=4)
    max = models.DecimalField(max_digits=19, decimal_places=4)
    cycle_object_id = models.IntegerField()
    cycle_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'CycleParameters'


class Cycles(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = 'Cycles'




class Currencies(models.Model):
    name = models.CharField(max_length=100)
    is_pivot = models.BooleanField()
    code = models.CharField(max_length=20)
    is_swapped = models.BooleanField()
    use_cents = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'Currencies'


class Exchangerates(models.Model):
    exchange_date = models.DateTimeField()
    exchange_rate = models.FloatField()
    currency_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'ExchangeRates'




class Exotics(models.Model):
    name = models.CharField(unique=True, max_length=200)

    class Meta:
        managed = False
        db_table = 'Exotics'




