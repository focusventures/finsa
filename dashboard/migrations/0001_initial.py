# Generated by Django 3.0.4 on 2020-03-15 08:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SnapShot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tenants', models.IntegerField(blank=True, default=0, null=True, verbose_name='Registered Tenants')),
                ('active_tenants', models.IntegerField(blank=True, default=0, null=True, verbose_name='Active Tenants')),
                ('clients', models.IntegerField(blank=True, default=0, null=True, verbose_name='Registered Clients')),
                ('active_clients', models.IntegerField(blank=True, default=0, null=True, verbose_name='Active Clients')),
                ('units', models.IntegerField(blank=True, default=0, null=True, verbose_name='Registered Units')),
                ('active_units', models.IntegerField(blank=True, default=0, null=True, verbose_name='Registered Units')),
                ('houses', models.IntegerField(blank=True, default=0, null=True, verbose_name='Registered Houses')),
                ('active_houses', models.IntegerField(blank=True, default=0, null=True, verbose_name='Active Houses')),
                ('cumulative_arrears', models.FloatField(blank=True, default=0.0, null=True, verbose_name='Cumulative Arrears')),
                ('fresh_arrears', models.FloatField(blank=True, default=0.0, null=True, verbose_name='Fresh Arrears')),
                ('cumulative_overpays', models.FloatField(blank=True, default=0.0, null=True, verbose_name='Cumulative Overpays')),
                ('fresh_overpays', models.FloatField(blank=True, default=0.0, null=True, verbose_name='Fresh Overpays')),
                ('cumulative_clients_owe', models.FloatField(blank=True, default=0.0, null=True, verbose_name='Clients Net')),
                ('fresh_clients_owe', models.FloatField(blank=True, default=0.0, null=True, verbose_name='Clients Net')),
                ('period_start', models.DateField(auto_now=True, verbose_name='Year Start Date')),
                ('period_end', models.DateField(auto_now=True, verbose_name='Year End Date')),
                ('date_created', models.DateField(auto_now=True, verbose_name='Date created')),
                ('date_modified', models.DateField(auto_now=True, verbose_name='Date Modified')),
                ('creator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Payment Creator+', to=settings.AUTH_USER_MODEL, verbose_name='Created by')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Payment Modifier+', to=settings.AUTH_USER_MODEL, verbose_name='Modified by')),
            ],
        ),
    ]
