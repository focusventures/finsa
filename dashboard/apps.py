from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'dashboard'
    icon_name = 'dashboard'
    def ready(self):
        import dashboard.signals
