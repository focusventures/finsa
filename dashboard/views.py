from django.shortcuts import render
from .models import SnapShot
from members.models import Member
from extras.models import Notes
from loans.models import LoanContract

from django.contrib.auth.decorators import login_required
from django.db.models import Sum, Q
from savings.models import Savingevents
from parameters.models import Events

@login_required()
def dash(request):
    # snapshot = SnapShot.objects.all()[0]
    # context = {
    #     'snapshot': snapshot
    # }
    return render(request, 'dashboard/index.html')

import datetime
from chartsoa.models import Account
from parameters.models import Month, Periods

@login_required()
def daily(request):
    month = Month.objects.get(id=request.parameters["settings"]["financial_month"])
    year = Periods.objects.get(id=request.parameters["settings"]["financial_period"])
    snapshot, new = SnapShot.objects.get_or_create(id=1)
    late_members = LoanContract.objects.filter(Q(id__gt=1) & Q(is_overdue=True)).select_related('member')
    loan_requests = LoanContract.objects.all().count()
    loan_requests_cash = LoanContract.objects.filter(Q(requested_amount__gt=0) & Q(date_created=datetime.datetime.today())).aggregate(Sum("requested_amount"))
    savings_total = Savingevents.objects.filter(date_created=datetime.datetime.today()).count()
    savings_cash_total = Savingevents.objects.filter(date_created=datetime.datetime.today()).aggregate(Sum('amount'))
    disburdsed_loans = LoanContract.objects.filter(Q(is_disbursed=True) & Q(date_disbursed=datetime.datetime.today())).count()
    disburdsed_loans_total = LoanContract.objects.filter(Q(is_disbursed=True) & Q(date_disbursed=datetime.datetime.today())).aggregate(Sum('requested_amount'))
    confirmed_loans = LoanContract.objects.filter(Q(is_confirmed=True) & Q(date_confirmed=datetime.datetime.today())).count()
    approved_loans = LoanContract.objects.filter(Q(is_approved=True) & Q(date_approved=datetime.datetime.today())).count()
    requests_today = LoanContract.objects.filter(Q(date_created=datetime.datetime.today())).count()
    notes = Notes.objects.all().select_related("member")
    context = {
        "late_members": late_members,
        'voteheads': Account.objects.filter(Q(transaction_type__add_to_dashboard=True) & Q(financial_year=year) & Q(financial_month=month)).select_related('transaction_type'),
        'snapshot': snapshot,
        "late_members": late_members,
        "loan_request": loan_requests,
        "loan_requests_cash": loan_requests_cash,
        "savings_total": savings_total,
        "savings_cash_total": savings_cash_total,
        'disburdsed_loans_total':disburdsed_loans_total,
        "confirmed_loans": confirmed_loans,
        "disburdsed_loans": disburdsed_loans,
        "approved_loans": approved_loans,
        "requests_today": requests_today,
        'notes': notes
    }
    return render(request, 'dashboard/daily.html', context)


@login_required()
def monthly(request):
    # late_members = Member.objects.all()
    snapshot = SnapShot.objects.all()[0]
    # notes = Notes.objects.all().select_related("member")
    context = {
        # "late_members": late_members,
        'snapshot': snapshot,
        # 'notes': late_members
    }
    return render(request, 'dashboard/monthly.html', context)

@login_required()
def annual(request):
    # late_members = Member.objects.all()
    snapshot = SnapShot.objects.all()[0]
    # notes = Notes.objects.all().select_related("member")
    context = {
        # "late_members": late_members,
        'snapshot': snapshot,
        # 'notes': late_members
    }
    return render(request, 'dashboard/annual.html', context)