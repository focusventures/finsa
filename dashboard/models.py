from django.db import models
from django.utils.translation import ugettext_lazy as _


class SnapShot(models.Model):
    members = models.IntegerField(_("Registered Members"), default=0, null=True, blank=True)
    active_members = models.IntegerField(_("Active Members"),  default=0, null=True, blank=True)

    users = models.IntegerField(_("Registered Members"), default=0, null=True, blank=True)
    active_users = models.IntegerField(_("Active Members"),  default=0, null=True, blank=True)

    loan_agents = models.IntegerField(_("Registered Loan Agents"), default=0, null=True, blank=True)
    active_loan_agents = models.IntegerField(_("Active Loan Agents"),  default=0, null=True, blank=True)

    member_groups = models.IntegerField(_("Registered Member Groups"), default=0, null=True, blank=True)
    branches = models.IntegerField(_("Registered Branches"),  default=0, null=True, blank=True)

    marketers = models.IntegerField(_("Registered Marketers"),  default=0, null=True, blank=True)
    active_marketers = models.IntegerField(_("Active Marketers"),   default=0, null=True, blank=True)

    loan_requests = models.IntegerField(_("Loan Requests"),  default=0, null=True, blank=True)
    loan_disbursed = models.IntegerField(_("Loans Disbursed"),   default=0, null=True, blank=True)

    locations = models.IntegerField(_("Locations"),   default=0, null=True, blank=True)
    branches = models.IntegerField(_("Branches"),   default=0, null=True, blank=True)
    departments = models.IntegerField(_("Departments"),   default=0, null=True, blank=True)

    d_loan_requests = models.IntegerField(_("Loan Requests Today"),  default=0, null=True, blank=True)
    d_loan_disbursed = models.IntegerField(_("Loans Disbursed This Month"),   default=0, null=True, blank=True)

    overdue_today = models.IntegerField(_("Loans Over Due Today"),   default=0, null=True, blank=True)
    overdue_this_month = models.IntegerField(_("Loans Over Due This Month"),   default=0, null=True, blank=True)

    locations = models.IntegerField(_("Registered Locations"),  default=0, null=True, blank=True)
    active_locations = models.IntegerField(_("Active Locations"),  default=0, null=True, blank=True)

    cumulative_arrears = models.FloatField(_("Cumulative Arrears"),  default=0.00, null=True, blank=True )
    fresh_arrears = models.FloatField(_("Fresh Arrears"),  default=0.00, null=True, blank=True )

    cumulative_overpays = models.FloatField(_("Cumulative Overpays"),  default=0.00, null=True, blank=True )
    fresh_overpays = models.FloatField(_("Fresh Overpays"),   default=0.00, null=True, blank=True )

    cumulative_clients_owe = models.FloatField(_("Clients Net"),  default=0.00, null=True, blank=True)
    fresh_clients_owe = models.FloatField(_("Clients Net"),  default=0.00, null=True, blank=True)


    users = models.IntegerField(_("User"),  default=0, null=True, blank=True)

    period_start = models.DateField(_("Year Start Date"), auto_now=True)
    period_end = models.DateField(_("Year End Date"), auto_now=True)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Payment Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)



class TodaySnapShot(models.Model):
    new_members = models.IntegerField(_("Active Members"),  default=0, null=True, blank=True)

    new_users = models.IntegerField(_("Active Members"),  default=0, null=True, blank=True)

    deposit_fees_amount = models.DecimalField(_("Deposit Fee Today"), max_digits=10, decimal_places=2, default=0.00)
    deposit_amount = models.DecimalField(_("Deposits Today"), max_digits=10, decimal_places=2, default=0.00)

    withdraw_fees_amount = models.DecimalField(_("Withdraw Fee Today"), max_digits=10, decimal_places=2, default=0.00)
    withdraw_amount = models.DecimalField(_("Withdraw Today"), max_digits=10, decimal_places=2, default=0.00)

    loan_fees_amount = models.DecimalField(_("Deposit Fee Today"), max_digits=10, decimal_places=2, default=0.00)

    new_groups = models.IntegerField(_("Registered Member Groups"), default=0, null=True, blank=True)
    new_branches = models.IntegerField(_("Registered Branches"),  default=0, null=True, blank=True)

    day = models.DateField(_("Date"), auto_now=True)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="Payment Creator+", verbose_name=_("Created by"), null=True, blank=True, on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="Payment Modifier+", verbose_name=_("Modified by"), null=True, blank=True, on_delete=models.CASCADE)





class AgencyProjections(models.Model):
    is_current = models.BooleanField(_("Projection is Current"), default=True)
    amount_received = models.FloatField(_("Amount Already Paid"), default=0.0)
    year = models.DateField(_("Year of Projection"), auto_now=False, auto_now_add=False)
    amount_expected = models.FloatField(_("Amount Expected to be received"), default=0.0)
    previous_projection = models.ForeignKey("dashboard.AgencyProjections", verbose_name=_("Proction Before Recalculation"), on_delete=models.CASCADE)
    is_recalculated = models.BooleanField(_("Values Are recalculated"))
    reason_for_recalculation = models.CharField(_("Reason For Projection Recalculation"), max_length=200)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  related_name="AgencyProjections Invoice creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser",  related_name="AgencyProjections Invoice Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)
