from django.urls import path

from . import views



urlpatterns = [
    path('', views.daily, name="dashboard"),
    path('daily', views.daily, name="dashboard_daily"),
    path('monthly', views.monthly, name="dashboard_monthly"),
    path('annual', views.annual, name="dashboard_annual"),

]
