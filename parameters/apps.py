from django.apps import AppConfig


class ParametersConfig(AppConfig):
    name = 'parameters'
    icon_name = 'add'
