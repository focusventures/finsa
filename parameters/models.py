from django.db import models
from django.utils.translation import ugettext_lazy as _

class Periodicity(models.Model):
    # loan = 
    name = models.CharField(_("Title"), max_length=50, null=True, blank=True, default=None)
    loan_percentage = models.IntegerField(_("Loan Percentage"))
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Loan Periodicity"
        verbose_name_plural = "Loan Periodicity"

    def __str__(self):
        return " %s - %s " % (self.name, self.loan_percentage)
    
currency = (
    ("Kshs", "Kshs"),
    
)
class LOC(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    name = models.CharField(_("Line Of Credit"), max_length=50)
    code = models.CharField(_("Code"), null=True,max_length=50)
    currency = models.CharField(_("Currency"), choices=currency,  default="Kshs", max_length=50)
    # start_date = models.DateField(_("Begin Date"), auto_now=False, auto_now_add=False, default="2020-01-01")
    # end_date = models.DateField(_("End Date"), auto_now=False, auto_now_add=False, default="2020-01-01")
    type = models.ForeignKey("parameters.Events", verbose_name=_("Events"), related_name="Event Type+", on_delete=models.DO_NOTHING, null=True)
    year_opening_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    initial_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    anticipated_remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    is_cash = models.BooleanField(_("Is Cash LOC"), default=False)
    is_bank = models.BooleanField(_("Is Bank LOC"), default=False)
    is_active = models.BooleanField(_("LOC is Active"), default=False)
    # loan_percentage = models.IntegerField(_("Loan Percentage"))
    date = models.DateField(_("Date created"), auto_now=False, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Line Of Credit"
        verbose_name_plural = "Line Of Credit"

    @property
    def change(self):
        return self.remainder_amount - self.initial_amount


    def __str__(self):
        return "{} - {}".format(self.name, self.id)








class LOCArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    loc = models.ForeignKey("parameters.LOC", verbose_name=_(""), null=True, blank=True, on_delete=models.DO_NOTHING)
    name = models.CharField(_("Line Of Credit"), null=True, blank=True, max_length=50)
    is_cash = models.BooleanField(_("Is Cash LOC"), default=False)
    is_bank = models.BooleanField(_("Is Bank LOC"), default=False)
    code = models.CharField(_("Code"), null=True, blank=True,max_length=50)
    currency = models.CharField(_("Currency"), choices=currency,  default="Kshs", max_length=50)
    initial_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    anticipated_remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    # loan_percentage = models.IntegerField(_("Loan Percentage"))
    date = models.DateField(_("Date created"), auto_now=False, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Line Of Credit Archive"
        verbose_name_plural = "Line Of Credit Archive"


    # def __str__(self):
    #     return self.initial_amount


class LOCFinancing(models.Model):
    description = models.CharField(_("Description"), max_length=100, null=True, blank=True)
    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line Of Credit"), null=True, blank=True, on_delete=models.DO_NOTHING)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    remainder =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="LOC Financing"
        verbose_name_plural = "LOC Financing"

    def __str__(self):
        return "%s %s %s" % (self.loc, self.amount, self.date_created)


    
types = (
    ('Dr', 'Debit'),
    ('Cr', 'Credit')
)

account = (
    ("cash", "Cash"),
    ("bank", "Bank"),
)

class LOCEvent(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    loc_account = models.CharField(_("Loc Account"), choices=account, max_length=50, default='cash')
    type = models.CharField(_("Type"), choices=types, max_length=50, null=True, blank=True)
    description = models.CharField(_("Description"), max_length=300, null=True, blank=True)
    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line Of Credit"), on_delete=models.DO_NOTHING)
    slip = models.ForeignKey("fosa.Slips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_("Transaction"), on_delete=models.DO_NOTHING, null=True,blank=True)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    remainder =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    date = models.DateField(_("Date"), null=True, blank=True, auto_now=False, auto_now_add=False)
    is_payment= models.BooleanField(_("Is Payment"), default=False)
    cancelled = models.BooleanField(_("Is cancelled"), default=False)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="LOC Event"
        verbose_name_plural = "LOC Event"

    def __str__(self):
        return "%s %s %s" % (self.loc, self.amount, self.date_created)


class LOCEventArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    loc_account = models.CharField(_("Loc Account"), choices=account, max_length=50, default='cash')
    type = models.CharField(_("Type"), choices=types, max_length=50, null=True, blank=True)
    description = models.CharField(_("Description"), max_length=300, null=True, blank=True)
    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line Of Credit"), on_delete=models.DO_NOTHING)
    slip = models.ForeignKey("fosa.Slips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    transaction = models.ForeignKey("financial.GroupTransactions", verbose_name=_("Group Transaction"), on_delete=models.DO_NOTHING, null=True,blank=True)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    remainder =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="LOC Event"
        verbose_name_plural = "LOC Event"

    def __str__(self):
        return "%s %s %s" % (self.loc, self.amount, self.date_created)


types = (
    ('Dr', 'Debit'),
    ('Cr', 'Credit')
)

class LOCBanking(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    type = models.CharField(_("Type"), choices=types, max_length=50, null=True, blank=True)
    description = models.CharField(_("Description"), max_length=100, null=True, blank=True)
    loc = models.ForeignKey("parameters.LOC", verbose_name=_("Line Of Credit"), on_delete=models.DO_NOTHING)
    date = models.DateField(_("Date"), null=True, blank=True, auto_now=False, auto_now_add=False)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_(""), on_delete=models.DO_NOTHING, null=True,blank=True)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="LOC Banking"
        verbose_name_plural = "LOC Banking"

    # def __str__(self):
    #     return "%s %s %s" % (self.loc, self.amount, self.date_created)







#Secondaries

class SecondaryLOC(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    name = models.CharField(_("Line Of Credit"), max_length=50)
    code = models.CharField(_("Code"), null=True,max_length=50)
    currency = models.CharField(_("Currency"), choices=currency,  default="Kshs", max_length=50)
    # start_date = models.DateField(_("Begin Date"), auto_now=False, auto_now_add=False, default="2020-01-01")
    # end_date = models.DateField(_("End Date"), auto_now=False, auto_now_add=False, default="2020-01-01")
    type = models.ForeignKey("parameters.Events", verbose_name=_("Events"), related_name="Event Type+", on_delete=models.DO_NOTHING, null=True)
    year_opening_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    initial_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    anticipated_remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    is_cash = models.BooleanField(_("Is Cash LOC"), default=False)
    is_bank = models.BooleanField(_("Is Bank LOC"), default=False)
    is_active = models.BooleanField(_("LOC is Active"), default=False)
    # loan_percentage = models.IntegerField(_("Loan Percentage"))
    date = models.DateField(_("Date created"), auto_now=False, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Secondary Line Of Credit"
        verbose_name_plural = "Secondary Line Of Credit"

    @property
    def change(self):
        return self.remainder_amount - self.initial_amount


    def __str__(self):
        return "{} - {}".format(self.name, self.id)








class SecondaryLOCArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    loc = models.ForeignKey("parameters.SecondaryLOC", verbose_name=_(""), null=True, blank=True, on_delete=models.DO_NOTHING)
    name = models.CharField(_("Line Of Credit"), null=True, blank=True, max_length=50)
    is_cash = models.BooleanField(_("Is Cash LOC"), default=False)
    is_bank = models.BooleanField(_("Is Bank LOC"), default=False)
    code = models.CharField(_("Code"), null=True, blank=True,max_length=50)
    currency = models.CharField(_("Currency"), choices=currency,  default="Kshs", max_length=50)
    initial_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    anticipated_remainder_amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    # loan_percentage = models.IntegerField(_("Loan Percentage"))
    date = models.DateField(_("Date created"), auto_now=False, null=True)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Secondary Line Of Credit Archive"
        verbose_name_plural = "Secondary Line Of Credit Archive"


    # def __str__(self):
    #     return self.initial_amount


class SecondaryLOCFinancing(models.Model):
    description = models.CharField(_("Description"), max_length=100, null=True, blank=True)
    loc = models.ForeignKey("parameters.SecondaryLOC", verbose_name=_("Line Of Credit"), null=True, blank=True, on_delete=models.DO_NOTHING)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    remainder =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Secondary LOC Financing"
        verbose_name_plural = "Secondary LOC Financing"

    def __str__(self):
        return "%s %s %s" % (self.loc, self.amount, self.date_created)


    
types = (
    ('Dr', 'Debit'),
    ('Cr', 'Credit')
)

account = (
    ("cash", "Cash"),
    ("bank", "Bank"),
)

class SecondaryLOCEvent(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    loc_account = models.CharField(_("Loc Account"), choices=account, max_length=50, default='cash')
    type = models.CharField(_("Type"), choices=types, max_length=50, null=True, blank=True)
    description = models.CharField(_("Description"), max_length=300, null=True, blank=True)
    loc = models.ForeignKey("parameters.SecondaryLOC", verbose_name=_("Line Of Credit"), on_delete=models.DO_NOTHING)
    slip = models.ForeignKey("fosa.GroupSlips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    transaction = models.ForeignKey("financial.GroupTransactions", verbose_name=_("Group Transaction"), on_delete=models.DO_NOTHING, null=True,blank=True)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    remainder =  models.DecimalField(max_digits=19, default=0.00, decimal_places=2)
    date = models.DateField(_("Date"), null=True, blank=True, auto_now=False, auto_now_add=False)
    is_payment= models.BooleanField(_("Is Payment"), default=False)
    cancelled = models.BooleanField(_("Is cancelled"), default=False)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Secondary LOC Event"
        verbose_name_plural = "Secondary LOC Event"

    def __str__(self):
        return "%s %s %s" % (self.loc, self.amount, self.date_created)


class SecondaryLOCEventArchive(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    loc_account = models.CharField(_("Loc Account"), choices=account, max_length=50, default='cash')
    type = models.CharField(_("Type"), choices=types, max_length=50, null=True, blank=True)
    description = models.CharField(_("Description"), max_length=300, null=True, blank=True)
    loc = models.ForeignKey("parameters.SecondaryLOC", verbose_name=_("Line Of Credit"), on_delete=models.DO_NOTHING)
    slip = models.ForeignKey("fosa.Slips", verbose_name=_("Slip"), on_delete=models.DO_NOTHING, null=True, blank=True)
    payment_method = models.ForeignKey("chartsoa.PaymentMethod", verbose_name=_("Payment Method"), on_delete=models.DO_NOTHING, null=True, blank=True)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_("Transaction"), on_delete=models.DO_NOTHING, null=True,blank=True)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    remainder =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Secondary LOC Event"
        verbose_name_plural = "Secondary LOC Event"

    def __str__(self):
        return "%s %s %s" % (self.loc, self.amount, self.date_created)


types = (
    ('Dr', 'Debit'),
    ('Cr', 'Credit')
)

class SecondaryLOCBanking(models.Model):
    financial_year = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Month"), on_delete=models.DO_NOTHING, null=True, blank=True)
    type = models.CharField(_("Type"), choices=types, max_length=50, null=True, blank=True)
    description = models.CharField(_("Description"), max_length=100, null=True, blank=True)
    loc = models.ForeignKey("parameters.SecondaryLOC", verbose_name=_("Line Of Credit"), on_delete=models.DO_NOTHING)
    date = models.DateField(_("Date"), null=True, blank=True, auto_now=False, auto_now_add=False)
    transaction = models.ForeignKey("financial.Transactions", verbose_name=_(""), on_delete=models.DO_NOTHING, null=True,blank=True)
    amount =  models.DecimalField(max_digits=19, default=0.00, decimal_places=4)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)

    class Meta:
        verbose_name ="Secondary LOC Banking"
        verbose_name_plural = "Secondary LOC Banking"

































class Codes(models.Model):
    code = models.CharField(_("Code"), max_length=12)
    description = models.CharField(_("Code Description"), max_length=50)
    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.CASCADE)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.CASCADE)


    class Meta:
        verbose_name ="Code"
        verbose_name_plural = "Codes"



    def __str__(self):
        return " %s" % (self.code)


from django.db import models
from django.utils.translation import ugettext_lazy as _

theme = (
    ("Default", "Default"),
)

accs = (
    ("Primary", "Group Based"),
    ("Secondary", "Individual Based"),

)

fields_sources = (
    ("phone_number", "Phone Number"),
    ('member_number', 'Member Number'),
    ('id_number', 'ID Number'),
    ('random', 'Random')
)
class SystemParameters(models.Model):
    financial_period = models.ForeignKey("parameters.Periods", verbose_name=_("Active Financial Period"), on_delete=models.DO_NOTHING, null=True, blank=True)
    financial_month = models.ForeignKey("parameters.Month", verbose_name=_("Active Financial Months"), on_delete=models.DO_NOTHING, null=True, blank=True)
    share_capital = models.DecimalField(_("Share Capital"), max_digits=10, decimal_places=2, default=0.00)
    paginantion = models.IntegerField(_("Number of results per Page"))
    theme = models.CharField(_("Theme"), choices=theme, max_length=50)
    account_number_source = models.CharField(_("Use This Field To Set The field To Use To Generate Account Numbers"), choices=fields_sources, default='member_number', max_length=50)
    system_accounts = models.CharField(_("System Primary Accounts"), choices=accs, default="Primary", max_length=50)
    allow_automatic_creation_of_accounts = models.BooleanField(_("Allow Automatic Creation Of Accounts"), default=False)
    

    
    class Meta:
        verbose_name ="System Parameters"
        verbose_name_plural = "System Parameters"


    def __str__(self):
        return " %s - %s " % (self.financial_period, self.financial_month)






class Theme(models.Model):
    title = models.CharField(_("Title Of THe Theme"), max_length=50)

    class Meta:
        verbose_name ="Theme"
        verbose_name_plural = "Theme"


class Periods(models.Model):
    name = models.CharField(_("Name of The Period"), max_length=50, null=True)
    start = models.DateField(_("Start Of The Financial Period"), auto_now=False, auto_now_add=False)
    end = models.DateField(_("End Of The Financial Period"), auto_now=False, auto_now_add=False)
    is_closed = models.BooleanField(_("Financial Period is Closed"), default=False)

    # period_range = get_period()

    def get_period(self):
        return "%s - %s to %s" % (self.name, self.start, self.end)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name ="Period"
        verbose_name_plural = "Period"


months = (
    ('January', 'January'),
    ('February', 'February'),
    ('March', 'March'),
    ('April', 'April'),
    ('May', 'May'),
    ('June', 'June'),
    ('July', 'July'),
    ('August', 'August'),
    ('September', 'September'),
    ('Octomber', 'Octomber'),
    ('November', 'November'),
    ('December', 'December'),

)

class Month(models.Model):
    name = models.CharField(_("Name of The Month"), choices=months, max_length=50, null=True)

    class Meta:
        verbose_name ="Months"
        verbose_name_plural = "Months"


    def __str__(self):
        return "%s" % (self.name)

choices = (
    ('savings', 'Savings'),
    ('settling', 'Settling'),
    ('expense', 'Expenses'),
    ('revenue', 'Revenue'),
    ('transfer', 'Transfer'),
)

# choices = (
#     ('assets', 'Assets'),
#     ('liablity', 'Liability'),
#     ('equity', 'Ownership Equity'),

# )


sub_choices = (
    ('none', 'None'),
    ('c_assets', 'Current Assets'),
    ('f_assets', 'Fixed Assets'),
    ('c_liablity', 'Current Liability'),
    ('l_liablity', 'Long Term Liability'),
    ('equity', 'Owners Equity'),

)

acs = (
    ('cash', 'Cash'),
    ('bank', 'Bank')
)

trial = (
    ('dr', 'Debit'),
    ('cr', 'Credit'),
)

classifications = (
    ('asset', 'Asset'),
    ('liability', 'Liability'),
    ('capital', 'Capital'),
)


sub_classifications = (
    ('current_asset', 'Current Asset'),
    ('fixed_asset', 'Fixed Asset'),
    ('current_liability', 'Current Liability'),
    ('fixed_liability', 'Fixed Liability'),
    ('capital', 'Owners Equity'),
)
class Events(models.Model):
    title = models.CharField(_("Title"), max_length=50, null=True)
    code = models.CharField(_("Event Code"), max_length=50, help_text="This code will be used to generate accounts. Once saved can not be editted. Make it unique")
    is_income = models.BooleanField(_("Revenue"), default=False)
    is_expense = models.BooleanField(_("Payments"), default=False)
    affecting = models.CharField(_("Account Affected"), choices=acs, default='cash', max_length=50)
    balance_sheet_item = models.CharField(_("Balance Sheet Item"), choices=sub_choices, default='none', max_length=50)
    secondary_event = models.ForeignKey("parameters.Events", verbose_name=_("Secondary Event"), null=True, blank=True, on_delete=models.DO_NOTHING)
    product = models.ForeignKey("products.Products", verbose_name=_("Product Associated With this event"), null=True, blank=True, default=None, on_delete=models.DO_NOTHING)
    is_trial_balance_account = models.BooleanField(_("Is Trial Balance Event"), default=False)
    trial_balance_placement = models.CharField(_("Trial Balance Placement"), choices=trial, default='dr', max_length=50)
    is_reducing = models.BooleanField(_("Has a credit Balance"), default=False)
    is_banking = models.BooleanField(_("Is Banking"), default=False)
    is_cash_from_bank = models.BooleanField(_("Is Cash From Bank"), default=False)
    is_loan_event = models.BooleanField(_("Is Loan Eevent"), default=False)


    vote = models.CharField(_("Vote Head"), max_length=50, null=True, blank=True)
    type = models.CharField(_("Event Type"), choices=choices, default='savings', max_length=50)
    maximum = models.DecimalField(_("Maximum Value"), max_digits=20, decimal_places=2, help_text="Account's with this type of event can hold this maximum value", null=True, blank=True)
    minimum = models.DecimalField(_("Minimum Value"), max_digits=20, decimal_places=2, help_text="Account's with this type of event can hold this minimum value", null=True, blank=True)
    is_primary = models.BooleanField(_("Is Primary"), default=False)

    classication = models.CharField(_("Event Classification"), choices=classifications, default='assets', max_length=50)
    sub_classification = models.CharField(_("Event Sub Classification"), choices=sub_classifications, default='current_assets', max_length=50)

    can_create_accounts = models.BooleanField(_("This Event Can Be Uses To Create Group Or Member Accounts"), default=False)
    add_to_dashboard = models.BooleanField(_("Display On The Dashboard"), default=False)
    allow_transfers_btwn_acc = models.BooleanField(_("Enable Transfer between Accounts"), default=False)

    event_requires_disbursing = models.BooleanField(_("Should Tranasctions based on Groups reflect on Individual accounts"), default=False)

    include_in_pos = models.BooleanField(_("Show In POS For Event Transactions"), default=False)

    def __str__(self):
        return self.title

    @property
    def vote_h(self):
        return self.vote

    class Meta:
        verbose_name ="Event"
        verbose_name_plural = "Events"