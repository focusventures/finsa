from django.contrib import admin

from .models import Periodicity, LOC, Month, Events




admin.site.register(Periodicity)
admin.site.register(LOC)


from django.contrib import admin

from .models import SystemParameters, Theme, Periods, Codes, LOC, LOCBanking, LOCEvent, LOCFinancing, LOCArchive, SecondaryLOC, SecondaryLOCBanking, SecondaryLOCEvent, SecondaryLOCFinancing






class EventAdminModel(admin.ModelAdmin):
    list_display = ['title',     'code', 'is_income', 'is_expense','vote', 'classication', 'sub_classification' ]
    ordering = ['title',     'code', 'is_income', 'is_expense', ]
    icon_name = 'map'

    search_fields = [
      'title',
    ]
    list_filter = [
   'title',     'code', 'is_income', 'is_expense',
    ]

    fieldsets = (
        (
            ' Event Title', {
                'fields': (('title',), )
            }
        ),

         (
            ' Event Settings', {
                'fields': (('code', 'vote',), )
            }
        ),

        (
            'Revenues And Payments', {
                'fields': (('is_income', 'is_expense',),)
            }
        ),

        (
            'Assests And Liablities', {
                'fields': (('classication', 'sub_classification'),)
            }
        ),

         (
            'Accounts Settings', {
                'fields': (('can_create_accounts','add_to_dashboard', 'allow_transfers_btwn_acc'),)
            }
        ),

        (
            'Point Of Sale', {
                'fields': (('include_in_pos',),)
            }
        ),

         (
            'Linkages', {
                'fields': (('secondary_event',), )
            }
        ),
        (
            'Event Product Info', {
                'fields': (('product', ), )
            }
        ),
          (
            'Other Settings Info', {
                'fields': (('is_trial_balance_account', 'trial_balance_placement', 'is_reducing' ), )
            }
        ),
        (
            'Other Info', {
                'fields': (('is_banking', 'is_cash_from_bank',))
            }
        ),
        (
            'Other Info', {
                'fields': (('is_loan_event', 'type'),)
            }
        ),

 
    )




admin.site.register(SystemParameters)
admin.site.register(Month)
# admin.site.register(Codes)
admin.site.register(Periods)
admin.site.register(Events, EventAdminModel)
admin.site.register(LOCBanking)
admin.site.register(LOCEvent)
admin.site.register(LOCFinancing)
admin.site.register(LOCArchive)

# admin.site
admin.site.register(SecondaryLOC)
admin.site.register(SecondaryLOCBanking)
admin.site.register(SecondaryLOCEvent)
admin.site.register(SecondaryLOCFinancing)