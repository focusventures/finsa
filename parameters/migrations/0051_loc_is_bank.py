# Generated by Django 3.0.4 on 2020-08-26 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0050_loc_is_cash'),
    ]

    operations = [
        migrations.AddField(
            model_name='loc',
            name='is_bank',
            field=models.BooleanField(default=False, verbose_name='Is Bank LOC'),
        ),
    ]
