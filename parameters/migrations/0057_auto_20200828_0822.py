# Generated by Django 3.0.4 on 2020-08-28 05:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0056_auto_20200828_0822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='locarchive',
            name='type',
        ),
        migrations.AddField(
            model_name='locarchive',
            name='loc',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.LOC', verbose_name=''),
        ),
        migrations.AlterField(
            model_name='locarchive',
            name='code',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Code'),
        ),
        migrations.AlterField(
            model_name='locarchive',
            name='name',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Line Of Credit'),
        ),
    ]
