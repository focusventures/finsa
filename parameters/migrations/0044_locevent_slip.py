# Generated by Django 3.0.4 on 2020-08-24 18:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fosa', '0016_auto_20200803_1250'),
        ('parameters', '0043_auto_20200824_2059'),
    ]

    operations = [
        migrations.AddField(
            model_name='locevent',
            name='slip',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='fosa.GroupSlips', verbose_name='Slip'),
        ),
    ]
