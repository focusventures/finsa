# Generated by Django 3.0.4 on 2020-07-22 22:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0026_locbanking'),
    ]

    operations = [
        migrations.AddField(
            model_name='events',
            name='vote',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Vote Head'),
        ),
    ]
