# Generated by Django 3.0.4 on 2020-08-27 06:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0053_auto_20200826_2055'),
    ]

    operations = [
        migrations.AddField(
            model_name='loc',
            name='type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='Event Type+', to='parameters.Events', verbose_name='Events'),
        ),
    ]
