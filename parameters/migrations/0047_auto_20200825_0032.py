# Generated by Django 3.0.4 on 2020-08-24 21:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0046_auto_20200824_2232'),
    ]

    operations = [
        migrations.AddField(
            model_name='loc',
            name='financial_month',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.Month', verbose_name='Active Financial Month'),
        ),
        migrations.AddField(
            model_name='loc',
            name='financial_year',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.Periods', verbose_name='Active Financial Period'),
        ),
        migrations.AddField(
            model_name='locarchive',
            name='financial_month',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.Month', verbose_name='Active Financial Month'),
        ),
        migrations.AddField(
            model_name='locarchive',
            name='financial_year',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='parameters.Periods', verbose_name='Active Financial Period'),
        ),
    ]
