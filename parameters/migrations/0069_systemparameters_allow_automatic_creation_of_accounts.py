# Generated by Django 3.0.4 on 2020-09-01 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0068_events_event_requires_disbursing'),
    ]

    operations = [
        migrations.AddField(
            model_name='systemparameters',
            name='allow_automatic_creation_of_accounts',
            field=models.BooleanField(default=False, verbose_name='Allow Automatic Creation Of Accounts'),
        ),
    ]
