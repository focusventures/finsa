# Generated by Django 3.0.4 on 2020-09-01 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0066_auto_20200901_1305'),
    ]

    operations = [
        migrations.AddField(
            model_name='events',
            name='allow_transfers_btwn_acc',
            field=models.BooleanField(default=False, verbose_name='Enable Transfer between Accounts'),
        ),
    ]
