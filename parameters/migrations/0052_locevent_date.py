# Generated by Django 3.0.4 on 2020-08-26 13:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parameters', '0051_loc_is_bank'),
    ]

    operations = [
        migrations.AddField(
            model_name='locevent',
            name='date',
            field=models.DateField(blank=True, null=True, verbose_name='Date'),
        ),
    ]
