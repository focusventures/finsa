from django.db import models
from django.utils.translation import ugettext_lazy as _

class Hook(models.Model):
    schema_name = models.CharField(_("Schema Name"), max_length=50)
    org_name = models.CharField(_("Organisation  Name"), max_length=50)
    tenant_id = models.CharField(_("Tenant Id"), max_length=50)
    approved = models.BooleanField(_("Hook is Approved"), default=False)
    rejected = models.BooleanField(_("Hook is rejected"), default=False)
    user = models.ForeignKey("portal_auth.CustomUser", verbose_name=_("User"), on_delete=models.CASCADE)

