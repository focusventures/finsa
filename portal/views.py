from django.shortcuts import render, redirect, reverse
from django.db import connection

from .models import Hook

def index(request):
    return render(request, 'portal/index.html')

def search_hooks(request):
    hooks = []
    with connection.cursor() as cursor:
       cursor.execute("select * from main_tenant")
       ten = [row for row in cursor.fetchall()]
       print(ten)
    return render(request, 'portal/search.html')


def add_hooks(request, id):
    with connection.cursor() as cursor:
       cursor.execute("select * from main_tenant where id=" + str(pk))
       ten = [row for row in cursor.fetchall()][0]
       hook = Hook()
       hook.schema_name = ten[1]
       hook.org_name = ten[2]
       hook.tenant_id = ten[0]
       hook.user = request.user
       hook.save()
       cursor.close()
    return redirect(reverse('hooks'))

def hooks(request):
    hooks = Hook.objects.filter(user=request.user)

    context = {
        "hooks":hooks
    }

    return render(request, 'portal/hooks.html', context)

def hook_menu(request):

    return render(request, 'portal/hook_menu.html')

def savings(request):
    return render(request, 'portal/savings.html')

def loans(request):
    return render(request, 'portal/loans.html')

def projects(request):
    return render(request, 'portal/projects.html')

def groups(request):
    return render(request, 'portal/groups.html')


def loan_request(request):
    return render(request, 'portal/loan_request.html')


def about(request):
    return render(request, 'portal/about.html')


def contact(request):
    return render(request, 'portal/contact.html')