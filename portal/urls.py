from django.urls import path

from . import views



urlpatterns = [
    path('', views.index, name="index"),
    path('search', views.search_hooks, name="search"),
    path('hooks', views.hooks, name="hooks"),
    path('hooks/<int:pk>', views.add_hooks, name="add_hook"),
    path('menu', views.hook_menu, name="menu"),
    path('savings', views.savings, name="savings"),
    path('loans', views.loans, name="loans"),
    path('groups', views.groups, name="groups"),
    path('projects', views.projects, name="projects"),
    path('requests', views.loan_request, name="requests"),
    path('about', views.about, name="about"),
    path('contact', views.contact, name="contact"),
]
