from django.contrib import admin

# Register your models here.
from .models import Products


class ProductAdminModel(admin.ModelAdmin):
    list_display = ['name',     'client_type', 'creator', 'date_created','date_modified', 'modified_by' ]
    ordering = ['date_created',  ]
    icon_name = 'map'

    search_fields = [
      'name', 

    ]
    list_filter = [
    'name',
    ]

    fieldsets = (
        (
            ' Savings Product Info', {
                'fields': (('name','client_type',), )
            }
        ),

        (
            'Initial Amount Info', {
                'fields': (('initial_amount_min', 'initial_amount_max', 'initial_amount_code',),)
            }
        ),

         (
            'Balance Info', {
                'fields': (('balance_min','balance_max',), )
            }
        ),
        (
            'Deposit Info', {
                'fields': (('deposit_min','deposit_max','deposit_fee', 'deposit_fees_code', 'deposit_code',), )
            }
        ),
        (
            'Interest Rate Info', {
                'fields': (('interest_rate','interest_rate_min'),('interest_rate_max', 'interest_code'))
            }
        ),
        (
            'Entry Fees Info', {
                'fields': (('entry_fees','entry_fees_min','entry_fees_max','entry_fees_code'),)
            }
        ),

            (
            'Withdraw Info', {
                'fields': (('withdraw_min','withdraw_max','withdraw_fees_code',),)
            }
        ),

          (
            'Withdraw Fees Info', {
                'fields': (('withdraw_fees',),)
            }
        ),

          (
            'Transfer Fees Info', {
                'fields': (('transfer_min','transfer_max', 'transfer_fee','transfer_fees_code',),)
            }
        ),
        (
            'Chart of Accounts Info', {
                'fields': (('savings_deposit_acc','deposit_fines_acc',),
                ('interest_earned_acc','deposit_fee_acc',  ))
            }
        ),
        (
            'System Specific Info', {
                'fields': (( 'creator', 'modified_by',),)
            }
        ),

           (
            'Deleted Info', {
                'fields': (( 'deleted',),)
            }
        ),
    )



admin.site.register(Products, ProductAdminModel)