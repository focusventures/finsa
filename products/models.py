from django.db import models
from django.utils.translation import ugettext_lazy as _

curency = (
    ("Ksh", "Ksh"),
    
)


clients = (
    ("Individual", "Individual"),
    ("Group", "Group"),
    ("Corporate", "Corporate"),
)
class Products(models.Model):
    deleted = models.BooleanField(default=False)
    name = models.CharField(unique=True, max_length=100)
    client_type = models.CharField(max_length=15, choices=clients, blank=True, null=True)
    initial_amount_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    # event = models.ForeignKey("parameters.Events", verbose_name=_("Events"), on_delete=models.DO_NOTHING, null=True, blank=True)


    initial_amount_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    balance_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    balance_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdraw_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdraw_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    withdraw_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_fee = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    deposit_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)

    interest_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Interest Code"), related_name="Interest Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    initial_amount_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Opening Amount Code"), related_name="Initial Amount Code+",  on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    withdraw_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Withdraw Code"), related_name="Withdraw Fees Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    transfer_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Transfer Code"), related_name="Transfer Fee Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    deposit_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Deposit Fees Code"), related_name="Deposit Fee Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    entry_fees_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Entry Fees Code"), related_name="Entry Fees Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    deposit_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Deposit Code"), related_name="Savings Deposit Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    withdraw_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Withdraw Code"), related_name="Withdraw Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)
    transfer_code  = models.ForeignKey("parameters.Codes", verbose_name=_("Transfer Code"), related_name="Transfer Code+", on_delete=models.DO_NOTHING, null=True, blank=True, default=None)

    interest_rate = models.FloatField(blank=True, null=True)
    interest_rate_min = models.FloatField(blank=True, null=True)
    interest_rate_max = models.FloatField(blank=True, null=True)
    currency = models.CharField(_("curency"),choices=curency,  default="Kshs", max_length=10)
    entry_fees_min = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    entry_fees_max = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    entry_fees = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    transfer_fee = models.DecimalField(max_digits=19, decimal_places=2, default=0.00)
    transfer_min = models.DecimalField(max_digits=19, decimal_places=4)
    transfer_max = models.DecimalField(max_digits=19, decimal_places=4)

    savings_deposit_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Savings Deposit Account"), null=True, related_name="Savings Deposit Account+", on_delete=models.DO_NOTHING)
    deposit_fines_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Deposit Fines Account"), null=True,  related_name="Deposit Fines Account+", on_delete=models.DO_NOTHING)
    interest_earned_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Interest Earned Account"), null=True,  related_name="Interest Earned Account+",on_delete=models.DO_NOTHING)
    deposit_fee_acc = models.ForeignKey("chartsoa.Account", verbose_name=_("Deposit Fee Account"), null=True,  related_name="Deposit Fee Account+",on_delete=models.DO_NOTHING)

    date_created = models.DateField(_("Date created"), auto_now=True)
    creator = models.ForeignKey("authentication.CustomUser",  blank=True, null=True, related_name="creator+", verbose_name=_("Created by"), on_delete=models.DO_NOTHING)
    date_modified = models.DateField(_("Date Modified"), auto_now=True)
    modified_by = models.ForeignKey("authentication.CustomUser", blank=True, null=True, related_name="Modifier+", verbose_name=_("Modified by"), on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = 'Products'
        verbose_name_plural = "Products"
    
    def __str__(self):
        return self.name