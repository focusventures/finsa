from django.shortcuts import render
from financial.models import MemberCustomAccount, GroupCustomAccount
from products.models import Products
# Create your views here.

def products(request):
    context = {
        "products": Products.objects.all()

    }
    return render(request, 'products/products.html', context)


def aoeproducts(request, pk):
    product = Products.objects.get(id=pk)

    

    context = {
        "products": Products.objects.all()

    }
    return render(request, 'products/products.html', context)




def member_accounts(request):
    context = {
        "accounts": MemberCustomAccount.objects.filter(acc_type__can_create_accounts=True)
    }
    return render(request, 'products/member_accounts.html', context)



def group_accounts(request):
    context = {
        "accounts": GroupCustomAccount.objects.filter(acc_type__can_create_accounts=True)
  
    }
    return render(request, 'products/group_accounts.html', context)