# Generated by Django 3.0.4 on 2020-08-31 13:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_products_event'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='products',
            name='event',
        ),
    ]
