from django import forms
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Button
from products.models import Products

class ProductForm(ModelForm):
    

    class Meta:
        model = Products
        fields = ['name','currency', 'initial_amount', 'remainder_amount','anticipated_remainder_amount']
        exclude = ('modified_by', 'date_modified','creator', 'date_created' )
    

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['remainder_amount'].required = False
        self.helper.layout = Layout(
            Row(
                Column('name', css_class='form-group col-md-4 mb-0'),
                Column('currency', css_class='form-group col-md-4 mb-0'),

                css_class='form-row'
            ),
            Row(
                Column('initial_amount', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),

            # Row(
            #     Column('remainder_amount', css_class='form-group col-md-6 mb-0'),
              
            #     css_class='form-row'
            # ),

            Row(
                Column('anticipated_remainder_amount', css_class='form-group col-md-6 mb-0'),
              
                css_class='form-row'
            ),
           
            Row(
                Submit('submit', 'Save', css_class="btn btn-success m-2"),
                
                css_class='form-row text-right'
            ),
         
            
        )

