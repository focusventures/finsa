from django.urls import path

from . import views



urlpatterns = [
    path('products', views.products, name="products"),
    path('member/accounts', views.member_accounts, name="member_accounts"),
    path('group/accunts', views.group_accounts, name="group_accounts"),
]
